% +-----------------------------------------------------------------------------------------------+
% |                                                                                               |
% |                      Thermal Spike Modell  -  Main Simulation Code                            |
% |                                                                                               |
% +-----------------------------------------------------------------------------------------------+

% Gesellschaft f�r Schwerinonenforschung (GSI):  Material Science Group 

% Responsible People: Dr. Markus Bender   &   Student: Christian Zimmermann

% Last updated: 30 / April / 2019

% Recommended Screensize: ( 1366 x 768 ) Pixels

% Run this code to start simulation routine!


%% ------------------------------- Main Simulation User Interface --------------------------------------- %%

try
    
close all;  % Close pre-opened windows or figures

catch
    
end

allowed_Warnings = {'MATLAB:hg:AutoSoftwareOpenGL', ...
                    'MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame', ...
                    'MATLAB:handle_graphics:exceptions:SceneNode', ...
                    'MATLAB:hg:EraseModeIgnored'};
% Any kind of warning that does not influence further calculation process

 for w = 1 : 1 : length(allowed_Warnings)
                 
     warning('off',allowed_Warnings{w}); % Disable specific warnings in command window
 
 end
 
evalin('base','lasterror(''reset'')'); % Reset any pre-existing errors
evalin('base','lastwarn('''')');       % Reset lastwarning to empty state

clearvars;  % Clear already existing variables
clc;        % Clear Matlab command window

addpath( genpath( pwd ) ); % Add all subfolders of current folder to MATLAB path

% --------------------------------------- Initial Values ------------------------------------------------- %

Thermal_Properties = Thermal_Properties_Read(); 
% Read thermal properties to calculate polynomial values of heat capacity and thermal conductivity

Global_Constants(); % Load physical constants as global values for any calculation process

assignin('base','Radial_Mesh_1',10^(-9)*linspace(0,100,200)); 
assignin('base','Radial_Mesh_2',10^(-9)*linspace(0,100,200));
assignin('base','Radial_Grid',[0,LogSpace([Radial_Mesh_1(2),max(Radial_Mesh_1)],49)]);
% Define a space meshgrid for numerical calculation. Unit: m

assignin('base','Time_Mesh_1',logspace(-17,-10,200));
assignin('base','Time_Mesh_2',logspace(-17,-10,200));
assignin('base','Time_Grid',LogSpace([min(Time_Mesh_1),max(Time_Mesh_1)],50));
% Define a time meshgrid for numerical calculation. Unit: s

assignin('base','Electronic_Temperature',zeros(length(Radial_Mesh_1),length(Time_Mesh_1))); 
% Initialize temperature of the electronic subsystem
assignin('base','Lattice_Temperature',zeros(length(Radial_Mesh_1),length(Time_Mesh_1)));
% Initialize temperature of the atomic subsystem

assignin('base','Time_Index',min(Time_Grid));   % Initialize minimum waitbar time
assignin('base','Wait_Index',0);                % Initialize waitbar to zero percent
assignin('base','Reset_Plot',true);             % Detect the inital run of simulation process
assignin('base','Mouse_Down',false);            % Initialize mouse button press to false state

% ------------------------------------ Simulation Window ------------------------------------------------ %

Fig = figure(1); % Create a main window containing simulation user interface

set(Fig,'Windowstyle', 'normal','WindowButtonDownFcn',@Mouse_Down);
set(Fig,'WindowButtonMotionFcn',@Mouse_Move, 'WindowButtonUpFcn',@Mouse_Up );
set(Fig,'Name','Thermal Spike Calculations','NumberTitle','off','Resize','off');
set(Fig,'Units','normalized','OuterPosition',[0 0 1 1],'Toolbar','none','Menubar','none');
set(Fig,'Color','w','CloseRequestFcn',@closerequest); 

font = 'Cambria';
set(Fig,'defaultUipanelFontName',font,'defaultUicontrolFontName',font,...
    'defaultTextFontName',font,'defaultAxesFontName',font)
% Set meaningful simulation window properties

try
    
    pause(eps); % Apply a short pause in the range of calculation pressicion
    frame_h = get(handle(gcf),'JavaFrame'); % Get the properties of figure's java frame 
    set(frame_h,'Maximized',1); % Maximize simulation window
    delete(frame_h) % Delete java frame handle
    
    catch
    
end

% ----------------------------------------- Panels ------------------------------------------------------ %

Mainpanel = uipanel('Title','Simulation Panel','TitlePosition','centertop','FontSize',13,...
    'Units','normalized','BorderType','etchedout','BorderWidth',2,'BackgroundColor','w',...
    'Position',[0.01 0.02 0.98 0.32],'ForegroundColor',[0.20 0.20 0.20]);
% Create a main simulation panel in the lower part of the user interface ( gui )

Subpanel_1 = uipanel('Parent',Mainpanel,'Title','Collision System','TitlePosition','centerbottom',...
    'FontSize',12,'BorderWidth',2,'Units','normalized','Position',[0.02 0.06 0.40 0.85], ...
    'ForegroundColor',[0.20 0.20 0.20]);
% Define a subpanel in the left part of main panel to determine collision system parameters

Subpanel_2 = uipanel('Parent',Mainpanel,'Title','Control Parameter & Results','TitlePosition','centerbottom',...
    'FontSize',12,'BorderWidth',2,'Units','normalized','Position',[0.58 0.06 0.40 0.85], ...
    'ForegroundColor',[0.20 0.20 0.20]);
% Define a subpanel in the right part of main panel to display control parameters and results

Subsubpanel_1 = uipanel('Parent',Subpanel_1,'Title','Energy Distributions','TitlePosition','centertop',...
    'FontSize',10.5,'BorderWidth',2,'Units','normalized','BackgroundColor',[0.96 0.96 0.96],...
    'Position',[0.30 0.40 0.65 0.55],'ForegroundColor',[0.20 0.20 0.20]);
% Generate a subpanel in the upper part of first subpanel to get energy distributions

Subsubpanel_2 = uipanel('Parent',Subpanel_2,'Title','Overview','TitlePosition','centertop',...
    'FontSize',10.5,'BorderWidth',2,'Units','normalized','BackgroundColor',[0.96 0.96 0.96],...
    'Position',[0.57 0.05 0.40 0.90],'ForegroundColor',[0.20 0.20 0.20]);
% Generate a subpanel in the upper part of second subpanel to display simulation overview

Statuspanel = uipanel('Parent',Subsubpanel_2,'Title','','TitlePosition','centertop',...
    'FontSize',8.5,'BorderWidth',2,'Units','normalized','BackgroundColor',[0.96 0.96 0.96],...
    'Position',[0.10 0.31 0.80 0.30],'BackgroundColor','w');
% Create a subpanel to show simulation accuracy and mesh information based on simulation settings

Subsubpanel_3 = uipanel('Parent',Subpanel_2,'Title','','TitlePosition','centertop',...
    'FontSize',8.5,'BorderWidth',2,'Units','normalized','BackgroundColor',[0.96 0.96 0.96],...
    'Position',[0.03 0.05 0.51 0.52]);
% Allocate a further subpanel to second subpanel which contains data export and PDE coefficients

Subsubpanel_4 = uipanel('Parent',Subpanel_2,'Title','','TitlePosition','centertop',...
    'FontSize',8.5,'BorderWidth',2,'Units','normalized','BackgroundColor',[0.96 0.96 0.96],...
    'Position',[0.03 0.65 0.51 0.26]);
% Create a subpanel within second subpanel to dispaly successful calculation process

% --------------------------------------- Indicators ----------------------------------------------------%

Status_Lamp = annotation(Subsubpanel_4,'ellipse','Units','pixels','Position',[10 10 15 15]);
set(Status_Lamp,'Color',Subsubpanel_4.ShadowColor,'LineWidth',1.5,'FaceColor',[0.85 0.85 0.85]);
% Create a calculation process indicator with possible states: < green >  < orange >  < red > 
% Successfully completed = green, Warnings available = orange, Errors occured = red.

% --------------------------------------- Pushbuttons -------------------------------------------------- %

Button_1 = uicontrol('Style', 'pushbutton', 'String', ' Solver Options','Units','normalized',...
        'Position',[0.45 0.20 0.10 0.05],'FontSize',10,'Callback',@pushbutton1_Callback,...
        'ForegroundColor',[0.10 0.10 0.10]);
% Generate a pushbutton to set numeric differential equation ( PDE ) solver options    
    
Button_2 = uicontrol('Style', 'pushbutton', 'String', '  Compute','Units','normalized',...
        'Position',[0.45 0.10 0.10 0.07],'FontSize',12,'Callback',@pushbutton2_Callback,...
        'ForegroundColor',[0.10 0.10 0.10]);
% Create a pushbutton that launches numeric calculation process to determine subsystem temperatures

Button_3 = uicontrol(Subsubpanel_1,'Style', 'pushbutton', 'String', ' Electronic','Units','normalized',...
        'Position',[0.07 0.30 0.40 0.40],'FontSize',9,'Callback',@pushbutton3_Callback,...
        'ForegroundColor',[0.20 0.20 0.20],'FontWeight','bold');
% Define a pushbutton to set electronic subsystem properties with respect to energy distributions
    
button3_add = evalin('base','Button_3');
b_3 = uicontextmenu;
button3_add.UIContextMenu = b_3;
% Contextmenu that is subordinated to the << electronic properties >> pushbutton

Waligorski = uimenu(b_3,'Label','Waligorski Distribution','Callback',@Waligorski_Info,...
    'Tag','Waligorski Info');
% Display the calculated electronic coupling radius ( waligorski ) based on swift heavy ion energy
    
Button_4 = uicontrol(Subsubpanel_1,'Style', 'pushbutton', 'String', ' Atomic','Units','normalized',...
        'Position',[0.53 0.30 0.40 0.40],'FontSize',9,'Callback',@pushbutton4_Callback,...
        'ForegroundColor',[0.20 0.20 0.20],'FontWeight','bold'); 
% Define a pushbutton to set atomic subsystem properties with respect to energy distributions
    
button4_add = evalin('base','Button_4');
b_4 = uicontextmenu;
button4_add.UIContextMenu = b_4;
% Contextmenu that is subordinated to the  << atomic properties >> pushbutton

Atomic_Coupling = uimenu(b_4,'Label','Atomic energy distribution',...
    'Tag','Atomic Coupling Info','Callback',@Atomic_Coupling_Info);        
% Display the calculated atomic coupling radius based on swift heavy ion energy    
Substitute_Radius = uimenu(b_4,'Label','Set atomic coupling radius',...
    'Tag','Atomic Radius','Callback',@Atomic_Coupling_Substitution);      

Button_5 = uicontrol(Subsubpanel_3,'Style', 'pushbutton', 'String', 'Data Export','Units','normalized',...
        'Position',[0.07 0.30 0.39 0.40],'Enable','off','FontSize',9,'Callback',@pushbutton5_Callback,...
        'ForegroundColor',[0.10 0.10 0.10]);    
% Define a pushbutton to run subsystem data export by creating a respective excel document
    
Button_6 = uicontrol(Subsubpanel_3,'Style', 'pushbutton', 'String', ' Coefficients','Units','normalized',...
        'Position',[0.52 0.30 0.41 0.40],'Enable','off','FontSize',9,'Callback',@pushbutton6_Callback,...
        'ForegroundColor',[0.10 0.10 0.10]);  
% Create a pushbutton to display all sort of neccessary PDE coefficients 
    
% ---------------------------------------- Editboxes --------------------------------------------------- %    
    
Edit_1 = uicontrol(Subpanel_1,'Style', 'edit','Units','normalized','String','300 K',...
        'Position',[0.65 0.02 0.27 0.17],'FontSize',9,'ForegroundColor',[0.20 0.20 0.20]);  

set(Edit_1,'Tag','Editbox','Callback',@edit_1_Callback,'KeyPressFcn',@key_pressed);
set(Edit_1,'Enable','inactive','ButtonDownFcn','set(Edit_1,''Enable'',''on'')','FontWeight','bold');
% Define kind of editbox to set intial subsystem temperatures

% --------------------------------------- Static Text -------------------------------------------------- %    
    
Text_1 = annotation(Subsubpanel_2,'textbox','Units','normalized','Position', ...
    [0.02 0.02 0.25 0.30],'FontSize',9.5,'string','poor',...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none','Visible','on');
% Create a text message to display << poor accuracy >> calculation 

Text_2 = annotation(Subsubpanel_2,'textbox','Units','normalized','Position', ...
    [0.77 0.02 0.25 0.30],'FontSize',9.5,'string','good',...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none','Visible','on');
% Create a text message to display << good accuracy >> calculation 

Text_3 = annotation(Subpanel_1,'textbox','Units','normalized','Position',[0.03 0.88 0.08 0.04],...
    'string','Target Material','Interpreter','latex','FitBoxToText','on','LineStyle','none');
% Create a text message that identifies the << target material >> popup-menu

Text_4 = annotation(Subpanel_1,'textbox','Units','normalized','Position',[0.02 0.31 0.10 0.04],...
    'string','Valence Electrons','Interpreter','latex','FitBoxToText','on','LineStyle','none');
% Create a text message that identifies the << valence electrons >> popup-menu

Text_5 = annotation(Subpanel_1,'textbox','Units','normalized','Position',[0.66 0.31 0.25 0.04],...
    'string','Initial Temperature','Interpreter','latex','FitBoxToText','on','LineStyle','none');
% Create a text message that identifies the << intial temperature >> editbox

Text_6 = annotation(Subpanel_1,'textbox','Units','normalized','Position',[0.36 0.20 0.22 0.15],...
    'string','Restore Defaults','Interpreter','latex','FitBoxToText','on','LineStyle','none');
% Create a text message that identifies the << restore defualts >> checkbox

Text_7 = annotation(Subsubpanel_4,'textbox','Units','normalized','Position',[0.12 0.68 0.60 0.20],...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none');
% Create an empty textbox that contains a text add-on to display solver statistics and warnings

text_add = evalin('base','Text_7');
txt = uicontextmenu;
text_add.UIContextMenu = txt;
% Contextmenu that is subordinated to the << empty textbox >>

Message = uimenu(txt,'Label','Information'); % Uimenu that opens on right mouse-click 
uimenu(Message,'Label','Solver Statistics','Callback',@solver_statistics);
uimenu(Message,'Label','Warning Message','Callback',@warning_message);  
uimenu(Message,'Label','Error Message','Callback',@error_message);
% Launch repsective callback functions if user chooses any option

Text_8 = annotation(Subsubpanel_2,'textbox','Units','normalized','Position', ...
    [0.29 0.03 0.25 0.30],'FontSize',10,'string','- Accuracy -',...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none','Visible','on');
% Create a text message to display definition of accuracy

Text_9 = annotation(Subsubpanel_2,'textbox','Units','normalized','Position', ...
    [0.29 0.51 0.25 0.30],'FontSize',10,'string','- Meshgrid -',...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none','Visible','on');
% Create a text message to display definition of meshgrid

Text_10 = annotation(Subsubpanel_2,'textbox','Units','normalized','Position', ...
    [0.01 0.51 0.25 0.30],'FontSize',9.5,'string','rough',...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none','Visible','on');
% Create a text message to display a << rough mesh >>

Text_11 = annotation(Subsubpanel_2,'textbox','Units','normalized','Position', ...
    [0.79 0.51 0.25 0.30],'FontSize',9.5,'string','fine',...
    'Interpreter','latex','FitBoxToText','on','LineStyle','none','Visible','on');
% Create a text message to display a << fine mesh >>

% ---------------------------------------- Popupmenus -------------------------------------------------- % 

Popup_1 = uicontrol(Subpanel_1,'Style','popup','String',fieldnames(Thermal_Properties),...
    'FontSize',9,'Units','normalized','Position',[0.03 0.60 0.22 0.15],...
    'Callback',@popup1_Callback,'ForegroundColor',[0.20 0.20 0.20],'FontWeight','bold');
% Compute a popup-menu to choose some target material

popup1_Callback(); 
% Launch target material popup-menu to get information regarding chosen material

popup1_add = evalin('base','Popup_1');
p_1 = uicontextmenu;
popup1_add.UIContextMenu = p_1;
% Contextmenu that is subordinated to the << target material >> popupmenu

Substitute  = uimenu(p_1,'Label','Substitute Coefficients','Callback',@substitute_coefficients);
Diffusivity = uimenu(p_1,'Label','Electron thermal diffusivity','Callback',@thermal_diffusivity);
Mean_path = uimenu(p_1,'Label','Electron-Phonon mean free path','Callback',@Mean_free_path);
% Run accordingly callback functions if user chooses any option

Popup_2 = uicontrol(Subpanel_1,'Style','popup','String',num2cell(linspace(1,2,11)'),'Value',6,...
    'FontSize',9,'Units','normalized','Position',[0.03 0.03 0.22 0.15],...
    'Callback',@popup2_Callback,'ForegroundColor',[0.20 0.20 0.20],'FontWeight','bold');
% Compute a popup-menu to choose the number of valence electrons per atom

popup2_Callback(); 
% Launch valence electron number popup-menu to get the amount of delta electrons

popup2_add = evalin('base','Popup_2');
p_2 = uicontextmenu;
popup2_add.UIContextMenu = p_2;
% Contextmenu that is subordinated to the << valence electron >> popupmenu

Items = uimenu(p_2,'Label','Add Item','Callback',@add_item);
% Open callback function if user presses the add item option
  
 
% ----------------------------------------- Checkboxes ------------------------------------------------- % 

Check_1 = uicontrol(Subpanel_1,'Style','checkbox','Value',0,'Units','normalized',...
        'Position',[0.45 0.09 0.05 0.10],'Callback',@check_1_Callback);
% Create a checkbox to restore simulation defaults
 
% ------------------------------------ Initial Graphics Plot ------------------------------------------- %    

Graphics_Plot(Reset_Plot,Radial_Mesh_1,Radial_Mesh_2,Time_Mesh_1,Time_Mesh_2,Electronic_Temperature,Lattice_Temperature);
% Display temperatures of electronic and amtomic subsystem in main simulation interfaces

Context_Menu();
% Generate a main contextmenu in the menubar of simulation desktop

jButton = java(findjobj(Button_1));
myIcon = fullfile(matlabroot,'/toolbox/matlab/icons/pin_icon.gif');
jButton.setIcon(javax.swing.ImageIcon(myIcon));
jButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
jButton.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

jButton = java(findjobj(Button_2));
myIcon = fullfile(matlabroot,'/toolbox/matlab/icons/greenarrowicon.gif');
jButton.setIcon(javax.swing.ImageIcon(myIcon));
jButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
jButton.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

jButton = java(findjobj(Button_3));
myIcon = fullfile(matlabroot,'/toolbox/matlab/icons/unknownicon.gif');
jButton.setIcon(javax.swing.ImageIcon(myIcon));
jButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
jButton.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

jButton = java(findjobj(Button_4));
myIcon = fullfile(matlabroot,'/toolbox/matlab/icons/unknownicon.gif');
jButton.setIcon(javax.swing.ImageIcon(myIcon));
jButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
jButton.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

jButton = java(findjobj(Button_5));
myIcon = fullfile(matlabroot,'/toolbox/matlab/icons/reficon.gif');
jButton.setIcon(javax.swing.ImageIcon(myIcon));
jButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
jButton.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

jButton = java(findjobj(Button_6));
myIcon = fullfile(matlabroot,'/toolbox/matlab/icons/HDF_grid.gif');
jButton.setIcon(javax.swing.ImageIcon(myIcon));
jButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
jButton.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

%% Any errors occur during calculation process

function error_message(~,~)

 [~,Error_id] = evalin('base','lasterr'); % Read last error in matlab workspace
 
 errorstring = {strcat(sprintf('%10s',Error_id))}; % Print the error ID to a character                
 dialogname = 'Errror Message';  % Dialog contains an error message
 
 errordlg(errorstring,dialogname,'replace'); % Create an error dialog
 
end

%% Any warnings are available after succesful calculation process

function warning_message(~,~)

 W = warning('query','last'); % Query last active warning
 
 warningstring = W.identifier; % Get repsective warning identifier
 dialogname = 'Warning Message'; % Dialog contains a warning message
 
 warndlg(warningstring,dialogname,'replace'); % Create a warning dialog
 
end

%% Display solver statistics received after numeric calculation process

function solver_statistics(~,~)

 dialogname = 'Solver Statistics'; % Dialog contains solver statistics
 space = linspace(32,32,8); % Create 8 x " blank spaces " ( char( 32 ) )
 stats = evalin('base','ode_sol.stats'); % Get statistics from ODE solution structure
 
 [~, warn_id] = evalin('base','lastwarn'); % Get the last warning from matlab workspace
 time_id = 'MATLAB:pdepe:TimeIntegrationFailed'; % Message ID that time integration is failed
 time_status = 'done'; % Initialize time integration status to << done = succesful >> 
    
 if ~isempty( warn_id ) % Check if warnings are available
        
     if isequal( warn_id, time_id ) % Check warning ID to match time integration ID
            
         time_status = 'failed'; % Set time integration status to << failed = not successful >>
            
     end
        
 end
 
 [~,Error_id] = evalin('base','lasterr'); % % Get the last error from matlab workspace
 spatial_id = 'MATLAB:pdepe:SpatialDiscretizationFailed'; % Error ID that spatial integration is failed
 spatial_status = 'done'; % Initialize spatial integration to  << done = successful >>
 
 if ~isempty( Error_id ) % Check if errors occured during calculation process
 
     if isequal( Error_id, spatial_id ) % Check error ID to match spatial integration ID
        
         spatial_status = 'failed'; % Set spatial integration status to << failed = not successful >>
         
     end
     
 end  
   
 info = {'',strcat(space,'Spatial integration:',sprintf('%16s',spatial_status)), '' , ...
            strcat(space,'Time integration:',sprintf('%19s',time_status)), '' , '' , ...    
            strcat(space,'Succesful steps:',sprintf('%19s',num2str(stats.nsteps))), '' , ...
            strcat(space,'Failed attempts:',sprintf('%21s',num2str(stats.nfailed))), '' , '', ...            
            strcat(space,'Function evaluations:',sprintf('%11s',num2str(stats.nfevals))), '' , ...
            strcat(space,'Partial derivatives:',sprintf('%16s',num2str(stats.npds))), '' , ...
            strcat(space,'LU decompositions:',sprintf('%14s',num2str(stats.ndecomps))), '' , ...
            strcat(space,'Linear Solutions:',sprintf('%18s',num2str(stats.nsolves))), '', ''};
 % Print and format << solver statistics >> to meaningful character variables     
        
 helpdlg(info,dialogname); % Open a help dialog window to display solver statistics

end

%% Display the atomic coupling radius based on swift heavy ion energy

function Atomic_Coupling_Substitution(hObject,~)

 global r_n_0; % Load global variable containing atomic coupling radius. Units: m
 global E_ion; % Load global variable containing swift heavy ion energy. Units: MeV/u

 prompt = '\fontsize{12}{12}\selectfont Atomic coupling radius: $\;r_0\;\;(nm)$'; 
 
 dialogname = 'Set atomic coupling radius'; % Dialog containing user information

 default = {num2str(10^9 * r_n_0)}; % Use atomic coupling radius in units of nanometers
 
 options.Interpreter = 'latex';
 
 num_lines = 1;
 
 answer = inputdlg(prompt,dialogname,num_lines,default,options);
 % Open an input dialog window to set atomic coupling radius
 
 if ~isempty(answer)
     
     r_n_0 = round(10^(-9) * str2double(answer{1}), 3, 'significant');
     r_n_0( r_n_0 < 0 ) = 10^(-11);
     r_n_0( r_n_0 > 10^(-6) ) = 10^(-6);
     
     set(hObject,'Checked','on');
     
 else
     
     set(hObject,'Checked','off');
     r_n_0 = 10^(-9) * Coupling_Radius( E_ion, 'Energy' );
     
 end
 
end

%% Substitution of partiell differential equation ( PDE ) coefficients

function substitute_coefficients(~,~)

 coeff_fig = figure(3); % Open an additional matlab figure
 coeff_fig.Name = 'Differential Equation Coefficients';
 coeff_fig.NextPlot = 'replace'; % Replace open figure when called again 
 coeff_fig.NumberTitle = 'off'; % No number title is displayed 
 coeff_fig.Resize = 'off'; % Do not allow manual window resizing 
 coeff_fig.MenuBar = 'none'; % Menubar is not available
 coeff_fig.ToolBar = 'none'; % Toolbar is not available
 coeff_fig.Units = 'normalized'; % Position units are set to normalized
 coeff_fig.Position = [0.28 0.20 0.45 0.50]; % Define figure position on screen
 coeff_fig.Color = [0.97 0.97 0.97];
 
 %          >>  Activate coefficients window movement detection 
 % +------------------------------------------------------------------------------------------+
    pause(0.01) % Wait for the figure construction complete
    jFig = get(coeff_fig, 'JavaFrame'); % Get respective JavaFrame
    jWindow = jFig.fHG2Client.getWindow; % Open java window client of the figure  
    jbh = handle(jWindow,'CallbackProperties'); % Prevent memory leak
    set(jbh,'ComponentMovedCallback',@Table_Resize_Fcn); % Run table resize callback function
 % +------------------------------------------------------------------------------------------+
  
 coeff_tab = uitable(coeff_fig,'CellEditCallback',@Cell_Edit_Callback); 
 % Open a user interface table subordinated to coefficients figure 
 
 if ~evalin('base','exist(''subs_coeff'',''var'')') 
 % Check that coefficients substitution variable is not yet member of workspace
     
     subs_coeff = cell(6,1); % Initialize an empty cell variable of dimension ( 6 x 1 )
     subs_coeff(cellfun('isempty',subs_coeff)) = {'No'}; 
     % Set all cells equal to 'No' ( = do not substitute coefficient)
     
 else
     
     subs_coeff = evalin('base','subs_coeff'); 
     % Read coefficients substitution in matlab workspace
     
 end
 
 Edit_1 = evalin('base','Edit_1');
 Temp = str2double(Edit_1.String(~isletter(Edit_1.String)));
    
 if any([~evalin('base','exist(''coeff_tab'',''var'')'),isempty(cell2mat(strfind(subs_coeff,'Yes')))])
 % Check if variable doesn't already exist in workspace or if there is no substitution active
    
    Properties = evalin('base','Thermal_Properties'); % Read thermal properties from workspace
    Material = evalin('base','Material_Selection'); % Read material selection from workspace
 
    default = {arrayfun(@(x) sprintf('%.2e\n',x), 10^(-6) * Electron_Heat_Calculation(Temp,Temp),'un',0); ...
               arrayfun(@(x) sprintf('%.2f\n',x), 10^(-2) * Electron_Thermal_Conductivity_Calculation(Temp,Temp),'un',0); ...
               arrayfun(@(x) sprintf('%.1f\n',x), 10^( 4) * Electron_Thermal_Diffusivity(Temp),'un',0); ...
               arrayfun(@(x) sprintf('%.2f\n',x), 10^(-6) * Lattice_Heat_Calculation(Temp,Properties,Material),'un',0); ...
               arrayfun(@(x) sprintf('%.2f\n',x), 10^(-2) * Lattice_Thermal_Conductivity_Calculation(Temp,Properties,Material),'un',0); ...
               arrayfun(@(x) sprintf('%.2e\n',x), 10^(-6) * Coupling_Factor_Calculation(Temp,Temp),'un',0)};         
    % Define meaningful coefficient default values using matlab array functions ( find units in below data! )
               
    data = {str2html('C','subscript','e'), str2html('J ( cm� K )','superscript','-1'), char(default{1}), 'No'; ...
            str2html('K','subscript','e'), str2html('W ( cm K )' ,'superscript','-1'), char(default{2}), 'No'; ...
            str2html('D','subscript','e'), str2html('cm� s'      ,'superscript','-1'), char(default{3}), 'No'; ...
            str2html('C','subscript','a'), str2html('J ( cm� K )','superscript','-1'), char(default{4}), 'No'; ...
            str2html('K','subscript','a'), str2html('W ( cm K )' ,'superscript','-1'), char(default{5}), 'No'; ...
            str2html('g','subscript',' '), str2html('W ( cm� K )','superscript','-1'), char(default{6}), 'No'};
    % Format the symbols and coefficients data using << string to html >> definitions 
        
    assignin('base','coeff_data',data); % Transfer coefficients data to workspace   
        
 else % If coefficients variable already exists in workspace
     
    data = evalin('base','coeff_data'); % Read the coefficients data from workspace 
      
 end     
 
 RowNames = {'Electronic specific heat'; 'Electronic heat conductivity'; 'Electronic thermal diffusivity'; ...
             'Atomic specific heat'; 'Atomic heat conductivity'; 'Coupling factor'};
 % Define the row names of coefficients substitution table 
           
 coeff_tab.Data = data; % Assign coefficients data to uitable
 coeff_tab.Units = 'normalized'; % Set table measurement units to normalized
 coeff_tab.Position = [0.10 0.27 0.818 0.62]; % Table position with respect to coefficients figure
 coeff_tab.ColumnName = {'Symbol','Unit','Value','Substitute'}; % Coefficients table column names
 coeff_tab.ColumnEditable = [false,false,true,true]; % Define whether table columns are editable or not
 coeff_tab.ColumnFormat = ({'char','char','char' {'Yes' 'No'}}); % Set uitable column format
 coeff_tab.RowName = RowNames; % Assign pre-defined row names to coefficients uitable   
  
 assignin('base','coeff_tab',coeff_tab); % Assign handle to coefficients table in workspace
 assignin('base','tab_restore',coeff_tab.Data);
 
 Table_Resize_Fcn(); % Initial call of coefficients table resize function
   
 function Cell_Edit_Callback(hObject,~)
 % A nested user defined function that shall react on any edit of a table cell
     
  tab_data= hObject.Data; % Get table data from objects handle ( = hObject ) 

  assignin('base','subs_coeff',tab_data(:,4)); 
  % Transfer substitution coefficients variable to workspace
  assignin('base','coeff_data',tab_data);
  % Transfer coefficients data variable to workspace
   
  Edit_1 = evalin('base','Edit_1');
  Temp = str2double(Edit_1.String(~isletter(Edit_1.String)));
  
  Properties = evalin('base','Thermal_Properties'); % Read thermal properties from workspace
  Material = evalin('base','Material_Selection'); % Read material selection from workspace
      
  dat = {arrayfun(@(x) sprintf('%.2e\n',x), 10^(-6) * Electron_Heat_Calculation(Temp,Temp),'un',0); ...
         arrayfun(@(x) sprintf('%.2f\n',x), 10^(-2) * Electron_Thermal_Conductivity_Calculation(Temp,Temp),'un',0); ...
         arrayfun(@(x) sprintf('%.1f\n',x), 10^( 4) * Electron_Thermal_Diffusivity(Temp),'un',0); ...
         arrayfun(@(x) sprintf('%.2f\n',x), 10^(-6) * Lattice_Heat_Calculation(Temp,Properties,Material),'un',0); ...
         arrayfun(@(x) sprintf('%.3f\n',x), 10^(-2) * Lattice_Thermal_Conductivity_Calculation(Temp,Properties,Material),'un',0); ...
         arrayfun(@(x) sprintf('%.2e\n',x), 10^(-6) * Coupling_Factor_Calculation(Temp,Temp),'un',0)};
  % Assign meaningful coefficient values for room temperature ( T = 300 K ) using matlab array functions
         
  for i = 1 : 1 : length(tab_data(:,4)) % Iterate table data containing coefficient values
         
     if ~isequal(tab_data{i,4},'Yes') % Check if coefficient substitution is necessary ( = 'Yes' )
                     
         tab_data{i,3} = char(dat{i}); % Overwrite coefficients table data 
                          
     end
        
  end
           
  hObject.Data = tab_data; % Update data of coefficients substitution table    
%   assignin('base','tab_data',tab_data); % Assign table data in workspace
  assignin('base','coeff_data',tab_data); % Assign coefficients data in workspace            
     
 end
    
 Label = annotation(coeff_fig,'textbox','Units','normalized','LineStyle','none');
 set(Label,'String',strcat('@ $$T_0\;=\;\;$$',num2str(Temp),'$$\;K $$'));
 set(Label,'Position',[0.15 0.12 0.30 0.05],'FontSize',13,'Interpreter','latex');

 uicontrol(coeff_fig,'Style', 'pushbutton', 'String', 'Confirm',...
          'Units','normalized','Position', [0.70 0.08 0.15 0.10],...
          'Callback',{@confirm_button,coeff_fig,coeff_tab}); 
 % Create a pushbbutton to confirm coefficients subsitution settings

 function confirm_button(~,~,coeff_fig,coeff_tab)
 % User defined function that becomes active on confirm buttom pressed
        
    coeff_data = coeff_tab.Data; % Get temporary coefficients table data    
    assignin('base','coeff_data',coeff_data); % Transfer coefficients data to workspace
    
    assignin('base','subs_coeff',coeff_data(:,4)); 
    % Assign pseudo-boolean substitution information ( Yes / No ) in workspace
    
    Fig = evalin('base','Fig'); % Get the handle to main simulation figure
    
    if any(find(strcmp(coeff_data(:,4),'Yes'))) % Find any data to be substituted      
        
        set(findobj(Fig,'Label','Substitute Coefficients'),'Checked','on');
        % Activate << check symbol >> of coefficients substitution uimenu
        
    else
        
        set(findobj(Fig,'Label','Substitute Coefficients'),'Checked','off');
        % Disactivate << check symbol >> of coefficients substitution uimenu
        
    end
    
    close(coeff_fig); % Close additional coefficients substitution figure
    evalin('base','clear tab_restore;'); % Clear restore table in workspace
     
 end     
 
 uicontrol(coeff_fig,'Style', 'pushbutton', 'String', 'Cancel',...
          'Units','normalized','Position', [0.50 0.08 0.15 0.10],...
          'Callback',{@cancel_button,coeff_fig}); % Only close the additional figure
 % Create a pushbutton to cancel coefficients substitution settings
      
 function cancel_button(~,~,coeff_fig)
     
  close(coeff_fig); % Close additional coefficients substitution figure
  assignin('base','coeff_data',evalin('base','tab_restore'));
  evalin('base','clear tab_restore;'); % Clear restore table in workspace    
     
 end
 
end

%% Coefficients table resize function

function Table_Resize_Fcn(~,~)
% User defined function to resize given coefficients table on any window
% movement of the coefficients substitution figure. 

    coeff_tab = evalin('base','coeff_tab'); % Read the coefficients data from workspace 
    
    % Get the row header of uitable by manipulating java control properties
    jscroll = findjobj(coeff_tab); % Get java object settings by using << find java object >>
    jTable = jscroll.getViewport.getView;
    jTable.setRowHeight(36); % Set new row heights in units of pixels
    rowHeaderViewport = jscroll.getComponent(4); % View components vector is ( 1 x 4 ) dimensional
    rowHeader = rowHeaderViewport.getComponent(0);
    rowHeader.setSize(80,360) % Complete row header size

    % Resize the row header
    newWidth = 200; % Define new header width in units of pixels
    rowHeaderViewport.setPreferredSize(java.awt.Dimension(newWidth,0)); % Set new header width 
    height = rowHeader.getHeight; %Get the actual row header heights
    rowHeader.setPreferredSize(java.awt.Dimension(newWidth,height));
    rowHeader.setSize(newWidth,height); % Set complete header appearance

    cellStyle = jTable.getCellStyleAt(0,0); % Get temporary cell style information
    cellStyle.setHorizontalAlignment(cellStyle.CENTER); % Align cells to horizontal centered

    jTable.repaint; % Table must be redrawn for the change to take affect

end
    
%% Thermal electronic diffusivity settings

function thermal_diffusivity(hObject,~)

 global D_e_300, global D_e_min; % Load global variables for thermal diffusivity dependencies

 prompt = {'\fontsize{12}{12}\selectfont Diffusivity @ 300 K: $$\;D_e^{\;300}\;\;(cm^2/s)$$',...
           '\fontsize{12}{12}\selectfont Minimum diffusivity: $$\;D_e^{\;min}\;\;(cm^2/s)$$'}; 
 % Prompt text messages coded with latex sequence 
 
 default_1 = sprintf( '%.1f', 10^4 * D_e_300 ); % Default value diffusivity at T = 300 K. Unit: cm^2/s
 default_2 = sprintf( '%.2f', 10^4 * D_e_min ); % Default value for minimal diffusivity.  Unit: cm^2/s
 
 default = { default_1, default_2 }; % Set dynamic default values

 title = 'Thermal diffusivity of the electron gas'; % Title of the dialog window

 lineNo = 1; % Character input fields only consist of one line

 options.Interpreter = 'latex';  % Use Latex Interpreter

 % Open simulation input dialog window and read electronic parameter setting

 strings = inputdlg ( prompt, title, lineNo, default, options ); % Run input dialog routine
  
 Answer = str2double( strings ); % Convert obtained characters into numeric variables
 
 if ~isempty( Answer ) % Apply only if OK button is pressed (not cancel button)
     
     D_e_300 = 10^( -4 ) * Answer( 1 ); 
     D_e_min = 10^( -4 ) * Answer( 2 );
     % Assgin entered values for both diffusivity constants to global variables
     
     if any([~isequal(Answer(1),150),~isequal(Answer(2),1)]) % Trigger any value changed
         
         set(hObject,'Checked','on'); % Set the uimenu to checked state 
         
     else % Values remained default settings
         
         set(hObject,'Checked','off'); % Set the uimenu to not checked
         
     end
     
     try % Apply following routine since no errors occur
    
         evalin('base','clear coeff_tab;'); % Clear handle to coefficients table in workspace
         evalin('base','clear coeff_data;'); % Clear coefficients data variable in workspace
         evalin('base','clear subs_coeff;'); % Clear coefficients substitution in workspace
         set(findobj(evalin('base','Fig'),'Label','Substitute Coefficients'),'Checked','off');
         % Set coefficients substitution uimenu to unchecked state
    
         catch % Do not catch any errors !
    
     end
     
 end
 
end

%% Thermal electronic diffusivity settings

function Mean_free_path(hObject,~)

 global lambda; % Load global variable of electron phonon mean free path

 prompt = {'\fontsize{12}{12}\selectfont Electron-Phonon mean free path: $$\lambda\;(nm)$$'}; 
 % Prompt text messages coded with latex sequence 
 
 default = {sprintf( '%.2f', lambda )}; % Default value of mean free path. Unit: nm
  
 title = 'Electron - Phonon - Interaction'; % Title of the dialog window

 lineNo = 1; % Character input fields only consist of one line

 options.Interpreter = 'latex';  % Use Latex Interpreter

 % Open simulation input dialog window and read parameter settings

 strings = inputdlg ( prompt, title, lineNo, default, options ); % Run input dialog routine
  
 Answer = str2double( strings ); % Convert obtained characters into numeric variables
 
 if ~isempty( Answer ) % Apply only if OK button is pressed (not cancel button)
     
     lambda = Answer; 
    % Assgin entered value to electron-phonon mean free path global variable
    
    Thermal_Properties = evalin('base','Thermal_Properties');
    Material_Selection = evalin('base','Material_Selection');
    
    if ~isequal(Answer,Thermal_Properties.(Material_Selection).('Mean_free_path')) 
        % Trigger electron-phonon mean free path has changed
         
         set(hObject,'Checked','on'); % Set the uimenu to checked state 
         
         try % Apply following routine since no errors occur
    
            evalin('base','clear coeff_tab;'); % Clear handle to coefficients table in workspace
            evalin('base','clear coeff_data;'); % Clear coefficients data variable in workspace
            evalin('base','clear subs_coeff;'); % Clear coefficients substitution in workspace
            set(findobj(evalin('base','Fig'),'Label','Substitute Coefficients'),'Checked','off');
            % Set coefficients substitution uimenu to unchecked state
    
         catch % Do not catch any errors !
    
         end
         
     else % Values remained default settings
         
         set(hObject,'Checked','off'); % Set the uimenu to not checked
         
    end  
     
 end
 
end

%% Add user defined valence electron values

function add_item(obj,~)

 prompt = {'Valence Electron Number: $$\;N_{e^-}\;\;\left[counts\right]$$'};
 % Prompt text messages coded with latex sequence
 default = {''}; % Default value is set to empty
 title = 'Valence Electron Settings'; % Title of the dialog window
 lineNo = 1; % Character input fields only consist of one line
 options.Interpreter = 'latex';  % Use Latex Interpreter
 str = inputdlg (prompt, title, lineNo, default, options); % Run input dialog routine 
 str = regexprep(str,',','.'); % Replace any << comma >> within the string by a << dot >>
     
 Answer = str2double(str); % Convert obtained characters into numeric variables
      
 if ~isempty(Answer) % Apply only if OK button is pressed (not cancel button) 
    
     if ~isnan(Answer) % Check if value is a valid numeric format
    
         Popup_2 = evalin('base','Popup_2'); 
         % Read handle to valence electron popupmenu from workspace
         
         if ~any(find(strcmp(Popup_2.String,num2str(Answer)))) 
         % Check if value is not already item of valence electron popupmenu
             
            Popup_2.String = [Popup_2.String(:)', num2str(Answer)];
            set(obj,'Checked','on'); 
            % Add value to popupmenu and set uimenu state to checked
            
         else % Value already exists in valence electron popupmenu
             
              warndlg({'No update on valence electron settings.'; ...
                'Chosen valence electron number already exists.'}, ...
                '!! Warning !!','replace'); 
              % Open a warning message to inform user that no update was done
            
         end
        
     else % Entered value is not valid numeric format
        
        warndlg({'No update on valence electron settings.'; ...
                'Please enter a numeric floating point value.'}, ...
                '!! Warning !!','replace');
        % Open a warning message to inform user that no update was not possible
            
     end
     
 else  % Apply only if cancel button is pressed
        
     warndlg('Update procedure canceled by user.', ...
         ' << Message >>','replace');
     % Open a warning message to inform that process was canceled by the user
        
 end  
 
end

%% A mouse button or any other key is pressed

function key_pressed(~,event)

 assignin('base','Current_Character',event.Key); 
 % Transfer key event data ( character variable ) to workspace 
          
end
    
%% Any selection is done by left mouse-click

function Mouse_Down (~,~)
% User defined function that reacts on any << mouse button down >> detection

  if all([evalin('base','exist(''line_ax'',''var'')'), ...
          evalin('base','isequal(gcf,Fig)')]) % Check line plot is drawn in current figure

      if ishandle(evalin('base','line_ax')) % Check axes to line plot is a valid handle
          
        line_ax = evalin('base','line_ax'); % Get line axes handle from workspace
    
        assignin('base','Mouse_Down',true); % Set the mouse down flag in workspace
        assignin('base','Prev_Mouse_Position',get(line_ax,'CurrentPoint'));  
        % Assign the current mouse position in workspace
          
      end
         
  end    

  if isequal(get(gco,'Tag'),'Editbox') % Mouse left click in initial temperature editbox
    
      obj = gco; % Get the handle to current object 
      set(obj,'String',obj.String(~isletter(obj.String))); % Remove unit [K] from text string
      set(obj,'Enable','on'); % Toggle editbox enable state to active
      
  else
      
      edit_1_Callback(findobj('Tag','Editbox')); 
      % User applies left mouse click outside editbox after changing inital temperature.
      % This action is assumed to be similar to pressing return key on a keyboard.
      
      assignin('base','Current_Character','return'); % Assign << current key = return key >>
      
  end  
           
end   

%% Mouse pointer moves along the user inteface

function Mouse_Move(~,~)
% User defined function that reacts on any << mouse movement >>
 
  if all([evalin('base','exist(''line_ax'',''var'')'), ...
          evalin('base','isequal(gcf,Fig)')]) % Check line plot is drawn in current figure
      
    if ishandle(evalin('base','line_ax')) % Check axes to line plot is a valid handle   
        
        % A line is drawn in current subsystem and thus the user might want to drag or drop
        % given line within the respective subplot of main simulation figure.
        
        Drag_Drop_Line(); % Run drag or drop the line function      
               
    end
        
  end
        
end

%% Selection is removed by right mouse-click
 
function Mouse_Up(~,~)
% User defined function that reacts on any << mouse button up >> detection

    set(gcf,'Pointer','arrow');          % reset the mouse pointer to arrow sign
    assignin('base','Mouse_Down',false); % reset the mouse down flag
    assignin('base','Move_Line',false);  % reset the line movement flag

end

%% ---------------------------------- Simulation Close Request --------------------------------------- %%

function closerequest(hObject,~)
% Close request function to display a question dialog box 

   selection = questdlg('Do you want to exit?',...
      'Close Request','Yes','No','Yes'); % close request text message and possible answers
  
   switch selection % React on answer of question dialog
       
      case 'Yes'  % User pressed << yes >> button
          
         delete(hObject) % Delete GUI figure
         evalin('caller','clearvars');  % Clear variables in workspace
         
      case 'No'  % User pressed << no >> button
          
        return  % Return to GUI
      
   end
   
end 
