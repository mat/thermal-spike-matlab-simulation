# How to simulate subsystem temperatures using thermal-spike-model:


- Run "GUI_Thermal_Spike.m in MATLAB and select >> Change Folder!
  Maybe this simulation also works fine with other MATLAB versions?!

- Once all kind of settings are done, press "Compute" button
  and MATLAB is going to calculate subsystem temperatures!

- To add any material to the GUI, just open "Thermal_Properties.xls"
  file and fill the information according to the pre-existing settings.

- Some user interface menu do react on either Left-mouse-click or 
  Right-mouse-click depending on the type of menu.

- Set meaningful solver parameter and subsystem radius or time values
  and save a temperature plot once the simulation shows up good results. 

---
## Compatibility

The software was originally built in MATLAB 2016b.

Successfully tested MATLAB versions:
- 2018b
- 2022b