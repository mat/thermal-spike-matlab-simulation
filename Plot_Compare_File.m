
clearvars; % Clear all variables in workspace
clc; % Clear command window


%% Read C++ Code input document

[xlsfile_1,xlspath_1] = uigetfile({'*.xls;*.xlsx','Excel Files'},...
            'Select a file containing << C++ Code: Subsystem >>');
% Open an user interface file dialog to get excel filename and folder path

filename_1 = fullfile(xlspath_1,xlsfile_1); 
% Create a complete windows path using chosen filename and folder path
        
[~,~,ExcelRaw_1] = xlsread(filename_1,1); % Read excel raw data of each sheet 

Data_1 = ExcelRaw_1; % Assign excel raw data to variable

r_1 = Data_1(1,2:end); % Get radius information
r_1 = cell2mat(r_1); % Convert cell data to a matrix
t_1 = Data_1(2:end,1); % Get time information
t_1 = cell2mat(t_1); % Convert cell data to a matrix

Data_1(1,:) = []; % Remove empty rows
Data_1(:,1) = []; % Remove empty columns

Data_1 = cell2mat(Data_1); % Convert cell data to a matrix

[Radius_1,Time_1] = meshgrid(r_1',t_1); 
% Create a meshgrid with radius and time data


%% Read MATLAB Code input document

[xlsfile_2,xlspath_2] = uigetfile({'*.xls;*.xlsx','Excel Files'},...
            'Select a file containing << MATLAB Code: Subsystem >>');
% Open an user interface file dialog to get excel filename and folder path

 filename_2 = fullfile(xlspath_2,xlsfile_2); 
        % Create a complete windows path using chosen filename and folder path
        
[~,~,ExcelRaw_2] = xlsread(filename_2,2); % Read excel raw data of each sheet 

Data_2 = ExcelRaw_2; % Assign excel raw data to variable


r_2 = Data_2(3,3:end); % Get radius information
r_2 = cell2mat(r_2); % Convert cell data to a matrix
t_2 = Data_2(4:end,2); % Get radius information
t_2 = cell2mat(t_2); % Convert cell data to a matrix

Data_2(1:3,:) = []; % Remove empty rows
Data_2(:,1:2) = []; % Remove empty columns

Data_2 = cell2mat(Data_2); % Convert cell data to a matrix

[Radius_2,Time_2] = meshgrid(r_2',t_2);
% Create a meshgrid with radius and time data

%% Create subsystem compare panel

fig = figure(1); % Open a new figure
fig.Name = 'Two Temperature Model: C++ Code vs. MATLAB Code';
fig.NumberTitle = 'off';
fig.Units = 'normalized';
fig.Position = [0.05 0.10 0.90 0.70];
fig.Color = 'w';
% Set all kind of figure properties

Panel_1 = uipanel('Parent',fig,'Title','C++ Code','Units','normalized',...
    'Position',[0.01, 0.02, 0.48, 0.96],'BackgroundColor','w','BorderWidth',2,...
    'BorderType','etchedin','FontSize',12,'ForegroundColor',[0.3 0.3 0.3]);
% Create a panel that will ccontain temperatures of C-Code

ax1 = axes('Position',[0.13, 0.10, 0.80, 0.82],'Units','normalized','parent',Panel_1);
% First system axes settings

surf(ax1,Radius_1,Time_1,Data_1); % Create a first surface plot with temperature data

shading(ax1,'interp');
view(ax1,[0 90]);
axis(ax1,'tight');

ax1.YScale = 'log';
ax1.Layer = 'top';
colormap(ax1,'jet');
colorbar(ax1)
grid on;
box(ax1,'on');
xlabel(ax1,'Radius $$\;r\;\left[nm\right]$$','Interpreter','latex');
ylabel(ax1,'Time $$\;t\;\left[s\right]$$','Interpreter','latex');
title(ax1,'Atomic Subsystem','Interpreter','latex');
ax1.TickLabelInterpreter = 'latex';
% Set all sort of plot appearance properties

Panel_2 = uipanel('Parent',fig,'Title','MATLAB Code','Units','normalized',...
    'Position',[0.51, 0.02, 0.48, 0.96],'BackgroundColor','w','BorderWidth',2,...
    'BorderType','etchedin','FontSize',12,'ForegroundColor',[0.3 0.3 0.3]);
% Create a panel that will ccontain temperatures of MATLAB-Code

ax2 = axes('Position',[0.13, 0.10, 0.80, 0.82],'Units','normalized','parent',Panel_2);
% Second system axes settings

surf(ax2,Radius_2,Time_2,Data_2); % Create a second surface plot with temperature data

shading(ax2,'interp');
view(ax2,[0 90]);
axis(ax2,'tight');

ax2.YScale = 'log';
ax2.Layer = 'top';
colormap(ax2,'jet');
colorbar(ax2);
grid on;
box(ax2,'on');
xlabel(ax2,'Radius $$\;r\;\left[nm\right]$$','Interpreter','latex');
ylabel(ax2,'Time $$\;t\;\left[s\right]$$','Interpreter','latex');
title(ax2,'Atomic Subsystem','Interpreter','latex');
ax2.TickLabelInterpreter = 'latex';
ax2.XMinorGrid = 'on';
% Set all sort of plot appearance properties

if max(ax1.XLim) < max(ax2.XLim) % Find greatest of both maximum radius values
        
    ax2.XLim = get(ax1,'XLim');
    
else
    
    ax1.XLim = get(ax2,'XLim');
    
end

if max(ax1.YLim) < max(ax2.YLim) % Find greatest of both maximum time values
        
    ax2.YLim = get(ax1,'YLim');
    
else
    
    ax1.YLim = get(ax2,'YLim');
    
end

if max(ax1.CLim) < max(ax2.CLim) % Find greatest of both maximum temperature values
    
    ax1.CLim = get(ax2,'CLim');
    
else
    
    ax2.CLim = get(ax1,'CLim');
    
end

%% Display relative Deviation between both calculations

Vq = interp2(Radius_2,Time_2,Data_2,Radius_1,Time_1); 
% Interpolate MATLAB-Code meshgrid to match gridsize of C-Code temperatures

Fig = figure(2); % Open a new figure
Fig.Name = 'Deviation of both temperature plots';
Fig.NumberTitle = 'off';
set(Fig,'Color','w');

Abs_Diff = 100 * abs(Data_1 - Vq) / max(max(Data_1)); 
% Calculate the deviation bewteen each temperature value of both arrays 
surf(gca,r_1,t_1,Abs_Diff); % Create a surface plot to display deviation
set(gca,'YScale','log');
colormap(gca,'hsv');
colorbar(gca);
shading(gca,'interp');
axis(gca,'tight');
view(gca,[0 90]);
xlabel(gca,'Radius  r  $$\left[nm\right]$$','Interpreter','latex');
ylabel(gca,'Time  t  $$\left[s\right]$$','Interpreter','latex');
set(gca,'Layer','top');
grid(gca,'on');
% Set plot appearance properties

