function[Interp_1,Interp_2] = Temperature_Interpolation(Var_1,Var_2)
% A user defined function to interpolate the calculated subsystem temperatures
% at certain radius and time values to allow a variable subsystem plot range.

 Radial_Mesh_1 = evalin('base','Radial_Mesh_1'); % Read spatial mesh of the electronic subsystem
 Radial_Mesh_2 = evalin('base','Radial_Mesh_2'); % Read spatial mesh of the atomic subsystem
 
 Time_Mesh_1 = evalin('base','Time_Mesh_1'); % Read time mesh of the electronic subsystem
 Time_Mesh_2 = evalin('base','Time_Mesh_2'); % Read time mesh of the atomic subsystem
 
 Radial_Grid = evalin('base','Radial_Grid'); % Get calculation radius mesh from workspace
 Time_Grid = evalin('base','Time_Grid'); % Get calculation time mesh from workspace

 [X,Y] = meshgrid(Radial_Grid,Time_Grid); % Generate a two dimensional meshgrid of origin values
 assignin('base','X',X); % Assign origin radius meshgrid in workspace
 assignin('base','Y',Y); % Assign origin time meshgrid in workspace
 
 [Xq_1,Yq_1] = meshgrid(Radial_Mesh_1,Time_Mesh_1); % Generate a two dimensional electronic meshgrid
 [Xq_2,Yq_2] = meshgrid(Radial_Mesh_2,Time_Mesh_2); % Generate a two dimensional atomic meshgrid
 
 assignin('base','Xq_1',Xq_1); % Assign requested radius meshgrid of electronic subsystem in workspace
 assignin('base','Yq_1',Yq_1); % Assign requested time meshgrid of electronic subsystem in workspace
 
 assignin('base','Xq_2',Xq_2); % Assign requested radius meshgrid of atomic subsystem in workspace
 assignin('base','Yq_2',Yq_2); % Assign requested timemeshgrid of atomic subsystem in workspace
 
 Interp_1 = interp2(X,Y,Var_1,Xq_1,Yq_1,'spline'); % Apply a spline interpolation to electronic subsystem temperatures
 Interp_2 = interp2(X,Y,Var_2,Xq_2,Yq_2,'spline'); % Apply a spline interpolation to atomic subsystem temperatures

end