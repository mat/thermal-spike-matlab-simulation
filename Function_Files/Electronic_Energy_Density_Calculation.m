function A = Electronic_Energy_Density_Calculation(r,t)
% User defined function to determine the electronic energy density per unit
% time A(r,t) mainly according to atomic energy losses ( dE/dx )_e  due to 
% swift heavy ion impact E_ion.

 %% Load Global Constants

 global t_e_0;
 % Mean flight time of delta ray electrons within the target material. Unit: s

 global sigma_e_t;
 % Half width maximum of the gaussian distribution in time. Unit: s

 global S_e;
 % Electronic energy loss dE/dx due to electronic slowing down process. Unit: J/m

 global b;
 % Electronic energy density scaling factor b to ensure normalized energy input. Unit: (m*s)^-1


 %% Calculate Energy Density per Unit Time

 r( r == 0 ) = 0.1 * evalin('base','theta'); % Avoid any radius values equal zero due to infinty states
 
 Norm_f = evalin('base','Norm_f'); % Load radial distribution normalization factor from workspace
 r_max = evalin('base','r_max'); % Load maximum energy tranfer radius from workspace
 Radial_Expression = evalin('base','f_r'); % Get symbolic waligorski function handle 
 Radial_Expression = double( Radial_Expression(r) .* Norm_f .* heaviside( r_max - r ) );
 % Radial distribution function of delta electrons. Unit: m^-1

 Time_Expression = exp( -1/2 * ( ( t - t_e_0 ) / sigma_e_t ).^2 ); 
 % Determine the time dependent expression of the electronic energy input
 
 A = b * S_e * Time_Expression .* Radial_Expression;
 % Energy density per unit time A(r,t) according to electronic energy loss (dE/dx)_e . Unit: W/m^3
 % This expression solves the integral equation to determine energy density in cylindric coordinates. 
 
 A(isnan(A)) = 0; % Set all values that are not numeric to almost zero 
 
end