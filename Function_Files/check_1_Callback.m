function check_1_Callback(~,~)
% User defined function to restore all default values and expressions while
% the checkbox was activated.

pause(1.5) 
% Apply a short pause of 1.5 seconds that allows the user to recognize the checked state

Global_Constants; % Load all global constants

assignin('base','Radial_Mesh',10^(-9)*linspace(0,100,200));
% Re-define a default space meshgrid for subsystem settings. Unit: m
Radial_Mesh = evalin('base','Radial_Mesh');
assignin('base','Radial_Grid',[0,LogSpace([Radial_Mesh(2),max(Radial_Mesh)],49)]);
% Re-define a default space meshgrid for numerical calculation. Unit: m

assignin('base','Time_Mesh',logspace(-17,-10,200));
% Re-define a default time meshgrid for subsystem settings. Unit: s
Time_Mesh = evalin('base','Time_Mesh');
assignin('base','Time_Grid',LogSpace([min(Time_Mesh),max(Time_Mesh)],50));
% Re-define a default time meshgrid for numerical calculation. Unit: s

set(evalin('base','Popup_1'),'Value',1), popup1_Callback(); % Reset target material
set(evalin('base','Popup_2'),'String',num2cell(linspace(1,2,11))); 
set(evalin('base','Popup_2'),'Value',6), popup2_Callback(); % Reset valence electron number   
set(evalin('base','Check_1'),'Value',0); % Reset checkbox state to false
set(evalin('base','Edit_1'),'String','300 K','Enable','inactive'); % Reset initial temperature 
set(findobj(gcf,'Label','Substitute Coefficients'),'Checked','off'); % Reset coefficients substitution
set(findobj(gcf,'Label','Add Item'),'Checked','off'); % Reset valence electron items
set(findobj(gcf,'Label','Subsystem Settings'),'Enable','off'); % Do not allow subsystem settings

try
    
    evalin('base','clear coeff_tab;');  % Clear coefficients uitable 
    evalin('base','clear coeff_data;'); % Clear coefficients table data
    evalin('base','clear subs_coeff;'); % Clear coefficients substitution pseudo-boolean
    
catch % Do not catch any conditions
    
end


end