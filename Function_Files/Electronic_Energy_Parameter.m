function Answer = Electronic_Energy_Parameter()
% The user defined function generates and opens an input dialog that allows
% to get all simulation parameters with respect to the electronic energy 
% input due to an incidence of swift heavy ions. 
%
% Function Routine:
%
%  Pushbutton  --->  Input Dialog  --->  get parameters  --->  output
% 
% Definition of output values:
%
% The output of simulation parameters are given as a cell variable which  
% is specified as << Answer >>


 %% Electronic Subsystem Parameters

 % Definition of input dialog window to read neccessary simulation parameters

 prompt = {'\fontsize{11}{11}\selectfont Ion energy: $$\;E_{Ion}\;\;(MeV/u)$$',...
           '\fontsize{11}{11}\selectfont Oscillation time of electrons: $$\;t_0\;\;(fs)$$',...
           '\fontsize{11}{11}\selectfont Oscillation time deviation: $$\;\sigma_t\;\;(fs)$$',...
           '\fontsize{11}{11}\selectfont Electronic stopping loss: $$\;S_e\;\;(keV/nm)$$ ',...
           '\fontsize{11}{11}\selectfont Ionization potential: $$\;I\;\;(ev)$$',...
           '\fontsize{11}{11}\selectfont Beam energy radius parameter: $$\;\alpha_r$$'};
 % Prompt text messages coded with latex sequence 
        
 global E_ion, global t_e_0, global sigma_e_t, global S_e, global alpha;  
 global Electron_Charge, global I_ion; % Load necessary global variables
  
 default_1 = num2str( E_ion );                                                   % Unit: MeV/u
 default_2 = num2str( 10^15 * t_e_0 );                                           % Unit:  s
 default_3 = num2str( 10^15 * sigma_e_t );                                       % Unit:  s
 default_4 = num2str( 10^(-3) * 10^(-9)* S_e / Electron_Charge );                % Unit: keV/nm
 default_5 = num2str( I_ion);                                                    % Unit: eV
 default_6 = num2str( alpha );                                                   % Unit: nm
 
 default = {default_1, default_2, default_3, default_4, default_5, default_6}; 
 % Set dynamic default values for any kind of metals
     
 title = 'Electronic Subsystem'; % Title of the dialog window

 lineNo = 1; 
 % Character input fields only consist of one line

 options.Interpreter = 'latex';  % Use Latex Interpreter

 strings = inputdlg (prompt, title, lineNo, default, options);
 % Open simulation input dialog window and read electronic parameter setting

 Answer = str2double(strings); 
 % Convert string variables to double precission numeric variables

end