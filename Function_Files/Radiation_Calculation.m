function Radiation_Heat = Radiation_Calculation(Lattice_Temperature)
% A user defined function to determine thermal radiation heat according to the spatial 
% resolved, transient temperatures of the atomic subsystem.

%% Radiation heat calculation

 if evalin('base','exist(''Radiation_Status'',''var'')')
 % Check if radiation status variable is member of Matlab workspace
     
     switch evalin('base','Radiation_Status')
     % Distinguish different radiation states

         case 'enabled' % Simulation of radiation heat is enabled        

             Ambient_Temperature = evalin('base','Ambient_Temperature');
             % Read ambient temperature value from workspace
             Ambient = Ambient_Temperature * ones(size(Lattice_Temperature));
             % Ambient temperature matrix according to the lattice temperature dimensions

             Ambient_Emissivity = evalin('base','Ambient_Emissivity');
             % Get ambient emissivitye value from workspace
             Surface_Emissivity = evalin('base','Surface_Emissivity');
             % Evaluate surface emissivity value in workspace
             Properties = evalin('base','Thermal_Properties');
             % Get thermal properties structure from main workspace
             Material = evalin('base','Material_Selection');
             % Read target material variable from workspace 


             Radiation_Heat =  Radiation_Multiplier(Lattice_Temperature,Properties,Material) * ...
                 ( Ambient_Emissivity * Ambient.^3 - Surface_Emissivity * Lattice_Temperature.^3 );
             % Calculate thermal radiation heat with respect to surface and ambient properties. Units: W/m^3

             assignin('base','Radiation_Heat',Radiation_Heat);
             
         otherwise % Simulation of radiation heat is disabled  

             Radiation_Heat = zeros(size(Lattice_Temperature)); 
             % Set all thermal radiation heat values equal to zero
     end
     
 else
     
     Radiation_Heat = zeros(size(Lattice_Temperature));
     % Set all thermal radiation heat values equal to zero

end