function [Thermal_Properties] = Thermal_Properties_Read ()
% User defined function to read neccessary thermal properties of any available target material
% and to also assign obtained data in a respective thermal properties structure variable.

Member = exist('Thermal_Constants.xlsx','file'); 
% Thermal constants excel file exist in current matlab folder

switch Member % Distinguish existance of excel file
    
    case 0 % Excel file is not found in current folder
        
        [xlsfile,xlspath] = uigetfile({'*.xls;*.xlsx','Excel Files'},...
            'Select a file containing << Thermal Constants >>');
        % Open an user interface file dialog to get excel filename and folder path

        filename = fullfile(xlspath,xlsfile); 
        % Create a complete windows path using chosen filename and folder path
    
    case 2 % Excel file is meber of current folder
        
        filename = fullfile(pwd,'/Thermal_Properties/Thermal_Constants.xlsx'); 
        % Create a complete windows path using current directory and default filename
        
end

try % Apply routine since no errors occur during thermal properties reading process
    
    try % Apply routine since no errors occur during excel information process
        
        [~,sheets,~] = xlsfinfo(filename); % Get the amout of excel sheets in directory
        
    catch % Catch any erros while processing routine
        
        errordlg('Thermal properties file not found in directory.', ...
            'File Error','replace'); 
        % Create an error dialog to inform user that process was not successful
        
    end

        sheets(cellfun(@length,sheets) > 20) = [];
        % Find all sheets that consist of chemical elements as sheet names ( e.g. Cu, Au, ... )

        Thermal_Properties = struct(); % Initialize thermal properties structure variable

    for i = 1 : length(sheets) % Iterate the amount of matching excel sheets
    
        %% Excel raw data clean up
        
        [~,~,ExcelRaw] = xlsread(filename,i); % Read excel raw data of each sheet       
        ExcelRaw(cellfun(@(x) isnumeric(x) && isnan(x), ExcelRaw)) = {''};
        ExcelRaw(all(cellfun('isempty',ExcelRaw),2),:) = [];
        % Remove specfic expressions ( e.g. NaN or empty rows ) from excel raw data
    
        %% Target material specific parameters
        
        Thermal_Properties.(sheets{i}).('Type') = ExcelRaw{3,2};       
        Thermal_Properties.(sheets{i}).('Molare_Mass') = ExcelRaw{3,3};
        Thermal_Properties.(sheets{i}).('Solid_Mass_Density') = ExcelRaw{3,4};
        Thermal_Properties.(sheets{i}).('Liquid_Mass_Density') = ExcelRaw{3,5};
        Thermal_Properties.(sheets{i}).('Melting_Point') = ExcelRaw{3,6};
        Thermal_Properties.(sheets{i}).('Vapor_Point') = ExcelRaw{3,7};
        Thermal_Properties.(sheets{i}).('Debye_Temperature') = ExcelRaw{3,8};
        % Extract thermal properties of each sheet and assign to fields of structure variable
        
        if ~isequal(char(ExcelRaw{3,2}),'Metal') % Check if material is not of type metal
            
            Thermal_Properties.(sheets{i}).('Mean_free_path') = ExcelRaw{3,9};
            % Determine the electron-phonon mean free path. Units: nm
            add = 1; % Shift the other columns by one
            
        else
            
            add = 0; % Do not apply any column shift 
            
        end        
       
    
        %% Temperature thresholds and polynomial coefficients of lattice heat capacity 
        
        Thresholds_1 = ExcelRaw(3:size(ExcelRaw,1),( 9 + add ) : (10 + add ));
        Thresholds_1(all(cellfun('isempty',Thresholds_1),2),:) = [];    
        Thermal_Properties.(sheets{i}).('Lattice_Heat').('Thresholds') = Thresholds_1;
        % Extract temperature thresholds of heat capacity and assign to field of structure variable
        
        Polynomial_1 = ExcelRaw(3:size(ExcelRaw,1),( 11 + add ) : ( 18 + add ));
        Polynomial_1(all(cellfun('isempty',Polynomial_1),2),:) = []; 
        Thermal_Properties.(sheets{i}).('Lattice_Heat').('Polynomial') = Polynomial_1;
        % Extract polynomial values of heat capacity and assign to field of structure variable
    
        %% Temperature thresholds and polynomial coefficients of lattice thermal conductivity 
        
        Thresholds_2 = ExcelRaw(3:size(ExcelRaw,1),( 19 + add ) : ( 20 + add ));
        Thresholds_2(all(cellfun('isempty',Thresholds_2),2),:) = [];    
        Thermal_Properties.(sheets{i}).('Thermal_Conductivity').('Thresholds') = Thresholds_2;
        % Extract temperature thresholds of thermal conductivity and assign to field of structure variable
        
        Polynomial_2 = ExcelRaw(3:size(ExcelRaw,1),( 21 + add ) : ( 28 + add ));
        Polynomial_2(all(cellfun('isempty',Polynomial_2),2),:) = []; 
        Thermal_Properties.(sheets{i}).('Thermal_Conductivity').('Polynomial') = Polynomial_2;
        % Extract polynomial values of thermal conductivity and assign to field of structure variable
        
    end
        
catch % Catch any erros while processing thermal properties structure
    
    errordlg('An error occured during thermal properties reading process.','','replace');           
    % Create an error dialog to inform user that thermal properties reading was not successful
    
end

end
