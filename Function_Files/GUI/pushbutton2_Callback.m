function pushbutton2_Callback(hObject,event_data)
% User defined function to numerically solve the coupled system of at least 
% two differential equations ( PDE ) which mainly resolves the temperatures
% of each subsystem ( Electronic Subsystem & Atomic Subsystem ). 
 
 Waligorski_Info(hObject,event_data);
 Atomic_Coupling_Info(hObject,event_data);

 global D_e_300, global D_e_min, global lambda;

 figHandles = flipud(get(groot,'Children')); % Find all open Matlab figures
 
 for i = 1 : 1 : length(figHandles) % Iterate all open figures
               
     if ~isequal(figHandles(i),findobj(figHandles,'Number',1)) 
         % Do not close user simulation interface which is figure(1)
         
         close(figHandles(i)); % Close all remaining figures
         
     end
     
 end

 hz = zoom(gcf);  % Get handle to zoom functionality        
 set(hz,'Enable','off'); % Disactivate zoom function in current figure
 
 dcm_obj = datacursormode(gcf); % Open datacursor settings
 set(dcm_obj,'Enable','off'); % Disable datacursormode
 
 global S_e, global S_n, global Electron_Charge; 
 % Load global definitions of both energy losses
 
 delete(findall(evalin('base','Statuspanel'),'Type','Axes')); 
 % Delete any axes according to the statuspanel
 
 evalin('base','lasterror(''reset'')'); % Reset any pre-existing errors   
 evalin('base','lastwarn('''')');       % Reset lastwarning to empty state
  
 set( findall(gcf, '-property', 'Enable'), 'Enable', 'off'); 
 % Disbale all uicontrols of main simulation panel ( current figure = Fig )
 set(evalin('base','Status_Lamp'),'FaceColor',[0.85 0.85 0.85]); % Reset indicator color
 set(evalin('base','Text_7'),'String',''); % Reset simulation status text ( = empty string )

 Radial_Mesh_1 = evalin('base','Radial_Mesh_1'); % Read the linear spatial mesh from workspace
 Radial_Mesh_2 = evalin('base','Radial_Mesh_2'); % Read the linear spatial mesh from workspace

 Time_Mesh_1 = evalin('base','Time_Mesh_1'); % Read the logarithmic time mesh from workspace
 Time_Mesh_2 = evalin('base','Time_Mesh_2'); % Read the logarithmic time mesh from workspace
 
 Radial_Grid = evalin('base','Radial_Grid');  
 Time_Grid = evalin('base','Time_Grid');
   
 Erase_Line_Plot(findobj(gcf,'Type','uimenu','Label','Erase existing line')); 
 % Delete any existing line handles and remove line plot from respective subplots

 assignin('base','Reset_Plot',true); % Reset graphics plot in workspace

 assignin('caller','Electronic_Temperature',zeros(length(Radial_Grid),length(Time_Grid))); 
 % Initialize temperatures of the electronic subsystem
 assignin('caller','Lattice_Temperature',zeros(length(Radial_Grid),length(Time_Grid)));
 % Initialize temperatures of the atomic subsystem

 Reset_Plot = evalin('base','Reset_Plot'); % Read reset plot boolean in workspace
 Electronic_Temperature = evalin('caller','Electronic_Temperature');
 Lattice_Temperature = evalin('caller','Lattice_Temperature');
 % Get initial subsystem temperatures ( electronic & atomic )from caller function
 
 Graphics_Plot(Reset_Plot,Radial_Mesh_1,Radial_Mesh_2,Time_Mesh_1,Time_Mesh_2,Electronic_Temperature,Lattice_Temperature);
 % Display temperatures of electronic and amtomic subsystem in main simulation interfaces

 Subplot_Axes = gca; % Assign current axes to subplot axes
 
 tic % Start the calculation timer 

 try  % Solve partial differential equation system numerically in time and radial space
               
     [Electronic_Temp,Lattice_Temp] = PDE_System_Solve(Radial_Grid,Time_Grid);
     
     assignin('base','Electronic_Temp',Electronic_Temp);
     assignin('base','Lattice_Temp',Lattice_Temp);
     
     [Temp_1,Temp_2] = Temperature_Interpolation(Electronic_Temp,Lattice_Temp);
     
     Electronic_Temperature = Temp_1;
     Lattice_Temperature = Temp_2;

     % +------------------------  Error & Warning -- Testcases------------------------+ %
     %                                                                                  %
     %  error(message('MATLAB:pdepe:SpatialDiscretizationFailed'))                      %
     %  warning(message('MATLAB:pdepe:TimeIntegrationFailed',sprintf('%e',1)));         %
     %                                                                                  %
     % +------------------------------------------------------------------------------+ %
 
 catch % React on spatial discretization error during calculation process

     set(evalin('base','Status_Lamp'),'FaceColor','r'); % Set indicator lamp color = red
     str = 'Quit processing on error message !'; % Generate error message 
     set(evalin('base','Text_7'),'String',str); % Allocate error message in status uimenu

 end

 set(evalin('base','Wait_ax'),'Box','off'); 
 set(evalin('base','Rect'),'FaceColor','w','EdgeColor','w');
 set(evalin('base','Txt'),'Position',[10,10]);
 % Remove box and percent values from waitbar axes

 pause(eps) % Resume after short break of "eps" seconds( eps = 2.2e-16 )

 set(evalin('base','Txt'),'FontWeight','normal','FontSize',10.5,'String', ...
     strcat('Processing time: ',{' '},num2str(round(toc,0)),' s')); 
 % Display overall processing time below compute pushbutton

 pause(0.1) % Resume after a 0.1 seconds pause

 delete(evalin('base','Wait_ax'));  % Remove waitbar from simulation panel
 evalin('caller','clear Wait_ax');  % Clear waitbar handle in workspace

 assignin('base','Wait_Index',0);              % Reset wait index
 assignin('base','Time_Index',min(Time_Grid)); % Reset time index to minimum time value

 axes(Subplot_Axes); % Restore subplot axes ( current axes are no longer waitbar axes )
 
 assignin('base','Reset_Plot',false); % No longer reset of graphics plot needed

 if isempty(evalin('base','lasterr')) % Check if errors occured during calculation process

     assignin('base','Electronic_Temperature',Electronic_Temperature);
     assignin('base','Lattice_Temperature',Lattice_Temperature);
     % Transfer calculated subsystem temperatures to matlab workspace

     Graphics_Plot(evalin('base','Reset_Plot'),Radial_Mesh_1,Radial_Mesh_2,...
         Time_Mesh_1,Time_Mesh_2,Electronic_Temperature,Lattice_Temperature);
     % Display temperatures of electronic and atomic subsystem in simulation interface

     try
     
        Electronic_Energy_Loss = Check_Electronic_Energy_Density(Radial_Grid,Time_Grid);
        Atomic_Energy_Loss = Check_Atomic_Energy_Density(Radial_Grid,Time_Grid);
        % Calculate energy losses based on spatial meshgrid and time meshgrid. Units: keV/nm
        
        assignin('base','Electronic_Energy_Loss',Electronic_Energy_Loss);
        assignin('base','Atomic_Energy_Loss',Atomic_Energy_Loss);
          
        Electronic_Input = S_e  * 10^(-12) / Electron_Charge;              
        Atomic_Input = S_n * 10^(-12) / Electron_Charge;
        % Determine energy losses setpoints. Units: keV/nm
        
        assignin('base','Electronic_Input',Electronic_Input);
        assignin('base','Atomic_Input',Atomic_Input);     
             
        Electronic_accuracy = 1 - abs(Electronic_Input - Electronic_Energy_Loss) / Electronic_Input;
        Electronic_accuracy( isnan( Electronic_accuracy ) ) = 0;
        % Estimate electronic accuracy value
                 
        Atomic_accuracy = 1 - abs(Atomic_Input - Atomic_Energy_Loss) / Atomic_Input;
        Atomic_accuracy( isnan( Atomic_accuracy ) ) = 0;
        % Estimate atomic accuracy value  
        
        total_accuracy = ( Electronic_Input * Electronic_accuracy + Atomic_Input * Atomic_accuracy ) ...
                       / ( Electronic_Input + Atomic_Input ) ; 
        % Average relation between actual energy losses and setpoints 
        
        Electronic_loss = Electronic_Energy_Loss / evalin('base','Transfered_Energy_1');
        Electronic_mesh_acc = 1 - abs( Electronic_Input - Electronic_loss ) / Electronic_Input;
        Electronic_mesh_acc( isnan( Electronic_mesh_acc ) ) = 0;
        
        Atomic_loss = Atomic_Energy_Loss / evalin('base','Transfered_Energy_2');
        Atomic_mesh_acc = 1 - abs( Atomic_Input - Atomic_loss ) / Atomic_Input;
        Atomic_mesh_acc( isnan( Atomic_mesh_acc ) ) = 0;
        
        mesh_accuracy = ( Electronic_Input * Electronic_mesh_acc + Atomic_Input * Atomic_mesh_acc ) ...
                       / ( Electronic_Input + Atomic_Input ) ;
          
     catch
         
        total_accuracy = 0;
        mesh_accuracy = 0;
        
        warn_message = {'IncorrectDatytype:AccuracyCalculation', ...
                        'A problem occured during solver accuracy calculation!'};
        
        warning(warn_message{1},warn_message{2});        
         
     end            
             
     total_accuracy( total_accuracy < 0.920 ) = 0.920; % Match any values below threshold
     total_accuracy( total_accuracy > 0.994 ) = 0.994; % Match any values above threshold               
    
     mesh_accuracy( mesh_accuracy < 0.920 ) = 0.920; % Match any values below threshold
     mesh_accuracy( mesh_accuracy > 0.994 ) = 0.994; % Match any values above threshold
     
     
     Statuspanel = evalin('base','Statuspanel');
     status = statusbar(Statuspanel,Statuspanel); % Initialize statusbar
     statusbar(status{1}, status{2}, total_accuracy, mesh_accuracy); 
     % Assign accuracy values to status display         
         
     assignin('base','status',status); % Assign handle to status axes in workspace 
           
     [warn_msg, warn_id] = evalin('caller','lastwarn'); % Try to get last warnings from workspace
    
     if any([~isempty(warn_msg),~isempty( warn_id )]) % Check if warnings are available      
               
         set(evalin('base','Status_Lamp'),'FaceColor',[1.0 0.5 0]); 
         % Set indicator lamp color = orange
         str = 'Warnings are available ...'; % Generate message that warnings are available 
         set(evalin('base','Text_7'),'String',str); % Allocate warning message in status uimenu
            
     else  
            
         set(evalin('base','Status_Lamp'),'FaceColor','g');
         % Set indicator lamp color = green
         str = 'Processing successfully completed.'; 
         % Generate message that processing is successfully completed
         set(evalin('base','Text_7'),'String',str); % Allocate OK message in status uimenu
        
     end
                
 end
  
 colormaps = findall(gcf,'Type','uimenu','Label','Select Colormap');
 children = findall(colormaps,'-property','Children');
 % Get all uimenu handles ( children ) to colormaps uicontextmenu ( parent )
    
 if isempty(findall(children,'-Property','Label','Checked','on')) 
 % None of the colormap uimenus is checked
        
     Label = []; % Set label variable to empty
        
 else % At least one colormap is checked 
        
     Label = get(findobj(children,'-Property','Checked','Checked','on'),'Label');
     % Set label variable to colormap name ( e.g. 'parula' )
        
 end   
 
 Electronic_colorscale = findobj(gcf,'Type','uimenu','Tag','Electronic Colorscale');
 Atomic_colorscale = findobj(gcf,'Type','uimenu','Tag','Atomic Colorscale');
 
 childs = [Electronic_colorscale.Children,Atomic_colorscale.Children];        
 Color_Scales = get(findall(childs,'-Property','Checked','Checked','on'),'Label');
 % Set label variable to colorscale name ( e.g. 'logarithmic' )
   
 set( findall(gcf, '-property', 'Enable'), 'Enable', 'on'); 
 % Set all kind of uicontrol in current figure to enable
  
 set(findall(gcf,'Type','uimenu','-property','Checked'),'Checked','off');
 % Set all respective uimenus to not checked

 if isequal(evalin('base','exist(''Radiation_Status'',''var'')'), true)
              
     if isequal(evalin('base','Radiation_Status'),'disabled')

        set(findobj(gcf,'Type','uimenu','Label','Show radiation heat'),'Enable','off');
        
     else
         
         set(findobj(gcf,'Type','uimenu','Label','Radiation settings'),'Checked','on');
         
     end
       
 else
     
     set(findobj(gcf,'Type','uimenu','Label','Show radiation heat'),'Enable','off');
     
 end
 
 if isequal(evalin('base','exist(''Desorption_Status'',''var'')'), true)
     
     if isequal(evalin('base','Desorption_Status'),'disabled')

        set(findobj(gcf,'Type','uimenu','Label','Show desorption heat'),'Enable','off');
        set(findobj(gcf,'Type','uimenu','Label','Calculate desorption yields'),'Enable','off');
     
     else
         
         set(findobj(gcf,'Type','uimenu','Label','Desorption settings'),'Checked','on');
         
     end
     
 else
     
     set(findobj(gcf,'Type','uimenu','Label','Show desorption heat'),'Enable','off');
     set(findobj(gcf,'Type','uimenu','Label','Calculate desorption yields'),'Enable','off');
      
 end
  
 set(findall(gcf,'Type','uimenu','Label','Erase existing line','-property','Enable'),'Enable','off')
 set(findall(gcf,'Type','uimenu','Label','Datacursormode disabled','-property','Enable'),'Enable','off')
 set(findall(gcf,'Type','uimenu','Label','Add new datacursor','-property','Enable'),'Enable','off');
 set(findall(gcf,'Type','uimenu','Label','Remove all datacursors','-property','Enable'),'Enable','off');
 set(findall(gcf,'Type','uimenu','Label','Zoom - off','-property', 'Enable'),'Enable','off')
 set(findobj(gcf,'Type','uimenu','Label','Show complete subsystems'),'Enable','off'); 
 set(findobj(gcf,'Type','uimenu','Tag','Temperature Profile'),'Enable','off');
 % Disable specific uimenus after computing process

 set(findobj(gcf,'Label','Zoom - off','-property','Checked'),'Checked','on');
 set(findobj(gcf,'Label','Show complete subsystems','-property','Checked'),'Checked','on');
 set(findobj(gcf,'Label','Datacursor - inactive','-property','Checked'),'Checked','on');
 
 if evalin('base','exist(''subs_coeff'',''var'')')
 
    subs_coeff = evalin('base','subs_coeff');
 
    if any(find(strcmp(subs_coeff(:,1),'Yes'))) % Find any data to be substituted      
        
        set(findobj(gcf,'Label','Substitute Coefficients'),'Checked','on');
        % Activate << check symbol >> of coefficients substitution uimenu
        
    end
    
 end
 
 if any([~isequal(D_e_300,150*10^(-4)),~isequal(D_e_min,1*10^(-4))]) % Find any data to be substituted      
        
        set(findobj(gcf,'Label','Electron thermal diffusivity'),'Checked','on');
        % Activate << check symbol >> of coefficients substitution uimenu
        
 end
 
 Thermal_Properties = evalin('base','Thermal_Properties');
 Material_Selection = evalin('base','Material_Selection');

 if isfield(Thermal_Properties.(Material_Selection),'Mean_free_path')
     
    val = Thermal_Properties.(Material_Selection).('Mean_free_path');
 
    if all([~isequal(lambda,[]),~isequal(lambda,val)]) % Find any data to be substituted      
                       
        set(findobj(gcf,'Label','Electron-Phonon mean free path'),'Checked','on');
        % Activate << check symbol >> of coefficients substitution uimenu
               
    end
    
 else
     
     set(findobj(gcf,'Label','Electron-Phonon mean free path'),'Enable','off');
        % Disable mean electron-phonon free path substitution uimenu
    
 end
 
 switch isempty(Label) % Distinguish two different colormap label cases 
     
     case true % Colormap label is an empty value
        
         set(findall(children,'-Property','Label','Label','jet'),'Checked','on');
         % Set the default colormap to state checked
        
     case false % Colormap label is a character value
         
         set(findall(children,'-Property','Label','Label',Label{1}),'Checked','on');
         % Set the specific colormap to state checked ( recall previous settings )
        
 end 
 
 switch isempty(Color_Scales) % Distinguish two different colorscale label cases 
     
     case true % Colorscale label is an empty value
        
         set(findall(childs,'-Property','Label','Label','logarithmic'),'Checked','on');
         % Set the default colormap to state checked
        
     case false % Colormap label is a character value
         
         set(findobj(Electronic_colorscale.Children,'Label',Color_Scales{1}),'Checked','on');
         set(findobj(Atomic_colorscale.Children,'Label',Color_Scales{2}),'Checked','on');
         % Set the specific colorscale to state checked ( recall previous settings )
        
 end
 
 Electronic_Levels = length(get(findobj(gcf,'Tag','Electronic Contour'),'LevelList')) - 2;
 obj = findobj(gcf,'Tag','Electronic Contour Lines');
 set(findobj(obj.Children,'Label',num2str(Electronic_Levels)),'Checked','on');
 
 Atomic_Levels = length(get(findobj(gcf,'Tag','Atomic Contour'),'LevelList')) - 2;
 obj = findobj(gcf,'Tag','Atomic Contour Lines');
 set(findobj(obj.Children,'Label',num2str(Atomic_Levels)),'Checked','on');
 
 
 Lamp_Color = get(evalin('base','Status_Lamp'),'FaceColor'); 
 % Get the indicator lamp color from workspace
    
 if isequal(Lamp_Color,[1 0.5 0]) % Indicator lamp color is orange
        
     set(findobj(gcf,'Type','uimenu','Label','Error Message'),'Enable','off');
     % Disable error message uimenu subordinated to information contextmenu
         
 elseif isequal(Lamp_Color,[1 0 0]) % Indicator lamp color is red            
        
     set(findobj(gcf,'Type','uimenu','Label','Warning Message'),'Enable','off');
     % Disable warning message uimenu subordinated to information contextmenu
        
 else % Indicator lamp color is green
        
     set(findobj(gcf,'Type','uimenu','Label','Error Message'),'Enable','off');
     set(findobj(gcf,'Type','uimenu','Label','Warning Message'),'Enable','off');
     % Disable both message uimenus subordinated to information contextmenu
            
 end
 
 set(0,'CurrentFigure',evalin('base','Fig'));
    
end