function Desorption_Menu(Source,~)
% A user defined function to create a desorption settings menu, while the
% user activates a respective context menu property.

set(Source,'Enable','off');

%% Create an empty desorption settings figure

 Des_fig = figure(5); % Open an additional matlab figure
 Des_fig.Name = 'Desorption Settings'; % Set matlab figure name
 Des_fig.NextPlot = 'replace'; % Replace open figure when called again 
 Des_fig.NumberTitle = 'off'; % No number title is displayed 
 Des_fig.Resize = 'off'; % Do not allow manual window resizing 
 Des_fig.MenuBar = 'none'; % Menubar is not available
 Des_fig.ToolBar = 'none'; % Toolbar is not available
 Des_fig.Units = 'normalized'; % Position units are set to normalized
 Des_fig.Position = [0.30 0.35 0.40 0.30]; % Define figure position on screen
 Des_fig.CloseRequestFcn = {@closerequest,Source};
 
 %% Read variables from workspace
 
 if evalin('base','exist(''Adsorbate_Coverage'',''var'')')
 % Check if adsorbate coverage variable is member of workspace
     
     Adsorbate_Coverage = evalin('base','Adsorbate_Coverage');
     % Get adsorbate coverage value from workspace. Units: normalized
     
 else % Adsorbate coverage variable does not exist in workspace
          
     Adsorbate_Coverage = 1.00; 
     % Set initial adsorbate coverage to unity. Units: normalized
          
 end
  
 if evalin('base','exist(''Desorption_Energy'',''var'')')
 % Check if desorption energy variable is member of workspace
     
     Desorption_Energy = evalin('base','Desorption_Energy');
     % Get desorption energy value from workspace. Units: eV
     
 else % Desorption energy variable does not exist in workspace
          
     Desorption_Energy = 0.40;
     % Set initial value of desorption energy. Units: eV
          
 end
 
 if evalin('base','exist(''Adsorbat_Mass'',''var'')')
 % Check if adsorbate mass variable is available in workspace
     
     Adsorbate_Mass = evalin('base','Adsorbat_Mass');
     % Get adsorbate mass value from workspace. Units: kg/mol
     
 else % Adsorbate mass variable does not exist in workspace
          
     Adsorbate_Mass = 28.00;
     % Set initial value of adsorbate's molare mass. Units: kg/mol
          
 end
 
 if evalin('base','exist(''Zero_Crossing'',''var'')')
 % Check if zero crossing parameter is available in workspace
     
     Zero_Crossing = evalin('base','Zero_Crossing');
     % Get zero crossing parameter from workspace. Units: nm
     
 else % Zero crossing variable does not exist in workspace
          
     Zero_Crossing = 0.20;
     % Set initial value of zero crossing parameter. Units: nm
          
 end
 
 %% Define thermal desorption active states
 
 Fig = evalin('base','Fig'); % Get the handle to main simulation interface ( = Fig )
    
 States = {' desorption disabled ' , ' desorption enabled '}; 
 % Define two different desorption states ( = disabled || enabled )
 
 Panel_1 = uipanel('Title','','TitlePosition','centertop','FontSize',11,...
    'Units','normalized','BorderType','etchedout','BorderWidth',2,'BackgroundColor','w',...
    'Position',[0.01 0.02 0.98 0.25]); 
 % Create a user interface panel that contains all meaningful desorption properties
  
     Desorption_Enable = uicontrol(Panel_1,'style','checkbox','units','normalized',...
                'position',[0.37,0.25,0.23,0.40],'string',States{1},'value',0);
    % Create a user interface checkbox to select the different desorption states
           
 Current_State = get(findobj(Fig,'Type','uimenu','label','Desorption settings'),'Checked');
 % Get the current desorption state with respect to desorption settings label
        
 if isequal(Current_State,'on') % Desorption settings label is checked                           
                   
     States = fliplr(States); 
     % Change the sequence of desorption states ( from front to back of the stack )
                
     set(Desorption_Enable,'string',States{1},'value',1); % Set checkbox to true appearance
        
 else % Desorption settings label is not checked
        
     set(Desorption_Enable,'string',States{1},'value',0); % Set checkbox to false appearance
            
 end        
            
 set(Desorption_Enable,'Backgroundcolor','w','Callback',@Desorption_States);
 % Assign a callback function ( reacts on any user click ) to desorption enable checkbox 
 % and set a white background color for thermal desorption checkbox
    
 function Desorption_States(hObject,~) 
 % A nested callback function to distinguish different desorption states

     States = fliplr(States); % Change the sequence of desorption states
     set(hObject,'string',States{1}); % Set new desorption state to the checkbox

 end
 
 %% Generate data while closing desorption figure

 uicontrol(Panel_1,'style','pushbutton','units','normalized','Tag','OK',...
         'position',[0.72,0.15,0.25,0.60],'string','OK','Callback',@Button_pressed);
 % Create a pushbutton to accept the desorption settings, once the uibutton is pressed

 uicontrol(Panel_1,'style','pushbutton','units','normalized','Tag','Cancel',...
         'position',[0.03,0.15,0.25,0.60],'string','Cancel','Callback',@Button_pressed);
 % Create a pushbutton to cancel the desorption settings, once the uibutton is pressed    
            
 function Button_pressed(hObject,~) % Nested callback function of both pushbuttons

     switch hObject.Tag % Distinguish two different pushbutton types ( = OK || Cancel )

         case 'OK' % OK button is pressed

             Status = get(findobj(gcf,'Type','uicontrol','Style','Checkbox'),'value');
             % Get the current desorption checkbox value ( = true || false )
             Fig = evalin('base','Fig'); % Get the handle to main simulation interface

             if isequal(Status,1) % Checkbox value is equal to true ( = 1 )

                 set(findobj(Fig,'Type','uimenu','Label','Desorption settings'),'Checked','on');
                 % Set the desorption settings label of UIcontextmenu to checked state
                 assignin('base','Desorption_Status','enabled'); 
                 % Set desorption state variable to enabled

             else % Checkbox value is equal to false ( = 0 )

                 set(findobj(Fig,'Type','uimenu','Label','Desorption settings'),'Checked','off');
                 % Set the desorption settings label of UIcontextmenu to unchecked state
                 assignin('base','Desorption_Status','disabled');
                 % Set desorption state variable to disabled

             end 

             Adsorbate_Coverage = get(findobj(gcf,'Tag','Adsorbate_Coverage'),'Value');
             % Get the value of initial adsorbate coverage  
             Edit_Element = get(findobj(gcf,'Tag','Degrees_of_freedom'),'Value');
             % Get the popup element value adsorbate's degrees of freedom 
             Edit_String = get(findobj(gcf,'Tag','Degrees_of_freedom'),'String');
             % Get the popup cell string ( '1'; '2'; '3'; ... ) adsorbate's degrees of freedom                                 
             Degrees_of_freedom = str2double(Edit_String(Edit_Element)); % Determine degrees of freedom
             Desorption_Energy = str2double(get(findobj(gcf,'Tag','Desorption_Energy'),'String'));
             % Read the desorption energy as character variable and convert it to floating point value 

             assignin('base','Adsorbate_Coverage',Adsorbate_Coverage);
             assignin('base','Degrees_of_freedom',Degrees_of_freedom);
             assignin('base','Desorption_Energy',Desorption_Energy);
             % Transfer all three variables to Matlab workspace

             if all([isequal(Txt_1.Visible,'on'),isequal(Txt_2.Visible,'on')])
             % Trigger lennnard-jones potential algorithm being active 

                 Adsorbate_Mass = str2double(get(findobj(gcf,'Tag','Molare_Mass'),'String'));
                 assignin('base','Adsorbate_Mass',Adsorbate_Mass);
                 % Get the adsorbate mass and assign in Matalab workspace 

                 Zero_Crossing = str2double(get(findobj(gcf,'Tag','Zero_Crossing'),'String'));
                 assignin('base','Zero_Crossing',Zero_Crossing);
                 % Get zero crossing parameter and assign in Matalab workspace 

             else

                 assignin('base','LJ_Active',false); 
                 % Set lennard-jones potential algorithm usage to inactive 

             end


         case 'Cancel' % Cancel button is pressed          

     end

     delete(gcf); % Delete the current thermal desorption figure       
     set(Source,'Enable','on');
     
 end
  
 %% Display thermal desorption parameter settings

 Panel_2 = uipanel(Des_fig,'Title','','TitlePosition','centertop','FontSize',11,...
     'Units','normalized','BorderType','etchedout','BorderWidth',2,'BackgroundColor','w',...
     'Position',[0.01 0.30 0.98 0.67]);
 % Create a user interface panel that contains all meaningful desorption properties

    Subpanel_1 = uipanel(Panel_2,'Title','','TitlePosition','centerbottom',...
        'FontSize',9,'Units','normalized','BorderType','etchedout','BorderWidth',2,...
        'BackgroundColor','w','Position',[0.01 0.04 0.63 0.92]);
    % Create a subpanel of second uipanel that contains desorption parameter input
    
        txt = annotation(Subpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.23 0.77 0.40 0.22],'String','Surface coverage: \boldmath$$\tilde{\theta}_0 = $$',...
            'BackgroundColor','w','EdgeColor','none'); % Define a surface coverage label        

        slider = uicontrol(Subpanel_1,'Style','slider','Units','normalized','Position',...
            [0.10 0.62 0.80 0.15],'Min',0,'Max',1,'Value',Adsorbate_Coverage,...
            'BackgroundColor','w','Tag','Adsorbate_Coverage','Callback',@slider_value);
        % Create a user interface slider control to initially set surface coverage values
        
        context = uicontextmenu; % Allow the usage of UIcontextmenu
        slider.UIContextMenu = context; % Assign a contextmenu to surface coverage slider control
        txt.UIContextMenu = context; %Assgin a contextmenu to surface coverage text label 
        
        uimenu(context,'Label','Set surface coverage','Callback',@Monolayer_Coverage);
        % Assign a callback function to surface coverage contextmenu
           
        Subsubpanel_1 = uipanel(Subpanel_1,'Title','','TitlePosition','centerbottom',...
            'FontSize',9,'Units','normalized','BorderType','etchedout','BorderWidth',2,...
            'Position',[0.02 0.02 0.97 0.54],'ButtonDownFcn',@Lennard_Jones_Algorithm);
        % A subpanel that contains all necessary lennard-jones potential parameter settings
        
            Active = annotation(Subsubpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.01 0.45 0.98 0.25],'String','...click here to use surface potential algorithm...',...
            'EdgeColor','none','ButtonDownFcn',@Lennard_Jones_Algorithm);
            % Display activation ( by a user left mouse click ) of the lennard-jones algorithm
        
            Inactive = annotation(Subsubpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.945 0.07 0.06 0.25],'String','x','FontSize',12,'FitBoxToText','on','Visible','off',...
            'Color',[0.3 0.3 0.3],'EdgeColor','none','ButtonDownFcn',@Lennard_Jones_Algorithm,...
            'FontName','Cambria','FontWeight','bold');
            % Show closing sign ( marked with an x ) to skip usage of lennard-jones algorithm
        
            Txt_1 = annotation(Subsubpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.00 0.63 0.27 0.25],'String','Zero-Crossing: {\boldmath$$\sigma$$} $$\left(nm\right)$$',...
            'BackgroundColor','w','EdgeColor','none','Visible','off','FontSize',9.5); 
            % Create a label to recently display zero-crossing parameter    
        
            uicontrol(Subsubpanel_1,'Style','edit','Units','normalized','Position',...
                [0.03 0.04 0.29 0.40],'String',num2str(Zero_Crossing),'Visible','off',...
                'BackgroundColor','w','Tag','Zero_Crossing','FontName','Cambria','FontSize',9,...
                'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2]);
            % Define an user interface editbox to put zero crossing parameter. Units: nm

            Txt_2 = annotation(Subsubpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.47 0.70 0.43 0.25],'String','Adsorbate mass: {\boldmath$$ M$$} $$\left(\frac{g}{mol}\right) $$',...
            'BackgroundColor','w','EdgeColor','none','Visible','off','FontSize',9.5);   
            % Show a textlabel with adsorbate mass description
            
            uicontrol(Subsubpanel_1,'Style','edit','Units','normalized','Position',...
                [0.50 0.04 0.31 0.40],'String',num2str(Adsorbate_Mass),'Visible','off',...
                'BackgroundColor','w','Tag','Molare_Mass','FontName','Cambria','FontSize',9,...
                'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2]);
            % Create a user interface editbox to put adsorbate mass parameter. Units: kg/mol
      
    Subpanel_2 = uipanel(Panel_2,'Title','','TitlePosition','centerbottom',...
        'FontSize',9,'Units','normalized','BorderType','etchedout','BorderWidth',2,...
        'BackgroundColor','w','Position',[0.65 0.04 0.34 0.92]); 
    % Create a additional subpanel of second uipanel that contains desorption parameter input 
        
        annotation(Subpanel_2,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.29 0.32 0.40 0.10],'String','Degrees of freedom',...
            'BackgroundColor','w','EdgeColor','none');
        % Show a textbox with adsorbate's degrees of freedom description
        
        annotation(Subpanel_2,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.32 0.15 0.40 0.10],'String','{\boldmath$$ f = f_{trans} + f_{rot} $$}',...
            'BackgroundColor','w','EdgeColor','none');
        % Show a textbox with adsorbate's degrees of freedom formula
        
        uicontrol(Subpanel_2,'Style','popup','Units','normalized','Position',...
            [0.05 0.28 0.24 0.12],'String',num2str(transpose(0:1:9)),'BackgroundColor','w',...
            'Tag','Degrees_of_freedom','Callback','','FontName','Cambria','FontWeight','bold');
        % Define a popupmenu to select adsorbate's degrees of freedom. Units: counts
         
        annotation(Subpanel_2,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.01 0.74 0.85 0.20],'FontSize',9,'String', ...
            'Desorption Energy: {\boldmath$$ E_{des}$$}$$\;\left(eV\right) $$',...
            'BackgroundColor','w','EdgeColor','none');
        % Show a textbox with adsorbate's desorption energy description
    
        uicontrol(Subpanel_2,'Style','edit','Units','normalized','Position',...
            [0.05 0.57 0.56 0.17],'String',num2str(Desorption_Energy),...
            'BackgroundColor','w','Tag','Desorption_Energy','Callback','',...
            'FontName','Cambria','FontSize',9,'FontWeight','bold',...
            'ForegroundColor',[0.2 0.2 0.2]);
        % Define an editbox to put adsorbate's desorption energy. Units: eV
        
    if evalin('base','exist(''Degrees_of_freedom'',''var'')')
    % Check if adsorbate's degrees of freedom variable exists in workspace 
     
         Degrees_of_freedom = evalin('base','Degrees_of_freedom'); % Get handle to variable
         pos = strfind(get(findobj(gcf,'Tag','Degrees_of_freedom'),'String')',num2str(Degrees_of_freedom));
         % Get selected item position of the degrees of freedom popupmenu
         set(findobj(gcf,'Tag','Degrees_of_freedom'),'Value',pos,'FontName','Cambria',...
             'FontSize',9,'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2]); 
         % Previously set popupmenu value to selected items position
                   
    end
            
    Sliders = findall(gcf,'Type','uicontrol','Style','Slider'); 
    % Get handle to all slider variables

    for i = 1 : 1 : length(Sliders) % Iterate all existing slider handles

        slider_value(Sliders(i)); 
        % Run slider_value functions and extract all selected values 

    end   
       
        
    function slider_value(hObject,~)
    % A nested function to extract and display a selected value of any present slider 
                
        val = round(hObject.Value,3,'significant'); 
        % Get respective slider value and round it significantly
        uicontrol(hObject.Parent,'Style','text','Units','normalized','Position',...
            hObject.Position + [0.57 0.16 -0.70 0],'String',val,'BackgroundColor','w',...
            'FontName','Cambria','FontSize',9,'FontWeight','bold','FontAngle','Italic',...
            'ForegroundColor',[0.2 0.2 0.2]);
        % Display the obtained slider value in a textlabel next to a chosen slider
            
    end

    function Lennard_Jones_Algorithm(hObject,~)
    % Nested function that determines the active state of the lennard-jones algorithm
                       
        if ~isequal(hObject,Inactive) % Use the lennard-jones algorithm
            
            set(findobj(Txt_1),'Visible','on');
            set(findobj(Txt_2),'Visible','on'); 
            % Show both textboxes of the algorithm subpanel
            set(findobj(gcf,'Tag','Zero_Crossing'),'Visible','on');
            set(findobj(gcf,'Tag','Molare_Mass'),'Visible','on');
            % Show zero crossing parameter input and adsorbate mass settings
            set(findobj(Active),'Visible','off');
            set(findobj(Inactive),'Visible','on');
            set(Subsubpanel_1,'BackgroundColor','w'); % Set a white background color
            assignin('base','LJ_Active',true);
            % Assign activation variable to logical state = true
           
            
        elseif isequal(hObject,Inactive) % Don not use the lennard-jones algorithm
                       
            set(findobj(Txt_1),'Visible','off');
            set(findobj(Txt_2),'Visible','off');
            % Hide both textboxes of the algorithm subpanel
            set(findobj(gcf,'Tag','Zero_Crossing'),'Visible','off');
            set(findobj(gcf,'Tag','Molare_Mass'),'Visible','off');
            % Hide zero crossing parameter input and adsorbate mass settings
            set(findobj(Active),'Visible','on');
            set(findobj(Inactive),'Visible','off');
            set(Subsubpanel_1,'BackgroundColor',[0.94 0.94 0.94]); 
            % Reset previously background color settings
            assignin('base','LJ_Active',false);
            % Assign activation variable to logical state = false
            
        end             
        
    end

    function Monolayer_Coverage(~,~)
    % Nested function to update the monolayer coverage on any user input. 
        
        global Monolayer; % Load global variable Monolayer, Value: tunable, Units: m^(-2)
        
        prompt = {'\fontsize{12}{12}\selectfont Monolayer density: $\tilde{n}_{mono}\;\;(m^{-2})$', ...
                  '\fontsize{12}{12}\selectfont Initial surface coverage: $\tilde{\theta}_{0}$'};
        % Define descriptions ( cell variable with characters ) of both input dialog parameters
        dlg_title = 'Surface coverage - Settings'; % Set input dialog title
        num_lines = 1; % Set the number of input lines ( 1 = single line )
        defaultans = {num2str(Monolayer),num2str(slider.Value)}; % Define default answer settings
        options.Interpreter = 'latex'; % Set input dialog interpreter to latex version
        answer = inputdlg(prompt,dlg_title,num_lines,defaultans,options); 
        % Open input dialog window and receive the answer settings
        
        if ~isempty(answer)
        
            Monolayer = str2double(answer{1}); % Get chosen monolayer particle density  
        
            if str2double(answer{2}) > 1 % Upper limit to values equal to unity
            
                slider.Value = 1; % Set value of unity
            
            elseif str2double(answer{2}) < 0 % Lower limit to values equal to zero
            
                slider.Value = 0; % Set value to zero           
            
            else % Value is bound between the upper and lower limit
            
                slider.Value = str2double(answer{2}); 
                % Assign chosen monolayer input to slider value
            
            end
            
        end
       
        uicontrol(slider.Parent,'Style','text','BackgroundColor','w','Units','normalized','Position',...
            slider.Position + [0.57 0.16 -0.70 0],'String',round(slider.Value,3,'significant'),...
            'FontName','Cambria','FontSize',9,'FontWeight','bold','FontAngle','Italic',...
            'ForegroundColor',[0.2 0.2 0.2]);
        % Update slider value in desorption settings window
                
    end
        
end

function closerequest(hObject,~,Source)
% Close request function

 set(Source,'Enable','on');   
 delete(hObject) % Delete open GUI figure
 
end 
