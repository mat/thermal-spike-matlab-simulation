function popup2_Callback(~,~)
% User defined function to determine the amount of valence electrons.

 Popup_2 = evalin('base','Popup_2'); % Get handle of second popumenu in workspace

 allItems = get(Popup_2 ,'String'); % Get items ( entries ) of second popupmenu
 selectedIndex = get(Popup_2,'Value'); % Get seleted index of second popupmenu
 selectedItem = allItems{selectedIndex}; % Extract selected item using selection index

 assignin('base','Valence_Electrons',str2double(selectedItem));
 % Convert amount of valence electrons into numeric datatype and assign it in workspace

end