function Button_Icon(button_handle,path,type,scaling)
% A user defined function to create a pushbutton symbol

 [ a, ~ ] = imread(path,type); % Read picture data according to the path and data type
 [ r, c, ~ ] = size(a); % Get the picture size in x and y direction
 x = ceil( r / scaling(1) ); % Rescale the pixels in x-direction 
 y = ceil( c / scaling(2)); % Rescale the pixels y-direction
 g = a(1 : x : end, 1 : y : end, :); % Get picture data in steps of x and y
 g( g == 255 ) = 5.5 * 255; 
 set(button_handle,'CData',g); % Set color data of the present pushbutton

end