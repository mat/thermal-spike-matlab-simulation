function pushbutton4_Callback(Source,~)
% User defined function to read atomic subsystem energy distribution parameters according 
% to a swift heavy ion impact and thereby emerged atomic energy losses.

 set(Source,'Enable','off');

 %% Atomic Input Parameter Read

 Answer = Atomic_Energy_Parameter(); % Open atomic subsystem energy input dialog

 %% Convert Tunable Constants

 % +-----------------------+
 % |  Atomic Energy Input  |
 % +-----------------------+

 global r_n_0, global t_n_0, global sigma_n_t, global S_n, global c;
 global Electron_Charge, global E_ion; % Load necessary global definitions
   
 if ~isempty(Answer) % Apply only if OK button was pressed (not cancel button)

    E_ion = Answer(1);
    E_ion( E_ion < 0.01)  = 0.01; % Limit ion energy to min. 0.01 MeV/u ( = lower threshold )
    E_ion( E_ion > 1000 ) = 1000; % Limit ion energy to max. 1000 MeV/u ( = upper threshold )
    % Energy of swift heavy ions. Unit: MeV/u 
    
    t_n_0 = 10^(-15) * Answer(2); 
    t_n_0( t_n_0 < 10^(-15) ) = 10^(-15);
    t_n_0( t_n_0 > 10^(-11) ) = 10^(-11);
    % Mean movement time of atoms within the target lattice. Unit: s

    sigma_n_t = 10^(-15) * Answer(3); 
    sigma_n_t( sigma_n_t < 10^(-15) ) = 10^(-15);
    sigma_n_t( sigma_n_t > 10^(-11) ) = 10^(-11);
    % Half width maximum of the atomic gaussian distribution in time. Unit: s

    S_n = 10^3 * 10^9 * Electron_Charge * Answer(4); % (dE/dx)_n [keV/nm] * e [J/eV]
    S_n( S_n < 0 ) = 0;
    S_n( S_n > 1.602 * 10^(-4) ) = 1.602 * 10^(-4);
    % Atomic energy loss dE/dx due to atomic slowing down process. Unit: J/m
          
    if ~isequal(get(findobj(gcf,'Tag','Atomic Radius'),'Checked'),'on')
    
        r_n_0 = 10^(-9) * Coupling_Radius( E_ion, 'Energy' ); 
        % Atomic Coupling Radius of a cylinder where 2/3 of the energy is transfered to the lattice. Unit: m
    
    end    
        
    c = Time_Scaling_Factor_Calculation(t_n_0,sigma_n_t) ...
            * Radius_Scaling_Factor_Calculation(r_n_0);
    % Update atomic energy density scaling factor c to ensure normalized energy input.
    
 end

 set(Source,'Enable','on'); % Set uibutton to enabled state
 
end