function pushbutton5_Callback(obj,~)
% The user defined function "pushbutton5_Callback" reliably processes the
% data export of both subsystems as well as line plot temperature if this
% sort of data is available. 

set(obj,'Enable','off');

drawnow;

Fig = evalin('base','Fig');

Fig.Pointer = 'watch';

drawnow;

ax1 = evalin('base','ax1'); % Get electronic subsystem axes from workspace
ax2 = evalin('base','ax2'); % Get atomic subsystem axes from workspace

Radial_Mesh_1 = get(findobj(ax1.Children,'Type','Surface'),'XData');
Radial_Mesh_2 = get(findobj(ax2.Children,'Type','Surface'),'XData');
% Read radial mesh from temperature surface plots
Time_Mesh_1 = get(findobj(ax1.Children,'Type','Surface'),'YData'); 
Time_Mesh_2 = get(findobj(ax2.Children,'Type','Surface'),'YData');
% Read time mesh from temperature surface plots

%% Format electronic subsystem data

Electronic_Temperature = evalin('base','Electronic_Temperature'); % Read electronic temperature
Data_1 = [ [ NaN, Time_Mesh_1 ]', [ Radial_Mesh_1; Electronic_Temperature ] ]; 
% Add radial mesh and time mesh to electronic temperature array
Data_1 = arrayfun(@num2str, Data_1,'UniformOutput',false); % Convert array to string variables
Data_1{1,1} = ''; % First matrix element is temperature label and thus remains empty
Data_1 = cellfun(@str2double, Data_1,'UniformOutput',false); % Convert all characters to array

%% Format atomic subsystem data

Lattice_Temperature = evalin('base','Lattice_Temperature'); % Read atomic temperature
Data_2 = [ [ NaN, Time_Mesh_2 ]', [ Radial_Mesh_2; Lattice_Temperature ] ];
% Add radial mesh and time mesh to atomic temperature array
Data_2 = arrayfun(@num2str, Data_2,'UniformOutput',false); % Convert array to string variables
Data_2{1,1} = ''; % First matrix element is temperature label and thus remains empty
Data_2 = cellfun(@str2double, Data_2,'UniformOutput',false); % Convert all characters to array

%% Write Data to XLS Document

[file,path] = uiputfile({'*.xlsx';'*.xls'},'Save Document as ...'); % Get path of save file

if all( [ ~isequal(file,0), ~isequal(path,0) ] ) % Check whether path is valid and not empty
    
    if exist(strcat(path,'\',file),'file') ~= 0  % Check if file already exists
    
        delete(fullfile(path,file)); % delete already existing file  
        
    end
    
    excel_write = true; % Creating a repsective excel document is possible
        
else
        
    warndlg('Process canceled by user !','Information'); % Launch a user warning
    excel_write = false; % Creating a repsective excel document is not possible       
    
end

if isequal(excel_write,true)  % Decide on writing excel file  
    
    %% Create the unformatted Excel Document
    
    xlswrite(fullfile(path,file),Data_1,1);  % Write electronic subsystem temperature  
    xlswrite(fullfile(path,file),Data_2,2);  % Write atomic subsystem temperature
    
    if evalin('base','exist(''L'',''var'')') % Check if line plot exists in workspace
        
        L1 = evalin('base','L1'); % Read radial line properties from workspace
        L2 = evalin('base','L2'); % Read time line properties from workspace
                
        Line_Data = [ L1.XData; L2.XData; L2.YData]'; % Create excel line plot data                
        
        if isequal(get(findobj(gcf,'Type','uimenu','Label','Melting point'),'Checked'),'on')
                    
            L3 = evalin('base','L3'); % Read melting point line properties from workspace
            
            Line_Data = [ Line_Data, L3.YData']; % Add melting temperature to line plot data
            
        end
        
        xlswrite(fullfile(path,file),Line_Data,3);  % Write line plot data  
        
    end
    
    %% Open Excel ActiveX Server

    xls = actxserver('Excel.Application'); 
    % Start Excel-Server ( COM Interface using VBA Definitions )

    wb = xls.Workbooks.Open(fullfile(path,file)); % Open given XLS File    
    
    %% Activate Excel Workbooks and define available Worksheets
    
    try % Generate user warning when processing not sucessfully completed
    
    for n = 1 : 1 : 2  % Iterate electronic and atomic subsystem
        
        wb.Sheets.Item(n).Activate; % Activate current worksheet for each subsystem
        
        switch n
            
            case 1
                
                wb_name = 'Electronic Subsystem'; % Electronic subsystem is sheet number one
                
            case 2
                
                wb_name = 'Atomic Subsystem'; % Atomic subsystem is sheet number two
                
        end
        
        wb.Worksheets.Item(n).Name = wb_name; % Assign workbook names to both sheets
        as = wb.ActiveSheet; % Transfer settings to active sheet variable ( as ) 
                
        %% Align cells and create horizontal and vertical border in Excel Document
    
        nRows= as.UsedRange.Rows.Count;     % Get the number of rows in active sheet
        nCols = as.UsedRange.Columns.Count; % Get the number of rcolumns in active sheet
    
        as.UsedRange.HorizontalAlignment = -4108;  % Vertical alignment = centered
        as.UsedRange.VerticalAlignment = -4108;    % Horizontal alignment = centered
        as.UsedRange.NumberFormat = '@';           % Treat all data as text

        as.Range(strcat('B1',':','AY','1')).Interior.ColorIndex = 36;
        as.Range(strcat('A2',':','A',num2str( nRows ))).Interior.ColorIndex = 37;
        % Set different colors for radial mesh row ( yellow ) and time mesh column ( blue )
        
        as.Range(strcat('A1:A',num2str( nRows ))).Insert;        % Insert empty column
        as.Range(strcat('A1:',xlscol( nCols + 1 ),'1')).Insert;  % Insert empty row        
                       
        Merge_Col = as.get('Range',strcat('A3:A',num2str( nRows + 1)));
        Merge_Col.MergeCells = 1; % Merge cells to insert the label for time values
        Merge_Col.Value = char(compose(string('\n') + string('Time:  t  [s]'))); % Insert time label
        Merge_Col.HorizontalAlignment = -4108; % Horizontal alignment = centered
        Merge_Col.VerticalAlignment = -4160; % Vertical alignment = centered
        
        Merge_Row = as.get('Range',strcat('C1:',xlscol( nCols + 1 ),'1'));
        Merge_Row.MergeCells = 1; % Merge cells to insert the label for radial values       
        Merge_Row.Value = strcat(linspace(32,32,10),'Radius:  r  [nm]'); % Insert radial label
        Merge_Row.HorizontalAlignment = -4131; % Horizontal alignment = left
        Merge_Row.VerticalAlignment = -4108; % Vertical alignment = centered
        
        Merge_Head = as.get('Range','A1:B2'); % Get the respective sheet range
        Merge_Head.MergeCells = 1; % Merge cells to insert the temperature label
        Merge_Head.Value = 'Temperature:  T  [K]'; % Insert temperature label
        Merge_Head.HorizontalAlignment = -4108; % Horizontal alignment = centered
        Merge_Head.VerticalAlignment = -4108; % Vertical alignment = top
                
        Left_Border = as.Range(strcat('A1',':','A',num2str( nRows + 1 ))); % Left document border
        Left_Border.Borders.Item('xlEdgeLeft').LineStyle =  1; % Apply continious line
        Left_Border.Borders.Item('xlEdgeLeft').Weight = -4138; % Draw line bold
        
        Upper_Border = as.Range(strcat('A1',':',xlscol( nCols + 1 ),'1')); % Upper document border
        Upper_Border.Borders.Item('xlEdgeTop').LineStyle =  1; % Apply continious line
        Upper_Border.Borders.Item('xlEdgeTop').Weight = -4138; % Draw line bold
        
        Mid_Line_1 = as.Range(strcat('A3',':',xlscol( nCols + 1 ),'3'));
        Mid_Line_1.Borders.Item('xlEdgeTop').LineStyle =  1; % Apply continious line
        Mid_Line_1.Borders.Item('xlEdgeTop').Weight = -4138; % Draw line bold
        
        Mid_Line_2 = as.Range(strcat('C1',':','C',num2str( nRows + 1 )));
        Mid_Line_2.Borders.Item('xlEdgeLeft').LineStyle =  1; % Apply continious line
        Mid_Line_2.Borders.Item('xlEdgeLeft').Weight = -4138; % Draw line bold 
                        
        Right_Border = as.Range(strcat(xlscol( nCols + 1 ),'1',':',xlscol( nCols + 1 ),num2str( nRows + 1 ))); 
        % Right document border
        Right_Border.Borders.Item('xlEdgeRight').LineStyle =  1; % Apply continious line
        Right_Border.Borders.Item('xlEdgeRight').Weight = -4138; % Draw line bold
        
        Lower_Border = as.Range(strcat('A',num2str( nRows + 1 ),':',xlscol( nCols + 1 ),num2str( nRows + 1 )));
        % Lower document border
        Lower_Border.Borders.Item('xlEdgeBottom').LineStyle =  1; % Apply continious line
        Lower_Border.Borders.Item('xlEdgeBottom').Weight = -4138; % Draw line bold
        
        as.Range(strcat('A1:A',num2str( nRows + 1 ))).Insert;    % Insert empty column
        as.Range(strcat('A1:',xlscol( nCols + 2 ),'1')).Insert;  % Insert empty row
        
        as.Range(strcat('B1',':','B',num2str( nRows + 1 ))).ColumnWidth = 15; % Set column width
        as.Range(strcat('A1',':','A',num2str( nRows + 1 ))).ColumnWidth =  3; % Set column width
        as.Range(strcat('A2:',xlscol( nCols + 1 ),'3')).RowHeight = 35; % Set row heigth
                          
    end 
     
    if evalin('base','exist(''L'',''var'')') % Check if line plot exists in workspace
    
        wb.Sheets.Item(3).Activate; % Activate line plot worksheet    
        wb.Worksheets.Item(3).Name = 'Line Plot'; % Line plot is sheet number three
        as = wb.ActiveSheet; % Transfer workbook three settings to active sheet variable ( as )
        as.Range('A1:D1').Insert;  % Insert empty row at the top of the worksheet
        as.Range('A1').Value = 'Radius:  r  [nm]';
        as.Range('B1').Value = 'Time:  t  [s]';
        
        try  % Throw marginally errors while inserting subsystem temperature label
            
            line_ax = evalin('base','line_ax'); % Get line plot axes from workspace
            
            if isequal( line_ax.Tag, 'Atomic Subsystem' ) % Check temporary subsystem type
        
                as.Range('C1').Value = 'Atomic Temperature:  T_a  [K]'; 
                % Insert atomic subsystem temperature label in line plot worksheet 
                
            else
                
                as.Range('C1').Value = 'Electronic Temperature:  T_e  [K]';
                % Insert electronic subsystem temperature label in line plot worksheet 
                                
            end
            
            if isequal( size(Line_Data,2), 4 ) % Check presence of melting temperature plot 
            
                as.Range('D1').Value = 'Melting Point:  T_M  [K]';
                % Insert melting temperature label in line plot worksheet
            
            end
            
        catch
            
            as.Range('C1').Value = 'Temperature:  T  [K]'; 
            % Catch errors by simply inserting temperature label in line plot worksheet                     
            
        end               
         
        as.UsedRange.HorizontalAlignment = -4108;  % Vertical alignment = centered
        as.UsedRange.VerticalAlignment = -4108;    % Horizontal alignment = centered
        as.UsedRange.NumberFormat = '@';           % Treat all data as text

        nRows= as.UsedRange.Rows.Count;      % Get the number of rows in active sheet
        nCols = as.UsedRange.Columns.Count;  % Get the number of rcolumns in active sheet

        Left_Border = as.Range(strcat('A1',':','A',num2str( nRows ))); % Left document border
        Left_Border.Borders.Item('xlEdgeLeft').LineStyle =  1; % Apply continious line
        Left_Border.Borders.Item('xlEdgeLeft').Weight = -4138; % Draw line bold

        Upper_Border = as.Range(strcat('A1',':',xlscol( nCols ),'1')); % Upper document border
        Upper_Border.Borders.Item('xlEdgeTop').LineStyle =  1; % Apply continious line
        Upper_Border.Borders.Item('xlEdgeTop').Weight = -4138; % Draw line bold

        Header_Line = as.Range(strcat('A2',':',xlscol( nCols ),'3'));
        Header_Line.Borders.Item('xlEdgeTop').LineStyle =  1; % Apply continious line
        Header_Line.Borders.Item('xlEdgeTop').Weight = -4138; % Draw line bold

        Vetical_Line_1 = as.Range(strcat('B1',':','B',num2str( nRows ))); % Vertical line
        Vetical_Line_1.Borders.Item('xlEdgeLeft').LineStyle =  1; % Apply continious line
        Vetical_Line_1.Borders.Item('xlEdgeLeft').Weight = -4138; % Draw line bold

        Vetical_Line_2 = as.Range(strcat('C1',':','C',num2str( nRows ))); % Vertical line
        Vetical_Line_2.Borders.Item('xlEdgeLeft').LineStyle =  1; % Apply continious line
        Vetical_Line_2.Borders.Item('xlEdgeLeft').Weight = -4138; % Draw line bold

        if isequal( size(Line_Data,2), 4 ) % Check presence of melting temperature plot 

            Vetical_Line_3 = as.Range(strcat('D1',':','D',num2str( nRows ))); % Vertical line
            Vetical_Line_3.Borders.Item('xlEdgeLeft').LineStyle =  1; % Apply continious line
            Vetical_Line_3.Borders.Item('xlEdgeLeft').Weight = -4138; % Draw line bold

        end

        Right_Border = as.Range(strcat(xlscol( nCols ),'1',':',xlscol( nCols ),num2str( nRows ))); 
        % Right document border
        Right_Border.Borders.Item('xlEdgeRight').LineStyle =  1; % Apply continious line
        Right_Border.Borders.Item('xlEdgeRight').Weight = -4138; % Draw line bold

        Lower_Border = as.Range(strcat('A',num2str( nRows ),':',xlscol( nCols ),num2str( nRows )));
        % Lower document border
        Lower_Border.Borders.Item('xlEdgeBottom').LineStyle =  1; % Apply continious line
        Lower_Border.Borders.Item('xlEdgeBottom').Weight = -4138; % Draw line bold

        as.Range(strcat('A1:A',num2str( nRows ))).Insert;    % Insert empty column
        as.Range(strcat('A1:',xlscol( nCols + 1 ),'1')).Insert;  % Insert empty row

        as.UsedRange.ColumnWidth    = 20;  % General column width of line plot worksheet
        as.Range('A1').ColumnWidth  =  3;  % Set empty column width to a small value ( spacing )
        as.Range('D1').ColumnWidth  = 35;  % Match subsystem temperature column width
        as.Range('E1').ColumnWidth  = 25;  % Change column width of melting temperature
        as.Range('A2:E2').RowHeight = 30;  % Set row height for header containing labels
        
    end
    
    helpdlg('Data export successfully completed.','Information'); 
    % User information dialog when data was computed successfully
            
    catch
        
        warndlg('An error occured while processing data export.','!! Warning !!','replace');
        % User warning dialog on any errors while processing export data
        
    end
    
    %% Save Worksheet & close XLS Server

    wb.Save; % Save Datasheet
    wb.Close; % Close Datasheet
    delete(xls); % Close Excel Server

end

set(obj,'Enable','on');

Fig.Pointer = 'arrow';
drawnow;

end