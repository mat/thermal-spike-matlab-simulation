function pushbutton3_Callback(Source,~)
% User defined function to read electronic subsystem energy distribution parameters according 
% to a swift heavy ion impact and thereby emerged electronic energy losses.

set(Source,'Enable','off');

 %% Electronic Input Parameter Read

 Answer = Electronic_Energy_Parameter(); % Open electronic subsystem energy input dialog 

% % Convert Tunable Constants

 % +---------------------------+
 % |  Electronic Energy Input  |
 % +---------------------------+

 global E_ion, global t_e_0, global sigma_e_t, global S_e, global alpha, global r_n_0;
 global Electron_Charge, global I_ion, global b; % Load necessary global definitions
 
 Thermal_Properties = evalin('base','Thermal_Properties');
 Material_Selection = evalin('base','Material_Selection');
 
 if ~isempty(Answer) % Apply only if OK button was pressed (not cancel button)
       
    E_ion = Answer(1);
    E_ion( E_ion < 0.01)  = 0.01; % Limit ion energy to min. 0.01 MeV/u ( = lower threshold )
    E_ion( E_ion > 1000 ) = 1000; % Limit ion energy to max. 1000 MeV/u ( = upper threshold )
    % Energy of swift heavy ions. Unit: MeV/u
       
    t_e_0 = 10^(-15) * Answer(2);
    t_e_0( t_e_0 < 10^(-17) ) = 10^(-17);
    t_e_0( t_e_0 > 10^(-13) ) = 10^(-13);
    % Mean flight time of delta ray electrons within the target material. Unit: s

    sigma_e_t = 10^(-15) * Answer(3); 
    sigma_e_t( sigma_e_t < 10^(-17) ) = 10^(-17);
    sigma_e_t( sigma_e_t > 10^(-13) ) = 10^(-13);
    % Half width maximum of the electronic gaussian distribution in time. Unit: s

    S_e = 10^3 * 10^9 * Electron_Charge * Answer(4); % (dE/dx)_e [keV/nm] * e [J/eV]
    S_e( S_e < 0 ) = 0;
    S_e( S_e > 1.602 * 10^(-4) ) = 1.602 * 10^(-4);
    % Electronic energy loss dE/dx due to electronic slowing down process. Unit: J/m
    
    I_ion = Answer(5); % Ionization potential of target electrons. Unit: eV
    I_ion( I_ion <= 0 ) = 0.001;
    I_ion( I_ion > 1000 ) = 1000;
    
    alpha = Answer(6);
    alpha( alpha < 0 ) = 0;
    % Electronic energy transfer parameter due to electronic slowing down process. Unit: nm

    rho = Thermal_Properties.(Material_Selection).('Solid_Mass_Density');
 
    Waligorski_Function( rho ); % Run waligorski distribution calculation 
                  
    b = Time_Scaling_Factor_Calculation( t_e_0, sigma_e_t );
    % Update electronic energy density scaling factor b to ensure normalized energy input.
            
    if ~isequal(get(findobj(gcf,'Tag','Atomic Radius'),'Checked'),'on')
    
        r_n_0 = 10^(-9) * Coupling_Radius( E_ion, 'Energy' );
        
    end
    
 end
 
 set(Source,'Enable','on'); % Set uibutton to enabled state
    
end