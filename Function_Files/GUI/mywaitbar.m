function mywaitbar(Panel,Time_Mesh,Time)
% User defined function to display the calculation progress in main simulation panel. Since
% the PDE solving process uses repetitive spatial steps, actual time steps shall be used to 
% determine an ongoing process while numerically solving the coupled equation system.  

 Quotient = ( log10(min(Time_Mesh)) - log10(Time) ) / ( log10(min(Time_Mesh)) - log10(max(Time_Mesh)) );
 % Define a logarithmic scaled quotient with respect to the actual time value

 switch evalin('base','exist(''Wait_ax'',''var'')') 
 % Handle to waitbar axes already exist in workspace
    
     case false % No
        
         Wait_ax = axes(Panel,'Position',[0.45 0.10 0.10 0.09],'NextPlot','replaceall');
         set(Wait_ax,'Xtick',[],'Ytick',[],'Xlim',[0 1000],'Box','on'); 
         % Create a new axes setting and assign it to waitbar axes
         color = [0.35, 0.81, 0.96]; % Define a waitbar background color 
         Rect = rectangle(Wait_ax,'Position',[0,0,(round(1000*Quotient)),20]);
         set(Rect,'FaceColor',color,'EdgeColor',[0.10 0.10 0.10]);
         Txt = text(Wait_ax,400,11,[num2str(round(100*Quotient)),' %']);
         set(Txt,'FontSize',9,'FontWeight','bold','Color',[0.10 0.10 0.10]);
         % Create a rectangle and a textbox to display current waitbar status ( bar & percent )
        
         assignin('base','Wait_ax',Wait_ax); % Assign waitbar axes handle in workspace
         assignin('base','Rect',Rect); % Assign rectangle handle in workspace
         assignin('base','Txt',Txt); % Assign textbox handle in workspace
        
     case true % Yes
        
         set(evalin('base','Rect'),'Position',[0,0,(round(1000*Quotient)),20]);
         set(evalin('base','Txt'),'String',[num2str(round(100*Quotient)),' %']);
         % Reset rectangle and textbox to inital settings
                
 end

end
