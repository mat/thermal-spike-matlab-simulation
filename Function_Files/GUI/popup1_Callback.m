function popup1_Callback(~,~)
% User defined function to determine the target material selection.

 global lambda; % Load global definition of electron-phonon mean free path
  
 Popup_1 = evalin('base','Popup_1'); % Get handle of first popumenu in workspace

 allItems = get(Popup_1 ,'String'); % Get items ( entries ) of first popupmenu
 selectedIndex = get(Popup_1,'Value'); % Get seleted index of first popupmenu
 selectedItem = allItems{selectedIndex}; % Extract selected item using selection index 
 
 if evalin('base','exist(''Material_Selection'',''var'')')
 
    if ~isequal(selectedItem,evalin('base','Material_Selection')) 
    % Check type of meterial has changed
     
        Global_Constants(); % Reload global constants on any material changes
        
    end
          
 end

 assignin('base','Material_Selection',selectedItem); 
 % Assign target material selection in workspace

 try % Apply following routine since no errors occur
    
     evalin('base','clear coeff_tab;'); % Clear handle to coefficients table in workspace
     evalin('base','clear coeff_data;'); % Clear coefficients data variable in workspace
     evalin('base','clear subs_coeff;'); % Clear coefficients substitution in workspace
     set(findobj(evalin('base','Fig'),'Label','Substitute Coefficients'),'Checked','off');
     % Set coefficients substitution uimenu to unchecked state
    
 catch % Do not catch any errors !
    
 end
 
 Thermal_Properties = evalin('base','Thermal_Properties'); % Get thermal properties from workspace
 
 if ~isequal(Thermal_Properties.(selectedItem).('Type'),'Metal') % Check if material type is not metal
     
     lambda = Thermal_Properties.(selectedItem).('Mean_free_path'); % Get mean free path value
     set(findobj(gcf,'Label','Electron-Phonon mean free path'),'Enable','on');
     % Use mean free path between electron-phonon-interaction 
     
 else
     
     lambda = [];
     set(findobj(gcf,'Label','Electron-Phonon mean free path'),'Enable','off');
     % Set mean free path value to empty
    
 end
 
 rho = Thermal_Properties.(selectedItem).('Solid_Mass_Density'); % Get mass density value
 
 Waligorski_Function(rho); % Run waligorski distribution calculation
 
end