function pushbutton1_Callback(Source,~)
% User defined function to set ODE solver settings by requesting an input 
% dialog window. 

set(Source,'Enable','off'); % Set uibutton to disabled state

global Options; % Load global definition of solver options

prompt = {'\fontsize{11}{11}\selectfont Minimum Time: $$\;t_{min}\;\;(s)$$', ...
          '\fontsize{11}{11}\selectfont Maximum Time: $$\;t_{max}\;\;(s)$$', ...
          '\fontsize{11}{11}\selectfont Number of iterations in time: $$\;N_t$$', ...
          '\fontsize{11}{11}\selectfont Maximum radius: $$\;R\;\;(nm)$$', ...
          '\fontsize{11}{11}\selectfont Number of iterations in space: $$\;N_r$$', ... 
          '\fontsize{11}{11}\selectfont Relative solver tolerance: $$\;\Delta T/T$$', ...
          '\fontsize{11}{11}\selectfont Absolute solver tolerance: $$\;\Delta T\;\;(K)$$', ...
          '\fontsize{11}{11}\selectfont Normalization control: [on/off]'};
% Define input dialog to get PDE solving options

Time_Grid = evalin('base','Time_Grid'); % Evaluate time meshgrid in workspace
Radial_Grid = 10^9 * evalin('base','Radial_Grid'); % Evaluate spatial meshgrid in workspace
% strcat('[',num2str(Time_Grid(1)),',',num2str(Time_Grid(end)),']')
default_min_Time = num2str(Time_Grid(1));
default_max_Time = num2str(Time_Grid(end));
default_Iterations_Time = num2str(length(Time_Grid));
default_Radius = num2str(Radial_Grid(end));
default_Iterations_Radius = num2str(length(Radial_Grid));
default_RelTol = num2str(Options.RelTol);
default_AbsTol = num2str(Options.AbsTol);
default_NormContr = Options.NormControl;
% Obtain temporary default values reagrding time mesh, spatial mesh, iterations, relative
% and absolute tolerance of the PDE solver as well as solving normalization control.

default = {default_min_Time,default_max_Time,default_Iterations_Time,default_Radius, ...
           default_Iterations_Radius,default_RelTol,default_AbsTol,default_NormContr}; 
% Assign defaults to cell variable

title = 'Solver Options'; % Set title of input dialog window 

options.Interpreter='latex'; % Use latex interpreter

lineNo = 1;
%[1 57; 1 57; 1 57; 1 57; 1 57; 1 57; 1 57; 1 57]
Solver_Options = inputdlg(prompt,title,lineNo,default,options);
% Get solver options by requesting input dialog

assignin('base','Solver_Options',Solver_Options); % Assign solver options in workspace

if ~isempty(Solver_Options) % User did not cancel input dialog

    Time_Mesh = [ min([evalin('base','Time_Mesh_1'),evalin('base','Time_Mesh_2')]), ...
                  max([evalin('base','Time_Mesh_1'),evalin('base','Time_Mesh_2')]) ];
        
    Time = [str2double(char(Solver_Options(1))),str2double(char(Solver_Options(2)))];
    % Get time interval [t_min, t_max]
    
    Time_lower = Time(1);
    Time_lower( Time_lower > min(Time_Mesh) ) = min(Time_Mesh);
    Time_lower( Time_lower < 10^(-19) ) = 10^(-19);
    Time(1) = Time_lower;
        
    Time_upper = Time(2);
    Time_upper( Time_upper < max(Time_Mesh) ) = max(Time_Mesh);
    Time_upper( Time_upper > 10^(-7) ) = 10^(-7);
    Time(2) = Time_upper;
    
    Time_Iterations = round(str2double(Solver_Options(3)),0); % Get number of time iterations
    Time_Iterations( Time_Iterations < 30 ) = 30;
    Time_Iterations( Time_Iterations > 500 ) = 500;
    
    assignin('base','Time_Grid',LogSpace(Time,Time_Iterations)); 
    % Assign logarithmic scaled time mesh in workspace

    Radial_Mesh = [ min([evalin('base','Radial_Mesh_1'),evalin('base','Radial_Mesh_2')]),...
                    max([evalin('base','Radial_Mesh_1'),evalin('base','Radial_Mesh_2')]) ];
    
    Radius = round(str2double(Solver_Options(4)),1); % Get maximum radius r_max
    Needed_Radius = 10^(9) * max(Radial_Mesh);
    Radius( Radius < Needed_Radius ) = Needed_Radius;
    Radius( Radius > 10^5 ) = 10^5;
    Radius_Iterations = round(str2double(Solver_Options(5)),0); % Get number of spatial iterations
    Radius_Iterations( Radius_Iterations < 30 ) = 30;
    Radius_Iterations( Radius_Iterations > 500 ) = 500;
    
    theta = evalin('base','theta');
    
    assignin('base','Radial_Grid',[0,LogSpace([theta,10^(-9)*Radius],Radius_Iterations - 1)]);
    % Assign linear scaled spatial mesh in workspace

    RelTol = str2double(Solver_Options(6)); % Get relative solver tolerance
    RelTol( RelTol > 0.01 ) = 0.01;
    RelTol( RelTol < 10^(-12) ) = 10^(-12);
        
    AbsTol = str2double(Solver_Options(7)); % Get absolute solver tolerance
    AbsTol( AbsTol > 0.001 ) = 10^(-3);
    AbsTol( AbsTol < eps ) = str2double(sprintf('%.0e',eps));
    
    NormContr = char(Solver_Options(8));    % Get normalization control active state

    if ~any([isequal(NormContr,'on'),isequal(NormContr,'off')])
        
        NormContr = 'on';
        
    end
    
    Options = odeset('RelTol',RelTol,'AbsTol',AbsTol,'NormControl',NormContr,'Stats','on','NonNegative',1);
    % Assign ODE solver settings to global options variable
    
end

set(Source,'Enable','on'); % Set uibutton to enabled state

end