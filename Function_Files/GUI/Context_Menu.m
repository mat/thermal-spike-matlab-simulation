function Context_Menu()
% This user defined function creates specific uicontextmenus in the menubar
% of main simulation interface.

 Fig = evalin('base','Fig'); % Get the main simulation figure
 set(0,'currentfigure',Fig);
 
 %% Contextmenu 1 = View

 View = uimenu(Fig,'Label','View'); % Create UIContextmenu with label << view >>

 uimenu(View,'Label','Show complete subsystems','Callback',@Full_Callback); 
 % uimenu << Show complete subsystem >>

 function Full_Callback(source,~)

     ax1 = evalin('base','ax1'); % Get axes of first subsystem from workspace
     ax2 = evalin('base','ax2'); % Get axes of second subsystem from workspace
               
     Radial_Mesh_1 = get(findobj(ax1.Children,'Type','Surface'),'XData'); 
     Radial_Mesh_2 = get(findobj(ax2.Children,'Type','Surface'),'XData');      
     % Get spatial mesh from subsystem temperature surface plot
     Time_Mesh_1 = get(findobj(ax1.Children,'Type','Surface'),'YData'); 
     Time_Mesh_2 = get(findobj(ax2.Children,'Type','Surface'),'YData');
     % Get time mesh from subsystem temperature surface plot
        
     set(ax1,'XLim', [min( Radial_Mesh_1 ), max( Radial_Mesh_1 )] );
     set(ax1,'YLim', [min( Time_Mesh_1 ), max( Time_Mesh_1 )] );
        
     set(ax2,'XLim', [min( Radial_Mesh_2 ), max( Radial_Mesh_2 )] );
     set(ax2,'YLim', [min( Time_Mesh_2 ), max( Time_Mesh_2 )] );
     % Set axes limits to absolute minimum and maximum coordinates of given meshgrid
                
     set(source.Parent.Children(1),'Enable','on','Checked','off'); 
     % Reset checked state of antithetical uimenu
     set(source,'Enable','off','Checked','on'); % Toggle source to checked state

 end

 uimenu(View,'Label','Show detailed subsystems','Callback',@Section_Callback); 
 % uimenu << Reset to origin view >>

 function Section_Callback(source,~)
        
     ax1 = evalin('base','ax1'); % Get axes of first subsystem from workspace
     ax2 = evalin('base','ax2'); % Get axes of second subsystem from workspace
        
     Radial_Mesh_1 = get(findobj(ax1.Children,'Type','Surface'),'XData'); 
     Radial_Mesh_2 = get(findobj(ax2.Children,'Type','Surface'),'XData');      
     % Get spatial mesh from subsystem temperature surface plot
     Time_Mesh_1 = get(findobj(ax1.Children,'Type','Surface'),'YData'); 
     Time_Mesh_2 = get(findobj(ax2.Children,'Type','Surface'),'YData');
     % Get time mesh from subsystem temperature surface plot
                        
     set(ax1,'XLim', 0.5 * [min( Radial_Mesh_1 ), max( Radial_Mesh_1 )] );
     set(ax1,'YLim', [min( Time_Mesh_1 ), max( Time_Mesh_1 )] );
        
     set(ax2,'XLim', 0.7 * [min( Radial_Mesh_2 ), max( Radial_Mesh_2 )] );
     set(ax2,'YLim', [min( Time_Mesh_2 ), max( Time_Mesh_2 )] );
     % Set axes limits to fractions of minimum and maximum coordinates
                
     set(source.Parent.Children(2),'Enable','on','Checked','off'); 
     % Reset checked state of antithetical uimenu
     set(source,'Enable','off','Checked','on'); % Toggle source to checked state
        
 end

 %% Contextmenu 2 = Zoom

 Zoom = uimenu(Fig,'Label','Zoom'); % Create UIContextmenu with label << Zoom >>

 uimenu(Zoom,'Label','Zoom - on','Callback',@Zoom_On_Callback); % uimenu << Zoom on >>

 function Zoom_On_Callback(source,~)        
                
     hz = zoom(gcf); % Get handle to zoom functionality
     set(hz,'Enable','on','Direction','in','RightClickAction','InverseZoom');
     % Activate zoom function in current figure
        
     set(source,'Enable','off','Checked','on'); % Toggle source to checked state
                
     global Zoom_Status, Zoom_Status = get(findall(gcf,'Type','uimenu'));
     % Zoom status acts as a memory for temporary uimenu enable states
        
     set(findall(gcf,'Type','uimenu','-property','Enable'),'Enable','off');
     % Disable all other uimenus to avoid any kind of interaction troubles
     set(source.Parent.Children(1),'Enable','on','Checked','off');
     set(source.Parent,'Enable','on'); % Toogle source to enabled state 
                
     assignin('base','Current_Position',get(gcf,'CurrentPoint')); 
     % Get current mouse position from workspace
        
     Trigger = zeros(1,3); % Initialize subsystem trigger variable
        
     for i = 1 : 1 : 3 % Interate all subsystem axes
        
         ax = strcat('ax',num2str(i)); % Dynamically assign subsystem axes
                        
         Trigger(i) =  evalin('base',strcat( strcat('all( [ Current_Position > ',ax,'.Position(1),'), ...
            strcat('Current_Position < ( ',ax,'.Position(1) + ',ax,'.Position(3) ),'), ...
            strcat('Current_Position > ',ax,'.Position(2),'), ...
            strcat('Current_Position < ( ',ax,'.Position(2) + ',ax,'.Position(4) )'),'] );' ) );
        % Check whether the current position sweeps any subsystem           
              
        if isequal(Trigger(i),true) % Mouse is detected to sweep a specific subsystem                           
                         
           set(gcf,'CurrentAxes', evalin('base',ax)); % Make subsystem axes to current axes
              
           break; % Quit << for-loop >> routine
              
        end
           
     end            
       
     if ~isequal(findobj(gca,'Type','surface'),[]) % Current axes contains a << surface plot >>
        
         X_Data = get(findobj(gca,'Type','surface'),'XData'); % Use x data of surface plot
        
     else % Current axes must contain a << line plot >>
           
         X_Data = get(findobj(gca,'Type','line'),'XData'); % Use x data of line plot
           
     end        
        
     X_Limits = get(gca,'XLim'); % Get the x limits of current axis
        
     if any( [ X_Limits( 1 ) < min( X_Data ),  X_Limits( 2 ) > max( X_Data ) ] )
     % All kind of x data within the axes x limits
            
         set(gca,'XLim',[ min( X_Data ), max( X_Data ) ]); 
         % Set new x limits to minimum and maximum values of x data
            
     end    
         
 end

 uimenu(Zoom,'Label','Zoom - off','Callback',@Zoom_Off_Callback); % uimenu << Zoom off >>

 function Zoom_Off_Callback(source,~)       
            
     hz = zoom(gcf);  % Get handle to zoom functionality        
     set(hz,'Enable','off'); % Disactivate zoom function in current figure
                       
     global Zoom_Status % Load zoom status variable ( contains enable values of uimenus )
        
     Menu_Objects = findall(gcf,'Type','uimenu'); % Get all kind of uimenu in current figure

     for j = 1 : 1 : length(Menu_Objects) % Iterate the variety of uimenus    

         Menu_Objects(j).Enable = char(Zoom_Status(j).Enable); 
         % Set all enable states to previous values ( before zoom on became true )

     end
                
     set(findall(gcf,'Type','uimenu','Label','Reset to origin view','-property','Checked'),'Checked','off');
     set(findall(gcf,'Type','uimenu','Label','Show complete subsystems','-property','Checked'),'Checked','off');
     % Reset both << view uimenus >> to unchecked state 
        
     set(source.Parent.Children(2),'Enable','on','Checked','off'); % Toggle source to unchecked state
     set(source,'Enable','off','Checked','on'); % Toggle source to checked state 
        
 end

 %% Contextmenu 3 = Datacursor

 Datacursor = uimenu(Fig,'Label','Datacursormode');
 % Create UIContextmenu with label << Datacursor >>

 uimenu(Datacursor,'Label','Datacursormode enabled','Callback',@Datacursormode_On); 
 % uimenu << Datacursormode on >>
 
 function Datacursormode_On(source,~)        
  
     assignin('base','Cursor_Source',source);
     dcm_obj = datacursormode(gcf); % Open datacursor settings
     set(dcm_obj,'Enable','on'); % Enable datacursormode
     set(source,'Enable','off','Checked','on'); % Toggle checked state to on
     set(source.Parent.Children(1),'Enable','on','Checked','off'); % Enable remaining uimenu
               
 end
  
 uimenu(Datacursor,'Label','Add new datacursor','Enable','off','Callback',@Add_Datacursor);

 function Add_Datacursor(source,~)
     
     dcm_obj = datacursormode(gcf);
     ax = gca;
     Cursor_Plot = findobj(ax.Children,'Type','Surface');
     dcm_obj.createDatatip(Cursor_Plot);          
               
     set(source,'Enable','on','Checked','on');
     set(source.Parent.Children(2),'Checked','off');
        
 end 

 uimenu(Datacursor,'Label','Remove all datacursors','Enable','off','Callback',@Delete_Datacursor);
 
 function Delete_Datacursor(source,~)
     
     assignin('caller','Cursor_Object',datacursormode(gcf));
     evalin('caller','Cursor_Object.removeAllDataCursors()');
               
     set(source,'Enable','off','Checked','on');
     set(source.Parent.Children(3),'Enable','off','Checked','off');
        
 end

 uimenu(Datacursor,'Label','Datacursormode disabled','Callback',@Datacursormode_Off);
 % uimenu << Datacursormode off >>

 function Datacursormode_Off(source,~)
        
     dcm_obj = datacursormode(gcf); % Open datacursor settings
     set(dcm_obj,'Enable','off'); % Disable datacursormode
     set(source,'Enable','off','Checked','on'); % Toggle checked state to off
          
     if ~isequal(get(source.Parent.Children(2),'Checked'),'on')
     
        set(source.Parent.Children(2),'Enable','on','Checked','off'); % Enable remaining uimenu
        
     end
     
     set(source.Parent.Children(3),'Enable','off','Checked','off'); % Enable remaining uimenu  
     set(source.Parent.Children(4),'Enable','on','Checked','off'); % Enable remaining uimenu
     
     
 end

%% Contextmenu 4 = Subsystem Settings ( surface plots )

 Subsystem_Settings = uimenu(Fig,'Label','Subsystem Settings'); 
 % Create UIContextmenu with label << Subsystem Settings >>
 
 Radius_Range = uimenu(Subsystem_Settings,'Label','Radius range');
 set(Radius_Range,'Enable','on','Callback',@Radius_Scale);
 % uimenu << Subsystems - Radius Range >>  
 
 Time_Range = uimenu(Subsystem_Settings,'Label','Time range'); 
 set(Time_Range,'Enable','on','Separator','on','Callback',@Time_Scale);
 % uimenu << Subsystems - Time Range >>  


 %% Contextmenu 5 = Lineplot

 Lineplot = uimenu(Fig,'Label','Lineplot'); % Create UIContextmenu with label << Lineplot >>

    uimenu(Lineplot,'Label','Draw a new line','Callback',@Line_Plot); 
    % uimenu << Draw a new line >>  
    uimenu(Lineplot,'Label','Erase existing line','Callback',@Erase_Line_Plot);
    % uimenu << Erase existing line >>

 %% Contextmenu 6 = Surface Properties    

 Surface_Properties = uimenu(Fig,'Label','Surface Properties'); 
 % Create UIContextmenu with label << Surface Properties >>

    Radiation = uimenu(Surface_Properties,'Label','Thermal heat radiation'); 
    % uimenu << Thermal heat radiation >>   
    
        Radiation_Settings = uimenu(Radiation,'Label','Radiation settings');
        set(Radiation_Settings,'Enable','on','Callback',@Radiation_Menu);
        % create an uimenu << Show radiation heat >> 
            
        uimenu(Radiation,'Label','Show radiation heat','Enable','off','Callback',@Radiation_Heat);
        % create an uimenu << Show radiation heat >> 
    
    Desorption = uimenu(Surface_Properties,'Label','Thermal induced desorption');
    set(Desorption,'Separator','on');
    % uimenu << Thermal induced desorption >> 
        
        Desorption_Settings = uimenu(Desorption,'Label','Desorption settings');
        set(Desorption_Settings,'Enable','on','Callback',@Desorption_Menu);
        % create an uimenu << Show desorption heat >> 
            
        Desorption_Heat = uimenu(Desorption,'Label','Show desorption heat');
        set(Desorption_Heat,'Enable','off','Callback',@Desorption_Yields);
        % create an uimenu << Show desorption heat >> 
             
        Desorption_Yield = uimenu(Desorption,'Label','Calculate desorption yields'); 
        set(Desorption_Yield,'Enable','off','Callback',@Desorption_Yields);
        % create an uimenu << Calculate desorption yields >> 
        
        
 %% Contextmenu 7 = Video Sequence    

 uimenu(Fig,'Label','Video Sequence','Callback',@Sequence); 
 % Create UIContextmenu with label << Video Sequence >>


 %% Contextmenu 8 = Save

 Save_Menu = uimenu(Fig,'Label','Save'); % Create UIContextmenu with label << Save >>

    uimenu(Save_Menu,'Label','Electronic Subsystem     ( surface plot )', ...
        'Tag','Electronic Subsystem','Callback',@save_Callback);   
    uimenu(Save_Menu,'Label','Atomic Subsystem         ( surface plot )', ...
        'Tag','Atomic Subsystem','Callback',@save_Callback);
    uimenu(Save_Menu,'Label','Temperature Profile           ( line plot )', ...
        'Tag','Temperature Profile','Callback',@save_Callback);
    uimenu(Save_Menu,'Label','Simulation Interface             ( figure )', ...
        'Tag','Simulation Interface','Callback',@save_Callback);
    % uimenus << Save subsystems, Lineplot or Simulation Interface  >>
                
 function save_Callback(source,~)       
        
     assignin('base','Save_Source',source); % Assign activated uimenu in workspace
        
     Screen_Size = get(0,'ScreenSize'); % Get current screen size from root object
     Screen_x = Screen_Size(3); % Obtain horizontal screen size
     Screen_y = Screen_Size(4); % Obtain vertical screen size
        
     set(gcf,'Units','pixels'); % Set current figure axes units to pixels
     
     if isequal(source.Tag,'Simulation Interface') 
     % Find out if source is simulation interface
         
         pos = get(gcf,'OuterPosition'); % Get current figure position
         rect = [ -5 , -5 , 0.995 * pos(3) , 0.990 * pos(4)]; 
         % Define a rectangle that cover up the complete screen
            
         set(gcf,'Units','pixels'); % Set current figure axes units to pixels
                    
     else % Source variable is not equal to simulation interface         
                  
         switch source.Tag

             case 'Electronic Subsystem' % Save electronic subsystem

                 save_ax = evalin('base','ax1'); 
                 % Get axes handle of first subplot and assign to save axes                
                 factor_x = 0.040; % Scaling factor in horizontal direction
                 factor_y = 0.080; % Scaling factor in vertical direction

             case 'Atomic Subsystem' % Save atomic subsystem

                 save_ax = evalin('base','ax2'); 
                 % Get axes handle of second subplot and assign to save axes                        
                 factor_x = 0.040; % Scaling factor in horizontal direction
                 factor_y = 0.080; % Scaling factor in vertical direction

            case 'Temperature Profile' % Save temperature line plot

                 save_ax = evalin('base','ax4'); 
                 % Get axes handle of third subplot and assign to save axes
                 factor_x = 0.016; % Scaling factor in horizontal direction
                 factor_y = 0.080; % Scaling factor in vertical direction              

         end
        
         save_ax.Units = 'pixels'; % Set position units of axes handle to pixels
         pos = save_ax.Position; % Get save axes position ( 1 x 4 dimensional vector )
            
         marg_x = ceil( factor_x * Screen_x ); 
         % Marginally enlarge save section in horizontal direction
         marg_y = ceil( factor_y * Screen_y );
         % Marginally enlarge save section in vertical direction
 
         rect = [ pos(1) - 1.1 * marg_x, pos(2) - 1.0 * marg_y, ...
                  pos(3) + 3.0 * marg_x, pos(4) + 2.0 * marg_y ];
         % Define a rectangle that cover up the complete subsystem ( including axis labels )

     end              
         
     F = getframe(gcf,rect); 
     % Get a frame of main simulation interface with respect to created rectangle

     [im,map] = frame2im(F); % Convert the frame into a binary image and a colormap
                        
     [file, path] = uiputfile({'*.png';'*.jpg';'*.bmp';'*.pgm';'*.*'},'Save as');
     % Read the filename and respective path chosen by the user
     
     if all([~isequal(file,0),~isequal(path,0)]) % User skip save process
        
        if isempty(map) % If screenshot does not contain any colormap data
            
            imwrite(im,fullfile(path,file)); % Save screenshot according to binary image data
            
        else
            
            imwrite(im,map,fullfile(path,file)); % Save screenshot according to both image data
            
        end
        
     end
        
     save_ax.Units = 'normalized'; % Reset save axes units to normalized        
                
 end

 set(findall(gcf,'Type','uimenu','-property','Enable'),'Enable','off');
 % Generally disable all kind of uimenu

 set(findobj(gcf,'Label','Add Item'),'Enable','on');
 set(findobj(gcf,'Label','Substitute Coefficients'),'Enable','on');
 set(findobj(gcf,'Label','Waligorski Distribution'),'Enable','on');
 set(findobj(gcf,'Label','Atomic energy distribution'),'Enable','on');
 set(findobj(gcf,'Label','Electron thermal diffusivity'),'Enable','on');
 set(findobj(gcf,'Label','Surface Properties'),'Enable','on');
 set(findobj(gcf,'Label','Thermal heat radiation'),'Enable','on');
 set(findobj(gcf,'Label','Radiation settings'),'Enable','on');
 set(findobj(gcf,'Label','Thermal induced desorption'),'Enable','on');
 set(findobj(gcf,'Label','Desorption settings'),'Enable','on');
 set(findobj(gcf,'Label','Subsystem Settings'),'Enable','on');
 set(findobj(gcf,'Label','Radius range'),'Enable','on');
 set(findall(gcf,'Label','Time range'),'Enable','on');
 % Enable specific uimenus that shall be available before starting computation process

 Thermal_Properties = evalin('base','Thermal_Properties');
 Material_Selection = evalin('base','Material_Selection');

 if ~isequal(Thermal_Properties.(Material_Selection).('Type'),'Metal')

     set(findobj(gcf,'Label','Electron-Phonon mean free path'),'Enable','on');
    
 end

end