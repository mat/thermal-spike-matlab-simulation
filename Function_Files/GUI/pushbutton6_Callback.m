function pushbutton6_Callback(obj,~)
% User defined function to visualize matrix data of any selected PDE coefficient by creating a
% user interface table. Furthermore a brief overview ( min, median & max ) of repsective data 
% will be drawn in an additional user interface table. At least the user is able to request a 
% surface plot regarding a selected coefficients data.

warning('off'); % Disactivate all kind of warning messages

set(obj,'Enable','off'); % Disable << PDE coefficients >> pushbutton 

drawnow; % Refresh calculation routine

Fig = evalin('base','Fig'); % Evaluate handle to main simulation window in workspace

Fig.Pointer = 'watch'; % Set mouse pointer to watch symbol

drawnow; % Refresh calculation routine

%% Create user interface figure and first button group

ui_fig = uifigure('Name','Inspection Board','Position',[100 250 1050 420],'Resize','off');
set(ui_fig,'CloseRequestFcn',@closereq); 
% Create a user interface figure that will contain a coefficients table

bg_1 = uibuttongroup(ui_fig,'Position',[ 10 10 140 400],'SelectionChangedFcn',@bg_1_Selection);
set(bg_1,'Title','Subsystem','TitlePosition','centertop','FontSize',13);
% Define a primary section user interface button group 

assignin('base','bg_1',bg_1); % Assgin handle to primary button group in workspace

Radial_Mesh = 10^(9) * evalin('base','Radial_Mesh_1');
Time_Mesh = evalin('base','Time_Mesh_1');

    %% Electronic subsystem button groups

    tb_1 = uitogglebutton(bg_1,'Position',[10 330 120 30]);
    tb_1.Text = 'Electronic';
    tb_1.Tag = 'Electronic';
    % Add first toggle button << electronic subsystem >> to user interface button group

        sub_bg_1 =uibuttongroup(ui_fig,'Position',[160 10 140 400]);
        set(sub_bg_1,'Title','Electronic','TitlePosition','centertop','FontSize',13);
        set(sub_bg_1,'Visible','off','SelectionChangedFcn',@sub_bg_Selection);
        % Subordinate a button group to << electronic subsystem >> that will open while the
        % electronic toggle button becomes active.
        
            tb_1_1 = uitogglebutton(sub_bg_1,'Position',[10 330 120 30]);
            tb_1_1.Text = 'Heat Capacity';
            tb_1_1.Tag = 'J / ( cm� K ),\;\;C_e\;\;\left(\frac{J}{cm^3\cdot K}\right)';
            tb_1_2 = uitogglebutton(sub_bg_1,'Position',[10 290 120 30]);
            tb_1_2.Text = 'Heat Conductivity'; 
            tb_1_2.Tag = 'W / ( cm K ),\;\;K_e\;\;\left(\frac{W}{cm\cdot K}\right)';
            tb_1_3 = uitogglebutton(sub_bg_1,'Position',[10 250 120 30]);
            tb_1_3.Text = 'Energy Density';
            tb_1_3.Tag = 'W / cm�,\;\;A\left(r,t\right)\;\;\left(\frac{W}{cm^3}\right)';
            tb_1_4 = uitogglebutton(sub_bg_1,'Position',[10 210 120 30]);
            tb_1_4.Text = 'Coupling Factor';
            tb_1_4.Tag = 'W / ( cm� K ),\;\;g\;\;\left(\frac{W}{cm^3\cdot K}\right)';
            tb_1_5 = uitogglebutton(sub_bg_1,'Position',[10 170 120 30]);
            tb_1_5.Text = 'Thermal Diffusivity';
            tb_1_5.Tag = 'cm� / s,\;\;D_e\;\;\left(\frac{cm^2}{s}\right)';
            % Create toggle buttons in subordinated electronic subsystem button group
            % with button text according to PDE coefficients under inspection. 
   
    %% Atomic subsystem button groups        
            
    tb_2 = uitogglebutton(bg_1,'Position',[10 290 120 30]);
    tb_2.Text = 'Atomic';
    tb_2.Tag = 'Atomic';
    % Add second toggle button << atomic subsystem >> to user interface button group
    
        sub_bg_2 =uibuttongroup(ui_fig,'Position',[160 10 140 400]);
        set(sub_bg_2,'Title','Atomic','TitlePosition','centertop','FontSize',13);
        set(sub_bg_2,'Visible','off','SelectionChangedFcn',@sub_bg_Selection);
        % Subordinate a button group to << atomic subsystem >> that will open while the
        % atomic toggle button becomes active.
        
            tb_2_1 = uitogglebutton(sub_bg_2,'Position',[10 330 120 30]);
            tb_2_1.Text = 'Heat Capacity';
            tb_2_1.Tag = 'J / ( cm� K ),\;\;C_a\;\;\left(\frac{J}{cm^3\cdot K}\right)';
            tb_2_2 = uitogglebutton(sub_bg_2,'Position',[10 290 120 30]);
            tb_2_2.Text = 'Heat Conductivity';
            tb_2_2.Tag = 'W / ( cm K ),\;\;K_a\;\;\left(\frac{W}{cm\cdot K}\right)';
            tb_2_3 = uitogglebutton(sub_bg_2,'Position',[10 250 120 30]);
            tb_2_3.Text = 'Energy Density';
            tb_2_3.Tag = 'W / cm�,\;\;B\left(r,t\right)\;\;\left(\frac{W}{cm^3}\right)';
            tb_2_4 = uitogglebutton(sub_bg_2,'Position',[10 210 120 30]);
            tb_2_4.Text = 'Coupling Factor';
            tb_2_4.Tag = 'W / ( cm� K ),\;\;g\;\;\left(\frac{W}{cm^3\cdot K}\right)';
            % Create toggle buttons in subordinated electronic subsystem button group
            % with button text according to PDE coefficients under inspection.          
 
 %% First button group selection           
            
 function bg_1_Selection(~,~)
 % Detect active toggle button

     toggle_1 = bg_1.SelectedObject; % Get handle to selected toggle button 
     assignin('base','toggle_1',toggle_1); % Assign handle of toggle button in workspace

     switch toggle_1.Tag % Distinguish two different subsystems

         case 'Electronic' % Electronic toggle button is active

              set(sub_bg_1,'Visible','on'); % Set subordinated electronic button group to visible
              set(sub_bg_2,'Visible','off'); % Set subordinated atomic button group to invisible
              
              Temperature = evalin('base','Electronic_Temperature'); % Read electronic temperatures
              Radial_Mesh = 10^(9) * evalin('base','Radial_Mesh_1');
              Time_Mesh = evalin('base','Time_Mesh_1');
              
              formatSpec = string('T = %.1e %s'); % Set string to exponential format
              
         otherwise % Atomic toggle button is active

              set(sub_bg_2,'Visible','on'); % Set subordinated atomic button group to visible
              set(sub_bg_1,'Visible','off'); % Set subordinated electronic button group to invisible
             
              Temperature = evalin('base','Lattice_Temperature'); % Read lattice temperatures

              Radial_Mesh = 10^(9) * evalin('base','Radial_Mesh_2');
              Time_Mesh = evalin('base','Time_Mesh_2');
              
              formatSpec = string('T = %.1f %s'); % Set string to exponential format
              
     end
     
     columnname_1 = arrayfun(@(x) sprintf('%.2f\n',x),Radial_Mesh,'un',0);
     rowname_1 = arrayfun(@(x) sprintf('%.2e\n',x),Time_Mesh,'un',0);
     columnformat_1 = repmat({'char'}, 1, length(Radial_Mesh));
             
     tab_1.Data = [];
     
     set(Temp_panel,'Visible','off');
     set(Temp_label,'Visible','off');
     set(label,'Visible','off');
              
     sub_bg_Selection(); % Request subordinated button group selection
     
     if evalin('base','exist(''selected_cell'',''var'')')
         
        selected_cell = evalin('base','selected_cell');
        row = selected_cell.Indices(1);
        col = selected_cell.Indices(2);
        
        try
        
            expr_1 = Temperature(row,col); % Define a string with temperature data
            
        catch
            
            expr_1 = Temperature(1,1);
            
        end
        
        expr_2 = 'K'; % Define a string expression with temperatures units [K]
        Temp_Data = char(sprintf(formatSpec,expr_1,expr_2)); 
        % Print the complete string as a character variable
        label.Text = Temp_Data; % Assign temperature string to the text label
        
     end
     
        Temp_label.Text = strcat(toggle_1.Tag,' Temperature');

 end

 %% Determine PDE coefficients data

 [C_e, D_e, K_e, C_a, K_a, g, A, B] = Coefficients_Calculation(); 
 % Calculate PDE coefficients = dim ( radius x times ) 

 tb_1_1.UserData = arrayfun(@(x) sprintf('%.2f\n',x),10^(-6) * C_e,'un',0); % Units:  J/(cm^3 K)
 tb_1_2.UserData = arrayfun(@(x) sprintf('%.2f\n',x),10^(-2) * K_e,'un',0); % Units:  W/(cm K)
 tb_1_3.UserData = arrayfun(@(x) sprintf('%.2e\n',x),10^(-6) * A,'un',0);   % Units:  W/cm^3
 tb_1_4.UserData = arrayfun(@(x) sprintf('%.2e\n',x),10^(-6) * g,'un',0);   % Units:  W/(cm^3 K)
 tb_1_5.UserData = arrayfun(@(x) sprintf('%.2f\n',x),10^(4) * D_e,'un',0);  % Units:  cm^2/s
 % Assign electronic subsystem coefficients data to respective toggle buttons
          
 tb_2_1.UserData = arrayfun(@(x) sprintf('%.2f\n',x),10^(-6) * C_a,'un',0); % Units:  J/(cm^3 K)
 tb_2_2.UserData = arrayfun(@(x) sprintf('%.2f\n',x),10^(-2) * K_a,'un',0); % Units:  W/(cm K)
 tb_2_3.UserData = arrayfun(@(x) sprintf('%.2e\n',x),10^(-6) * B,'un',0);   % Units:  W/cm^3
 tb_2_4.UserData = arrayfun(@(x) sprintf('%.2e\n',x),10^(-6) * g,'un',0);   % Units:  W/(cm^3 K)
 % Assign atomic subsystem coefficients data to respective toggle buttons

 %% Create a main user interface table subordinated to a second button group

 bg_2 = uibuttongroup(ui_fig,'Position',[310 10 730 400]);
 set(bg_2,'TitlePosition','centertop','FontSize',13,'Visible','off');
 % Create a second button group that will contain user interface table 

 columnname_1 = arrayfun(@(x) sprintf('%.2f\n',x),Radial_Mesh,'un',0);
 rowname_1 = arrayfun(@(x) sprintf('%.2e\n',x),Time_Mesh,'un',0);
 columnformat_1 = repmat({'char'}, 1, length(Radial_Mesh));
 tab_1 = uitable(bg_2,'Position',[10 10 555 360],'ColumnFormat',columnformat_1,'Visible','on',...
    'CellSelectionCallback',@Cell_selection_callback,'FontName','Cambria','FontSize',12,...
    'FontWeight','bold','ForegroundColor',[0.15 0.15 0.15]);
 % Create a user table with radius columns and time rows subordinated to second button group 
   
 function Cell_selection_callback(~,event_data)
 % A nested user defined function that displays the respectively subsystem 
 % temperature value to a marked uitable cell ( cell = row and colum value)             
  
  assignin('base','selected_cell',event_data);
  row = event_data.Indices(1); % Get the row index of the present cell
  col = event_data.Indices(2); % Get the column index of the present cell 
  toggle_1 = evalin('base','toggle_1'); % Get handle to first toggle button 
  
  if isequal(toggle_1.Text,'Electronic') % Check if electronic subsystem is chosen
      
      formatSpec = string('T = %.1e %s'); % Set string to exponential format
      Temperature = evalin('base','Electronic_Temperature'); % Read electronic temperatures
      label.FontSize = 11.5; % Set text fontsize of uilabel
      
  else % Atomic subsystem is present
  
      formatSpec = string('T = %.1f %s'); % Set string to decimal format
      Temperature = evalin('base','Lattice_Temperature'); % Read atomic temperatures
      label.FontSize = 11.5; % Set text fontsize of uilabel     
           
  end
  
  expr_1 = Temperature(row,col); % Define a string with temperature data
  expr_2 = 'K'; % Define a string expression with temperatures units [K]
  Temp_Data = char(sprintf(formatSpec,expr_1,expr_2)); 
  % Print the complete string as a character variable
  
% Pos = get(0,'PointerLocation'); % Get current mouse pointer position
  label.Visible = 'on'; % Set the temperature value to visible
  label.Text = Temp_Data; % Assign temperature string to the text label
  Temp_label.Visible = 'on'; % Set the uilabel to visible
  Temp_panel.Visible = 'on';
        
 end

 txt_1 = uilabel(bg_2,'Text','Radius r [nm]','Position',[18 353 100 15],'FontSize',12);
 set(txt_1,'FontColor',[0.50 0.50 0.50]); % Add a label to visualize radius columns of uitable

 txt_2 = uilabel(bg_2,'Text','Time t [s]','Position',[30 13 100 15],'FontSize',12);
 set(txt_2,'FontColor',[0.50 0.50 0.50]); % Add a label to visualize time rows of uitable
  
 txt_3 = uilabel(bg_2,'Text','User information','Position',[600 87 130 20],'FontSize',12);
 set(txt_3,'FontColor',[0.30 0.30 0.30]); % Add a label to display user information

 %% Display subsystem temperatures ( Information on coefficients table cells )
 
 uipanel(bg_2,'Title','','Position',[575 250 145 150]);
 Temp_panel = uipanel(bg_2,'Title','','Position',[575 190 145 70],...
     'BackgroundColor',[0.97 0.97 0.97],'Visible','off');
  
 Temp_label = uilabel(Temp_panel,'Text','','Visible','off');
 Temp_label.Position = [5 32 130 25]; 
 % Set uilabel position with respect to second button group
 Temp_label.BackgroundColor = 'none';
 Temp_label.FontColor = [0.30 0.30 0.30];
 Temp_label.FontSize = 11.5;
 Temp_label.HorizontalAlignment = 'center';
 Temp_label.VerticalAlignment = 'center';
 
 label = uilabel(Temp_panel,'Text','','Visible','off');
 label.Position = [5 7 130 25];
 % Set uilabel position with respect to second button group
 label.BackgroundColor = 'none';
 label.FontColor = 'b';
 label.HorizontalAlignment = 'center';
 label.VerticalAlignment = 'center';
 % Set meaningful user interface label  properties ( to display subsystem temperatures )
  
 button_1 = uibutton(bg_2,'Position',[590 285 115 70],'ButtonPushedFcn',@plot_button_pushed);
 set(button_1,'HorizontalAlignment','center','VerticalAlignment','center','BackgroundColor','w');
 set(button_1,'icon','Matlab_Icon.jpg','IconAlignment','left','Text',strcat('Plot',32,32,32,''),...
     'FontName','Cambria','FontWeight','bold','FontColor',[0.20 0.20 0.20]);
 % Define a pushbutton to plot present coefficients data

 
 %% Visualize coefficients data ( surface plot )
 
 function plot_button_pushed(~,~) 
 % Create a surface plot of present coefficient data
     
     fig = figure; % Open a new undocked figure
     set(fig,'NumberTitle','off','Name','','Color','w','Tag','Figure 2','Toolbar','none');
     set(fig,'Units','normalized','Position',[0.15 0.10 0.60 0.75],'Menubar','none');
     
     Zoom = uimenu(fig,'Label','Zoom');
     
        uimenu(Zoom,'Label','Zoom - on','Enable','on','Callback',@Zoom_On_Callback);
        uimenu(Zoom,'Label','Zoom - off','Enable','off','Callback',@Zoom_Off_Callback);
     
     function Zoom_On_Callback(source,~)
         
        hz = zoom(gcf); % Get handle to zoom functionality
        set(hz,'Enable','on','Direction','in','RightClickAction','InverseZoom');
        % Activate zoom function in current figure  
        set(source.Parent.Children(1),'Enable','on','Checked','off');
        set(source,'Enable','off','Checked','on'); % Toggle source to checked state 
        
        Rotate.Enable = 'off';
        
     end
     
     function Zoom_Off_Callback(source,~)       
            
        hz = zoom(gcf);  % Get handle to zoom functionality        
        set(hz,'Enable','off'); % Disactivate zoom function in current figure
        set(source.Parent.Children(2),'Enable','on','Checked','off');
        set(source,'Enable','off','Checked','on');  % Toggle source to checked state 
                
        Rotate.Enable = 'on';
        
     end
     
     Rotate = uimenu(fig,'Label','Rotate 3D');
     
        uimenu(Rotate,'Label','Enable Rotation','Enable','on','Callback',@Rotation_On_Callback);
        uimenu(Rotate,'Label','Disable Rotation','Enable','off','Callback',@Rotation_Off_Callback);
     
    function Rotation_On_Callback(source,~)
         
        rot = rotate3d(fig);
        rot.Enable = 'on';
        % Activate 3D rotation function in current figure  
        set(source.Parent.Children(1),'Enable','on','Checked','off');
        set(source,'Enable','off','Checked','on'); % Toggle source to checked state 
        
        Zoom.Enable = 'off';
                
     end
     
     function Rotation_Off_Callback(source,~)       
            
        rot = rotate3d(fig);
        rot.Enable = 'off';
        set(source.Parent.Children(2),'Enable','on','Checked','off');
        set(source,'Enable','off','Checked','on');  % Toggle source to checked state 
             
        Zoom.Enable = 'on';
                
     end
     
     uimenu(fig,'Label','Save','Callback',@Save_Callback); 
     
     function Save_Callback(~,~)
         
         set(View,'Visible','off');
         drawnow;
         
         [FileName,PathName] = uiputfile({'*.jpg';'*.png'},'Save figure as');
         
         if all([~isequal(FileName,0),~isequal(PathName,0)])
             
            filename = fullfile(PathName,FileName);
            saveas(fig,filename);
            
         end
         
         set(View,'Visible','on');
         
     end            
        
     View = uicontrol(fig,'Style','pushbutton','String','2D - View','Units','normalized', ...
         'Position',[0.87 0.03 0.11 0.06],'Callback',@view_Callback); 
     % Create a << hot button >> to change surface plot view from two dimensions to three dimensions
      
     axes(fig,'Units','normalized','Position',[0.15 0.15 0.65 0.75]);
     
     function view_Callback(obj,~)
     % Change surface plot view ( 2dim <--> 3dim )
     
         switch obj.String % Distinguish current plot button text
             
             case '2D - View' % Two dimensional view is requested
                 
                obj.String = '3D - View'; % Change plot button text to remaining option
                view([0 90]); % Set surface plot view to color values of a radius over time plain           
                
             case '3D - View' % Three dimensional view is requested
                 
                obj.String = '2D - View'; % Change plot button text to remaining option
                view([-45 25]); % Set surface plot view to three dimension settings
                                
         end
         
     end
          
     [radius,time] = meshgrid(Radial_Mesh,Time_Mesh);
     % Generate a meshgrid of respective radius and time values
     data = cellfun(@str2num, tab_1.Data); % Convert table data characters into numeric format
     
     toggle_2 = evalin('base','toggle_2');     
     str = char(strcat(bg_1.SelectedObject.Text,{' '},toggle_2.Text));
     title_name = {str,''};
     % Set title name to present coefficient label
     title(gca,title_name,'Interpreter','latex','FontSize',12);
     
     xlabel(gca,'Radius: $$\;r\;\left(nm\right)$$','Interpreter','latex','FontSize',12); % Horizontal axis label
     ylabel(gca,'Time: $$\;t\;\left(s\right)$$','Interpreter','latex','FontSize',12); % Vertical axis label
     
     toggle_2 = evalin('base','toggle_2'); % Evaluate handle of second toggle button in workspace
     z_str = toggle_2.Tag(strfind(toggle_2.Tag,',')+1:end); % Extract toggle button text string
     z_label = strcat(toggle_2.Text,':','$$',z_str,'$$');
     % Assign extracted toggle button text string to surface plot out of plain axis label 
     assignin('base','z_label',z_label); % Assign previous axis label in workspace
     zlabel(gca,{'';z_label;''},'Interpreter','latex','FontSize',12); % Use latex interpreter to display axis label
     
     set(gca,'XScale','linear','YScale','log'); % Define scales of current x-y axis
     view(gca,[-45,25]); % Apply almost diagonal view on surface plot 
     box(gca,'on'); % Draw a solid line around surface plot axis
     hold(gca,'on'); % Hold current axes settings
     
     cmap = str2func('parula'); % Use default colormap to display color data
     colormap(gca,cmap(128)); % Set a 128 Bit sized colormap 
     cb = colorbar(gca,'TickLabelInterpreter','latex','FontSize',10); % Display a colorbar
     cb_pos = get(cb,'Position'); % Get temporary position of electronic colorbar
     cb.Position = cb_pos + [ 0.15, 0.03, -0.01, -0.10];
     cb.Label.Interpreter = 'latex';
     cb.Label.FontSize = 11.5;
     cb.Label.String = strcat('$$',z_str,'$$');
     cb.Label.Rotation = 0;
     cb.Label.Units = 'normalized'; % Set colorbar label units to normalized
     cb.Label.Position = [cb_pos(1) - 0.5, cb_pos(2) + cb_pos(4) + 0.28, 0];
     cb.TickLength = 0.050; % Set length of the horizontal colorbar ticks
     
     data_min = min(min(data)); % Find the minimum value of current table data
     data_max = max(max(data)); % Find the maximum value of current table data
         
     if data_min <= 0 % Detect minimum value less or equal to zero
     
         diff = log ( data_max ); 
         % Set the logaritmic difference according to the maximum value of data
        
     else % Detect minimum value being greater than zero
         
         diff = log( data_max ) - log( data_min );
         % Set the logaritmic difference according to minimum and maximum data values
         
     end
     
     if all([any([isequal(data_min,0), diff < 3]),data_max < 1/eps])
     % Find any zeros of coefficients data or log. differences less than a given value
            
         surf(radius,time,data); 
         % Create a surface plot of coefficient data in current figure 
         
         hold(gca,'on');
         
         scale_num = 9;         
         
         if isequal(data_min,data_max)
             
             contours = linspace(data_min,data_max + 1,scale_num);
             
         else
             
             contours = linspace(data_min,data_max,scale_num);
             
         end
         
         contour3(gca,radius,time,data,contours,'k')
         
         colormap(gca,parula(scale_num - 1));         
         
         cb.Ticks = contours; % Set logaritmic spaced colorbar steps
         
         xstr = num2str(min(contours), '%32.16g');
         floating = length(xstr) - find(xstr == '.');
         
         if all([isempty(floating),~isequal(data_min,data_max)])
             
             cb.TickLabels = sprintf('%.2e\n',contours);
             
         elseif isequal(data_min,data_max)
             
             cb.TickLabels = sprintf('%.2f\n',contours);
             
         else
             
             cb.TickLabels = sprintf(strcat('%.',num2str(floating),'f\n'),contours);
                          
         end
                        
         caxis(gca,[min(contours) max(contours)]); 
         
         axis(gca,'tight'); % Set plot axis to match limitations
            
         if isequal(data_min,0)
             
              z_limits = get(gca,'ZLim');         
              set(gca,'ZLim',[0, z_limits(2)]);
              set(gca,'CLim',[0, z_limits(2)]);
              
         end
         
         
     else % No zero or below values found in coefficients data
                  
         log_min = min(data(data ~= 0));
         data( data == 0 ) = log_min;
         data_min = min(min(data)); 
         data_max = max(max(data));
         
         Log_Plot = surf(radius,time,log10(data)); 
         % Create a surface plot of coefficient data in current figure
         
         hold(gca,'on');
         
         scale_num = 9; % Number of contour lines 
                 
         if all([data_min < eps, data_max > eps])
             
             data( data < eps*data_max ) = eps*data_max;
             
             scale = LogSpace([eps*data_max,data_max],scale_num);
             cb.Ticks = log10(scale); % Set logaritmic spaced colorbar steps 
             cb.TickLabels = scale;
             Log_Plot.ZData = log10(data);
             caxis(gca,log10([eps*data_max data_max]));
             
             assignin('base','cb',cb);
             Labels = str2num(cb.TickLabels); %#ok<ST2NM>
             Labels(1) = 0;  
             cb.TickLabels = {'0',sprintf('%.1e\n',Labels(2:end))};
             
         else
         
            scale = LogSpace([data_min,data_max],scale_num);
            cb.Ticks = log10(scale); % Set logaritmic spaced colorbar steps 
            caxis(gca,log10([scale(1) scale(end)])); 
            
            scale_labels = arrayfun(@(x) sprintf('%.2e\n',x),scale,'un',0);
            scale_labels = round(str2double(scale_labels),2,'significant');
            % Define logarithmic color axis of given surface plot
            set(gca,'ZTickLabel',scale_labels); 
            cb.TickLabels = scale_labels; % Set the manual colorbar labels
            
         end
         
         contour3(gca,radius,time,log10(data),log10(scale),'k')
              
         colormap(gca,parula(scale_num-1));  
                          
         axis(gca,'tight'); % Set plot axis to match limitations
                           
     end
     
     shading(gca,'interp'); % Interpolate axes shading
     set(gca,'Layer','top'); % Bring current axes layer to the top
     grid(gca,'on'); % Draw a grid with respect to surface plot data               
     hold(gca,'off');
     
     drawnow;
     
%      evalin('base','clc');
     
 end

 %% Create a second user interface table with coefficients data overview

 tab_2 = uitable(bg_2,'Position',[590 10 121 75],'Visible','off'); % Define an empty uitable
 columnname_2 = {'Value'}; % Set user interface table column name ( just one column )
 rowname_2 = {'Min','Mean','Max'}; % Set user interface table row names

 %% Assign meaningful coefficients data to both user interface tables

 function sub_bg_Selection(~,~)
 % Generate meaningful coefficients table data

      switch sub_bg_1.Visible % Distinguish between electronic and atomic buttongroup
     
          case 'on' % Electronic subsystem buttongroup is active
              
              toggle_2 = sub_bg_1.SelectedObject; 
              % Assign selected button of electronic subsystem group to toggle button handle
                             
          otherwise % Atomic subsystem buttongroup is active
          
              toggle_2 = sub_bg_2.SelectedObject;
              % Assign selected button of atomic subsystem group to toggle button handle
                  
      end

      assignin('base','toggle_2',toggle_2); % Transfer toggle button handle to workspace
      
      title = strcat(toggle_2.Text,32,32,32,32,'(',32,...
          toggle_2.Tag(1:strfind(toggle_2.Tag,',')-1),32,')');
      set(bg_2,'Title',title,'Visible','on'); 
      % Set second button group title to current toggle button text
        
      set(tab_1,'ColumnName',columnname_1','RowName',rowname_1','ColumnFormat',columnformat_1); 
      % Set row names and column names of coefficients user interface table
      
      set(tab_1,'Data',toggle_2.UserData,'Visible','on'); 
      % Assign toggle button user data to coefficients table data        
                 
      assignin('base','tab_1',tab_1); % Assign handle to ceofficients table in workspace
                  
      data = cellfun(@str2num, tab_1.Data); % Convert coefficients table data into numeric format           
      data_2 = {min(min(data)),mean(mean(data)),max(max(data))};
      % Determine minmum, median and maximum value of coefficients data and assign it to a cell variable
      dat = cell2mat(data_2); % Create a numeric matrix from cell data of second user interface table
      
      for i = 1 : 1 : length(dat) % Iterate amount of parameters
          
            if dat(i) > 1000 || dat(i) < 0.01 % Data value exceeds one of the thresholds
          
                data_2{i} = sprintf('%.2e\n',dat(i)); % Use exponetiell format to display value                           
                            
            else % Data value does not exceed threshold
          
                data_2{i} = sprintf('%.2f\n',dat(i)); % Use floating point format to display value
        
            end
            
            if isequal(dat(i),0) % Data value is equal to zero
                
                data_2{i} = sprintf('%g\n',dat(i)); % Use integer format to display value
                
            end
        
      end      
            
      set(tab_2,'Data',data_2','RowName',rowname_2','Visible','on');       
      set(tab_2,'ColumnName',columnname_2);
      % Set row names and column names of overview user interface table 
      
 end

 bg_1_Selection(); % Run button group selection function
 
 pause(1);
 drawnow;
 
 Fig.Pointer = 'arrow';
 
 drawnow;
 
end

function closereq(source,~)
% Close request function to display a question dialog box
    
 selection = questdlg('Close the < Inspection Board > window?', ...
                      'Close Request','Yes','No','Yes');
 % close request text message and possible answers

 switch selection % React on answer of question dialog
     
     case 'Yes' % User pressed << yes >> button
         
         delete(source); % Delete GUI figure
         evalin('caller','clc');  % Clear workspace screen
         set(evalin('base','Button_6'),'Enable','on'); 
         % Enable << PDE Coefficients >> pushbutton in main simulation interface
         
         warning('on'); % Acitvate warning messages
         
     case 'No' % User pressed << no >> button
         
         return % Return to GUI
 end
 
end
