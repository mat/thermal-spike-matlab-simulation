function Factor = Time_Scaling_Factor_Calculation(t_0,sigma_t)
% User defined function to determine a subsystem energy input scaling factor due to finite
% energy losses and normalized continuos distribution in time.

%% Symbolic solution of the integral expression for subsystem input energy density calculation

syms t
% Create sysmbolic variables for time values

Time_Expression = exp( - ( 1 / 2 ) * ( ( t - t_0 ) / sigma_t )^2 ); 
% Time distribution function of delta electrons (not normalized Gaussian Distribution). Unit: none

Factor = 1 / int( Time_Expression, t, 0, inf  );
Factor = double( Factor );
% Electronic energy density time scaling factor b to ensure normalized energy input. Unit: s^(-1)

end