function Line_Plot(source,~)
% This user defined function draws a line in any selected subsystem due to a
% setting of two decisively endpoints. Furthermore the creation of subordinated
% uimenu is needed ensuring the ability to change temperature line plot appearance.

set(source,'Checked','on','Enable','off'); 
% Set uimenu << Draw a new line >> to disabled state
set(source.Parent.Children(1),'Checked','off','Enable','on'); 
% Set uimenu <<Erase existing line >> to enabled state

Previous_Status = get(findall(gcf,'Type','uimenu')); 
% Find all available uimenus and save temporary settings to previous status variable

set(findall(gcf,'Type','uimenu','-property','Enable'),'Enable','off');
% Disbale all uimenu of current simulation figure ( avoid any kind of interaction )


%% Draw a line in any subsystem

colormaps = findall(gcf,'Type','uimenu','Label','Select Colormap');
children = colormaps.Children;
checked = get(findall(children,'-Property','Checked','Checked','on'),'Label');
% Find currently checked colormap and extract its label
    
try % Apply following routine since no errors occur

    switch checked 
    % Distinguish different sort of colormap labels and further define line plot colors

        case {'parula','jet','hsv'} % matching colormap labels

            draw_color = 'k'; % Define the color settings of the line plot
            colorname = 'black'; % Set a colorname accordingly

       case 'hot'

            draw_color = 'b'; 
            colorname = 'blue';

       case 'gray' 

           draw_color = 'r'; 
           colorname = 'red';

    end
    
catch % React on any malfunctionality by applying next section
    
    draw_color = 'k'; % Set line plot color to black
    colorname = 'black'; % Set a colorname accordingly
    
end

if evalin('base','~exist(''L'',''var'')') 
% Handle to subsystem line does not exist in workspace
    
   hlp = helpdlg(' Please select two datapoints of any subsystem.','Lineplot - Information'); 
   % Create a user help dialog regarding line endpoints information

    drawnow     % Necessary to print the message
    waitfor(hlp); % Resume after help dialog window is closed   
    
end        
 
u = zeros(1,2);     % Initialize horizontal coordinates of both line endpoints
v = zeros(size(u)); % Initialize vertical coordinates of both line endpoints

for i = 1 : 1 : 2 % Iterate the amount of line endpoints ( one line -> two endpoints )
        
    Flag = false; % Set subsystem detection flag initially to false
    
    while Flag == false % Apply the routine until the detection flag becomes true      
                    
        [u(i),v(i)] = ginput(1); % Read endpoint coordinates using graphical user input cursor
        
        if isequal(i,1) % Define current subsystem line axes while user selects first endpoint
            
            line_ax = gca; % Set handle of line axes to current axes
            
        else % Only active on second endpoint selection
            
            set(gcf,'CurrentAxes',line_ax); % Set current axes to previously defined line axes              
            
        end
        
        assignin('base','u',u);
        assignin('base','v',v);
        
        line_ax.Layer = 'top'; % Bring line axes layer to the top of the graphics
                
        x_limits = line_ax.XLim; % Extract line axes horizontal limitations
        y_limits = line_ax.YLim; % Extract line axes vertical limitations
        
        P = findobj(get(line_ax,'Children'),'Type','Surface'); % Find surface data of selected subsystem
        assignin('base','line_ax',line_ax); % Assign handle to line axes in workspace
        z_data = get(P,'ZData'); % Extract the temperature data of given surface plot
                        
        try
                
            if isequal(P.Tag,'Electronic Subsystem') 
            % Line plot target equals electronic subsystem
            
                z_plot = max(max(evalin('base','Electronic_Temperature'))) + eps;
                % Determine electronic temperatures slightly greater than a maximum value
            
            else % Line plot target equals atomic subsystem
            
                z_plot = max(max(evalin('base','Lattice_Temperature'))) + eps; 
                % Determine atomic temperatures slightly greater than a maximum value
               
                        
            end
            
        catch % Avoid any errors due to missmatching conditions
            
            z_plot = max(max(z_data)) + eps; % Determine a value slightly greater than plot data
            
        end
        
        if u(i) >= x_limits(1) && u(i) <= x_limits(2) && v(i) >= y_limits(1) && v(i) <= y_limits(2)
        % Detect line endpoint lies within subsystem borders
        
            assignin('base',strcat('LP_',num2str(i)),scatter3(line_ax,u(i),v(i),z_plot,'x'));
            set(evalin('base',strcat('LP_',num2str(i))),'MarkerEdgeColor',draw_color);
            set(evalin('base',strcat('LP_',num2str(i))),'LineWidth',2.0);
            % Plot line endpoint at selected position and set color matching colormap appearance
            Flag = true; % Set subsystem detection flag to true and thus exit the while condition
        
        else % Selected line endpoint exceed subsystem borders
        
            opts = struct('WindowStyle','modal','Interpreter','tex');
            W = warndlg('\color{black} This datapoint is not within the subsystem boundary.',...
                'Lineplot - Information', opts);
            % Create a user warning message that endpoint exceed subsystem borders

            drawnow     % Necessary to print the message
            waitfor(W); % Resume after warning dialog window is closed    
                    
        end
    
    end       
    
    assignin('base','u',u); % Assign horizontal coordinates of line endpoints in workspace
    assignin('base','v',v); % Assign vertical coordinates of line endpoints in workspace

end

Points = 25 * min( size( z_data ) ); % Determine needed amount of line points

if Points > 1000 % Limit amount of line points to a certain value
    
    Points = 1000;
    
end

assignin('base','Points',Points); % Assign amount of line points in workspace

L = plot3(line_ax,linspace(u(1),u(2),Points),LogSpace([v(1),v(2)],Points), ...
          linspace(z_plot,z_plot,Points),'color',draw_color,'linewidth',1.5);
set(L,'LineStyle','none','Marker','.','MarkerSize',5);
% Plot selected line in respective subsystem by connecting both line endpoints
L.Tag = 'Line Temperature'; % Assign a tag to the general temperature line
assignin('base','L',L); % Assign handle to subsystem line in workspace

Update_Line_Plot(); % Run line plot update function to generate a temperature profile

    
%% Create a line properties selection menu

Menu_Objects = findall(gcf,'Type','uimenu'); % Find all kind of uimenu in current figure

for j = 1 : 1 : length(Menu_Objects) % Iterate the amount of uimenus

    Menu_Objects(j).Enable = char(Previous_Status(j).Enable);
    % Set any uimenu previously enabled to equivalent state

end

plotline = evalin('base','L2'); % Get handle to time based line plot from workspace
c = uicontextmenu;
plotline.UIContextMenu = c; 
% Assgin a UIContextMenu to respective temperature line plot ( curve of third subplot )

Line_Settings = uimenu(c,'Label','Line Settings'); 
% Create a uimenu to change line curve settings

    Line_Color = uimenu(Line_Settings,'Label','Change Color'); 
    % Create a uimenu to change the color of generated line curve

        uimenu('Parent',Line_Color,'Label','grey','Callback',@changecolor);        
        uimenu('Parent',Line_Color,'Label','red','Callback',@changecolor);
        uimenu('Parent',Line_Color,'Label','cyan','Callback',@changecolor);
        uimenu('Parent',Line_Color,'Label','magenta','Callback',@changecolor);
        uimenu('Parent',Line_Color,'Label','yellow','Callback',@changecolor);
        uimenu('Parent',Line_Color,'Label','green','Callback',@changecolor);
        uimenu('Parent',Line_Color,'Label','blue','Callback',@changecolor);
        uimenu('Parent',Line_Color,'Label','black','Callback',@changecolor)
        % Create subordinated uimenus to select any of the supported colors

        object = get(findobj(gcf,'Type','uimenu','Label','Change Color'),'Children');
        set(findall(object,'-property','Checked'),'Checked','off');
        set(findobj(object,'-property','Label','Label',colorname),'Checked','on');
        % Set << Change Color >> uimenu to checked state
                
        function changecolor(source,~) 
        % User defined function to change the color of temperature line plot
                       
            for n = 1 : 1 : length(source.Parent.Children) 
            % Iterate the amount of available line colors
                
                set(source.Parent.Children(n),'Checked','off'); 
                % Set all check states with respect to << color change >> to false
                
            end
            
            set(source,'Checked','on'); % Set selcted line plot color to checked state

            switch source.Label % Define line plot color according to selected color label

                case 'grey' % Matching color label
                    plotline.Color = [0.35 0.35 0.35]; % Set line plot color of third subplot                                       

                case 'red'
                    plotline.Color = 'r';

                case 'cyan'
                    plotline.Color = 'c';

                case 'magenta'
                    plotline.Color = 'm';

                case 'yellow'
                    plotline.Color = 'y';    

                case 'green'
                    plotline.Color = 'g';

                case 'blue'
                    plotline.Color = 'b';

                case 'black'
                    plotline.Color = 'k';

            end 
            
        end

    Line_Style = uimenu(Line_Settings,'Label','Change Style');
    % Create a uimenu to change line style settings

        uimenu('Parent',Line_Style,'Label','solid','Callback',@changestyle);
        uimenu('Parent',Line_Style,'Label','dashed','Callback',@changestyle);
        uimenu('Parent',Line_Style,'Label','dotted','Callback',@changestyle);
        % Create subordinated uimenus to select any of the supported line styles

        set(Line_Style.Children(3),'Checked','on'); % Initially set line style to solid
        
        function changestyle(source,~)
        % User defined function to change the style of temperature line plot
            
            for n = 1 : 1 : length(source.Parent.Children)
            % Iterate the amount of available line styles
                
                set(source.Parent.Children(n),'Checked','off');
                % Set all check states with respect to << style change >> to false
                
            end
            
            set(source,'Checked','on'); % Set selcted line plot style to checked state

            switch source.Label % Define line plot style according to selected style label

                case 'solid' % Matching style label
                    plotline.LineStyle = '-'; % Set line plot style of third subplot

                case 'dashed'
                    plotline.LineStyle = '--';

                case 'dotted'
                    plotline.LineStyle = ':';

            end

        end
    
    Line_Width = uimenu(Line_Settings,'Label','Change Width');
    % Create a uimenu to change line width settings

        uimenu('Parent',Line_Width,'Label','1.0','Callback',@changewidth);
        uimenu('Parent',Line_Width,'Label','1.5','Callback',@changewidth);
        uimenu('Parent',Line_Width,'Label','2.0','Callback',@changewidth);
        uimenu('Parent',Line_Width,'Label','2.5','Callback',@changewidth);
        % Create subordinated uimenus to select any of the supported line widths

        set(Line_Width.Children(3),'Checked','on'); % Initially set line width to 1.5 points
        
        function changewidth(source,~)
        % User defined function to change the width of temperature line plot

            for n = 1 : 1 : length(source.Parent.Children)
            % Iterate the amount of available line widths
                
                set(source.Parent.Children(n),'Checked','off');
                % Set all check states with respect to << width change >> to false
                
            end
            
            set(source,'Checked','on'); % Set selcted line plot style to checked state
            
            switch source.Label % Define line plot width according to selected width label

                case '1.0' % Matching width label
                    plotline.LineWidth = 1.0; % Set line plot width of third subplot

                case '1.5'
                    plotline.LineWidth = 1.5;

                case '2.0'
                    plotline.LineWidth = 2.0;

                case '2.5'
                    plotline.LineWidth = 2.5;

                case '3.0'
                    plotline.LineWidth = 3.0;                       

            end

        end


Melting_Plot = uimenu('Parent',c,'Label','Target Material Properties');
% Create a uimenu to display melting point temperature versus line temperature

    uimenu('Parent',Melting_Plot,'Label','Melting point','Checked','on','Callback',@Melting_Plot_Callback);
    % Create subordinated uimenus to select melting point temperature plot
    
    set(Melting_Plot.Children(1),'Checked','off'); % Set melting point checked state to false
    
    if isequal(line_ax.Tag,'Electronic Subsystem') 
    % Do not enable melting point plot if line plot contains electronic temperature
        
        set(Melting_Plot,'Enable','off'); % Disbale melting point uimenu
        
    end

    function Melting_Plot_Callback(source,~)
    % User defined function to display melting point temperature in thrid subplot
        
        if isequal(get(source,'Checked'),'off') % Detect unchecked state of melting point uimenu
            
            set(source,'Checked','on'); % Set melting point uimenu to checked state
                        
            Thermal_Properties = evalin('base','Thermal_Properties');
            Material_Selection = evalin('base','Material_Selection');
            Melting_Point = Thermal_Properties.(Material_Selection).Melting_Point;
            % Get melting point temperature with respect to target material selection
                       
            ax2 = evalin('base','ax2'); % Evaluate handle to atomic subsystem in workspace
            x_limits = ax2.YLim; 
            % Set time limits of melting temperature plot to time limits of atomic subsystem
             
            Melting_x = linspace(x_limits(1),x_limits(2),Points);  
            Melting_y = linspace(Melting_Point,Melting_Point,Points);
            % Generate suitable melting temperature plot data
                     
            ax4 = evalin('base','ax4'); % Evaluate handle to line plot secondary axes in workspace
            hold(ax4,'on'); % Keep secondary ( time ) axes settings
            
            L3 = line(Melting_x,Melting_y,'Parent',ax4,'Color',[1.0 0.5 0.14],'LineWidth',1.5);
            L3.Tag = 'Melting Temperature';
            assignin('base','L3',L3);
            % Plot melting point temperature with respect to secondary axes of third subsystem
            
            L2 = evalin('base','L2'); % Evaluate handle to time dependent line plot in workspace
            
            if Melting_Point > max(L2.YData) 
            % Rescale line plot axes if melting point exceed vertical axis limits
            
                set(ax4,'YLim',[ax4.YLim(1), 1.2 * Melting_Point]);
                % Set line plot vertical axis limit to a value 20 percent greater than melting point 
                
            else % Rescale line plot axes to inital settings
                
                set(ax4,'YLim',[ax4.YLim(1), 1.1 * ax4.YLim(2)]);
                % Set line plot vertical axis limit to inital settings
                
            end
            
            leg = findobj(gcf, 'Type', 'Legend'); % Get handle to line plot legend object 
            str = string((leg.String)); % Extract previous legend entries
            str(2) = strcat('Melting Point (',32,Material_Selection,32,')'); % Add melting point legend entry
            leg = legend(cellstr(str)); % Convert legend emtries to a cell string variable
            leg.Interpreter = 'latex'; % Set legend text interpreter to latex   
            
        else % Melting point uimenu is already checked ( = remove melting temperature from line plot )
        
             set(source,'Checked','off'); % Set melting point uimenu to unchecked state
             ax4 = evalin('base','ax4'); % Evaluate handle to line plot secondary axes in workspace
             
             try % Apply following routine since no errors occur
                
                L2 = evalin('base','L2'); % Evaluate handle to time dependent line plot data  in workspace
                set(ax4,'YLim',[ax4.YLim(1), 1.1 * max(L2.YData)]); 
                % Set line plot vertical axis limit to inital settings
                L3 = evalin('base','L3'); % Evaluate handle to spatial dependent line plot data in workspace
                delete(L3); % Delete spatial line plot data in third subplot
                
                leg = findobj(gcf, 'Type', 'Legend'); % Get handle to line plot legend object
                leg.String = leg.String{1}; % Restore inital legend entry

             catch % Do no catch any erros !
                 
             end
             
        end
            
    end

set(findobj(gcf,'Type','uimenu','Tag','Temperature Profile'),'Enable','on');
% Enable all subordinated uimenu of temperature profile plot

end


