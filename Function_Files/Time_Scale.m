function Time_Scale(handle_object,~)
% A user defined function to ensure any changes in time scales of either 
% the electronic subsystem or atomic subsystem, which are in agreement with 
% the chosen Thermal-Spike-Simulation time range.
 
 set(handle_object,'Checked','on','Enable','off'); 
 % Set time selection uimenu to disabled state and not checked

 Radius_1 = evalin('base','Radial_Mesh_1'); % Evaluate electronic subsystem radius in workspace 
 Radius_2 = evalin('base','Radial_Mesh_2'); % Evaluate atomic subsystem radius in workspace
 
 Time_1 = evalin('base','Time_Mesh_1'); % Evaluate electronic subsystem time in workspace
 Time_2 = evalin('base','Time_Mesh_2'); % Evaluate atomic subsystem time in workspace
 
 Time_Grid = evalin('base','Time_Grid'); % Get simulation time grid from workspace
 
 TS = figure; % Create a new figure
 TS.Name = 'Subsystem Time'; % Set figure name to "subsystem time"
 TS.Color = 'w'; % Set a white figure background color
 TS.NextPlot = 'replace'; % Replace open figure when called again 
 TS.NumberTitle = 'off'; % No number title is displayed 
 TS.Resize = 'off'; % Do not allow manual window resizing 
 TS.MenuBar = 'none'; % Menubar is not available
 TS.ToolBar = 'none'; % Toolbar is not available
 TS.Units = 'normalized'; % Position units are set to normalized
 TS.Position = [0.35 0.30 0.25 0.25]; % Define figure position on screen
 TS.CloseRequestFcn = @closerequest; % Set a figure close request function
 
 %% Create a bunch of user interface control to set meaningful subsystem time values
 
 PT = uipanel(TS,'Title','< Vertical axis >','FontSize',10,'ForegroundColor',[0.50 0.50 0.50], ...
     'BackgroundColor','w','Units','normalized','Position',[0.02 0.03 0.96 0.95],...
     'FontWeight','bold','FontAngle','Italic');
 
 PS = uipanel(PT,'Title','','FontSize',11,'ForegroundColor',[0.50 0.50 0.50], ...
     'BackgroundColor',[0.94 0.94 0.94],'Units','normalized','Position',[0.02 0.03 0.96 0.70]);
 
 uicontrol(PT,'Style', 'popup','String', {'Electronic Subsystem','Atomic Subsystem'},...
           'Units','normalized','Position',[0.022 0.825 0.415 0.11],'Callback',@Subsystem_Selection,...
           'FontName','Cambria','FontSize',8,'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2],...
           'Tag','Subsystem Selection');     
 
 annotation(PS,'textbox',[0.05 0.60 0.25 0.20],'String','Min. Time','FontUnits','normalized',...
     'Interpreter','latex','Color',[0.30 0.30 0.30],'LineStyle','none','HorizontalAlignment','center');
  
 t_min = uicontrol(PS,'Style','edit','String',num2str(min(Time_1)),'Units','normalized',...
     'Position',[0.05 0.35 0.25 0.22],'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2],...
     'FontName','Cambria');
 
 annotation(PS,'textbox',[0.05 0.10 0.25 0.20],'String','{\boldmath$t_{min}$}$$\;\;\left(s\right)$$', ...
     'FontUnits','normalized','Interpreter','latex','Color',[0.30 0.30 0.30], ...
     'LineStyle','none','HorizontalAlignment','center');
 
 annotation(PS,'textbox',[0.36 0.60 0.30 0.20],'String','Iterations','FontUnits','normalized',...
     'Interpreter','latex','Color',[0.30 0.30 0.30],'LineStyle','none','HorizontalAlignment','center');
 
 Iterations = uicontrol(PS,'Style','edit','String',num2str(length(Time_1)),'Units','normalized',...
     'Position',[0.375 0.35 0.25 0.22],'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2],...
     'FontName','Cambria');
 
 annotation(PS,'textbox',[0.38 0.10 0.25 0.20],'String','{\boldmath$N_t$}$$\;\;\left(counts\right)$$', ...
     'FontUnits','normalized','Interpreter','latex','Color',[0.30 0.30 0.30], ...
     'LineStyle','none','HorizontalAlignment','center');
 
 annotation(PS,'textbox',[0.67 0.60 0.30 0.20],'String','Max. Time','FontUnits','normalized',...
     'Interpreter','latex','Color',[0.30 0.30 0.30],'LineStyle','none','HorizontalAlignment','center');
 
 t_max = uicontrol(PS,'Style','edit','String',num2str(max(Time_1)),'Units','normalized',...
     'Position',[0.70 0.35 0.25 0.22],'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2],...
     'FontName','Cambria'); 
 
 annotation(PS,'textbox',[0.70 0.10 0.25 0.20],'String','{\boldmath$t_{max}$}$$\;\;\left(s\right)$$', ...
     'FontUnits','normalized','Interpreter','latex','Color',[0.30 0.30 0.30], ...
     'LineStyle','none','HorizontalAlignment','center');
 
 uicontrol(PT,'Style','pushbutton','String','Cancel','Units','normalized','FontSize',10,...
     'Position',[0.48 0.80 0.19 0.20],'Callback',@cancel_button,'FontName','Cambria',...
     'ForegroundColor',[0.15 0.15 0.15]);
  
 OK = uicontrol(PT,'Style','pushbutton','String','','Units','normalized','FontSize',10,...
     'Position',[0.71 0.80 0.27 0.20],'Callback',@confirm_button,'FontName','Cambria',...
     'ForegroundColor',[0.15 0.15 0.15]);

 %% Apply a calculation routine to draw changed subsystem
 
 if all([evalin('base','exist(''Electronic_Temp'',''var'')'),...
             evalin('base','exist(''Lattice_Temp'',''var'')')])
 % Check that both subsystem temperatures already exist in workspace
 
        set(OK,'String','Refresh plot'); % Set OK button label to "refresh data"
        
 else % Subsystem temperatures not yet calculated ( inital run )
     
     set(OK,'String','Set values'); % Set OK button label to "set values"
         
 end
 
 function Subsystem_Selection(Source,~)
 % A nested function that determine a meaningful range of radius values
     
     strings = Source.String; % Get all subsystem entries
     selection = char(strings(Source.Value)); % Determine a selected subsystem
          
     switch selection % Distinguish subsystem selection
         
         case 'Electronic Subsystem' % Electronic subsystem is selected
             
             t_min.String = num2str(min(Time_1));
             % Get minimum electronic subsystem time
             t_max.String = num2str(max(Time_1));
             % Get maximum electronic subsystem time
             Iterations.String = num2str(length(Time_1));
             % Get amount of time iterations in electronic subsystem             
             
         otherwise % Atomic subsystem is selected
             
             t_min.String = num2str(min(Time_2));
             % Get minimum atomic subsystem time
             t_max.String = num2str(max(Time_2));
             % Get maximum atomic subsystem time
             Iterations.String = num2str(length(Time_2));
             % Get amount of time iterations in atomic subsystem
             
     end
     
     
 end  
 
 function cancel_button(~,~)
 % A nested function to react on cancel button pressed by the user 
     
     set(handle_object,'Checked','off','Enable','on'); % Set uimenu to enabled state and not checked
     close(TS); % Close time settings figure
     
 end 
 
 function confirm_button(hObject,~)
 % A nested function to react on confirm button pressed by the user
     
     set(hObject,'Enable','off'); % Set uimenu to disabled state
     
     UI_Menu = findobj(evalin('base','Fig'),'Label','Erase existing line');
     Erase_Line_Plot(UI_Menu,''); % Erase any existing line plots in both subsystem
          
     pause(eps); % Wait a certain time before continuing
     
     Min_Time = str2double(t_min.String); % Read minimum time input
     Min_Time( Min_Time < min(Time_Grid) ) = min(Time_Grid); % Match lower time limit
         
     Max_Time = str2double(t_max.String); % Read maximum time input
     Max_Time( Max_Time > max(Time_Grid) ) = max(Time_Grid); % Match upper time limit
      
     Min_Time( Min_Time > Max_Time ) = min(Time_Grid); % Avoid wrong user input
     Max_Time( Max_Time < Min_Time ) = max(Time_Grid); % Avoid wrong user input
     
     Steps = str2double(Iterations.String); % Read amount of time iterations
     Steps( Steps <  20 ) =  20; % Match a minimum amount of iteration points
     Steps( Steps > 500 ) = 500; % Match a maximum amount of iteration points    
     
     obj = findobj(TS,'Type','uicontrol','Tag','Subsystem Selection');
     strings = obj.String; % Get all subsystem entries
     selection = char(strings(obj.Value)); % Determine a selected subsystem
     
     if isequal(selection,'Electronic Subsystem') % Electronic subsystem is selected
             
        Time_1 = LogSpace([Min_Time,Max_Time],Steps); % Calculate required time grid of the electronic subsystem
        assignin('base','Time_Mesh_1',Time_1); % Assign time mesh of electronic subsystem in workspace
              
        
     else % Atomic subsystem is selected
         
        Time_2 = LogSpace([Min_Time,Max_Time],Steps); % Calculate required time of the atomic subsystem
        assignin('base','Time_Mesh_2',Time_2); % Assign time mesh of atomic subsystem in workspace
                       
     end
     
     pause(eps); % Wait a certain time before continuing
     
     if all([evalin('base','exist(''Electronic_Temp'',''var'')'),...
             evalin('base','exist(''Lattice_Temp'',''var'')')])
      % Check that both subsystem temperatures already exist in workspace
                           
         Electronic_Temp =evalin('base','Electronic_Temp'); % Evaluate electronic temperatures in workspace
         Lattice_Temp =evalin('base','Lattice_Temp'); % Evaluate atomic temperatures in workspace
         
         [Temp_1, Temp_2] = Temperature_Interpolation(Electronic_Temp,Lattice_Temp);
         % Interpolate subsystem temperatures at any given radius and time value
         
         Electronic_Temperature = Temp_1; % Assign electronic temperatures in caller
         Lattice_Temperature = Temp_2; % Assign atomic temperatures in caller
         
         assignin('base','Electronic_Temperature',Electronic_Temperature);
         % Assign electronic temperatures in workspace
         assignin('base','Lattice_Temperature',Lattice_Temperature);         
         % Assign atomic temperatures in workspace
         
         Fig = evalin('base','Fig'); % Evaluate handle to main simulation window in workspace            
         set(0,'currentfigure',Fig);% Set main simulation window to current figure
         set(Fig,'CurrentAxes',evalin('base','ax1')); % Set electronic subsystem to current axes
                 
         Array_1 = zeros(length(Radius_1),length(Time_1)); % Initalize an empty electronic temperatures array
         Array_2 = zeros(length(Radius_2),length(Time_2)); % Initalize an empty atomic temperatures array
         
         Graphics_Plot(true,Radius_1,Radius_2,Time_1,Time_2,Array_1,Array_2); % Reset both subsystem plots
         
         pause(eps); % Wait a certain time before continuing
         
         Graphics_Plot(false,Radius_1,Radius_2,Time_1,Time_2,Electronic_Temperature,Lattice_Temperature);
         % Plot subsystem temperatures according to time settings
         
         set(hObject,'Enable','on'); % Set time settings uimenu to enabled state
                          
     else
         
         set(handle_object,'Checked','off','Enable','on'); 
         % Set time settings uimenu to enabled state and not checked  
                          
         close(TS); % Close time settings figure
         
     end     
             
 end 

 function closerequest(~,~)
 % Close request function to display a question dialog box 
            
  delete(TS) % Close GUI figure 
  set(handle_object,'Checked','off','Enable','on');
   
 end 

end