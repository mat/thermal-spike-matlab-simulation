function status = statusbar(varargin)
% A calculation accuracy statusbar that can be embedded in a GUI handle.

%% Routine applied if statusbar function is called with status axes and status value

 if ishandle(varargin{1}) && size(varargin, 2) > 2
 % Function is called with both, handle to statusbar axes and statusbar value,
 % which is similar to any pre-defined statusbar properties.
 
     ax_1 = varargin{1};                % Get handle to statusbar axes 
     value_1 = round(varargin{3},5);    % Get the temporary statusbar value [ 0 < x < 1 ]
     p = get(ax_1,'Child');             % Read the children objects ( patch ) to axes handle 
     x_1 = get(p,'XData');              % Obtain respective x axis data of patch plot
     x_1(1:2) = 0;                      % Define lower border of statusbar
     x_1(3:4) = value_1 + 0.0010;       % Define upper border of statusbar
     set(p,'XData',x_1)                 % Draw new x axis data of given patch plot
     
      if value_1 < 0.94
     
         p.FaceColor = 'r'; % Set accuracy status bar to red color ( 'r' = red )    
     
     elseif value_1 > 0.97
 
         p.FaceColor = 'g'; % Set accuracy status bar to green color ( 'g' = reen )
    
     else
         
         p.FaceColor = [1.0 0.5 0]; % Set accuracy status bar to orange color
         
     end
     
     ax_2 = varargin{2};
     value_2 = round(varargin{4},5);   % Get the temporary statusbar value [ 0 < x < 1 ]    
     q = get(ax_2,'Child');            % Read the children objects ( patch ) to axes handle 
     x_2 = get(q,'XData');             % Obtain respective x axis data of patch plot
     x_2(1:2) = 0;                      % Define lower border of statusbar
     x_2(3:4) = value_2 + 0.0010;      % Define upper border of statusbar
     set(q,'XData',x_2)                % Draw new x axis data of given patch plot         
     
     if value_2 < 0.94
     
         q.FaceColor = 'r'; % Set meshsize status bar to red color ( 'r' = red )    
     
     elseif value_2 > 0.97
 
         q.FaceColor = 'g'; % Set meshsize status bar to green color ( 'g' = reen )
         
     else
         
         q.FaceColor = [1.0 0.5 0]; % Set meshsize status bar to orange color
    
     end
     
     return % Return to caller function ( pushbutton_2_Callback )
     
 end
 
 %% Routine applied since statusbar function is just called with an axes handle
 
 % -- Display accuracy status --
 
 bg_color = 'w'; % Define background color of statusbar axes ( w = white )   
 fg_color = 'k'; % Define marker color of statusbar          ( k = black )

 g = axes('Units','normalized','XLim',[0.915 0.999],'YLim',[0 1], ...
          'XTick',[],'YTick',[],'Color',bg_color,'Tag','Accuracy_axes', ...
          'XColor',bg_color,'YColor',bg_color,'Parent',varargin{1});
 % Generate status axes with repsect to axis limitations and color settings
 
 g.Position(1) = 0.00; % Move left position gently towards the inner border of statuspanel
 g.Position(2) = 0.06;
 g.Position(3) = 0.99; % Move right position towards the outer border of statuspanel
 g.Position(4) = 0.46;  
 
 assignin('base','g',g);
 
 p_1 = patch([0 0 0 0],[0 0.8 0.8 0],fg_color,'Parent',g,'EdgeColor',[0.60 0.60 0.60]); 
 p_1.Tag = 'Accuracy';  % Draw statusbar value     
 
 % -- Display meshgrid status --
 
 bg_color = 'w'; % Define background color of statusbar axes ( w = white )
 fg_color = 'k'; % Define marker color of statusbar          ( k = black )

 h = axes('Units','normalized','XLim',[0.915 0.999],'YLim',[0 1], ...
          'XTick',[],'YTick',[],'Color',bg_color,'Tag','Meshgrid_axes',  ...
          'XColor',bg_color,'YColor',bg_color,'Parent',varargin{2});
 % Generate status axes with repsect to axis limitations and color settings
 
 h.Position(1) = 0.00; % Move left position gently towards the inner border of statuspanel
 h.Position(2) = 0.55;
 h.Position(3) = 0.99; % Move right position towards the outer border of statuspanel
 h.Position(4) = 0.46; 
 
 assignin('base','h',h);
 
 p_2 = patch([0 0 0 0],[1 0.2 0.2 1],fg_color,'Parent',h,'EdgeColor',[0.60 0.60 0.60]); 
 p_2.Tag = 'Meshgrid'; % Draw statusbar value
 
 % -- Draw a horizontal line to separate both status informations --
 
 div_line = annotation(h.Parent,'line',[0.00 0.99],[0.53 0.53]);
 set(div_line,'Color',[0.65 0.65 0.65],'LineWidth',1.5);
 
 % -- Axes handles output --
 
 status = {g,h};
 
end