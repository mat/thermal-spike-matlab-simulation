function B = Atomic_Energy_Density_Calculation(r,t)
% User defined function to determine the atomic energy density per unit
% time B(r,t) mainly according to atomic energy losses ( dE/dx )_n due to 
% swift heavy ion impact E_ion.


 %% Load Global Constants

 global r_n_0;
 % Coupling Radius of a cylinder where 2/3 of the energy is transfered to the atomic lattice. Unit: m

 global t_n_0;
 % Mean collision time of atoms within the target material. Unit: s

 global sigma_n_t;
 % Half width maximum of the gaussian distribution in time. Unit: s

 global S_n;
 % Atomic energy loss dE/dx due to atomic slowing down process. Unit: J/m

 global c;
 % Atomicc energy density scaling factor c to ensure normalized energy input. Unit: (m*s)^-1


 %% Calculate Energy Density per Unit Time

 r( r == 0 ) = 10^(-10); % Avoid any radius values equal zero due to infinty states
 
 if ~isequal(S_n,0)
    
     B = c * S_n * exp( -1/2 * ( ( t - t_n_0 ) / sigma_n_t ).^2 ) .* exp( - r / r_n_0 ) ./ r;
     % Energy density per unit time B(r,t) according to atomic energy loss (dE/dx)_n . Unit: W/m^3
     % This expression solves the integral equation to determine energy density in cylindric coordinates. 

 else
    
     B = zeros(length(r),length(t));
    
 end

 B(isnan(B)) = 0; % Set all values that are not numeric to zero
 B(isinf(B)) = realmax('single'); % Set all infinitive values to maximum of single precission
 
    
end