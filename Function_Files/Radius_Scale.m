function Radius_Scale(handle_object,~)
% A user defined function to ensure any changes in radius scales of either 
% the electronic subsystem or atomic subsystem, which are in agreement with 
% the chosen Thermal-Spike-Simulation spatial range.

 set(handle_object,'Checked','on','Enable','off');
 % Set radius selection uimenu to disabled state and not checked
 
 Radius_1 = evalin('base','Radial_Mesh_1'); % Evaluate electronic subsystem radius in workspace
 Radius_2 = evalin('base','Radial_Mesh_2'); % Evaluate atomic subsystem radius in workspace
 
 Time_1 = evalin('base','Time_Mesh_1'); % Evaluate electronic subsystem time in workspace
 Time_2 = evalin('base','Time_Mesh_2'); % Evaluate atomic subsystem time in workspace
 
 Radial_Grid = evalin('base','Radial_Grid'); % Get simulation radius grid from workspace
 
 RS = figure; % Create a new figure
 RS.Name = 'Subsystem Radius'; % Set figure name to "subsystem radius"
 RS.Color = 'w'; % Set a white figure background color
 RS.NextPlot = 'replace'; % Replace open figure when called again 
 RS.NumberTitle = 'off'; % No number title is displayed 
 RS.Resize = 'off'; % Do not allow manual window resizing 
 RS.MenuBar = 'none'; % Menubar is not available
 RS.ToolBar = 'none'; % Toolbar is not available
 RS.Units = 'normalized'; % Position units are set to normalized
 RS.Position = [0.35 0.30 0.25 0.25]; % Define figure position on screen
 RS.CloseRequestFcn = @closerequest; % Set a figure close request function
 
 %% Create a bunch of user interface control to set meaningful subsystem radius values
 
 PR = uipanel(RS,'Title','< Horizontal axis >','FontSize',10,'ForegroundColor',[0.50 0.50 0.50], ...
     'BackgroundColor','w','Units','normalized','Position',[0.02 0.03 0.96 0.95],...
     'FontWeight','bold','FontAngle','Italic');
 
 PS = uipanel(PR,'Title','','FontSize',11,'ForegroundColor',[0.50 0.50 0.50], ...
     'BackgroundColor',[0.94 0.94 0.94],'Units','normalized','Position',[0.02 0.03 0.96 0.70]);
 
 uicontrol(PR,'Style', 'popup','String', {'Electronic Subsystem','Atomic Subsystem'},...
           'Units','normalized','Position',[0.022 0.825 0.415 0.11],'Callback',@Subsystem_Selection,...
           'FontName','Cambria','FontSize',8,'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2],...
           'Tag','Subsystem Selection'); 
 
 annotation(PS,'textbox',[0.05 0.60 0.25 0.20],'String','Min. Radius','FontUnits','normalized',...
     'Interpreter','latex','Color',[0.30 0.30 0.30],'LineStyle','none','HorizontalAlignment','center');
  
 R_min = uicontrol(PS,'Style','edit','String',num2str(10^(9)*min(Radius_1)),'Units','normalized',...
     'Position',[0.05 0.35 0.25 0.22],'Enable','inactive','FontName','Cambria','FontSize',9,...
     'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2]);
 
 annotation(PS,'textbox',[0.05 0.10 0.25 0.20],'String','{\boldmath$r_{min}$}$$\;\;\left(nm\right)$$', ...
     'FontUnits','normalized','Interpreter','latex','Color',[0.30 0.30 0.30], ...
     'LineStyle','none','HorizontalAlignment','center');
 
 annotation(PS,'textbox',[0.36 0.60 0.30 0.20],'String','Iterations','FontUnits','normalized',...
     'Interpreter','latex','Color',[0.30 0.30 0.30],'LineStyle','none','HorizontalAlignment','center');
 
 Iterations = uicontrol(PS,'Style','edit','String',num2str(length(Radius_1)),'Units','normalized',...
     'Position',[0.375 0.35 0.25 0.22],'FontName','Cambria','FontSize',9,...
     'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2]);
 
 annotation(PS,'textbox',[0.38 0.10 0.25 0.20],'String','{\boldmath$N_r$}$$\;\left(counts\right)$$', ...
     'FontUnits','normalized','Interpreter','latex','Color',[0.30 0.30 0.30], ...
     'LineStyle','none','HorizontalAlignment','center');
 
 annotation(PS,'textbox',[0.67 0.60 0.30 0.20],'String','Max. Radius','FontUnits','normalized',...
     'Interpreter','latex','Color',[0.30 0.30 0.30],'LineStyle','none','HorizontalAlignment','center');
 
 R_max = uicontrol(PS,'Style','edit','String',num2str(10^(9)*max(Radius_1)),'Units','normalized',...
     'Position',[0.70 0.35 0.25 0.22],'FontName','Cambria','FontSize',9,...
     'FontWeight','bold','ForegroundColor',[0.2 0.2 0.2]); 
 
 annotation(PS,'textbox',[0.71 0.10 0.25 0.20],'String','{\boldmath$r_{max}$}$$\;\;\left(nm\right)$$', ...
     'FontUnits','normalized','Interpreter','latex','Color',[0.30 0.30 0.30], ...
     'LineStyle','none','HorizontalAlignment','center');

 
 uicontrol(PR,'Style','pushbutton','String','Cancel','Units','normalized','FontSize',10,...
     'Position',[0.48 0.80 0.19 0.20],'FontName','Cambria','Callback',@cancel_button,...
     'ForegroundColor',[0.15 0.15 0.15]);
  
 OK = uicontrol(PR,'Style','pushbutton','String','','Units','normalized','FontSize',10,...
     'Position',[0.71 0.80 0.27 0.20],'Enable','on','FontName','Cambria',...
     'ForegroundColor',[0.15 0.15 0.15],'Callback',@confirm_button);

 %% Apply a calculation routine to draw changed subsystem
 
 if all([evalin('base','exist(''Electronic_Temp'',''var'')'),...
             evalin('base','exist(''Lattice_Temp'',''var'')')])
 % Check that both subsystem temperatures already exist in workspace
 
        set(OK,'String','Refresh plot'); % Set OK button label to "refresh data"
        
 else % Subsystem temperatures not yet calculated ( inital run )
     
     set(OK,'String','Set values'); % Set OK button label to "set values"
         
 end
   
 function Subsystem_Selection(Source,~)
 % A nested function that determine a meaningful range of radius values
     
     strings = Source.String; % Get all subsystem entries
     selection = char(strings(Source.Value)); % Determine a selected subsystem
          
     switch selection % Distinguish subsystem selection
         
         case 'Electronic Subsystem' % Electronic subsystem is selected
             
             R_min.String = num2str(10^(9)*min(Radius_1)); 
             % Get minimum electronic subsystem radius
             R_max.String = num2str(10^(9)*max(Radius_1)); 
             % Get maximum electronic subsystem radius
             Iterations.String = num2str(length(Radius_1)); 
             % Get amount of spatial iterations in electronic subsystem
                          
         otherwise % Atomic subsystem is selected
             
             R_min.String = num2str(10^(9)*min(Radius_2));
             % Get minimum atomic subsystem radius
             R_max.String = num2str(10^(9)*max(Radius_2)); 
             % Get maximum atomic subsystem radius
             Iterations.String = num2str(length(Radius_2)); 
             % Get amount of spatial iterations in atomic subsystem
             
     end
     
     
 end 
 
 function cancel_button(~,~)
 % A nested function to react on cancel button pressed by the user
     
     set(handle_object,'Checked','off','Enable','on'); % Set uimenu to enabled state and not checked
     close(RS); % Close radius settings figure
     
 end 
 
 function confirm_button(hObject,~)
 % A nested function to react on confirm button pressed by the user
     
     set(hObject,'Enable','off'); % Set uimenu to disabled state   
     
     UI_Menu = findobj(evalin('base','Fig'),'Label','Erase existing line');
     Erase_Line_Plot(UI_Menu,''); % Erase any existing line plots in both subsystem
          
     pause(eps); % Wait a certain time before continuing
     
     Min_Radius = 10^(-9) * str2double(R_min.String); % Read minimum radius input
     Min_Radius( Min_Radius < min(Radial_Grid) ) = min(Radial_Grid); % Match lower radius limit
     
     Max_Radius = 10^(-9) * str2double(R_max.String); % Read maximum radius input
     Max_Radius( Max_Radius > max(Radial_Grid) ) = max(Radial_Grid); % Match upper radius limit
     
     Min_Radius( Min_Radius > Max_Radius) = min(Radial_Grid); % Avoid wrong user input
     Max_Radius( Max_Radius < Min_Radius) = max(Radial_Grid); % Avoid wrong user input
          
     Steps = str2double(Iterations.String); % Read amount of spatial iterations
     Steps( Steps <  20 ) =  20; % Match a minimum amount of iteration points
     Steps( Steps > 500 ) = 500; % Match a maximum amount of iteration points    
          
     obj = findobj(RS,'Type','uicontrol','Tag','Subsystem Selection');
     strings = obj.String; % Get all subsystem entries
     selection = char(strings(obj.Value)); % Determine a selected subsystem
     
     if isequal(selection,'Electronic Subsystem') % Electronic subsystem is selected
     
        Radius_1 = linspace(Min_Radius,Max_Radius,Steps); % Calculate required radius grid of the electronic subsystem
        assignin('base','Radial_Mesh_1',Radius_1); % Assign radius mesh of electronic subsystem in workspace       
        
     else % Atomic subsystem is selected
         
         Radius_2 = linspace(Min_Radius,Max_Radius,Steps); % Calculate required radius grid of the atomic subsystem
         assignin('base','Radial_Mesh_2',Radius_2); % Assign radius mesh of atomic subsystem in workspace
         
     end
     
     pause(eps); % Wait a certain time before continuing
     
     if all([evalin('base','exist(''Electronic_Temp'',''var'')'),...
             evalin('base','exist(''Lattice_Temp'',''var'')')])
      % Check that both subsystem temperatures already exist in workspace
                           
         Electronic_Temp =evalin('base','Electronic_Temp'); % Evaluate electronic temperatures in workspace
         Lattice_Temp =evalin('base','Lattice_Temp'); % Evaluate atomic temperatures in workspace
         
         [Temp_1, Temp_2] = Temperature_Interpolation(Electronic_Temp,Lattice_Temp);
         % Interpolate subsystem temperatures at any given radius and time value
         
         Electronic_Temperature = Temp_1; % Assign electronic temperatures in caller
         Lattice_Temperature = Temp_2; % Assign atomic temperatures in caller
         
         assignin('base','Electronic_Temperature',Electronic_Temperature);
         % Assign electronic temperatures in workspace
         assignin('base','Lattice_Temperature',Lattice_Temperature);
         % Assign atomic temperatures in workspace
         
         Fig = evalin('base','Fig'); % Evaluate handle to main simulation window in workspace         
         set(0,'currentfigure',Fig); % Set main simulation window to current figure
         set(Fig,'CurrentAxes',evalin('base','ax1')); % Set electronic subsystem to current axes
         
         Array_1 = zeros(length(Radius_1),length(Time_1)); % Initalize an empty electronic temperatures array
         Array_2 = zeros(length(Radius_2),length(Time_2)); % Initalize an empty atomic temperatures array
         
         Graphics_Plot(true,Radius_1,Radius_2,Time_1,Time_2,Array_1,Array_2); % Reset both subsystem plots
         
         pause(eps); % Wait a certain time before continuing      
         
         Graphics_Plot(false,Radius_1,Radius_2,Time_1,Time_2,Electronic_Temperature,Lattice_Temperature);
         % Plot subsystem temperatures according to radius settings
         
         set(hObject,'Enable','on'); % Set radius settings uimenu to enabled state                
         
     else
         
         set(handle_object,'Checked','off','Enable','on');
         % Set radius settings uimenu to enabled state and not checked
                                    
         close(RS); % Close radius settings figure        
         
     end 
     
 end 

 function closerequest(~,~)
 % Close request function to display a question dialog box 
            
  set(handle_object,'Checked','off','Enable','on');
  delete(RS); % Close GUI figure 
  
   
 end 

end