function Global_Constants()
% User defined function to load expressions and variables that defined as
% global variables in Matlab. There are at least three different types:
%
% - General constant    ( e.g. Planck Constant, Boltzmann constant, ... )
% - Tunable constant    ( e.g. solver options, energy losses, ... )
% - Calculated constant ( e.g. normalization factors )

%% General Constant

global Boltzmann_Constant, Boltzmann_Constant = 1.38 * 10^(-23); 
% Boltzmann Constant, k_B ,  Value:  1.38 * 10^-23 ,     Unit: J/K;

global Planck_Constant, Planck_Constant = 1.05 * 10^(-34);
% Planck Constant, h_bar  ,  Value:  1.05 * 10^-34 ,    Unit: Js;

global Avogadro_Constant, Avogadro_Constant = 6.022 * 10^(23);
% Avogadro Constant, N_A ,  Value:  6.022 * 10^23 ,      Unit: 1/mol;

global Electron_Mass, Electron_Mass = 9.11 * 10^(-31);
% Resting mass of Electron, m_e , Value: 9.31 * 10^-31 , Unit: kg

global Speed_of_Light, Speed_of_Light = 2.998 * 10^(8);
% Speed of Light in Vacuum, c_0 , Value: 3.00 * 10^8 , Unit: m/s

global Electron_Charge, Electron_Charge = 1.602 * 10^(-19);
% Charge of an Electron, e , Value: 1.602 * 10^-19 , Unit: C

global Bohr_Radius, Bohr_Radius = 5.292 * 10^(-11);
% Bohr's atomic radius, a_0 , Value: 5.292 * 10^-11 , Unit: m


%% Tunable Constant

% +---------------------------+
% |    PDE Solver Options     |
% +---------------------------+

global Options, Options = odeset('RelTol',1e-3,'AbsTol',1e-5,'NormControl','on','Stats','on','NonNegative',1,'Stats','on');

% +---------------------------+
% |    Thermal Diffusivity    |
% +---------------------------+

global D_e_300, D_e_300 = 150 * 10^(-4);
% Electronic thermal diffusivity, D_e ( T = 300K ), Value: tunable , Unit: m^2/s

global D_e_min, D_e_min = 1 * 10^(-4);
% Minimum electronic thermal diffusivity, D_e_min,  Value: tunable , Unit: m^2/s

% +--------------------------------------+
% |    Electron-Phonon mean free path    |
% +--------------------------------------+

global lambda, lambda = [];
% Electron-Phonon mean free path, \lambda , Value: tunable , Unit: nm

% +---------------------------+
% |    Monolayer Coverage     |
% +---------------------------+

global Monolayer, Monolayer = 3 * 10^(20);
% Molecule surface density of an adsorbed monolayer, Value: tunable , Unit: m^(-2)

% +---------------------------+
% |      Molare Volume        |
% +---------------------------+

global Molare_Volume, Molare_Volume = 22.4 * 10^(-3);
% Molare volume of any molecules, Molare Volume, Value: tunable , Unit: m^3/mol

% +---------------------------+
% |  Swift Heavy Ion Energy   |
% +---------------------------+

global E_ion, E_ion = 1.4;
% Energy of swift heavy ion, E_ion, Value: tunable , Unit: MeV/u

% +---------------------------+
% |   Ionization Potential    |
% +---------------------------+

global I_ion, I_ion = 10;
% Ionization potential of target atoms, I_ion, Value: tunable , Unit: eV

% +---------------------------+
% |  Electronic Energy Input  |
% +---------------------------+

global r_e_0,  r_e_0 = 2.10 * 10^(-9); 
% Waligorski Radius of a cylinder where 2/3 of the energy is transfered to delta electrons. Unit: m

global t_e_0, t_e_0 = 10^(-15); 
% Mean flight time of delta ray electrons within the target material. Unit: s

global sigma_e_t, sigma_e_t = 10^(-15); 
% Half width maximum of the electronic gaussian distribution in time. Unit: s

global S_e, S_e = 38 * 10^3 * 10^9 * Electron_Charge; % (dE/dx)_e [keV/nm] * e [J/eV]
% Electronic energy loss dE/dx due to electronic slowing down process. Unit: J/m

global alpha, alpha = 1.667;
% Electronic energy transfer parameter due to electronic slowing down process. Unit: none

% +-----------------------+
% |  Atomic Energy Input  |
% +-----------------------+

global r_n_0,  r_n_0 = 3 * 10^(-9); 
% Waligorski Radius of a cylinder where 2/3 of the energy is transfered to the lattice. Unit: m

global t_n_0, t_n_0 = 10^(-13); 
% Mean movement time of atoms within the target lattice. Unit: s

global sigma_n_t, sigma_n_t = 10^(-13); 
% Half width maximum of the atomic gaussian distribution in time. Unit: s

global S_n, S_n = 0.00 * 10^3 * 10^9 * Electron_Charge; % (dE/dx)_e [keV/nm] * e [J/eV]
% Atomic energy loss dE/dx due to atomic slowing down process. Unit: J/m 


%% Calculated Constant

global b, b = Time_Scaling_Factor_Calculation(t_e_0,sigma_e_t);
% Electronic energy density scaling factor b to ensure normalized energy input. Unit: (m*s)^-1

global c, c = Time_Scaling_Factor_Calculation(t_n_0,sigma_n_t) ...
            * Radius_Scaling_Factor_Calculation(r_n_0);
% Electronic energy density scaling factor b to ensure normalized energy input. Unit: (m*s)^-1

end