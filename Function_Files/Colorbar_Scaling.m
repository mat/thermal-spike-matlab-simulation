function Colorbar_Scaling(hObject,~) 
% A user defined function to change the colorbar of each subsystem from
% logarithmic to linear and vice versa when requesting a uimenu entry.

 Fig = evalin('base','Fig'); % Evaluate main simulation figure in workspace
 
 try % Does not show up erros when fail to attempt 
 
    set(hObject,'Checked','on'); % Set uimenu to checked state
    Children = findobj(hObject.Parent.Children); % Find all uimenu entries 
 
    for i = 1 : 1 : length(Children) % Iterate all available colorscales
     
        child =  Children(i); % Assign current colorscale handle to a static variable
     
        if ~isequal(child.Label,hObject.Label) % Detect current colorscale label is not selected        
        
            set(Children(i),'Checked','off'); % Set current colorscale entry to not checked
        
        end
     
    end
    
    assignin('base','ColObj',hObject); % Assign colorscale object in workspace
    
 catch % How to react on any error during calculation process 
               
 end
    
 if all([~isequal(hObject.Parent.Tag,'Electronic Contour Lines'), ...
          isequal(hObject.Parent.Tag,'Electronic Colorscale')])
 % Detect electronic subsystem colorscale is requested
 
    Contour_Lines_1 = evalin('base','Contour_Lines_1'); % Get number of chosen contour lines
    Object = hObject; % Set object handles to a variable
    
 elseif all([isequal(hObject.Parent.Tag,'Electronic Contour Lines'), ...
            ~isequal(hObject.Parent.Tag,'Electronic Colorscale')])
 % Detect electronic subsystem contour lines are requested
     
     Contour_Lines_1 = str2double(hObject.Label) + 2; % Set number of electronic contour lines
     assignin('base','Contour_Lines_1',Contour_Lines_1); % Assign electronic contour lines in workspace 
     Object.Label = evalin('base','Electronic_Colorscale'); % Evaluate electronic colorscale in workspace   
     Object.Parent.Tag = 'Electronic Colorscale'; % Set electronic colorscale to current object
     
    
 elseif all([~isequal(hObject.Parent.Tag,'Atomic Contour Lines'), ...
          isequal(hObject.Parent.Tag,'Atomic Colorscale')])
 % Detect atomic subsystem colorscale is requested
 
    Contour_Lines_2 = evalin('base','Contour_Lines_2'); % Get number of chosen contour lines
    Object = hObject; % Set object handles to a variable
    
 else % Detect atomic subsystem contour lines are requested
     
     Contour_Lines_2 = str2double(hObject.Label) + 2; % Set number of atomic contour lines
     assignin('base','Contour_Lines_2',Contour_Lines_2); % Assign atomic contour lines in workspace 
     Object.Label = evalin('base','Atomic_Colorscale'); % Evaluate atomic colorscale in workspace     
     Object.Parent.Tag = 'Atomic Colorscale'; % Set atomic colorscale to current object
     
 end
 
 assignin('base','Object',Object); % Assign current object in workspace
 
 switch Object.Parent.Tag % Distinguish the different kind of subsystem temperatures
     
     case 'Electronic Colorscale' % Electronic Subsystem is chosen
 
        ax = evalin('base','ax1'); % Evaluate electronic axes in workspace
        sp = findobj(ax.Children,'Type','Surface'); % Find surface plot containing electronic subsystem axes
        cb = findobj(Fig,'Type','ColorBar','Tag','Electronic Colorbar'); % Evaluate electronic colorbar in workspace
        cts = findobj(Fig,'Type','Contour','Tag','Electronic Contour'); % Evaluate electronic contours in workspace
 
        z_data = abs(evalin('base','Electronic_Temperature')); % Set Z-Data to electronic subsystem temperatures
        
        Contour_Lines = Contour_Lines_1; % Set contour lines to electronic subsystem contours       
        Log_Contours = LogSpace([min(min(z_data)),max(max(z_data))],Contour_Lines_1);
        % Define logarithmic distributed contour lines
        Lin_Contours = linspace(min(min(z_data)),max(max(z_data)),Contour_Lines_1);
        % Define linear distributed contour lines
        
     otherwise % Atomic Subsystem is selected
                 
        ax = evalin('base','ax2'); % Evaluate atomic axes in workspace
        sp = findobj(ax.Children,'Type','Surface'); % Find surface plot containing atomic subsystem axes
        cb = findobj(Fig,'Type','ColorBar','Tag','Atomic Colorbar'); % Evaluate atomic colorbar in workspace
        cts = findobj(Fig,'Type','Contour','Tag','Atomic Contour'); % Evaluate atomic contours in workspace
        
        z_data = abs(evalin('base','Lattice_Temperature')); % Set Z-Data to atomic subsystem temperatures
        
        Contour_Lines = Contour_Lines_2; % Set contour lines to atomic subsystem contours      
        Log_Contours = LogSpace([min(min(z_data)),max(max(z_data))],Contour_Lines_2);
        % Define logarithmic distributed contour lines
        Lin_Contours = linspace(min(min(z_data)),max(max(z_data)),Contour_Lines_2);
        % Define linear distributed contour lines
 
 end 
 
 switch Object.Label % Distinguish different object lables
     
     case 'logarithmic' % Logarithmic scaling is requested
         
         sp.ZData = log10(z_data); % Set surface plot temperatures to logarithmic Z-Data
         sp.CData = log10(z_data); % Set surface plot colordata to logarithmic Z-Data        
                          
         cmap = evalin('base','cmap'); % Evaluate current colormap in workspace        
         colormap(ax,cmap(Contour_Lines - 1)); % -1 due to contour lines - 1                             
                
         cts.ZData = log10(z_data); % Set contour data due to logarithmic Z-Data
         cts.LevelList = log10(Log_Contours); % Set contour levels to logarithmic contour lines
                  
         cb.Ticks = log10(Log_Contours); % Set colorbar ticks to contour levels
         cb.TickLabels = round(Log_Contours,2,'significant'); % Round colorbar tick labels to significant values
         
         caxis(ax,log10([Log_Contours(1) Log_Contours(end)])); % Apply a logarithmic scaled colour axis
                  
     otherwise % Linear scaling is requested
                  
         sp.ZData = z_data; % Set surface plot temperatures to Z-Data
         sp.CData = z_data; % Set surface plot colordata to Z-Data
         
         set(ax,'CLim',[min(min(sp.ZData)), max(max(sp.ZData))]);         
                  
         cmap = evalin('base','cmap'); % Evaluate current colormap in workspace
         colormap(ax,cmap(Contour_Lines - 1)); % - 1 due to contour lines - 1         
                  
         cb.Ticks = Lin_Contours; % Set colorbar ticks to contour levels
         cb.TickLabels = round(Lin_Contours,2,'significant'); % Round colorbar tick labels to significant values
                 
         cts.ZData = z_data; % Set contour data due to Z-Data
         cts.LevelList = Lin_Contours; % Set contour levels to linear contour lines
                           
 end

 assignin('base',strrep(Object.Parent.Tag,' ','_') ,Object.Label); % Assign colorscale labels in workspace
 
end