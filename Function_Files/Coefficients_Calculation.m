function [C_e, D_e, K_e, C_a, K_a, g, A, B] = Coefficients_Calculation()
% User defined function that calculates PDE coefficients according to both subsystem 
% temperatures ( electronic as well as atomic ) and the selceted target material.

%% Read neccessary parameters

T_e = evalin('base','Electronic_Temperature'); % Evaluate electronic temperature in workspace
T_a = evalin('base','Lattice_Temperature'); % Evaluate atomic temperature in workspace

r_1 = evalin('base','Radial_Mesh_1'); % Evaluate spatial meshgrid ( radius values ) in workspace
r_2 = evalin('base','Radial_Mesh_2');

t_1 = evalin('base','Time_Mesh_1'); % Evaluate time meshgrid ( time values ) in workspace
t_2 = evalin('base','Time_Mesh_2');

Properties = evalin('base','Thermal_Properties'); % Get thermal properties from workspace
Material = evalin('base','Material_Selection'); % Get selected target material from workspace

%% Calculate PDE coefficients

C_e = Electron_Heat_Calculation(T_e,T_a); % Calculate the electronic heat capacity

D_e = Electron_Thermal_Diffusivity(T_e); % Calculate the electronic thermal diffusivity

K_e = Electron_Thermal_Conductivity_Calculation(T_e,T_a); 
% Calculate the electronic thermal heat conductivity

C_a = Lattice_Heat_Calculation(T_a,Properties,Material); % Calculate the atomic heat capacity

K_a = Lattice_Thermal_Conductivity_Calculation(T_a,Properties,Material);
% Calculate the atomic thermal heat conductivity

g = Coupling_Factor_Calculation(T_e,T_a); % Determine the subsystem coupling factor

[radius_1,time_1] = meshgrid(r_1,t_1); % Compute a meshgrid from given spatial and time values
[radius_2,time_2] = meshgrid(r_2,t_2); 

A = Electronic_Energy_Density_Calculation(radius_1,time_1);
% Calculate the electronic energy density per unit time
B = Atomic_Energy_Density_Calculation(radius_2,time_2); 
% Calculate the atomic energy density per unit time

end