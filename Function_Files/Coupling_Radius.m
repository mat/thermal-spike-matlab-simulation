function Output = Coupling_Radius( Input, Direction )
% User defined function to determine the subsystem's coupling radius based
% on subsystem identifier and ion energy. If the input data is ion energy, 
% the function calculates a respective coupling radius of given subsystem.
%
%    Type   = 'Electronic' or 'Atomic'
%
%   Input   = Ion energy or coupling radius
% 
% Direction = 'Energy' or 'Radius'
 

%% Control input data

 if Input < 0 % Avoid negative input data
    
    Input = 0; % Set input value to zero
    
 end
 
 %% Interpolation values
 
 Energy = [ 0.00, 0.01, 1.00, 1000, 10000]; % Mesh points to interpolate energy values
 Radius = [ 0.00, 1.00, 3.00, 1.00, 0.000]; % Mesh points to interpolate radius values
 

 %% Calculation direction settings
 
 if isequal( Direction, 'Energy' ) % Detect swift heavy ion energy input 

    Output = interp1( Energy, Radius, Input, 'linear', 'extrap'); 
    % Determine a coupling radius based on swift heavy ion energy input  
    
 elseif isequal( Direction, 'Radius' ) % Detect coupling radius input
     
     Output = interp1( Radius, Energy, Input, 'linear', 'extrap');
     % Determine a swift heavy ion energy based on coupling radius input
     
 end
 
 %% Generate output data
 
 if Output < 0 % Avoid negative output data
    
    Output = 0; % Set output value to zero
    
 end 
 
 Output = round(Output,2,'significant'); % Round output data to a significance of three
      
end