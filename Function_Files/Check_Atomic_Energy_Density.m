function Atomic_Energy_Loss = Check_Atomic_Energy_Density(Radial_Mesh,Time_Mesh)
% This user defined function accumulates differentially atomic energy losses
% which will afterwards be used to classify simulation results accuracy.

global r_n_0, global t_n_0, global sigma_n_t, global S_n, global c; % Load global variables

[gridsize_r,gridsize_t] = size(evalin('base','ode_sol.y')); 
% Determine grid sizes for time and space according to the size of ODE solution

multiplier = 5; % Number of calculation points = max. order of ode15s solver

r_mesh = linspace(min(Radial_Mesh), max(Radial_Mesh), dot(multiplier,gridsize_r));
% Generate a linear spatial vector with respect to parameter settings and grid size

t_mesh = LogSpace([min(Time_Mesh), max(Time_Mesh)], dot(multiplier,gridsize_t));
% Generate a logarithmic time vector with respect to parameter settings and grid size

[dr,dt] = meshgrid(gradient(r_mesh),gradient(t_mesh)); % Meshgrid containing gradient vectors
[r,t]= meshgrid(r_mesh,t_mesh); % Create a meshgrid that consists of values in time and space

Expression = c * 2 * pi * S_n .* exp( -1/2 * ( ( t - t_n_0 ) / sigma_n_t ).^2 ) .* exp( - r / r_n_0 ) .* dr .* dt;
% Determine atomic energy losses according to specific distributions in time and space.
% Time distribution     =     << normalized gaussian >>
% Spatial distribution  =  << scaled  exponetiell decay >>

Atomic_Energy_Loss = dot(Expression,ones(size(Expression)));
Atomic_Energy_Loss = dot(Atomic_Energy_Loss,ones(size(Atomic_Energy_Loss)))...
                        * 10^(-12) / ( 1.602 * 10^(-19) );
% Accumulate all atomic energy losses by applying << dot product >> 
% Unit: keV/nm

end