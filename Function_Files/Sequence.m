function Sequence(~,~)
% A user defined function that displays a suquence ( video ) of both transient subsystem 
% temperatures with respect to the radial distance from the swift heavy ion impact axis.

warning('off'); % Set all kind of warning to off mode

Fig = evalin('base','Fig'); % Evaluate figure handle in workspace

Fig.Pointer = 'watch'; % Set figure pointer to watch sign

drawnow; % Refresh calculation routine

pause(1); % Stop processing for a second

drawnow; % Refresh calculation routine

 %% Figure properties and parameter settings

 Seq = figure(6); % Open an additional matlab figure
 Seq.Name = 'Thermal Spike - Video Sequence'; % Define the description of given figure
 Seq.NextPlot = 'replace'; % Replace open figure when called again 
 Seq.NumberTitle = 'off'; % No number title is displayed 
 Seq.Resize = 'off'; % Do not allow manual window resizing 
 Seq.MenuBar = 'none'; % Menubar is not available
 Seq.ToolBar = 'none'; % Toolbar is not available
 Seq.Units = 'normalized'; % Position units are set to normalized
 Seq.Position = [0.15 0.10 0.70 0.80]; % Define figure position on screen
 Seq.Color = 'w'; % Set the figure background to white color 
 Seq.CloseRequestFcn = @close_request;
 % Run callback function "@close_request" on any figure close request

 ax1 = evalin('base','ax1'); 
 ax2 = evalin('base','ax2'); 
 
 phi = linspace(0,2*pi,100); % Create a linear vector containing azimuth angles
 rho_1 = get(findobj(ax1.Children,'Type','Surface'),'XData'); 
 rho_2 = get(findobj(ax2.Children,'Type','Surface'),'XData');
 % Get linear distributed radius from temperature surface plots
 time_1 = get(findobj(ax1.Children,'Type','Surface'),'YData'); 
 time_2 = get(findobj(ax2.Children,'Type','Surface'),'YData');
 % Get logarithmic time values from temperature surface plots

 Electronic_Temperature = evalin('base','Electronic_Temperature');
 Lattice_Temperature = evalin('base','Lattice_Temperature');
 % Evaluate both subsystem temperatures ( electronic & atomic ) in Matlab workspace

 Panel_1 = uipanel(Seq,'Title','','TitlePosition','centertop','FontSize',11,'Units','normalized',...
                       'BorderType','etchedout','BorderWidth',2,'Position',[0.01 0.30 0.98 0.68]);
 % Create a user interface panel that contains all subplot properties

 time = time_1;
 
 Contour_Lines = 13; 
 % Set temperature scale contour lines variable
 
 %% Subplot 1: Electronic Subsystem

 position_1 = [0.07, 0.15, 0.40, 0.70]; % Define normalized position of first subplot
 subplot('Position',position_1,'parent',Panel_1); % Create a first subplot of above uipanel

 X_1 = zeros(length(phi),length(rho_1)); % Initialize x-values of electronic subsystem
 Y_1 = zeros(length(phi),length(rho_1)); % Initialize x-values of electronic subsystem
 Temp_1 = ones(size(X_1)); % Initialize electronic temperatures in cartesian coordinates

 Ax_1 = Seq.CurrentAxes; % Get the axes of current sequence subplot

 P_1 = surf(Ax_1,X_1,Y_1,log10(Temp_1)); % Create a initial surface plot

 shading(Ax_1,'interp'); % Interpolate shadings of first axes plot
 box(Ax_1,'on'); % Draw a rectangular box around first subplot axis
 set(Ax_1,'Color','none'); % Remove background color of first subplot 

 Ax_1.XLim = 1.1 * [-rho_1(end), rho_1(end)]; % Set x-axis limits
 Ax_1.YLim = 1.1 * [-rho_1(end), rho_1(end)]; % Set y-axis limits

 colmap = str2func('hot'); % Set the colormap to << hot >> appearance
 colormap(Ax_1,colmap(128)); % Use a 128 Bit sized colormap
 
 view(Ax_1,[0 90]); % Apply a two dimensional view using colormap values 
 hold(Ax_1,'on'); % Hold on current axis settings
  
 Contours_1 = LogSpace([min(min(Electronic_Temperature)),max(max(Electronic_Temperature))],Contour_Lines); 
 colormap(Ax_1,colmap(128)); % Reset the colormap of first subplot axes

 cb1 = colorbar(Ax_1,'TickLabelInterpreter','latex','FontSize',10); % Display a colorbar
 cb1.Label.String = '$$\mathrm{Temperature\;\;T_e\;\;\left(K\right)}$$'; % Colorbar label
 cb1.Label.Interpreter = 'latex'; % Set latex colorbar interpreter
 cb1_pos = get(cb1,'Position'); % Get temporary position of electronic colorbar
 cb1.Label.Units = 'normalized'; % Set colorbar label units to normalized
 cb1.Label.Position = [cb1_pos(1), cb1_pos(2) + cb1_pos(4) + 0.27, 0]; 
 cb1.Label.Rotation = 0; % Rotate the label of electronic subsystem colorbar
 cb1.Ticks = log10(Contours_1); % Set logaritmic distributed colorbar steps
 cb1.TickLabels = round(Contours_1,2,'significant'); % Set the manual colorbar labels 
 cb1.TickLength = 0.095; % Set length of the horizontal colorbar ticks
 caxis(Ax_1,log10([Contours_1(1) Contours_1(end)])); 
 %Define logarithmic color axis of given surface plot

 Ax_1.Layer = 'top'; % Bring axes of first subplot to the top
 grid(Ax_1,'on'); % Set cartesian grid lines
 Ax_1.GridColor = 'b'; % Draw grid lines using blue color
 
 xlabel(Ax_1,'x $$\left(nm\right)$$','Interpreter','latex'); % Set x-axis label of electronic subsystem
 ylabel(Ax_1,'y $$\left(nm\right)$$','Interpreter','latex'); % Set y-axis label of electronic subsystem
 title(Ax_1,'Electronic Subsystem','Interpreter','latex'); % Set title of electronic subsystem

 
 %% Subplot 2: Atomic Subsystem

 position_2 = [0.58, 0.15, 0.40, 0.70]; % Define normalized position of second subplot
 subplot('Position',position_2,'parent',Panel_1); % Create a second subplot of above uipanel

 X_2 = zeros(length(phi),length(rho_2)); % Initialize x-values of atomic subsystem
 Y_2 = zeros(length(phi),length(rho_2)); % Initialize y-values of electronic subsystem
 Temp_2 = ones(size(X_2)); % Initialize atomic temperatures in cartesian coordinates

 Ax_2 = Seq.CurrentAxes; % Get the axes of current sequence subplot
 
 P_2 = surf(Ax_2,X_2,Y_2,log10(Temp_2)); % Create a initial surface plot

 shading(Ax_2,'interp'); % Interpolate shadings of second axes plot
 view(Ax_2,[0 90]); % Apply a two dimensional view using colormap values
 hold(Ax_2,'on'); 
 box(Ax_2,'on'); % Draw a rectangular box around second subplot axis
 set(Ax_2,'Color','none'); % Remove the background color of second subplot

 Ax_2.XLim = 1.1 * [-rho_2(end), rho_2(end)]; % Set x-axis limits
 Ax_2.YLim = 1.1 * [-rho_2(end), rho_2(end)]; % Set y-axis limits
 
 colmap = str2func('hot'); % Set the colormap to << hot >> appearance
 colormap(Ax_2,colmap(128)); % Use a 128 Bit sized colormap
 
 Contours_2 = LogSpace([min(min(Lattice_Temperature)),max(max(Lattice_Temperature))],Contour_Lines);
 
 cb2 = colorbar(Ax_2,'TickLabelInterpreter','latex','FontSize',10); % Drwa a colorbar
 cb2.Label.String = '$$\mathrm{Temperature\;\;T_a\;\;\left(K\right)}$$'; % Colorbar label
 cb2.Label.Interpreter = 'latex';  % Set latex colorbar interpreter
 cb2_pos = get(cb2,'Position'); % Get temporary position of atomic colorbar
 cb2.Label.Units = 'normalized'; % Set colorbar label units to normalized
 cb2.Label.Position = [cb2_pos(1) - 0.85 , cb2_pos(2) + cb2_pos(4) + 0.27, 0];
 % Change atomic subsystem colormap position
 cb2.Label.Rotation = 0; % Rotate the label of atomic subsystem colorbar    
 cb2.Ticks = log10(Contours_2); % Set logarithmic distributed colorbar steps
 cb2.TickLabels = round(Contours_2,2,'significant'); % Set the manual colorbar labels
 cb2.TickLength = 0.095; % Set length of the horizontal colorbar ticks
  
 caxis(Ax_2,log10([Contours_2(1) Contours_2(end)])); % Set color axis limits
 
 Ax_2.Layer = 'top'; % Bring axes of second subplot to the top
 grid(Ax_2,'on'); % Set cartesian grid lines
 Ax_2.GridColor = 'b'; % Draw grid lines using blue color
 
 xlabel(Ax_2,'x $$\left(nm\right)$$','Interpreter','latex'); % Set x-axis label of atomic subsystem
 ylabel(Ax_2,'y $$\left(nm\right)$$','Interpreter','latex'); % Set y-axis label of atomic subsystem
 title(Ax_2,'Atomic Subsystem','Interpreter','latex'); % Set title of atomic subsystem

 
 %% User Control Panel

 Panel_2 = uipanel(Seq,'Title','','TitlePosition','lefttop','FontSize',12,'Units','normalized',...
     'BorderType','etchedout','BorderWidth',2,'Position',[0.01 0.02 0.98 0.26],...
     'FontName','Cambria','ForegroundColor',[0.15 0.15 0.15]);
 % Create a user interface panel that contains meaningful user control properties

 % Determine number of simulation time steps  
 lower = 1; % Initialize lower time step threshold ( = min. time )
 step = 1; % Initialize time stepwidth ( = time difference )
 upper = length(time); % Initialize upper time step threshold ( = max. time )
 
 Sld = uicontrol(Seq,'Style','slider','Min',1,'Max',upper,'Value',1,'BackgroundColor','w',...
          'Units','normalized','Position',[0.18 0.12 0.60 0.03],'String','Simulation Time', ...
          'Callback',@Time_Control,'SliderStep',[1/(upper-1) , 1/(upper-1)],...
          'Tag','Electronic Subsystem');
 % Create a user control slider that shows and manipulates the current simulation time
 
 uicontrol(Sld.Parent,'Style','text','Units','normalized','Tag','Time Display','Position',...
      Sld.Position + [0.0 0.04 0 0],'ForegroundColor','k','FontSize',11,'FontName','Cambria');
 % Create a user information text label that characterizes the simulation times
    
 Btn = uicontrol(Seq,'Style','pushbutton','String','','Units','normalized','Backgroundcolor','w',...
            'Position',[0.04 0.085 0.10 0.14],'Callback',@Play_button_pressed);
 % Create a user interface pushbutton to launch the video sequence ( = play )
  
 Button_Icon(Btn,'Play_Icon.jpg','jpg',[90,90]); % Display a play button symbol
 
 Subpanel = uipanel(Panel_2,'Title','','TitlePosition','lefttop','Units','normalized',...
     'BorderType','etchedout','BorderWidth',2,'BackgroundColor',[0.92 0.92 0.92],...
     'Position',[0.81 0.06 0.18 0.88],'ForegroundColor',[0.15 0.15 0.15]);
 
 uipanel(Subpanel,'Title','','TitlePosition','lefttop','Units','normalized',...
     'BorderType','etchedout','BorderWidth',2,'BackgroundColor',[0.98 0.98 0.98],'Position',...
     [0.00 0.00 1.00 0.75],'ForegroundColor',[0.15 0.15 0.15]);

 uipanel(Panel_2,'Title','Start video','TitlePosition','centerbottom','Units','normalized',...
     'FontSize',11,'BorderType','etchedout','BorderWidth',2,'BackgroundColor',[0.92 0.92 0.92],...
     'Position',[0.01 0.05 0.137 0.88],'ForegroundColor',[0.15 0.15 0.15],'FontName','Cambria');
 
 txt = annotation(Subpanel,'textbox',[0.05 0.72 0.90 0.25],'String','Select any Subsystem');
 set(txt,'Interpreter','latex','LineStyle','none','FontSize',10.5,'EdgeColor','k');
 set(txt,'HorizontalAlignment','center','VerticalAlignment','middle');
  
 if isequal(time_1,time_2) % Electronic timescale and atomic timescale are equal
     
     select = {'Electronic Subsystem','Atomic Subsystem','Both Subsystems'};
     % Define a selction entry containing at least three different possibilities
    
 else
     
     select = {'Electronic Subsystem','Atomic Subsystem'};
     % Only two selection entries are meaningful   
 end
 
 uicontrol(Subpanel,'Style','popup','String',select,'Units','normalized',...
     'Position',[0.08 0.45 0.86 0.20],'FontName','Cambria','FontWeight','bold','FontSize',8.5,...
     'Tag','Subsystem Selection','Callback',@Subsystem_Selection,'ForegroundColor',[0.1 0.1 0.1],...
     'BackgroundColor',[0.98 0.98 0.98]);
 
 Speed = uicontrol(Panel_2,'Style','popup','String',{'1x','2x','3x','5x'},'Units','normalized',...
            'Backgroundcolor','w','Position',[0.25 0.15 0.05 0.10],'FontName','Cambria',...
            'FontWeight','bold','FontSize',8.5,'Tag','Speed','ForegroundColor',[0.1 0.1 0.1],...
            'BackgroundColor',[0.98 0.98 0.98]);
 % Create a popupmenu to select the speed of a video sequence
 
 annotation(Panel_2,'textbox',[0.18 0.11 0.07 0.10],'String','Speed:','Interpreter','latex',...
     'LineStyle','none','FontSize',10.5,'EdgeColor','k','HorizontalAlignment','center',...
     'VerticalAlignment','middle');
 
 window_request = false; % Initialize the window request to false ( = enable video )
  
 Time_Value = time(Sld.Value); % Get slider time value
 Time_String = strcat('Time after ion impact: ',{'  '},sprintf('%.2e\n',Time_Value),' s');
 set(findobj(Seq,'Tag','Time Display'),'String',Time_String);  
 
 function Subsystem_Selection(Obj,~)
 % Distinguish a subsystem selection
        
   strings = Obj.String; % Get all entries values of selection popupmenu
   selection = char(strings(Obj.Value)); % Determine selected subsystem
   
   Sld.Value = 1; % Set intial slider value
   
   if isequal(selection,'Electronic Subsystem') % Electronic subsystem is selected
            
       Sld.Tag = 'Electronic Subsystem'; % Set slider tag to electronic subsystem
       upper = length(time_1); % Determine upper time step threshold ( = max. time )
                    
       Time_Value = time_1(Sld.Value); % Obtain the current simulation time from given slider value
             
   elseif isequal(selection,'Atomic Subsystem') % Atomic subsystem is selected
 
      Sld.Tag = 'Atomic Subsystem'; % Set slider tag to atomic subsystem
      upper = length(time_2); % Determine upper time step threshold ( = max. time )
            
      Time_Value = time_2(Sld.Value); % Obtain the current simulation time from given slider value
       
   else % Both subsystem are requested
       
       Sld.Tag = 'Both Subsystems'; % Set slider tag to both subsystems    
       upper = length(time_1); % Determine upper time step threshold ( = max. time )
       Time_Value = time_1(Sld.Value); % Obtain the current simulation time from given slider value
      
   end
   
   Sld.Max = upper; % Set maximum slider value to upper time threshold
   Sld.SliderStep = [1/(upper-1) , 1/(upper-1)]; % Set slider steps according to maximum time values
   
   Temp_1 = ones(size(X_1)); % Initialize electronic temperatures array
   P_1.ZData = []; % Set electronic subsystem Z-Data to empty values
   
   Temp_2 = ones(size(X_2)); % Initialize atomic temperatures array
   P_2.ZData = []; % Set atomic  subsystem Z-Data to empty values
   
   Time_String = strcat('Time after ion impact: ',{'  '},sprintf('%.2e\n',Time_Value),' s');
   set(findobj(Seq,'Tag','Time Display'),'String',Time_String);   
   % Display current timesteps at the bottom of sequence window 
   
 end
 
 function Play_button_pressed(~,~)
 % A nested user defined function to launch the video sequence
    
   Source = findobj(Seq,'Type','uicontrol','Tag','Subsystem Selection');
   strings = Source.String;
   selection = char(strings(Source.Value));
        
   Btn.Enable = 'inactive'; % Disable user button control
   Sld.Enable = 'inactive'; % Disable user slider control
   Speed.Enable = 'inactive'; % Disbale user popupmenu
   Source.Enable = 'inactive'; % Disbale user popupmenu
   Time_Iteration_Loop(selection,lower,step,upper); % Run the time iteration loop ( = sequence calculation )
      
 end
 
 Fig.Pointer = 'arrow'; % Set mouse pointer to arrow symbol

 drawnow; % Refresh calculation routine

 evalin('base','clc'); % Clear maltlab command window screen
 
 %% Time Iteration Loop

 function Time_Iteration_Loop(selection,lower,step,upper)
 % A nested user defined function to calculate the video sequence with
 % respect to given time thresholds and time stepwidths.

  speed_strings = Speed.String; 
  speed_value = char(speed_strings(Speed.Value));   
  speed_selection = str2double(speed_value(~isletter(speed_value)));      
  speed_index = double(25/speed_selection);
  
  for i = lower : step : upper % Iterate time steps within given time thresholds
      
      if isequal(window_request,true) % Window close request is active
          
          break; % Skip loop iteration
          
      end
    
      % ---- Window close request is inactive ----
      
      if any([isequal(selection,'Electronic Subsystem'),isequal(selection,'Both Subsystems')])
      
         Temperature_1 = Electronic_Temperature(i,:); % Get electronic subsystem temperatures
         [~,Temp_1] = meshgrid(phi,Temperature_1); 
         % Build a two dimensional meshgrid of electronic temperatures and azimuth angles
         [Phi,Rho_1] = meshgrid(phi,rho_1); 
         % Build a two dimensional meshgrid of radius values and azimuth angles
         [X_1,Y_1] = pol2cart(Phi,Rho_1); 
         % Convert polar coordinates of the electronic subsystem into cartesian coordinates
         
         time = time_1;
         
         P_1.XData = X_1; % Assign electronic subsystem x-values to electronic subplot
         P_1.YData = Y_1; % Assign electronic subsystem y-values to electronic subplot
         P_1.ZData = log10(Temp_1); % Assign electronic subsystem temperatures to same subplot
         
      end
                  
      if any([isequal(selection,'Atomic Subsystem'),isequal(selection,'Both Subsystems')])
                   
         Temperature_2 = Lattice_Temperature(i,:); % Get atomic subsystem temperatures
         [~,Temp_2] = meshgrid(phi,Temperature_2);
         % Build a two dimensional meshgrid of atomic temperatures and azimuth angles
         [Phi,Rho_2] = meshgrid(phi,rho_2);       
         % Build a two dimensional meshgrid of radius values and azimuth angles
         [X_2,Y_2] = pol2cart(Phi,Rho_2);
         % Convert polar coordinates of the  atomic subsystem into cartesian coordinates
         
         time = time_2;
         
         P_2.XData = X_2; % Assign atomic subsystem x-values to atomic subplot
         P_2.YData = Y_2; % Assign atomic subsystem y-values to atomic subplot
         P_2.ZData = log10(Temp_2); % Assign atomic subsystem temperatures to same subplot
            
      end    
                 
      Sld.Value = i; % Set current slider value as loop iteration index
      Time_Value = time(Sld.Value); % Obtain the current simulation time from given slider value
      
      Time_String = strcat('Time after ion impact: ',{'  '},sprintf('%.2e\n',Time_Value),' s');
      set(findobj(Seq,'Tag','Time Display'),'String',Time_String);
      % Display the current simulation time inside the user information text label
                  
      pause(speed_index/upper); % Apply a short pause before going on with loop iteration
      
      if isequal(i,upper) % Check if last iteration step is reached
          
          Btn.Enable = 'on'; % Enable user button control
          Sld.Enable = 'on'; % Enable user slider control
          Speed.Enable = 'on'; % Enable user popupmenu
          
          Source = findobj(Seq,'Type','uicontrol','Tag','Subsystem Selection');
          Source.Enable = 'on'; % Enable user popumenu
          
          evalin('caller','lower = 1;'); % Reset the lower time threshold to initial settings
          
      end
      
      % -----------------------------------------------
            
  end
    
 end

 function Time_Control(hObject,~)
 % A nested user defined function that reacts on any user interaction with
 % the slider control obejct.
     
    lower = round(hObject.Value,0); % Set the lower time threshold to chosen slider value
    upper = lower; % Set upper time threshold equal to the lower threshold to avoid sequences
         
    Time_Iteration_Loop(Sld.Tag,lower,1,upper); 
    % Run the time iteration loop with respect to chosen time thresholds
    
    Source = findobj(Seq,'Type','uicontrol','Tag','Subsystem Selection');
    strings = Source.String;
    selection = char(strings(Source.Value));
    
    if isequal(selection,'Atomic Subsystem')
        
        upper = length(time_2);
        
    else
        
        upper = length(time_1);        
            
    end
      
 end

 function close_request(hObject,~)
 % Close request function to display a question dialog box 

    selection = questdlg('Do you want to exit the video sequence?',...
       'Close Request','Yes','No','Yes'); % close request text message and possible answers
  
    switch selection % React on answer of question dialog
       
       case 'Yes'  % User pressed << yes >> button
          
          delete(hObject) % Delete GUI figure
          
          window_request = true;
          
          warning('on');
                          
       case 'No'  % User pressed << no >> button
          
         return  % Return to GUI
      
    end
   
 end 

end

