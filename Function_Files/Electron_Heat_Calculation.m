function [Electron_Heat_Capacity] = Electron_Heat_Calculation(Electronic_Temperature,Lattice_Temperature)
% User defined function to determine the electron heat capacity due to both subsystem 
% temperatures and material properties ( eletron density ). Therefore the assumptions 
% of a quasi-free electron gas is used, scaled by a respective electron fermi energy.

 global Boltzmann_Constant; % Boltzmann Constant, k_B ,       Value:  1.38 * 10^-23 , Unit: J/K;
 global Planck_Constant;    % Planck Constant, h_bar  ,       Value:  1.05 * 10^-34 , Unit: Js;
 global Electron_Mass;      % Resting mass of Electron, m_e , Value:  9.31 * 10^-31 , Unit: kg

 %% Calculation routine
 
 [Electron_Density,~] = Particle_Density_Calculation(Lattice_Temperature); 
 % Calculate the actual electronic density, Unit: m^-3
 
 Fermi_Energy = Planck_Constant^2 / ( 2 * Electron_Mass ) * ( 3 * pi^2 * Electron_Density )^(2/3);
 % Fermi level of the Electrons E_F , Value: calculated , Unit: J

 assignin('base','Fermi_Energy',Fermi_Energy); % Transfer fermi energy to workpace

 Fermi_Temperature = Fermi_Energy / Boltzmann_Constant; 
 % Calculate a repsective fermi temperature according to a given fermi energy level. Unit: K 

 assignin('base','Fermi_Temperature',Fermi_Temperature'); % Transfer fermi temperature to workpace

 gamma = pi^2 * Boltzmann_Constant^2 * Electron_Density / ( 2 * Fermi_Energy );
 % Gamma factor for thermal conductivity , Value: calculated , Unit: J / ( m^3 * K^2 )

 T_lim = ( 3 / 2 ) * Electron_Density * Boltzmann_Constant / gamma; 
 % Estimate electronic heat capacity temperature limitation

 Electron_Heat_Capacity = gamma * Electronic_Temperature;
 % Determine the elctronic heat capacity, Unit: J / ( m^3 * K )

 lim = arrayfun(@(x) x > T_lim, Electronic_Temperature);             
 % Check actual electronic temperature to exceed respective temperature limit

 Electron_Heat_Capacity(lim) = 3 / 2 * Boltzmann_Constant * Electron_Density;
 % Determine the elctronic heat capacity, Unit: J / ( m^3 * K )

 if evalin('base','exist(''subs_coeff'',''var'')') 
 % Check if coefficients substitution variable does exist in workspace

     coeff_data = evalin('base','coeff_data'); % Read coefficients data in workspace

     if isequal(coeff_data{1,4},'Yes') 
     % Neccessary to apply substitution of electron heat capacity    

             Electron_Heat_Capacity = ones(size(Electronic_Temperature)) * ...
                    10^(6) * str2double(coeff_data{1,3});
             % Set electron heat capacity to values of coefficients substitution

     end

 end
 
 %% Data Output
 
 Electron_Heat_Capacity( Electron_Heat_Capacity < 0 ) = 0; % Avoid negative output values

end