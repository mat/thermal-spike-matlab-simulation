function [Coupling_Factor] = Coupling_Factor_Calculation(Electronic_Temperature,Lattice_Temperature)
% This user defined function mainly calculates the coupling factor g according to thermal 
% properties and both subsystem temperatures ( electronic temperature & atomic temperature ). 

 global Boltzmann_Constant;  % Boltzmann Constant, k_B ,  Value:  1.38 * 10^-23 ,   Unit: J/K;
 global Planck_Constant;     % Planck Constant, h_bar  ,  Value:  1.05 * 10^-34 ,   Unit: Js;
 global lambda;         % Electron_Phonon path, lambda ,  Value:  tunable       ,   Unit: nm
 % Load global constants for functional use

 %% Calculation routine
 
 Electron_Thermal_Conductivity = Electron_Thermal_Conductivity_Calculation(Electronic_Temperature,Lattice_Temperature);
 % Determine an electronic thermal conductivity according to the electronic system temperature. Unit: W/m
 
 Thermal_Properties = evalin('base','Thermal_Properties'); % Get thermal properties from workspace
 Material_Selection = evalin('base','Material_Selection'); % Get target material selection from workspace
  
 if all([~isequal(Thermal_Properties.(Material_Selection).('Type'),'Metal'),...
        isfield(Thermal_Properties.(Material_Selection),'Mean_free_path')])
             
     Coupling_Factor = Electron_Thermal_Conductivity ./ ( 10^-9 * lambda )^2;
    
 else

     Debye_Temperature = Thermal_Properties.(Material_Selection).Debye_Temperature; % T_D , Value: fixed , Unit: K
     % Get the Debye Temperature according to different type of metal

     [Electron_Density,Atomic_Density] = Particle_Density_Calculation(Lattice_Temperature);
     % Determine an electronic and atomic density with respect to chosen material properties. Unit: m^-3

     Sound_Velocity = Boltzmann_Constant * Debye_Temperature / ( Planck_Constant * ( 6 * pi^2 * Atomic_Density )^(1/3) );
     % Calculate the velocity of sound v by using atomic density n_a and debye temperature T_D. Unit: m/s
     assignin('base','Sound_Velocity',Sound_Velocity);
     
     Coupling_Factor = pi^4 * ( Boltzmann_Constant * Electron_Density * Sound_Velocity )^2 ./ ( 18 * Electron_Thermal_Conductivity );
     % Generate the electron-phonon coupling factor g with respect to electronic density n_e, velocity 
     % of sound v and electronic thermal conductivity K_e. Unit: W/(K*m^3)
    
 end
 
 if evalin('base','exist(''subs_coeff'',''var'')')  % Substitution variable already exist in workspace ?

     coeff_data = evalin('base','coeff_data'); % Read coefficients data from workspace

     if isequal(coeff_data{6,4},'Yes') % Check if coupling factor about to be substituted   
        
         Coupling_Factor = ones(size(Electronic_Temperature)) * ...
             10^(6) * str2double(coeff_data{6,3}); 
         % Use coefficients data and convert from characters into numeric values
        
     end
    
 end 
 
 %% Data output
 
 Coupling_Factor( Coupling_Factor < 0 ) = 0; % Avoid negative values of coupling factor
    
end