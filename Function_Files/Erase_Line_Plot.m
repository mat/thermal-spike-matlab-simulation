function Erase_Line_Plot(source,~)
% User defined function to erase all kind of data that belongs to previous
% line plot settings.

for k = 1 : 1 : length(source.Parent.Children)
                
    set(source.Parent.Children(k),'Checked','off'); 
    % Set uimenu << Draw a new line >> to unchecked state
                
end

set(source,'Checked','on','Enable','off'); % Set uimenu << Erase existing line >> to disabled state 
set(source.Parent.Children(2),'Enable','on'); % Set uimenu << Draw a new line >> to enable state
    
if evalin('base','exist(''L2'',''var'')') % Check that line plot variable exist in workspace 
    
    L2 = evalin('base','L2'); % Get handle to line plot variable from workspace
    
    Contexts = L2.UIContextMenu.Children; % Get handle to UIContextMenu of line plot curve
    delete(Contexts) % Delete handle to line plot curve 
    clear Contexts % Clear the UIContextMenu
        
end

delete(findall(gcf,'Type','Line','Tag','Line Temperature')); % Delete all temperature line plots
delete(findall(gcf,'Type','Scatter')); % Delete all sort of scatter data ( line endpoints )
delete(findobj(gcf,'Type','Legend'))   % Delete existing legend in line plot diagramm

evalin('base','clear L');
evalin('base','clear L1');
evalin('base','clear L2');
evalin('base','clear L3');
% Clear all variables in workspace that belong to the line plot

title(evalin('base','ax3'),'Temperature - Lineplot','Interpreter','latex');
set(evalin('base','ax3'),'XTick',[],'YTick',[],'XTickLabel',[],'YTickLabel',[]);
set(evalin('base','ax3'),'Box','on','Layer','top');          
xlabel(evalin('base','ax3'),'')
ylabel(evalin('base','ax3'),'')
% Reset line plot diagramm to origin settings

if evalin('base','exist(''ax4'',''var'')') 
% Check that hnadle to line axes variable already exist in workspace 
    
    set(evalin('base','ax4'),'XTick',[],'YTick',[],'XTickLabel',[],'YTickLabel',[]);
    xlabel(evalin('base','ax4'),'')
    ylabel(evalin('base','ax4'),'')
    % Reset line plot axes to origin settings

        
end

set(findobj(gcf,'Type','uimenu','Tag','Temperature Profile'),'Enable','off');
% Set uimenu << Temperature Profile >> to disabled state ( = member of save UIContextMenu )

end