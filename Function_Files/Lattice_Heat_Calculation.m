function [Lattice_Heat_Capacity] = Lattice_Heat_Calculation(Lattice_Temperature,Properties,Material)
 % User defined function to determine the latttice heat capacity of a selected material, 
 % based on temperature dependent polynomials.

 %% Read thermal thresholds, polynomials and material specific properties
 
 Heat_Thresholds = Properties.(Material).Lattice_Heat.Thresholds;
 Heat_Thresholds(strcmp(Heat_Thresholds,'inf') | strcmp(Heat_Thresholds,'Inf')) = {inf};
 Heat_Thresholds = cell2mat(Heat_Thresholds); % Read lattice heat temperature thresholds. Unit: K

 Lattice_Heat_Coefficients = cell2mat(Properties.(Material).Lattice_Heat.Polynomial);
 % Read lattice heat polynomial values from cell variable and convert into matrix format

 Solid_Mass_Density = Properties.(Material).Solid_Mass_Density * 10^3;    
 % Defined solid mass density of chosen target material. Unit: kg/m^3

 Liquid_Mass_Density = Properties.(Material).Liquid_Mass_Density * 10^3;    
 % Defined liquid mass density of chosen target material. Unit: kg/m^3

 Lattice_Heat_Capacity = zeros(size(Lattice_Temperature)); % Initialize lattice heat capacity matrix

 %% Calculate specific lattice heat capacity from polynomial values
 
 for i = 1 : 1 : size(Lattice_Temperature,1) % Iterate spatial dimension of lattice temperature
    
     for j = 1 : 1 : size(Lattice_Temperature,2) % Iterate time dimension of lattice temperature
    
         Temp_Vector = [ Lattice_Temperature(i,j)^(-3), Lattice_Temperature(i,j)^(-2), ...
                         Lattice_Temperature(i,j)^(-1), Lattice_Temperature(i,j)^( 0), ...
                         Lattice_Temperature(i,j)^( 1), Lattice_Temperature(i,j)^( 2), ...
                         Lattice_Temperature(i,j)^( 3), Lattice_Temperature(i,j)^( 4) ]; 
         % Define a temperature vector to a minimum power of minus two and a maximum power of four. Unit: K

         for k = 1 : 1 : length(Heat_Thresholds) 
         % Iterate different lattice heat thresholds sections
    
             if Lattice_Temperature(i,j) >= Heat_Thresholds(k,1) && Lattice_Temperature(i,j) < Heat_Thresholds(k,2)
             % Check actual temperature not to exceed given lattice heat thresholds
        
                 Polynomial = Lattice_Heat_Coefficients(k,:); 
                 % Lattice heat Capacity polynomial according to given setion
                
                 if Lattice_Temperature(i,j) < Properties.(Material).Melting_Point 
                 % Use solid mass density while below melting point of selected material
                
                     Lattice_Heat_Capacity(i,j) = 10^3 * Solid_Mass_Density * dot(Polynomial,Temp_Vector);
                     % Determine specific lattice heat capacity as a scalar multiplication of the temperature
                     % vetor , solid mass density and the polynomial vector. Unit: J/m^3
                    
                 else % Use liquid mass density while exceed melting point of selected material
                    
                    Lattice_Heat_Capacity(i,j) = 10^3 * Liquid_Mass_Density * dot(Polynomial,Temp_Vector);
                    % Determine specific lattice heat capacity as a scalar multiplication of the temperature
                    % vetor, liquid mass density and the polynomial vector. Unit: J/m^3
                    
                 end
            
             end
        
         end
        
     end
    
 end

 %% Apply coefficients substitution if neccessary
 
 if evalin('base','exist(''subs_coeff'',''var'')') 
 % Check if coefficients substitution variable does exist in workspace

      coeff_data = evalin('base','coeff_data'); % Read coefficients data in workspace

      if isequal(coeff_data{4,4},'Yes') % Neccessary to apply substitution of lattice heat capacity    

              Lattice_Heat_Capacity = ones(size(Lattice_Temperature)) * ...
                     10^(6) * str2double(coeff_data{4,3});
              % Set lattice heat capacity to values of coefficients substitution

      end

 end

  
 %% Data Output
 
 Lattice_Heat_Capacity( Lattice_Heat_Capacity < 0 ) = 0; % Avoid negative ouput values
 
end