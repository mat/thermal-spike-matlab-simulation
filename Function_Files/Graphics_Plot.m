function Graphics_Plot(Reset_Plot,Radial_Mesh_1,Radial_Mesh_2,Time_Mesh_1,Time_Mesh_2,Electronic_Temperature,Lattice_Temperature)
% User defined function that generates at least three diffrent kinds of subplot within the 
% main simulation graphical user interface. 

Fig = evalin('base','Fig'); % Get handle to simulation figure in workspace

if all([evalin('base','~exist(''Contour_Lines_1'',''var'')'), ...
        evalin('base','~exist(''Contour_Lines_2'',''var'')')])

    Contour_Lines_1 = 13; % Define the amount of electronic subsystem contour lines
    Contour_Lines_2 = 13; % Define the amount of atomic subsystem contour lines
    
    assignin('base','Contour_Lines_1',Contour_Lines_1); 
    assignin('base','Contour_Lines_2',Contour_Lines_2); 
    
else
    
    Contour_Lines_1 = evalin('base','Contour_Lines_1');
    Contour_Lines_2 = evalin('base','Contour_Lines_2');
    
    assignin('base','Contour_Lines_1',Contour_Lines_1); 
    assignin('base','Contour_Lines_2',Contour_Lines_2); 
    % Transfer contour lines variable to workspace
    
end
    
Electronic_Temperature = abs(Electronic_Temperature);
Lattice_Temperature = abs(Lattice_Temperature);

%% Create a subplot for the temperature of the electronic subsystem 

switch Reset_Plot % Distinguish plotting calculated data or plot reset 
    
    case false % Plot reset is not requested ( = plotting calculated data )
        
        ax1 = evalin('base','ax1'); % Get electronic subsystem axes from workspace

        P1 = surf(ax1,10^9*Radial_Mesh_1,Time_Mesh_1,log10(Electronic_Temperature));
        set(P1,'Tag','Electronic Subsystem');
        % Generate a surface plot based on spatial grid, time grid and electronic temperatures
        assignin('base','P1',P1); % Assign handle to electronic surface plot in workspace
                 
        set(ax1,'YScale','log','TickLabelInterpreter','latex'); 
        % Set axes vertical scale to logarithmic and use latex tick label interpreter
        ax1.XAxis.FontSize = 10.5; %  Set horizontal axis font size
        ax1.YAxis.FontSize = 11.5; % Set vertical axis font size
        axis(ax1,'tight'); % Set electronic subsystem axes to match axes limits 
        shading(ax1,'interp'); % Interpolate shading of electronic subsystem axes
               
        x1 = xlabel(ax1,'$$\mathrm{Radius\;\;r\;\;\left(nm\right)}$$','Interpreter','latex','FontSize',12);
        set(x1,'Units', 'Normalized','Position', [0.50, -0.10, 0]);                
        y1 = ylabel(ax1,'$$\mathrm{Time\;\;t\;\;\left(s\right)}$$','Interpreter','latex','FontSize',11);
        y1.Rotation = 0; % Rotate the vertical axis label of electronic subsystem
        set(y1, 'Units', 'Normalized', 'Position', [-0.05, 1.1, 0]);
        zlabel(ax1,'$$\mathrm{Electronic\;Temperature\;\;T_e\;\;\left(K\right)}$$','Interpreter','latex');
        c1 = colorbar(ax1,'TickLabelInterpreter','latex','FontSize',10.5,'Tag','Electronic Colorbar');
        % Set three dimensional axis labels and colorbar appearance of electronic subsystem
        cb_1 = uicontextmenu;
        c1.UIContextMenu = cb_1;
        Colormap_1_Select = uimenu(cb_1,'Label','Select Colormap','Tag','Electronic Colormap');
            uimenu('Parent',Colormap_1_Select,'Label','parula','Callback',@select_colormap);
            uimenu('Parent',Colormap_1_Select,'Label','hot','Callback',@select_colormap);
            uimenu('Parent',Colormap_1_Select,'Label','hsv','Callback',@select_colormap);
            uimenu('Parent',Colormap_1_Select,'Label','jet','Callback',@select_colormap);
            uimenu('Parent',Colormap_1_Select,'Label','gray','Callback',@select_colormap);
        % Add a UIContextMenu to electronic subsystem colormap to define a variety of colors
        
        Scaling_1 = uimenu(cb_1,'Label','Select Colorscale','Separator','on','Tag','Electronic Colorscale');
            uimenu('Parent',Scaling_1,'Label','linear','Callback',@Colorbar_Scaling);
            uimenu('Parent',Scaling_1,'Label','logarithmic','Callback',@Colorbar_Scaling);                   
            
        Contour_1 = uimenu(cb_1,'Label','Contour Quantity','Separator','on','Tag','Electronic Contour Lines');    
            uimenu('Parent',Contour_1,'Label','5','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','6','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','7','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','8','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','9','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','10','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','11','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','12','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','13','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_1,'Label','14','Callback',@Colorbar_Scaling);
        
        c1.Label.String = '$$\mathrm{Temperature\;\;T_e\;\;\left(K\right)}$$';
        c1.Label.FontSize = 11;
        c1.Label.Interpreter = 'latex'; % Set latex colorbar interpreter
        c1_pos = get(c1,'Position'); % Get temporary position of electronic colorbar
        c1.Position = c1_pos + [0.008,0, -0.008, 0];
        c1.Label.Units = 'normalized'; % Set colorbar label units to normalized
        c1.Label.Position = [c1_pos(1) - 0.5, c1_pos(2) + c1_pos(4) + 0.254, 0]; 
        % Change label position of the electronic subsystem colormap 
        c1.Label.Rotation = 0; % Rotate the label of electronic subsystem colorbar            
         
        ax1.Position = get(ax1,'Position') + [0, 0, -0.05, 0];
        
        try % Colorbar exist due to electronic subsystem data is plotted yet
            
            colormaps = findall(gcf,'Type','uimenu','Label','Select Colormap');
            children = findobj(colormaps,'-property','Children');
            ax1_label = get(findobj(children,'-Property','Checked','Checked','on'),'Label');
            
            % Find colormaps previously checked and thus deviant from default settings
            cmap = ax1_label{1};
            colormap(ax1,cmap(Contour_Lines_1 - 1)); % Activate colormap that was checked before
            assignin('base','cmap',cmap); % Assign current colormap label in workpspace
            
        catch % Colorbar does not already exist due to initial run or subplot data reset
            
            if evalin('base','exist(''cmap'',''var'')')
                
                cmap = evalin('base','cmap');
                
            else
                            
                cmap = str2func('jet');
                assignin('base','cmap',cmap); % Assign default colormap label in workpspace
                                
            end
            
            colormap(ax1,cmap(Contour_Lines_1 - 1)); % Set electronic subsystem colormap to default settings
            
        
        end                         
                    
        view(ax1,[0 90]) % View electronic subsystem axes in two dimensions
        set(ax1,'XLim',[ 0, 1.0 * 10^9*( max( Radial_Mesh_1 ) - min( Radial_Mesh_1 ) ) ]);
        % Set horizontal limit of electronic subsystem axes to 100 percent of maximum value
        grid(ax1,'on'); % Draw a grid within electronic subsystem
        hold(ax1,'on'); % Keep previous settings of electronic subsystem axes 
        
        colormap(ax1,flipud(colormap(ax1))); % Bring current colormap to the back of the stack
                               
        Contours = LogSpace([min(min(Electronic_Temperature)),max(max(Electronic_Temperature))],Contour_Lines_1);        
                    
        contour3(ax1,10^9*Radial_Mesh_1,Time_Mesh_1,log10(Electronic_Temperature),log10(Contours),'k',...
            'Tag','Electronic Contour');
        
        % Plot a certain number of contour lines with black color
        colormap(ax1,cmap(Contour_Lines_1 - 1));
                
        c1.Ticks = log10(Contours);
        c1.TickLength = 0.050;
        
        caxis(ax1,log10([Contours(1) Contours(end)]));
               
        if evalin('base','exist(''Electronic_Colorscale'',''var'')')
                            
            Color_Scale_1 = evalin('base','Electronic_Colorscale');
                                        
        else
                            
            Color_Scale_1 = 'logarithmic';
                                
        end          
       
        Object_1.Parent.Tag = 'Electronic Colorscale';
        Object_1.Parent = findobj(Fig,'Type','uimenu','Tag','Electronic Colorscale');
        Object_1.Label = Color_Scale_1;
        Events = [];
        Colorbar_Scaling(Object_1,Events);
        
        Colorscale = findobj(gcf,'Type','uimenu','Tag','Electronic Colorscale');
        set(findobj(Colorscale.Children,'Label',Color_Scale_1),'Checked','on');
        assignin('base','Electronic_Colorscale',Color_Scale_1);
        levels = length(get(findobj(Fig,'Tag','Electronic Contour'),'LevelList')) - 2;
        obj = findobj(Fig,'Tag','Electronic Contour Lines');
        set(findobj(obj.Children,'Label',num2str(levels)),'Checked','on');
        
        if mean(gradient(Contours)) < 1
            
            c1.TickLabels = sprintf('%.1f\n',Contours);
            
        elseif mean(gradient(Contours)) < 10
                        
            c1.TickLabels = sprintf('%.0f\n',Contours);
            
        elseif mean(gradient(Contours)) > 10
            
            c1.TickLabels = round(Contours,2,'significant');
                   
        end
        
        assignin('base','c1',c1);
        
    case true % Plot reset is requested ( = remove plotted data from subplots or inital run )
        
        S1 = subplot('Position',[0.05, 0.46, 0.28, 0.44],'Units','normalized');
        assignin('base','S1',S1);
        % Create a subplot that contains electronic subsystem data and assigin handle in workspace
        ax1 = gca; % Create an image of current axes and assignin it to electronic subsystem axes
        ax1.Tag = 'Electronic Subsystem'; % Tag to indentify the electronic subsystem
        set(ax1,'XTick',[],'YTick',[],'XTickLabel',[],'YTickLabel',[]); % Remove any axis tick labels
        hold(ax1,'off'); % Throw previous settings of electronic subsystem axes         
        delete(findall(Fig,'Type','uimenu','Label','Select Colormap','Tag','Electronic Colormap'));
        delete(findall(Fig,'Type','uimenu','Label','Select Colorscale','Tag','Electronic Colorscale'));
        delete(findall(Fig,'Type','uimenu','Label','Contour Quantity','Tag','Electronic Contour Lines'));
        delete(findall(Fig,'Type','Contour','Tag','Electronic Contour'));
        
end

title(ax1,'Electronic Subsystem','Interpreter','latex'); % Set title of electronic subsystem
box(ax1,'on'); % Draw a box around electronic subsystem axis
ax1.Layer = 'top'; % Bring electronic axes layer to the top
assignin('base','ax1',ax1); % Assign handle to atomic subsystem axes in workspace

%% Create a subplot for the temperature of the atomic subsystem 

switch Reset_Plot % Distinguish plotting calculated data or plot reset
    
    case false % Plot reset is not requested ( = plotting calculated data )
    
        ax2 = evalin('base','ax2'); % Get atomic subsystem axes from workspace 
        P2 = surf(ax2,10^9*Radial_Mesh_2,Time_Mesh_2,Lattice_Temperature);
        set(P2,'Tag','Atomic Subsystem');
        % Generate a surface plot based on spatial grid, time grid and atomic temperatures
        assignin('base','P2',P2); % Assign handle to atomic surface plot in workspace
                
        set(ax2,'YScale','log','TickLabelInterpreter','latex');
        % Set axes vertical scale to logarithmic and use latex tick label interpreter
        ax2.XAxis.FontSize = 10.5; %  Set horizontal axis font size
        ax2.YAxis.FontSize = 11.5; % Set vertical axis font size
        axis(ax2,'tight'); % Set atomic subsystem axes to match axes limits         
        shading(ax2,'interp'); % Interpolate shading of atomic subsystem axes
        
        x2 = xlabel(ax2,'$$\mathrm{Radius\;\;r\;\;\left(nm\right)}$$','Interpreter','latex','FontSize',12);
        set(x2,'Units', 'Normalized','Position', [0.50, -0.10, 0]);
        y2 = ylabel(ax2,'$$\mathrm{Time\;\;t\;\;\left(s\right)}$$','Interpreter','latex','FontSize',11);
        y2.Rotation = 0; % Rotate the vertical axis label of atomic subsystem
        set(y2, 'Units', 'Normalized', 'Position', [-0.05, 1.1, 0]);
        zlabel(ax2,'$$\mathrm{Atomic\;Temperature\;\;T_a\;\;\left(K\right)}$$','Interpreter','latex');        
        c2 = colorbar(ax2,'TickLabelInterpreter','latex','FontSize',10.5,'Tag','Atomic Colorbar');
        % Set three dimensional axis labels and colorbar appearance of atomic subsystem
        
        cb_2 = uicontextmenu;
        c2.UIContextMenu = cb_2;
        Colormap_2_Select = uimenu(cb_2,'Label','Select Colormap','Tag','Atomic Colormap');
            uimenu('Parent',Colormap_2_Select,'Label','parula','Callback',@select_colormap);
            uimenu('Parent',Colormap_2_Select,'Label','hot','Callback',@select_colormap);
            uimenu('Parent',Colormap_2_Select,'Label','hsv','Callback',@select_colormap);
            uimenu('Parent',Colormap_2_Select,'Label','jet','Callback',@select_colormap);
            uimenu('Parent',Colormap_2_Select,'Label','gray','Callback',@select_colormap);
        % Add a UIContextMenu to atomic subsystem colormap to define a variety of colors
        
        Scaling_2 = uimenu(cb_2,'Label','Select Colorscale','Separator','on','Tag','Atomic Colorscale');
            uimenu('Parent',Scaling_2,'Label','linear','Callback',@Colorbar_Scaling);
            uimenu('Parent',Scaling_2,'Label','logarithmic','Callback',@Colorbar_Scaling);   
               
        Contour_2 = uimenu(cb_2,'Label','Contour Quantity','Separator','on','Tag','Atomic Contour Lines');    
            uimenu('Parent',Contour_2,'Label','5','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','6','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','7','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','8','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','9','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','10','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','11','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','12','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','13','Callback',@Colorbar_Scaling);
            uimenu('Parent',Contour_2,'Label','14','Callback',@Colorbar_Scaling);
            
        c2.Label.String = '$$\mathrm{Temperature\;\;T_a\;\;\left(K\right)}$$';
        c2.Label.FontSize = 11;
        c2.Label.Interpreter = 'latex';  % Set latex colorbar interpreter
        c2_pos = get(c2,'Position'); % Get temporary position of atomic colorbar
        c2.Position = c2_pos + [0.008,0, -0.008, 0];
        c2.Label.Units = 'normalized'; % Set colorbar label units to normalized
        c2.Label.Position = [c2_pos(1)- 0.80, c2_pos(2) + c2_pos(4) + 0.254, 0];
        % Change atomic subsystem colormap position
        c2.Label.Rotation = 0; % Rotate the label of atomic subsystem colorbar
        c2.TickLength = 0.050;
        assignin('base','c2',c2);
        
        ax2.Position = get(ax2,'Position') + [0, 0, -0.05, 0];
        
        try % Colorbar exist due to atomic subsystem data is plotted yet
            
            colormaps = findall(gcf,'Type','uimenu','Label','Select Colormap');
            children = findobj(colormaps,'-property','Children');
            ax2_label = get(findobj(children,'-Property','Checked','Checked','on'),'Label');
             % Find colormaps previously checked and thus deviant from default settings
            colormap(ax2,ax2_label{1}); % Activate colormap that was checked before
            assignin('base','cmap',ax2_label{1}); % Assign current colormap label in workpspace
            
        catch % Colorbar does not already exist due to initial run or subplot data reset
           
           if evalin('base','exist(''cmap'',''var'')')
               
                colormaps = findall(gcf,'Type','uimenu','Label','Select Colormap');
                children = findobj(colormaps,'-property','Children');
                set(findobj(children,'Label', char(evalin('base','cmap'))),'Checked','on');
                cmap = evalin('base','cmap');
                colormap(ax2,cmap(Contour_Lines_2 - 1));        
                
           else 
            
                cmap = str2func('jet');%  Set atomic subsystem colormap to default settings                
                assignin('base','cmap',cmap); % Assign default colormap label in workpspace
           
           end
           
        end        
                        
        view(ax2,[0 90]); % View atomic subsystem axes in two dimensions
        set(ax2,'XLim',[ 0, 1.0 * 10^9*( max( Radial_Mesh_2 ) - min( Radial_Mesh_2 ) ) ]);
        % Set horizontal limit of atomic subsystem axes to 100 percent of maximum value
        grid(ax2,'on'); % Draw a grid within atomic subsystem
        hold(ax2,'on'); % Keep previous settings of atomic subsystem axes
        
        colormap(ax2,cmap(Contour_Lines_2 - 1));               
        Color_Lim_2 = caxis(ax2); % Get limits of coloraxis 
        
        Contours = linspace(Color_Lim_2(1),Color_Lim_2(2),Contour_Lines_2);
        
        contour3(ax2,10^9*Radial_Mesh_2,Time_Mesh_2,Lattice_Temperature,Contour_Lines_2,'k',...
            'Tag','Atomic Contour','LevelList',Contours);
        % Plot a certain number of contour lines with black color
                
        c2.Ticks = Contours;
        c2.TickLabels = round(Contours,2,'significant');
               
        if evalin('base','exist(''Atomic_Colorscale'',''var'')')
                            
            Color_Scale_2 = evalin('base','Atomic_Colorscale');
                                        
        else
                            
            Color_Scale_2 = 'linear';
                                
        end          
       
        Object_2.Parent.Tag = 'Atomic Colorscale';
        Object_2.Parent = findobj(Fig,'Type','uimenu','Tag','Atomic Colorscale');
        Object_2.Label = Color_Scale_2;
        Events = [];
        Colorbar_Scaling(Object_2,Events);
        
        Colorscale = findobj(gcf,'Type','uimenu','Tag','Atomic Colorscale');
        set(findobj(Colorscale.Children,'Label',Color_Scale_2),'Checked','on');
        assignin('base','Atomic_Colorscale',Color_Scale_2);    
        
        levels = length(get(findobj(Fig,'Tag','Atomic Contour'),'LevelList')) - 2;
        obj = findobj(Fig,'Tag','Atomic Contour Lines');
        set(findobj(obj.Children,'Label',num2str(levels)),'Checked','on');
        
    case true % Plot reset is requested ( = remove plotted data from subplots or intial run )
        
        S2 = subplot('Position',[0.41, 0.46, 0.28, 0.44],'Units','normalized');
        assignin('base','S2',S2);
        % Create a subplot that contains atomic subsystem data and assigin handle in workspace
        ax2 = gca; % Create an image of current axes and assignin it to atomic subsystem axes
        set(ax2,'XTick',[],'YTick',[],'XTickLabel',[],'YTickLabel',[]); % Remove any axis tick labels
        ax2.Tag = 'Atomic Subsystem'; % Tag to indentify the atomic subsystem
        hold(ax2,'off'); % Throw previous settings of atomic subsystem axes
        delete(findall(Fig,'Type','uimenu','Label','Select Colormap','Tag','Atomic Colormap'));
        delete(findall(Fig,'Type','uimenu','Label','Select Colorscale','Tag','Atomic Colorscale'));
        delete(findall(Fig,'Type','uimenu','Label','Contour Quantity','Tag','Atomic Contour Lines'));
        delete(findall(Fig,'Type','Contour','Tag','Atomic Contour'));
        
end

title(ax2,'Atomic Subsystem','Interpreter','latex'); % Set title of atomic subsystem
box(ax2,'on'); % Draw a box around atomic subsystem axis
ax2.Layer = 'top'; % Bring atomic axes layer to the top
assignin('base','ax2',ax2); % Assign handle to atomic subsystem axes in workspace

%% Create a subplot for line temperature graphics

switch Reset_Plot % Distinguish plotting calculated data or plot reset
    
    case false % Plot reset is not requested ( = plotting calculated data )
        
        ax3 = evalin('base','ax3'); % Get handle to line plot axes in workspace
        axis(ax3,'tight')        
        
    case true % Plot reset is requested ( = remove plotted data from subplots or intial run )
        
        S3 = subplot('Position',[0.74, 0.46, 0.22, 0.44],'Units','normalized');
        assignin('base','S3',S3);
        % Create a subplot that contains temperature line plot data and assigin handle in workspace
        ax3 = gca;  % Create an image of current axes and assignin it to line plot axes
        set(ax3,'XTick',[],'YTick',[],'XTickLabel',[],'YTickLabel',[]); % Remove any axis tick labels
        evalin('base','clear L1');
        evalin('base','clear L2');
        
        
end

title(ax3,'Temperature - Lineplot','Interpreter','latex'); % Set title of temperature line plot
xlabel(ax3,''); % Remove horizontal axis label
ylabel(ax3,''); % Remove vertical axis label
box(ax3,'on'); % Draw a box around temperature line plot axis
ax3.Layer = 'top';  % Bring temperature line plot axes layer to the top
ax3.Tag = 'Line Plot'; % Tag to indentify the temperature line plot primary axes
assignin('base','ax3',ax3); % Assign handle to temperature line plot primary axes in workspace

ax4 = axes('Position',ax3.Position,'Visible','off'); % Create secondary line plot axes ( time )
set(ax4,'XAxisLocation','top','YAxisLocation','right','Color','none'); % Define secondary axis location
ax4.Tag = 'Line Plot'; % Tag to indentify the temperature line plot secondary axes
assignin('base','ax4',ax4);  % Assign handle to temperature line plot secondary axes in workspace


%% Create a data cursor with respect to the GUI plots

set(findobj(gcf,'Label','Remove Datacursor','-property','Enable'),'Enable','off','Checked','on');

dcm = datacursormode(Fig);        % Create a handle to simulation GUI
set(dcm,'UpdateFcn', @myCursor);  % Call data cursor function

end

function output_txt = myCursor(~,event_obj)
% The function "myCursorn" defines a callback funktion of Data Cursor that
% makes use of event data of the object. Find variable declaration below :
%
% pos         --->   Actual position of data cursor
% event_obj   --->   Access to object events
% output_txt  --->   Data cursor string variable (cell array with strings)
%
% In addition to the actual simulation time and radial distance from the
% center of the incidence ion, the data cursor allows information on either
% electronic or atomic subsytem.
%
% Measuring sizes of the data cursor:
% 
% - Temporary radius          r      in    Nanometers     [ nm ]
% - Temporary time            t      in    Seconds        [ s  ]
% - Temporary temperature     T      in    Kelvin         [ K  ]


%% Determine actual cursor position

pos = get(event_obj,'Position');  %  Get cursor position from event data

Target = get(event_obj,'Target'); % Can either be surface plot or line plot
assignin('base','Target',Target); % Assign handlle to plot target in workspace  

if isequal( size( Target.ZData, 2 ), 0 ) % Detect target equals temperature line plot
    
    L = evalin('base','L'); % Evaluate handle to temperature line in workspace
    
    [~, ind] = min( abs( L.YData - pos(1) ) ); 
    % Detect a respective matrix element of current datacursor position
      
    pos = [ L.XData( ind ) pos ]; % Add line temperature value to datacursor position
    
else % Datacursor target is one of the surface plots
    
    if isequal(Target.Tag,'Electronic Subsystem') 
        
        colorscale = findobj(gcf,'Type','uimenu','Tag','Electronic Colorscale'); 
        checked_scale = findobj(colorscale.Children,'Checked','on');
        
        switch checked_scale.Label
            
            case 'logarithmic'                
        
                pos(3) = 10^pos(3); % Distinguish the logaritmic plot of electronic subsystem
                           
        end    
    end
    
    if isequal(Target.Tag,'Atomic Subsystem') 
        
        colorscale = findobj(gcf,'Type','uimenu','Tag','Atomic Colorscale'); 
        checked_scale = findobj(colorscale.Children,'Checked','on');
        
        switch checked_scale.Label
            
            case 'logarithmic'                
        
                pos(3) = 10^pos(3); % Distinguish the logaritmic plot of electronic subsystem
                           
        end    
    end
    
end

assignin('base','pos',pos); % Assign current data cursor position in workspace

%% Create data cursor output text

output_txt = {strcat('Radius:',32,32,'r ',32,' = ',32,32,sprintf('%.2f',pos(1)),32,'nm'),...
    strcat('Time:',32,32,32,32,32,'t',32,' = ',32,32,sprintf('%.2e',pos(2)),32,'s'),...
    strcat('Temp.:',32,32,'T',32,' = ',32,32,num2str(round(pos(3),0)),32,'K')};
% Output text of the data cursor is a cell variable consiting of each string

%% Define output of the data cursor

alldatacursors = findall(gcf,'type','hggroup'); % Find all type of valid datacursor
set(alldatacursors,'FontSize',10,'Marker','s','MarkerFaceColor','w','MarkerEdgeColor','k')
set(alldatacursors,'FontName','Times','Tag','DataTipMarker')
set(findall(gcf,'Type','text','Tag','DataTipMarker'), 'Interpreter','tex')
% Define datacursor appearance settings ( e.g. tex interpreter, fontstyle, markerstyle, ... )

if any([isequal(Target.Tag,'Electronic Subsystem'),isequal(Target.Tag,'Atomic Subsystem')])

    set(findobj(gcf,'Type','uimenu','Label','Add new datacursor'),'Enable','on','Checked','off');
    
end

set(findobj(gcf,'Type','uimenu','Label','Remove all datacursors'),'Enable','on','Checked','off');

end

function select_colormap(source,~)
% User defined function to change colormap settings of both surface plot subsystems

assignin('base','source',source);

colormaps = findall(gcf,'Type','uimenu','Label','Select Colormap'); 
% Find all colormap uimenus of current figure

 for i = 1 : 1 : length(colormaps) % Iterate the number of subsystems with colormaps

 cm = colormaps(i).Children; % Get subordinated characters of each uimenu

     for j = 1 : 1 : length(cm) % Iterate the amount of available colormaps
         
         if ~isequal(cm(j).Label,source.Label) % Iterated colormap is not equal to requested one
    
            set(cm(j),'Checked','off'); % Set checked state to false
            
         else
             
             set(cm(j),'Checked','on'); % Set checked state to true
             
         end
        
     end

 end
 
 try % Run the following routine since no errors occur    
     
    switch source.Label % Distinguish different colormap settings
     
        case {'parula','jet','hsv'} % Colormap is equal to << parula >> , << jet >> or << hsv >>
         
            set(findall(gcf,'Type','Line','Tag','Line Temperature'),'Color','k'); 
            % Set temperature line plot to color black
            set(findall(gcf,'Type','Scatter'),'MarkerEdgeColor','k'); 
            % Set subsystem line to color black
            colorname = 'black'; % Define a colorname label
              
         case 'hot' % Colormap is equal to << hot >>
         
            set(findall(gcf,'Type','Line','Tag','Line Temperature'),'Color','b'); 
            % Set temperature line plot to color blue
            set(findall(gcf,'Type','Scatter'),'MarkerEdgeColor','b'); 
            % Set subsystem line to color blue
            colorname = 'blue'; % Define a colorname label

        case 'gray' % Colormap is equal to << gray >>
         
            set(findall(gcf,'Type','Line','Tag','Line Temperature'),'Color','r'); 
            % Set temperature line plot to color red
            set(findall(gcf,'Type','Scatter'),'MarkerEdgeColor','r'); 
            % Set subsystem line to color red
            colorname = 'red'; % Define a colorname label
         
    end
    
    object = get(findobj(gcf,'Type','uimenu','Label','Change Color'),'Children');
    set(findall(object,'-property','Checked'),'Checked','off');
    set(findobj(object,'-property','Label','Label',colorname),'Checked','on');
    % Find specific uimenu with given colorname label and check it 
        
 catch % Do not catch any error cases
     
 end

 set(source,'Checked','on'); % Set the UICContextMenu to checked state
  
 assignin('base','source',source); % Assign UIContextMenu of colormaps in workspace

 evalin('base','cmap = str2func(source.Label);'); % Create a function according to colormap name
 evalin('base','colormap(ax1,cmap(Contour_Lines_1 - 1));'); % Finally change colormap of electronic subplot
 evalin('base','colormap(ax2,cmap(Contour_Lines_2 - 1));'); % Finally change colormap of atomic subplot
     
end
