function [Electronic_Temperature,Lattice_Temperature] = PDE_System_Solve(Radial_Grid,Time_Grid)
% The user defined function "PDE_System_Solve" numerically calculates the temperature values of 
% the electronic and atomic subsystem due to physical considerations of the Thermal Spike Modell.
% Therefore a coupled system of two elliptic partiell differential equations has to be solved
% according to specific heat, thermal conductivity, coupling factor and energy density per unit 
% time of each subsystem. 

m = 1; % Partial Differential Equation System will be solved in cylindric coordinates

global Options; % Load PDE Solver Options

Solution = pde_User_Defined(m,@pdex4pde,@pdex4ic,@pdex4bc,Radial_Grid,Time_Grid,Options);
% Numerically run Matlab's differetial equation system solver for elliptic PDE's

evalin('base','clc'); % Clear command window after claculation process

Electronic_Temperature = Solution(:,:,1); 
% Extract temperatures of the electronic subsystem from solution matrix. Unit: K
Lattice_Temperature = Solution(:,:,2);
% Extract temperatures of the atomic subsystem from solution matrix. Unit: K

end

%% Differential Equation Coefficients -------------------------------------------------------------

function [C,F,S] = pdex4pde(r,t,T,DTDr)
% This subsequently operating Matlab function reliably computes respective
% PDE coefficients according to any spatial, time or temperature dependency.

if evalin('base','Wait_Index') == 0 || t > evalin('base','Time_Index')
% Only run this section while wait index represents inital step or time
% index changes to greater values compared to previous ones.
    
    mywaitbar(evalin('base','Mainpanel'),evalin('base','Time_Grid'),t);
    % Udate waitbar status using current calculation time step
    
    pause(eps); % Resume after a pause ( needed to update waitbar status )

    assignin('base','Wait_Index',evalin('base','Wait_Index') + 1); % Enhance wait index by one

    assignin('base','Time_Index',t); % Assign current time index in workspace
end

% T(1) Obtain electronic subsystem temperatures from approx. temperature vector 
% T(2) Obtain atomic subsystem temperatures from approx. temperature vector 
Thermal_Properties = evalin('base','Thermal_Properties'); % Get thermal properties from workspace
Material_Selection = evalin('base','Material_Selection'); % Get target material info from workspace

C_e = Electron_Heat_Calculation(T(1),T(2));
% Calculate specific heat of the electronic subsystem with respect to a mean temperature of the electrons
   
C_a = Lattice_Heat_Calculation(T(2),Thermal_Properties,Material_Selection);
% Calculate specific heat of the atomic subsystem according to a mean temperature of the lattice

K_e = Electron_Thermal_Conductivity_Calculation(T(1),T(2));
% Calculate thermal conductivity of the electronic subsystem with respect to a mean temperature of the electrons

K_a = Lattice_Thermal_Conductivity_Calculation(T(2),Thermal_Properties,Material_Selection);
% Calculate thermal conductivity of the atomic subsystem with according to a mean temperature of the lattice

g = Coupling_Factor_Calculation(T(1),T(2));
% Determine the coupling factor with respect to a mean temperature of the electronic subsystem

C = [C_e; C_a]; % Heat capacity vector with electronic and atomic heat capacity. Unit: J/(m^3*K)

F = [K_e; K_a] .* DTDr; 
% Heat flux containing thermal conductivity of both subsystems and temperature gradient. Unit: W/m^2

y = T(1) - T(2); % Substitution of the temperature difference between both subsystems. Unit: K

G = g * y; % Coupling factor term which is one part of the heat source term. Unit: W/m^3

A = Electronic_Energy_Density_Calculation(r,t);
% Energy density per unit time A(r,t) according to electronic energy loss (dE/dx)_e . Unit: W/m^3
% This expression solves the integral equation to determine energy density in cylindric coordinates.

B = Atomic_Energy_Density_Calculation(r,t); 
% Energy density per unit time B(r,t) according to atomic energy loss (dE/dx)_n . Unit: W/m^3

% Edit_1 = evalin('base','Edit_1');
% Initial_Temp = str2double(Edit_1.String(~isletter(Edit_1.String)));
   
R = Radiation_Calculation(T(2));
% Radiation heat with respect to lattice temperatures and emisivity values. Unit: W/m^3
  
[D,~] = Desorption_Calculation(t,T(2));
% Desorption heat with respect to lattice temperatures and time values. Unit: W/m^3

S = [-G + A; G + B + R + D]; 
% Heat source term containing coupling factors and energy density per unit time. Unit: W/m^3

end

%% Differential Equation Initial Conditions ------------------------------------------------------

function T_0 = pdex4ic(~)
% This subsequently operating Matlab function defines inital temperatures for PDE solving process.

Edit_1 = evalin('base','Edit_1');
Initial_Temp = str2double(Edit_1.String(~isletter(Edit_1.String)));

T_0 = [Initial_Temp; Initial_Temp]; 
% Initial temperature of electronic and atomic subsystem. Unit: K   

end

%% Differential Equation Boundary Conditions ------------------------------------------------------

function [pl,ql,pr,qr] = pdex4bc(~,~,~,Tr,~)
% This subsequently operating Matlab function calculates boundary conditions for PDE solving process.

%Edit_1 = evalin('base','Edit_1');
%Initial_Temp = str2double(Edit_1.String(~isletter(Edit_1.String)));

pl = [0; 0]; % Left boundary polynomial temperature difference: T(r = 0,t) - T_fix
ql = [0; 0]; % left heat flux scaling factor either being equal to zero or never zero for all times.
pr = [0; 0];
%pr = [Tr(1) - Initial_Temp; Tr(2) - Initial_Temp]; 
% Right boundary polynomial temperature difference T(r = r_max,t) - T_inf 
qr = [-1; -1]; % Right heat flux scaling factor either being equal to zero or never zero for all times.

end
