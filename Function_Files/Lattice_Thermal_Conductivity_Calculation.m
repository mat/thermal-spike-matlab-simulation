function [Lattice_Thermal_Conductivity] = Lattice_Thermal_Conductivity_Calculation(Temperature,Properties,Material)
% User defined function to determine the latttice thermal conductivity of a selected material, 
% based on temperature dependent polynomials.

 %% Read thermal thresholds, polynomials and material specific properties

 Conductivity_Thresholds = Properties.(Material).Thermal_Conductivity.Thresholds;
 Conductivity_Thresholds(strcmp(Conductivity_Thresholds,'inf') | strcmp(Conductivity_Thresholds,'Inf')) = {inf};
 Conductivity_Thresholds = cell2mat(Conductivity_Thresholds);
  % Read lattice thermal conductivity temperature thresholds. Unit: K

 Conductivity_Coefficients = cell2mat(Properties.(Material).Thermal_Conductivity.Polynomial);
 % Read lattice thermal conducitvity polynomial values from cell variable and convert into matrix format

 Lattice_Thermal_Conductivity = zeros(size(Temperature)); 
 % Initialize lattice thermal conductivity matrix

 %% Calculate specific lattice thermal conductivity from polynomial values
 
 for i = 1 : 1 : size(Temperature,1) % Iterate spatial dimension of lattice temperature
    
     for j = 1 : 1 : size(Temperature,2) % Iterate time dimension of lattice temperature
    
         Temp_Vector = [ Temperature(i,j)^(-3), Temperature(i,j)^(-2), ...                       
                         Temperature(i,j)^(-1), Temperature(i,j)^( 0), ...
                         Temperature(i,j)^( 1), Temperature(i,j)^( 2), ...
                         Temperature(i,j)^( 3), Temperature(i,j)^( 4) ]; 
         % Define a temperature vector to a minimum power of minus two and a maximum power of four. Unit: K

         for k = 1 : 1 : size(Conductivity_Thresholds,1) 
         % Iterate different lattice thermal conductivity thresholds sections
        
             if Temperature(i,j) >= Conductivity_Thresholds(k,1) && Temperature(i,j) < Conductivity_Thresholds(k,2)
             % Check actual temperature not to exceed given lattice thermal conductivity thresholds
                 
                 Polynomial = Conductivity_Coefficients(k,:); 
                 % Lattice thermal conductivity polynomial according to given setion
                 Lattice_Thermal_Conductivity(i,j) = 100 * dot(Polynomial,Temp_Vector);
                 % Determine specific lattice thermal conductivity as a scalar multiplication of the
                 % lattice temperature vetor and the polynomial vector. Unit: W / ( m * K )
            
             end
        
         end
        
     end
    
 end

  %% Apply coefficients substitution if neccessary
 
 if evalin('base','exist(''subs_coeff'',''var'')')
 % Check if coefficients substitution variable does exist in workspace

      coeff_data = evalin('base','coeff_data'); % Read coefficients data in workspace

      if isequal(coeff_data{5,4},'Yes') 
      % Neccessary to apply substitution of lattice thermal conductivtiy       

              Lattice_Thermal_Conductivity = ones(size(Temperature)) * ...
                        10^(2) * str2double(coeff_data{5,3});
              % Set lattice thermal conductivity to values of coefficients substitution. 
              % Unit: W / ( m * K )

      end

 end
 
 %% Data Output
 
 Lattice_Thermal_Conductivity( Lattice_Thermal_Conductivity < 0 ) = 0; % Avoid any negative ouptut

end