function [Electron_Thermal_Conductivity] = Electron_Thermal_Conductivity_Calculation(Electronic_Temperature,Lattice_Temperature)
% User defined function to determine the electron heat conductivity due to both subsystem 
% temperatures and electron specific heat capacity.

 %% Calculation routine

 Electron_Heat = Electron_Heat_Calculation(Electronic_Temperature,Lattice_Temperature);
 % Evaluate electron thermal heat capacity based on both subsystem temperatures

 Electron_Diffusivity = Electron_Thermal_Diffusivity(Electronic_Temperature);
 % Evaluate electron thermal diffusivity based on electronic subsystem temperatures

            
 Electron_Thermal_Conductivity = Electron_Heat .* Electron_Diffusivity;            
 % Calculate electron thermal conductivity by multiplying electron thermal heat capacity 
 % and electron thermal diffusivity based on temporary selectronic subsystem temperatures.
  
 if evalin('base','exist(''subs_coeff'',''var'')') 
 % Check if coefficients substitution variable does exist in workspace
     
    coeff_data = evalin('base','coeff_data'); % Read coefficients data in workspace

    if isequal(coeff_data{2,4},'Yes') 
    % Neccessary to apply substitution of electron thermal heat conductivity
        
            Electron_Thermal_Conductivity = ones(size(Electronic_Temperature)) * ...
                10^(2) * str2double(coeff_data{2,3});
            % Set electron thermal heat conductivity to values of coefficients substitution. 
            % Unit: W / ( m * K )
        
    end
    
 end
 
 %% Data Output
 
  Electron_Thermal_Conductivity( Electron_Thermal_Conductivity < 0 ) = 0; % Avoid negative ouput

end