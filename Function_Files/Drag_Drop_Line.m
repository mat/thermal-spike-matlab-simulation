function Drag_Drop_Line(~,~)
% This user defined function calculates the distance between the current mouse pointer and 
% respective line coordinates and afterwards decides whether to update the line data which 
% mainly results in a drag & drop action on drawn subsystem line.

 if all( [ ~evalin('base','Mouse_Down'), isequal( gca, evalin('base','line_ax') )] ) 
 % Only run this section if the user has once clicked in subsystem containing line axes but 
 % the left mouse button is not constantly pressed down ( mouse move over subsystem )
 
     %% Calculate the distance between mouse pointer and each line endpoints
 
     try % Apply following routine since no errors are going to occur

         Screensize = get(0,'ScreenSize'); % Get the screen size from root settings
         Pointer = get(0, 'PointerLocation'); % Get the present mouse pointer position
         eval('Fig_Pos = get(gcf,''InnerPosition'');'); % Evaluate internal figure position
         eval('line_ax = evalin(''base'',''line_ax'');'); % Evaluate line plot axes in workspace
                  
         eval('LP_1 = evalin(''base'',''LP_1'');'); % Evaluate line end point (1) in workspace
         eval('LP_2 = evalin(''base'',''LP_2'');'); % Evaluate line end point (2) in workspace
         eval('L = evalin(''base'',''L'');'); % Evaluate line data in workspace

         Pointer_Position = [ Pointer(1) / Screensize(3), Pointer(2) / Screensize(4) ];
         % Determine current mouse pointer position in normalized units; 
                  
         assignin('base','Pointer_Position',Pointer_Position); 
         % Assign current mouse position in workspace                

         n = length(findall(gca,'Type','Scatter')); % Iterate the number of line endpoints
         
         Point_Dist = zeros(1,length(n)); 
         % Initialize each mouse pointer to line endpoint distance to zero
         
         for i = 1 : 1 : n % Generate endpoint distance calculation variables dynamically

            N = matlab.lang.makeValidName(strcat('Point_',num2str(i))); % Make a valid string name
            V = matlab.lang.makeUniqueStrings(N); % Make unique matlab string from valid string name

            eval(strcat('V = ',strcat('zeros(1,',num2str(n),');'))); % Initialize point variables

            x = eval(strcat('( Fig_Pos(1) + line_ax.Position(1) + line_ax.Position(3) *', ...
                strcat('LP_',num2str(i),'.XData'), ...
                ' / ( line_ax.XLim(2) - line_ax.XLim(1) ) ) * Fig_Pos(3);')); 
            % Evaluate horizontal values (x) of respective line endpoints

            y = eval(strcat('( Fig_Pos(2) + line_ax.Position(2) + line_ax.Position(4)', ...
                ' * ( log10(',strcat( 'LP_',num2str(i),'.YData'),') - log10( line_ax.YLim(1) ) )', ...
                ' / ( log10( line_ax.YLim(2) ) - log10( line_ax.YLim(1) ) ) ) * Fig_Pos(4);') );
            % Evaluate vertical values (y) of respective line endpoints
           
            eval(strcat('V = ',strcat('[ ',num2str(x),',',num2str(y),' ];'))); 
            % Dynamically assign x and y values to line endpoint variables
            
            assignin('base',N,V); % Transfer created point variables to workspace
            
            Point_Dist(i) = pdist2( Pointer_Position, V ); 
            % Calculate normalized distance between mouse pointer and line endpoints,
            % which is needed to detect single point dragging actions
            
         end
         
         if min( Point_Dist ) < 0.02 % Endpoint distance below a threshold

            set(gcf,'Pointer','hand'); % Set mouse pointer to point drag symbol

         else % End point distance exceeds given threshold

            set(gcf,'Pointer','arrow'); % Set mouse pointer to usual symbol

         end 

         assignin('base','Point_Dist',Point_Dist); 
         % Assign endpoint distance variables in workspace         
         
     catch % Do not catch any errors !
         
     end
    
     %% Calculate the minimum distance between mouse pointer and any linepoint
     
     try % Apply following routine since no errors are going to occur
         
        if ~isequal(get(gcf,'Pointer'),'hand') 
            % Dragging of line endpoints is not active ( requirement )
            
            Stepwidth = 25; % Iteration stepwidth in units of pixels
            
            eval(strcat('X_Data = L.XData(1:',num2str(Stepwidth),':end);')); 
            eval(strcat('Y_Data = L.YData(1:',num2str(Stepwidth),':end);'));
            % Calculate line data dynamically for given stepwidth
            
            n = length(findall(gca,'Type','Line')); % Find the number of lines
            p = eval('length(X_Data);'); % Get the length of line horizontal data
            N = matlab.lang.makeValidName('Line_Points'); % Make a valid string name
            V = matlab.lang.makeUniqueStrings(N); % Make unique matlab string from valid string name
            
            Line_Dist = zeros(1,p); % Initialize line distance variables
                        
            eval(strcat('V = ',strcat('zeros(',num2str(2*n),',',num2str(p),');')));
            % Dynamically initialize x and y values to line variables
            
            for j = 1 : 1 : n % Generate line distance calculation variables dynamically                                        
            
                x = eval(strcat('( Fig_Pos(1) + line_ax.Position(1) + line_ax.Position(3)', ...
                    '* X_Data / ( line_ax.XLim(2) - line_ax.XLim(1) ) ) * Fig_Pos(3);'));
                % Evaluate horizontal values (x) of respective line points

                y = eval(strcat('( Fig_Pos(2) + line_ax.Position(2) + line_ax.Position(4)', ...
                    ' * ( log10( Y_Data ) - log10( line_ax.YLim(1) ) )', ...
                    ' / ( log10( line_ax.YLim(2) ) - log10( line_ax.YLim(1) ) ) ) * Fig_Pos(4);'));           
                 % Evaluate vertical values (y) of respective line points
                
                eval(strcat('V = ',strcat('[ ',num2str(x),';',num2str(y),' ];')));
                % Dynamically assign x and y values to line point variables
                
                assignin('base',N,V); % Transfer created line point variables to workspace                              
                
                for k = 1 : 1 : length(V) 
                % Iterate the number of line points according to chosen stepwidth

                    Line_Dist(j,k) = pdist2(Pointer_Position,[x(j,k),y(j,k)]);
                    % Calculate the distance between mouse pointer and any line point

                end                
                
                assignin('base',strcat('Line_',num2str(j),'_Dist'),min(Line_Dist));
                % Transfer distance between mouse pointer and line points to workspace
                
            end 
            
            if  any( Line_Dist(j,:) < 0.01 ) % Any linepoint distance below a threshold

                set(gcf,'Pointer','fleur'); % Set mouse pointer to line drag symbol

            else 

                set(gcf,'Pointer','arrow'); % Set mouse pointer to usual symbol

            end

        end
        
     catch % Do not catch any errors !

     end
     
     assignin('base','Move_Line',false); % Assign line movement flag to false

 end
 
 %% Complete line or specific endpoints dragging mode is active
 
 if all( [ evalin('base','Mouse_Down'), ~isequal( get( gcf,'Pointer' ) ,'arrow') ] )
 % Only run this section if the mouse pointer is not equal to usual arrow symbol and the left
 % mouse button is constantly pressed down ( mouse pressed and move over subsystem )

     assignin('base','Move_Line',true); % Assign line movement flag to true
     eval('line_ax = evalin(''base'',''line_ax'');'); % Evaluate handle to line axes in workspace
     eval('set(line_ax,''Layer'',''top'');'); % Set current line axes layer to top
     eval('xlim(line_ax,''manual'');'); % Set line axes x limits to manual setting
     eval('ylim(line_ax,''manual'');'); % Set line axes y limits to manual setting

     eval('x_limits = line_ax.XLim;'); % Get line axes temporary x limits
     eval('y_limits = line_ax.YLim;'); % Get line axes temporary y limits

     eval('Prev_Mouse_Position = evalin(''base'',''Prev_Mouse_Position'');');
     eval('Mouse_Position = get(line_ax,''CurrentPoint'');');  
     % Get the current mouse point on the axes

     LP_1 = evalin('base','LP_1'); % Evaluate the line endpoint (1) in workspace       
     LP_2 = evalin('base','LP_2'); % Evaluate the line endpoint (2) in workspace       
     L = evalin('base','L'); % Evaluate line data in workspace

     n = length(findall(gca,'Type','Scatter')); % Find number of line endpoints of current axes
     
     eval('Y_Denom = 10^-( ( log10( Mouse_Position(1,2) ) - log10( Prev_Mouse_Position(1,2) ) ) );');
     % Calculate a suitable denominator with respect to logarithmic scaled time values
     
     for k = 1 : 1 : n % Iterate the number of line endpoints
             
        if isequal(get(gcf,'Pointer'),'fleur') % Complete line dragging action is activated          

            eval(strcat('LP_',num2str(k),'.XData = LP_',num2str(k),'.XData + ( Mouse_Position(1,1) - Prev_Mouse_Position(1,1) );'));
            eval(strcat('LP_',num2str(k),'.YData = LP_',num2str(k),'.YData / Y_Denom;'));
            
        elseif isequal(get(gcf,'Pointer'),'hand') % Only line endpoints dragging is required           

            Distance = evalin('base','Point_Dist'); % Evaluate the line endpoint distance in workspace

            if k < n % Use line endpoint values only once
            
            % Determine which line endpoint is closest to current mouse pointer
                
                if  Distance(k) < Distance(k+1) 
                % Set line endpoint (k) data to current mouse pointer positon

                    eval(strcat('LP_',num2str(k),'.XData = Mouse_Position(1,1);'));                                         
                    eval(strcat('LP_',num2str(k),'.YData = Mouse_Position(1,2);'));                

                elseif Distance(k) > Distance(k+1) 
                % Set line endpoint (k+1) data to current mouse pointer positon

                    eval(strcat('LP_',num2str(k+1),'.XData = Mouse_Position(1,1);'));                                         
                    eval(strcat('LP_',num2str(k+1),'.YData = Mouse_Position(1,2);'));                              

                end  
                
            end

        end         
                
        for Vec = {'X', 'Y'} % Check line endpoint coordinates possibly exceed axes limits
                    
            eval(strcat('LP_',num2str(k),'.',Vec{1},'Data = LP_',num2str(k), ...
                '.',Vec{1},'Data(LP_',num2str(k),'.',Vec{1}','Data >= ',lower(Vec{1}), ...
                '_limits(1));')); 
            % Horizontal value of line endpoint is greater than minimum of horizontal axes limits
         
            eval(strcat('LP_',num2str(k),'.',Vec{1},'Data(isempty(LP_',num2str(k), ...
             '.',Vec{1},'Data)) = ',lower(Vec{1}),'_limits(1);'));
            % Horizontal value of line endpoint is outside left border line axes subsystem
         
            eval(strcat('LP_',num2str(k),'.',Vec{1},'Data = LP_',num2str(k), ...
                '.',Vec{1},'Data(LP_',num2str(k),'.',Vec{1},'Data <= ',lower(Vec{1}), ...
                '_limits(2));'));
            % Vertical value of line endpoint is less than maximum of vertical axes limits
         
            eval(strcat('LP_',num2str(k),'.',Vec{1},'Data(isempty(LP_',num2str(k), ...
             '.',Vec{1},'Data)) = ',lower(Vec{1}),'_limits(2);'));
             % Horizontal value of line endpoint is outside right border line axes subsystem

        end
        
     end
     
     L.XData = linspace(LP_1.XData,LP_2.XData,evalin('base','Points')); 
     % Generate a linear spatial vector according to horizontal coordinates of line endpoints
          
     L.YData = LogSpace([LP_1.YData,LP_2.YData],evalin('base','Points'));
     % Generate a logarithmic time vector according to vertical coordinates of line endpoints

     Update_Line_Plot(); % Call line plot update function            

     assignin('base','Prev_Mouse_Position',Mouse_Position); 
     % Set present mouse pointer position to its previous normalized position

  end 

end