function Update_Line_Plot()
% This user defined function is going to udate the temperature line plot if requested from 
% Line_Plot function. Therefore it becomes neccessary to interpolate line temperature data
% due to an uncertainty of data points resulting from drawn line in one of the subsystems. 
% Furthermore it appears meaningful to plot any given temperature profile versus a primary 
% horizontal axis due to varying spatial values and a secondary axis describing time values.
% At least certain adjustments on axes limitations regarding melting point presence are made
% and the plotting direction of the temperature curve is customized on line vector reversal.

%% Determine plot data according to line selection

ax = gca;

Radius = get(findobj(ax.Children,'Type','Surface'),'XData'); % Evaluate spatial mesh in workspace. Unit: nm
Time = get(findobj(ax.Children,'Type','Surface'),'YData');  % Evaluate time mesh in workspace. Unit: s
line_ax = evalin('base','line_ax'); % Get handle to line plot axes from workspace
Points = evalin('base','Points'); % Evaluate the number of line points in workspace
L = evalin('base','L'); % Get handle to the line drawn in any subsystem 

P = findobj(get(line_ax,'Children'),'Type','Surface'); 
% Find surface plot properties as a member of line plot axes

assignin('base','P',P);

if isequal(P.Tag,'Electronic Subsystem') % Line plot is member of electronic subsystem
         
    Colorscale = findobj(gcf,'Tag','Electronic Colorscale');
    Colorscale = get(findobj(Colorscale.Children,'Checked','on'),'Label');
    
else % Line plot is member of atomic subsystem
    
    Colorscale = findobj(gcf,'Tag','Atomic Colorscale');
    Colorscale = get(findobj(Colorscale.Children,'Checked','on'),'Label');
    
end
    
if isequal(Colorscale,'logarithmic') % Detect a line plot with logaritmic temperature scaling
    
    Temperature = 10.^get(P,'ZData');
    Temp_log_scale = true;
    
else % Line plot is of linear temperature scaling

    Temperature = get(P,'ZData'); % Assign surface plot data to temperature variable
    Temp_log_scale = false;
    
end    
    
XData = L.XData; % Get spatial data of surface plot
YData = L.YData; % Get time data of surface plot

Min_Radius = find( Radius <= min( XData ) , 1, 'last'  ); % Determine slightest radius value
Max_Radius = find( Radius >= max( XData ) , 1, 'first' ); % Determine greatest radius value

assignin('base','Min_Radius',Min_Radius); % Assign slightest radius value in workspace
assignin('base','Max_Radius',Max_Radius); % Assign greatest radius value in workspace

Min_Time = find( Time <= min( YData ) , 1, 'last'  ); % Determine minimum time value
Max_Time = find( Time >= max( YData ) , 1, 'first' ); % Determine maximum time value

assignin('base','Min_Time',Min_Time); % Assign minimum time value in workspace
assignin('base','Max_Time',Max_Time); % Assign maximum time value in workspace

if ~any([isequal(Min_Time,Max_Time),isequal(Min_Radius,Max_Radius)])
% Detect whether drawn line is neither completely horizontal nor vertical oriented
    
    Time = Time( Min_Time : Max_Time ); 
    Radius = Radius( Min_Radius : Max_Radius );
    Temperature = Temperature( Min_Time : Max_Time, Min_Radius : Max_Radius );
    % Determine neccessary line data by logical array indexing method
    
else % Line is either completely horizontal or verticcal oriented
    
    Time = P.YData; 
    Radius = P.XData;    
    
    if isequal(Colorscale,'logarithmic') % Detect a line plot with logaritmic temperature scaling
    
        Temperature = 10.^get(P,'ZData');
        Temp_log_scale = true;
    
    else % Line plot is of linear temperature scaling

        Temperature = get(P,'ZData'); % Assign surface plot data to temperature variable
        Temp_log_scale = false;
    
    end   
                
    
end

%% Interpolate line plot data

[rq,tq] = meshgrid(linspace(min(Radius),max(Radius),Points),LogSpace([min(Time),max(Time)],Points));
% Generate a meshgrid of linear scaled radius and logarithmic scaled time with enhanced datapoints

[Time_Interp,Radius_Interp] = ndgrid(Time,Radius); 
% Create a meshgrid of radius and time values according to origin amount of datapoints

F = griddedInterpolant(Time_Interp,Radius_Interp,Temperature,'spline');
% Interpolate temperature data making use of spline method ( at least 4 datapoints required )

Interp_Temp = F( tq, rq ); % Calculate interpolated temperature with respect to enhanced meshgrid

idx = zeros(1,length(XData)); % Initialize array index in horizontal direction
idy = zeros(1,length(YData)); % Initialize array index in vertical direction
Line_Temperature = zeros(1,length(XData)); % Initalize line plot temperature array

for j = 1 : 1 : length(XData) % Iterate amount of horizontal data values

    [~,idx(j)] = min( abs( rq(1,:) - XData(1,j) ) );    
    [~,idy(j)] = min( abs( tq(:,1) - YData(1,j) ) );
    % Find indizes with minimum deviation between enhanced meshgrid of line data and surface plot

    Line_Temperature(j) = smooth(Interp_Temp(idy(j),idx(j)),'sgolay');
    % Apply a "sgolay" smoothing method on logical indexed interpolation temperature

end

Radius = rq(1,:); 
Radius = Radius(idx);
% Determine neccessary spatial values with respect to logical line temperature indexing

Time = tq(:,1);
Time = Time(idy);
% Determine neccessary time values with respect to logical line temperature indexing

assignin('base','Line_Temperature',Line_Temperature); % Assign line temperature in workspace

%% Create temperature line plot in remaining subplot window

ax3 = evalin('base','ax3'); % Evaluate spatial based line plot axes in workspace

switch isequal( min( Radius ), max( Radius ) ) % Detect horizontal temperature line

    case 1 % Temperature plot is an exactly horizontal line

        Radius = linspace( min( Radius ), 1.00001 * max( Radius ) + 0.00001 , length( Radius ) );
        % Slightly stretch spatial range to avoid errors while plotting temperature data
        ax3.XAxis.TickLabelFormat = '%,.2f'; % Change tick label format to floating point with decimals
        ax3.XAxis.TickValues = [ min(Radius) max(Radius) ]; 
        % Set horizontal tick values of primary horizontal axis manually
        ax3.XAxis.TickLabel = { num2str( round( min( Radius ), 3, 'significant' ) ), ...
                                num2str( round( min( Radius ), 3, 'significant' ) ) };
        % Set tick labels of minimum and maximum axis position to equal values

    case 0 % Temperature plot is not an exactly horizontal line

        ax3.XAxis.TickLabelFormat = '%g'; % Reset tick label format to default setting
        set(ax3,'XTickMode', 'auto', 'XTickLabelMode', 'auto'); % Use automatic tick labels
end

ax4 = evalin('base','ax4'); % Evaluate time based line plot axes in workspace
set(ax4,'YTickMode', 'auto', 'YTickLabelMode', 'auto'); % Use automatic tick labels

switch isequal(min(Time),max(Time)) % Detect vertical temperature line

    case 1 % Temperature plot is an exactly vertical line
        
        Time = 0.99999 * linspace(min( Time ), 1.00001 * max( Time ), length( Time ) );
        % Slightly stretch time range to avoid errors while plotting temperature data
        ax4.XAxis.TickValues = [ min(Time) max(Time) ]; 
        % Set horizontal tick values of secondary horizontal axis manually
        ax4.XAxis.TickLabel = { num2str(round(min(Time),3,'significant')), ...
                                num2str(round(min(Time),3,'significant')) };
        % Set tick labels of minimum and maximum axis position to equal values
       
    case 0 % Temperature plot is not an exactly vertical line
        
        ax4.XAxis.TickLabelFormat = '%g';  % Reset tick label format to default setting
        set(ax4,'XTickMode', 'auto', 'XTickLabelMode', 'auto'); % Use automatic tick labels
        
end

try % Apply routine since no errors occur during line handles read process

    L1 = evalin('base','L1'); % Get handle to spatial based line plot from workspace
    L2 = evalin('base','L2'); % Get handle to time based line plot from workspace

catch % Line plot handles do not yet exist in workspace due to intial run

end

if isequal(get(findobj(gcf,'Type','uimenu','Label','Draw a new line'),'Checked'),'on') && ~exist('L1','var')      
% Line is drawn in subsystem but line plot data is not yet available in workspace 
    
    L1 = line(ax3,Radius,Line_Temperature,'LineStyle','none');
    L1.Tag = 'Line Temperature'; % Assgin a tag to the first temperature line
    assignin('base','L1',L1); % Create a line with respect to spatial axes and assign handle in workspace
    set(ax3,'XTickLabelMode','auto','TickLabelInterpreter','latex'); % Interprete tick labels with latex language  
    title(ax3,''); % Remove spatial axis title
    ax3.XAxis.FontSize = 10.5; % Set axis font size manually  

    x3 = xlabel(ax3,'$$\mathrm{Radius\;\;r\;\;\left(nm\right)}$$','Interpreter','latex','FontSize',11);
    set(x3,'Units', 'Normalized','Position', [0.50, -0.10, 0]); % Set horizontal label position manually
    set(ax3,'XDir','normal'); % Horizontal axis of spatial line plot is set to normal direction

    ylabel(ax3,L.Parent.ZLabel.String,'Interpreter','latex','FontSize',12.5); 
    % Distinguish between electronic or atomic subsystem temperature as vertical axis label 
    assignin('base','ax3',ax3); % Assign spatial line plot axes in workspace

else % Line is drawn in subsystem and line plot data is available in workspace 
             
    L1.XData = Radius; % Assign radius values to horizontal data of spatial based line plot 
    L1.YData = Line_Temperature; % Assign temperature values to vertical data of spatial based line plot

end

set(ax3,'XLim',[ min( Radius ), max( Radius ) ]); 
% Set horizontal spatial based line plot axes limits to extreme values of radius array
    
set(ax3,'YLim',[ 0.999 * min( Line_Temperature) , 1.001 * max( Line_Temperature ) ],'YTick',[]); 
% Set vertical spatial based line plot axes limits to extreme values of line temperature array

assignin('base','ax3',ax3); % Assign handle to spatial line plot axes in workspace

if isequal(get(findobj(gcf,'Type','uimenu','Label','Draw a new line'),'Checked'),'on') && ~exist('L2','var')
% Line is drawn in subsystem but line plot data is not yet available in workspace 

    L2 = line(Time,Line_Temperature,'Parent',ax4,'Color',L.Color,'LineWidth',1.5);
    L2.Tag = 'Line Temperature'; % Assgin a tag to the second temperature line
    assignin('base','L2',L2); % Create a line with respect to time axes and assign handle in workspace
    grid(ax4,'on'); % Draw gridlines with respect to time based line plot axes
        
    set(ax4,'XScale','log','XTickLabelMode','auto','Visible','on','TickLabelInterpreter','latex');
    axis(ax4,'tight'); % Set axis appearance to tight mode
    
    if isequal(Temp_log_scale,true) % Use different temperature scales for each subsystem
    
        set(ax4,'YScale','log'); % Logarithmic scale for electronic subsystem
        
    else
        
        set(ax4,'YScale','linear'); % Linear scale for atomic subsystem
        
    end
    
    ax4.XAxis.FontSize = 10.5; % Set horizontal axis font size manually  
    ax4.YAxis.FontSize = 10.5; % Set vertical axis font size manually  

    x4 = xlabel(ax4,'$$\mathrm{Time\;\;t\;\;\left(s\right)}$$','Interpreter','latex','FontSize',11);
    set(x4,'Units', 'Normalized','Position', [0.50, 1.10, 0]);  % Set horizontal label position manually

    str = L.Parent.ZLabel.String; % Get a character variable with temperature of current subsystem
    leg = legend(ax4,strcat(str(1:strfind(str,'\;\;') - 1),'}$$')); 
    % Show temprature label of current subsystem ( e.g. electronic or atomic ) in a legend
    set(leg,'Location','northwest','Interpreter','latex'); % Set legend interpreter and location
    assignin('base','ax4',ax4); % Assign handle to time based line plot axes in workspace
   
else % Line is drawn in subsystem and line plot data is available in workspace

    L2.XData = Time; % Assign time values to horizontal data of time based line plot 
    L2.YData = Line_Temperature; % Assign temperature values to vertical data of time based line plot

end

ax4 = evalin('base','ax4'); % Evaluate time based line plot axes in workspace

set(ax4,'XLim',[ min( Time ), max( Time ) ]);
% Set horizontal time based line plot axes limits to extreme values of time array

if ~isequal(get(findobj(gcf,'Type','uimenu','Label','Melting point'),'Checked'),'on')
% No melting point presence in temperature line plot graphics
    
    set(ax4,'YLim',[ min( Line_Temperature ) - eps, 1.1 * max( Line_Temperature ) + eps ]);
    % Set vertical time based line plot axes limits to extreme values of line temperature array
    
else % Melting point is member of temperature line plot graphics
   
    set(ax4,'YLim',[ min( Line_Temperature ) - eps, 1.2 * max( get( evalin('base','L3'),'YData' ) ) ] );
    % Set vertical time based line plot axes limits to minimum of line temperature and maximum of melting point
    
end

assignin('base','ax4',ax4); % Assign handle to time based line plot axes in workspace

%% Determine line plotting direction with repsect to line endpoints

 if L1.XData(1) > L1.XData(end)
 % Elements of spatial based temperature line vector are reversed 
     
     set(ax3,'XDir','reverse'); % Reverse direction of spatial based line plot axis
        
 else % No elements reversal of spatial based temperature line vector
    
     set(ax3,'XDir','normal'); % Set direction of spatial based line plot axis to normal
    
 end
   
 if L2.XData(1) > L2.XData(end) 
 % Elements of time based temperature line vector are reversed 
   
     set(ax4,'XDir','reverse'); % Reverse direction of time based line plot axis

 else % No elements reversal of time based temperature line vector
      
     set(ax4,'XDir','normal'); % Set direction of time based line plot axis to normal
        
 end

end