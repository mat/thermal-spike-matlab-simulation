function Factor = Radiation_Multiplier(Lattice_Temperature,Properties,Material)
% A user defined function to calculate the heat radiation multiplier according 
% to a selected material and the present atomic subsystem temperatures. 

 %% Load global variables
 
 global Boltzmann_Constant; % Boltzmann Constant,       k_B ,  Value: 1.38 * 10^-23 , Unit: J/K;
 global Planck_Constant;    % Planck Constant   ,       h_bar, Value: 1.05 * 10^-34 , Unit: Js;
 global Avogadro_Constant;  % Avogadro Constant,        N_A ,  Value: 6.022 * 10^23 , Unit: 1/mol;
 global Speed_of_Light;     % Speed of Light in Vacuum, c_0 ,  Value: 3.00 * 10^8 ,   Unit: m/s
 
 %% Apply multiplier calculation routine
 
 if Lattice_Temperature < Properties.(Material).Melting_Point
 % Atomic subsystem temperature is less than the melting point [K] of a selected material
 
     Mass_Density = Properties.(Material).Solid_Mass_Density * 10^3;    
     % Defined solid mass density of chosen target material. Unit: kg/m^3

 else % Atomic subsystem temperature is greater than the melting point [K] of a selected material
     
     Mass_Density = Properties.(Material).Liquid_Mass_Density * 10^3;    
     % Defined liquid mass density of chosen target material. Unit: kg/m^3
     
 end
  
 Molare_Mass = Properties.(Material).Molare_Mass * 10^(-3); 
 % Get the molare mass of a selected material. Units: kg/mol

 
 Factor = 1.202 * Boltzmann_Constant^3 / ( 2 * pi * Speed_of_Light * Planck_Constant^2 ) * ...
     ( 3 * Molare_Mass / ( 4 * pi * Mass_Density * Avogadro_Constant ) )^(-2/3);
 % Calaculate the radiation heat multiplier according to a selected
 % material. Units: W/(m^3 K^3)

end