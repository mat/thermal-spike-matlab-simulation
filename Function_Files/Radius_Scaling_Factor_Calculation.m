function Factor = Radius_Scaling_Factor_Calculation(r_0)
% A user defined function to calculate a normalization constant ( Factor ) with
% respect to a given set of a parameter ( r_0 ) and spatial radius values ( r ).

 syms r % Define a symbolic radius variable

 Radial_Expression = exp( - ( r / r_0 ) ) / r; 
 % Radial distribution function of delta electrons. Unit: m^-1
   
 Factor = 1 / int( Radial_Expression * 2 * pi * r , r, 0, inf ); % Determine normalization factor
 Factor = double(Factor); % Set factor values to double precission
 
end