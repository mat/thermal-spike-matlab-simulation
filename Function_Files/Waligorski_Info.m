
function Waligorski_Info(Source,~)
% This user defined function determine the appearance of the radial energy
% distributions of the electronic subsystem and generates a graphical user output.

if isequal(Source.Tag,'Waligorski Info') % Check that uimenu source is electronic coupling

    set(Source,'Enable','off'); % Set uimenu to disabled state
    set(Source,'Checked','on'); % Set uimenu to checked state
    
end

drawnow; % Refresh properties before going on with this routine

Fig = evalin('base','Fig'); % Evaluate main simulation figure handle in workspace

Fig.Pointer = 'watch'; % Set mouse pointer to watch mode

drawnow; % Refresh properties before going on with this routine

%% Display the electronic coupling radius based on swift heavy ion energy

 global E_ion; % Load swift heavy ion energy.     Units: MeV/u
 global I_ion; % Load ionization potential.       Units:  eV
 global alpha; % Load energy transfer parameter.  Units:  ---
 
 Thermal_Properties = evalin('base','Thermal_Properties'); % Evaluate thermal properties in workspace
 Material_Selection = evalin('base','Material_Selection'); % Get selected material from workspace
  
 theta = evalin('base','theta'); % Evaluate minimum energy input radius in workspace
 r_max = evalin('base','r_max'); % Get maximum energy input radius from workspace
 
 rho = Thermal_Properties.(Material_Selection).('Solid_Mass_Density'); % Get mass density of given material
 
 r = LogSpace([theta,0.99*r_max],300); % Create a logarithmic scaled radius grid
 f = zeros(size(r)); % Initalize dose distribution function to zero values
 u = zeros(size(r)); % Initialize deposited energy function to zero values

 f_r = evalin('base','f_r'); % Evaluate spatial dose distribution function in matlab workspace
 grad_r = gradient(r); % Calculate diskrete gradients of the logarithmic scaled radius grid 
 
 sum = 0; % Initialize a summarization variable to zero
 
 for i = 1 : 1 : length(r) % Iterate all radius grid values
    
     a = r(i); % Assign current radius to a constant variable
     f(i) = f_r(a); % Run spatial dose distribution function with current radius value as input
     sum = sum + 2 * pi * f(i) * r(i) * grad_r(i); % Integrate energy input values 
     u(i) = 2*pi*a*f(i); % Assign cylindrical energies to energy input variable
     
     if isequal(i,1) % Intial iteration index is present
         
         u_ref = u(i); % Set inital energy input to reference variable
         
     end
         
 end 

 f_ref = f_r(theta); % Reference dose value is spatial dose at minimum radius
  
 Dose = abs ( f / f_ref ); % Assign exponential decay function to a radial dose variable using a reference value
 Energy = abs( u / u_ref ); % Normalize energy input variable using a reference value
 Deposited_Energy = zeros(size(grad_r)); % Initialize deposited energy variable
 sum_i = 0; % Initialize another summarization variable
 
 for i = 1 : 1 : length(r) % Iterate all radius grid values
    
     a = r(i); % Assign current radius to a constant variable     
     f(i) = f_r(a); % Run spatial dose distribution function with current radius value as input
     
     sum_i = sum_i + 2 * pi * f(i) * r(i) * grad_r(i); % Integrate deposited energy values
     
     Deposited_Energy(i) = sum_i / sum; % Normalize deposited energy variable using energy input variable
         
 end 
  
 assignin('base','Deposited_Energy',Deposited_Energy); 
 % Assign deposited energy variable of electronic subsystem in workspace
 
 radius_grid = evalin('base','Radial_Grid'); % Evaluate radius simulation grid in workspace
 
 [ ~, i_r ] = min( abs( r - max( radius_grid ) ) ); % Obtain current distance to upper limit of radius grid 
 
 Transfered_Energy = Deposited_Energy(i_r); 
 % Calculate transfered energy to the electronic subsystem at a certain radius
 assignin('base','Transfered_Energy_1',Transfered_Energy);
 % Assign transfered energy of the atomic subsystem in workspace
 
  
 %% Visualize "Waligorski" settings
 
 if isequal(Source.Type,'uimenu') % Detect "uimenu" is activated ( procced with uimenu after right mouse click )
 
    W = figure; % Create a new figure
 
    set(W,'Name','Electronic Subsystem','Units','normalized','Position',[0.05 0.10 0.90 0.80]);
    set(W,'Toolbar','none','Menubar','none','Color','w','NumberTitle','off');
    set(W,'CloseRequest',{@closerequest,Source},'Tag','Waligorski Plot');
    % Set meaningful figure properties
    
    str = strcat('\textbf{Collision System:}$$\;\;\;E_{ion} =\;$$',num2str(E_ion), ...
        '$$\;MeV/u$$','$$\;,\;\;I =\;$$',num2str(I_ion),'$$\;eV$$','$$\;,\;\;\varrho_{', ...
        Material_Selection,'}\;=\;$$',num2str(rho),'$$\;g/cm^3$$','$$\;\;\;\;\;\;\;\;\;\;$$', ...
        '\textbf{Energy Parameter:}','$$\;\;\alpha_r =\;$$',num2str(alpha));
    % Create a complete string to display collision energy and electronic coupling information
    
    t = annotation(W,'textbox','String',str,'Interpreter','latex','FontSize',12);
    t.Units = 'normalized';
    t.LineStyle = 'none';
    t.Position = [0.01 0.92 0.65 0.05];
    % Open a textbox to display calculated electronic coupling information
 
    P = uipanel(W,'Units','normalized','Position',[0.01 0.02 0.98 0.88]);
     % Create a user interface panel that contains spatial dose and deposited energy figure
    
    uicontrol(P,'Style', 'popup','String', {'Dose','Energy'},'Units','normalized',...
        'Position',[0.39 0.89 0.07 0.05],'Callback', @Distribution,'FontWeight','bold',...
        'FontName','Cambria','ForegroundColor',[0.20 0.20 0.20],'Tag','Selection');
    % Add a uibutton to toggle from radial dose plot to energy input curve   
 
    %% Subplot:  "Waligorski" distribution of the electronic energy transfer
 
    axes(P,'Units','normalized','Position',[0.08 0.12 0.38 0.75],'Tag','Dose Distribution');
    % Create left 1D-plot ( spatial dose distribution ) axes
    
    Walig = plot(gca,10^(9)*r,Dose,'LineWidth',1.5,'Color','k');
    xlabel(gca,'Radius: $$ \;\;r\;\; \left(nm\right)$$','Interpreter','latex','FontSize',12);
    ylabel(gca,'Radial Dose: $$\;\; \frac{f\left(r\right)}{f\left(r_{min}\right)}$$',...
        'Interpreter','latex','FontSize',13);
    % Plot spatial dose versus radius grid values

    title_name = {'Radial distribution of the Dose',''};
    title(gca,title_name,'Interpreter','latex'); % Set previous string to subplot title

    set(gca,'XScale','log','YScale','log'); % Use logarithmic horizontal and vertical scaling
    grid(gca,'on'); % Activate an axis grid
    axis(gca,'tight'); % Set axis appearance to tight mode
    box(gca,'on'); % Draw a box around plot graphics
    
    set(gca,'YLim',[min(Dose),1]); % Set vertical axes limit to [dose minimum, unitary]
 
    %% Subplot: Deposited energy of the electronic subsystem 
   
    axes(P,'Units','normalized','Position',[0.56 0.12 0.41 0.75],'Tag','Deposited Energy');
    % Create right 1D-plot ( deposited energy ) axes 
    
    line(gca,10^(9)*[max(radius_grid),max(radius_grid)],[0,Transfered_Energy],'Color','r')
   % Draw a horizontal line to dislay maximum grid radius 
    
    if Transfered_Energy > 0.50 % Transfered energy exceeds 50 percent of its maximum value
    
        txt_1 = strcat({'$$R\;=\;$$'},num2str(round(10^(9)*max(radius_grid),2)),'$$\;nm$$');  
        txt_2 = strcat({'$$D\left(R\right)=\;$$'},num2str(round(Transfered_Energy,3)), ...
                        '$$\;\cdot D_{max}$$');
    
        x_pos = 10^(0.50*(log10(10^(9)*max(radius_grid))+log10(10^(9)*min(r))));
        % textbox horizontal position should be at a certain x-value
        
        y_pos = Transfered_Energy + 0.02;
        % textbox vertical position should be at a certain y-value ( transfered energy + 2 percent )    
        
        if y_pos > 0.96 % Textbox vertical position has got to match plot axes limits
            
            x_pos = 10^(0.30*(log10(10^(9)*max(radius_grid))+log10(10^(9)*min(r))));
            y_pos = Transfered_Energy - 0.03;
            % Substitute origin y-position of textbox
           
        end           
        
        text(gca,x_pos,y_pos,txt_1,'Color','red','Interpreter','latex', ...
            'HorizontalAlignment','center','FontSize',11.5); 
        % Display first text according to pre-defined x and y-position
    
        xpos = 1.20 * 10^(9) * max(radius_grid); % Check horizontal position to match plot limits       
               
        if xpos > 0.96 * 10^(9) * r_max % Horizontal position is greater than 96 percent of maximum
            
            xpos = 0.88 * 10^(9) * max(radius_grid); % Set new horizontal textbox position
            
        end
                
        text(gca,xpos,0.50*Transfered_Energy,txt_2,'Color','red',...
            'Rotation',90,'Interpreter','latex','HorizontalAlignment','center','FontSize',12);
        % Display second text according to pre-defined x and y-position
        
    end
    
    line(gca,10^(9)*[min(r),max(radius_grid)],[Transfered_Energy,Transfered_Energy],'Color','r');
    % Draw a vertical line to mark transfered energy at maximum grid radius 
    hold(gca,'on'); % Keep settings of current axes
    
    str = strcat('\textbf{Subsystem Deposition:}$$\;D(R)\;\approx\;\;$$',...
                  num2str(round(Transfered_Energy,3)),'$$\;\cdot\;D_{max}$$');
    
    a = annotation(W,'textbox','String',str,'Units','normalized','Interpreter','latex');
    a.Position = [0.70 0.92 0.29 0.05];
    a.FontSize = 12;
    a.LineStyle = 'none';
    % Display a text regarding subsystem energy deposition
    
    s = scatter(gca,10^(9)*max(radius_grid),Transfered_Energy,'filled');       
    s.MarkerEdgeColor = 'r';
    s.MarkerFaceColor = 'r';
    % Scatter a transfered energy value at maximum grid radius
    
    plot(gca,10^(9)*r,Deposited_Energy,'LineWidth',1.5,'Color','b')
    xlabel(gca,'Radius: $$ \;\;r\;\; \left(nm\right)$$','Interpreter','latex','FontSize',12);
    ylabel(gca,'Deposited Energy: $$\;\; \frac{D\left(r\right)}{D_{max}}$$',...
        'Interpreter','latex','FontSize',13);
    % Plot transfered energy distribution versus radius grid

    title_name = {'Energy deposition of the Electronic Subsystem',''};
    title(gca,title_name,'Interpreter','latex'); 
    % Set subplot title to energy deposition within the electronic subsystem

    set(gca,'XScale','log','YScale','linear');
    % Use logarithmic horizontal scaling and a linear vertical scaling
    grid(gca,'on'); % Activate an axis grid
    axis(gca,'tight'); % Set axis appearance to tight mode
    box(gca,'on'); % Draw a box around plot graphics
    
    
 end
 
 function Distribution(hObject,~)
 % A nested function that reacts on a popupmenu callback that toggles
 % between radial dose to energyinput settings.
    
    value = hObject.Value; % Get current popup value
    entries = hObject.String; % Get all string entries of popupmenu
    selection = char(entries(value)); % Extract current string from given entry value
                    
    if isequal(selection,'Dose') % Radial dose distribution is being requested
        
        title_name = {'Radial distribution of the Dose',''}; % Set dose distribution title
        y_label = 'Radial Dose: $$\;\; \frac{f\left(r\right)}{f\left(r_{min}\right)}$$';
        Walig.YData = Dose; % Assign radial dose values to vertical plot data
        set(Walig.Parent,'YLim',[min(Dose),1.01*max(Dose)]); % Apply radial dose vertical limits
                
    else % Energy input distribution is chosen
        
        title_name = {'Radial distribution of the Energy',''}; % Set deposited energy title               
        y_label = 'Radial Energy: $$\;\; \frac{u\left(r\right)}{u\left(r_{min}\right)}$$';
        Walig.YData = Energy; % Assign deposited energy values to vertical plot data
        set(Walig.Parent,'YLim',[min(Energy),max(Energy)]); % Apply deposited energy vertical limits
               
    end
    
    assignin('base','Walig_ax',Walig.Parent);  % Assign electronic coupling axes in workspace
     
    set(Walig.Parent.Title,'String',title_name); % Change title name respectively
    set(Walig.Parent.YLabel,'String',y_label); % Set new vertical label to selected item
    
 end

Fig.Pointer = 'arrow'; % Set mouse pointer to arrow symbol
drawnow; % Refresh all figure properties

dcm = datacursormode(gcf); % Create a handle to Waligorski plot figure
dcm.Enable = 'on'; % Enable datacursor mode automatically
set(dcm,'UpdateFcn',@myCursor); % Call data cursor function

end

function closerequest(hObject,~,Source)
% Close request function

 if isequal(Source.Tag,'Waligorski Info')
 % Figure close process is requested by "elwctronic coupling inforamtion" window
     
    set(Source,'Enable','on'); % Set uimenu to enabled state
    set(Source,'Checked','off'); % Set uimenu to not checked state
     
 end

 delete(hObject) % Delete open GUI figure       
   
end 

function output_txt = myCursor(source,event_obj)
% The function "myCursorn" defines a callback funktion of Data Cursor that
% makes use of event data of the object. Find variable declaration below :
%
% pos         --->   Actual position of data cursor
% event_obj   --->   Access to object events
% output_txt  --->   Data cursor string variable (cell array with strings)
%
% In addition to the actual simulation time and radial distance from the
% center of the incidence ion, the data cursor allows information on radial
% dose distribution and energy deposition of the electronic subsystem.
%
% Measuring sizes of the data cursor:
% 
% - Temporary radius: r  in units of nanometers [ nm ]
% - Respective values:  all being normalized   [% max]       
  

%% Determine actual cursor position

pos = get(event_obj,'Position');  %  Get cursor position from event data


%% Create data cursor output text

switch source.Parent.Tag
    
    case 'Deposited Energy'

        output_txt = {strcat('Radius:',32,32,32,'r ',32,32,' = ',32,32,sprintf('%.2f',pos(1)),32,32,'nm'),...
            strcat('Energy:',32,32,'D',32,' = ',32,32,sprintf('%.2f',pos(2)),32,32,'max')};
        % Output text of the data cursor is a cell variable consiting of each string
    
    otherwise
        
        Obj = findobj(gcf,'Tag','Selection');
        Strings = Obj.String;
        Selection = char(Strings(Obj.Value));
        
        if isequal(Selection,'Dose')
        
            output_txt = {strcat('Radius:',32,32,'r ',32,' = ',32,32,sprintf('%.2f',pos(1)),32,32,'nm'),...
            	strcat('Dose:',32,32,32,32,'f',32,32,' = ',32,32,sprintf('%.1e',pos(2)),32,'max')};

        else
            
            output_txt = {strcat('Radius:',32,32,32,'r ',32,' = ',32,32,sprintf('%.2f',pos(1)),32,'nm'),...
            	strcat('Energy:',32,32,'u',32,' = ',32,32,sprintf('%.1e',pos(2)),32,'max')};

        end
            
end

%% Define output of the data cursor

alldatacursors = findall(gcf,'type','hggroup'); % Find all type of valid datacursor
set(alldatacursors,'FontSize',10,'Marker','s','MarkerFaceColor','w','MarkerEdgeColor','k')
set(alldatacursors,'FontName','Times','Tag','DataTipMarker')
set(findall(gcf,'Type','text','Tag','DataTipMarker'), 'Interpreter','tex')
% Define datacursor appearance settings ( e.g. tex interpreter, fontstyle, markerstyle, ... )

end
