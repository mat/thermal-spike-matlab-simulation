function Waligorski_Function(rho)
% A user defined function that determines the radial energy distribution of
% the electronic subsystem due to the impact of swift heavy ions.

 global E_ion, global I_ion, global alpha, global beta; 
 global Electron_Mass, global Speed_of_Light, global Electron_Charge;
 % Load necessary global parameter

 %% Input Parameter

 % Energy of the swift heavy ion:       E_ion      Units: MeV/u
 % Ionisation potential:                I          Units:  eV
 % Target material mass density:        rho        Units: g/cm^3
 
 %% Output Parameter
 
 % Radial dose distribution function:   f_r        Units:  ---
 % Radial dose normalization factor:    Norm_f     Units:  ---
 
 %% Calculation routine

 E_n = 931.256; % Mean energy of a nucleon ( = < proton + neutron > ).  Units:  MeV
 m_e = Electron_Mass; % Resting mass of the electron.                   Units:   kg
 c_0 = Speed_of_Light; % Speed oflight in a vacuum.                     Units:   m/s
 e = Electron_Charge; % Charge of the electron.                         Units:  J / eV

 atm_n = sqrt(E_ion^2 + 2 * E_n * E_ion); % Subst. mass of a swift heavy ion
 atm_d = sqrt(E_ion^2 + 2 * E_n * E_ion + E_n^2); % Subst. mass of a resting ion

 beta = atm_n / atm_d; % Ion speed parameter: beta = v/c    Units: ---

 if beta > 0.03 % Detect relativistic speed above a threshold of beta = 0.03
    
    alpha_r = alpha; % Exponential decay of the radial dose distribution
    A = 19 * beta^(1/3); % Hump modelling parameter. Units: m^(-1)
   
 else % Relativistic speed is below the threshold
    
     alpha_r = 1.079; % Exponential decay of the radial dose distribution
     A = 8 * beta^(1/3); % Hump modelling parameter. Units: m^(-1)

 end

 W = 2 * ( ( m_e * c_0^2 ) / ( 1000 * e ) ) * beta^2 / ( 1 - beta^2 );
 % Energy of a delta electron at a certain ion speed. Units: keV
 k = 6 * 10^(-6); % Waligorski distribution delta ray electron range factor

 theta = 0.01 * k * ( I_ion / 1000 )^1.079; % Range of an electron of energy w = I_ion. Units: m
 assignin('base','theta',theta); % Assign minimum value of delta electron excitation in workspace 
 r_max = ( 0.01 * k / rho ) * W^alpha_r; % Kinetically limited maximum range of delta rays. Units: m
 assignin('base','r_max',r_max); % Assign maximum range of delta electrons in workspace 

 r = sym('r'); % Declare a symbolic ( = continious ) radius variable

 %% Dose around the path of a swift heavy ion 
 
 D_1 = ( ( ( 1 - ( r + theta ) ./ ( r_max + theta ) ).^( 1 / alpha_r ) ) ./ ( r + theta ) ) ./ r;
 % Energy fraction deposited in a coaxial cylinder of radius r. Units:  m^(-1)
 
 %            Semi empirical adjustments to archive an additional hump
 % -------------------------------------------------------------------------------------------------
 
 B = 10^-(10); % Distance due to the beginning of a hump. Units: m
 C = 1.5 * 10^(-9) + 5 * 10^(-9) * beta; % Hump width scales with the ion speed. Units: m
 
 K =  A * ( ( r - B ) / C ) .* exp( - ( r - B ) / C ); % Empirical hump function. Units: m^(-1)
  
 D = D_1 .* ( 1 + K .* heaviside( r - B ) ); % Put the hump on top of the radial dose. Units: m^(-1)

 % -------------------------------------------------------------------------------------------------
 
 str = strcat('@(r)',char(D)); % Create a function handle string of the radial dose distribution
 str = strrep(str,'*','.*'); % Substitute matrix multiplication by elementwise multiplication
 str = strrep(str,'/','./'); % Substitute matrix division by an elementwise division
 str = strrep(str,'^','.^'); % Substitute matrix factorization by an elementwise factorization
 f_r = str2func(str); % Obtain the function handle according to the radial dose string
 assignin('base','f_r',f_r); % Assign radial dose distribution in workspace
 
 %% Normalization factor of the Waligorski distribution
 
 D_norm = 2 * pi * r .* D; % Calculate normalization factor in polar coordinates. Units: ---

 Norm_str = strcat('@(r)',char(D_norm)); % Create a function handle string of normalization expression
 Norm_str = strrep(Norm_str,'*','.*'); % Substitute matrix multiplication by elementwise multiplication
 Norm_str = strrep(Norm_str,'/','./'); % Substitute matrix division by an elementwise division
 Norm_str = strrep(Norm_str,'^','.^'); % Substitute matrix factorization by an elementwise factorization
 Norm_f_r = str2func(Norm_str);  % Obtain the function handle according to normalization string
 
 Norm_f = 1 / ( abs(integral(Norm_f_r,0,r_max,'RelTol',1e-12,'AbsTol',1e-15)) );
 % Calculate normalization factor by integration of the radial dose expression. Units: m
 assignin('base','Norm_f',Norm_f); % Assign radial dose normalization factor in workspace
 
 Radial_Grid = evalin('base','Radial_Grid'); % Evaluate radial grid in workspace
 
 assignin('base','Radial_Grid',[0,LogSpace([theta,max(Radial_Grid)],length(Radial_Grid)-1)]);
 % Assign radial grid with changed values in workspace
 
 
end

