function Radiation_Heat(hObject,~)
% User defined function that determines particle radiation heat due to 
% the spatial resolved transient lattice temperatures. 

 %% Load necessary variables and create a plot figure

 set(hObject,'Checked','on'); % Set current object to checked state
 
 Rad = figure; % Open an empty matlab figure 
 
 [Radius,Time] = meshgrid(10^9 * evalin('base','Radial_Mesh_2'), evalin('base','Time_Mesh_2'));
 % Calculate a ( radius x time ) meshgrid based on radius vector and time vector
 Lattice_Temperature = evalin('base','Lattice_Temperature');
 % Get atomic subsystem temperatures from Matlab workspace
 
 %% Radiation heat plot
 
 Wavelength = 1.2 * 10^(-5); % Wavelength of electromagnetic radiation ( = 12 �m) 
 Radiation_Heat = Wavelength * Radiation_Calculation(Lattice_Temperature);
 % Determine radiation heat values by running radiation calculation function. Units: W/m^3
 
 rad_ax = axes(Rad); % Obtain the empty axes of the heat radiation figure

 %surf(rad_ax,Radius,Time,log10(Radiation_Heat));
 surf(rad_ax,Radius,Time,Radiation_Heat);
 % Create a surface plot (3D-Plot) with heat radiation data
 set(rad_ax,'YScale','log'); % Set logarithmic time scales
 axis(rad_ax,'tight'); % Set all plot axis to tight appearance
 shading(rad_ax,'interp'); % Interpolate all kind of plot shadings
 box(rad_ax,'on'); % Draw a box around the given axis
 view(rad_ax,[0 90]); % Apply a two dimensional view using colormap values

%  colmap = str2func('jet'); % Set the colormap to << parula >> appearance
%  colormap(rad_ax,colmap(128)); % Use a 128 Bit sized colormap
%  hold(rad_ax,'on'); % Hold on current axis settings
 
 cb = colorbar(rad_ax,'TickLabelInterpreter','latex','FontSize',11.5); % Display a colorbar
 cb.Position = cb.Position + [0.02 0 -0.02 -0.11]; % Change the colorbar position slightly
 rad_ax.Position = rad_ax.Position + [0 0 -0.1 0]; % Change the position of plot axes
 cb.Label.String = '$$\mathrm{\dot{Q}_{rad}\;\;\left[\frac{W}{m^2}\right]}$$'; % Colorbar label
 cb.Label.Interpreter = 'latex'; % Set latex colorbar interpreter
 cb_pos = get(cb,'Position'); % Get temporary position of the colorbar
 cb.Label.Units = 'normalized'; % Set colorbar label units to normalized
 cb.Label.Position = [cb_pos(1) + 0.35, cb_pos(2) + cb_pos(4) + 0.43, 0]; 
 cb.Label.Rotation = 0; % Rotate the label of electronic subsystem colorbar
 cb.TickLength = 0.050; % Set length of the horizontal colorbar ticks

 title(rad_ax,'Radiation Heat','Interpreter','latex'); % Set title of radiation heat plot
 xlabel(rad_ax,'Radius r $$ \left[nm\right] $$','Interpreter','latex','FontSize',13); % Set x-axis label
 ylabel(rad_ax,'Time t $$ \left[s\right] $$','Interpreter','latex','FontSize',13.5); % Set y-axis label

 set(rad_ax,'XGrid','on','Layer','top'); % Show cartesian grid lines on top of the plot 
        
 Rad.NextPlot = 'replace'; % Replace open figure when called again 
 Rad.NumberTitle = 'off'; % No number title is displayed 
 Rad.Resize = 'off'; % Do not allow manual window resizing 
 Rad.MenuBar = 'none'; % Menubar is not available
 Rad.ToolBar = 'none'; % Toolbar is not available
 Rad.Units = 'normalized'; % Position units are set to normalized
 Rad.Color = 'w'; % Use a white background color 
 Rad.CloseRequestFcn = @close_request;
 % Run callback function "@close_request" on a figure close request
 
 function close_request(~,~)
 % Close request function to display a question dialog box 
              
    delete(gcf); % Delete GUI figure         
    set(hObject,'Checked','off');
    % Set the closed object to unchecked state ( figure does not exist any more )
  
 end

end