function Atomic_Coupling_Info(Source,~)
% This user defined function determine the appearance of the radial energy
% distribution of the atomic subsystem and generates a graphical user output.

 global r_n_0; % Load global variable containing atomic coupling radius. Unit: m
 global E_ion; % Load global variable containing ion energy. Unit: MeV/u
 
 if isequal(Source.Tag,'Atomic Coupling Info') % Check that uimenu source is atomic coupling
 
    set(Source,'Enable','off'); % Set uimenu to disabled state
    set(Source,'Checked','on'); % Set uimenu to checked state
    
 end
 
 drawnow; % Refresh properties before going on with this routine

 Fig = evalin('base','Fig'); % Evaluate main simulation figure handle in workspace

 Fig.Pointer = 'watch'; % Set mouse pointer to watch mode

 drawnow; % Refresh properties before going on with this routine
 
 radius_grid = evalin('base','Radial_Grid'); % Evaluate the simulation radius grid
 
 grid_max = max(radius_grid); % Get maximum radius value of spatial grid

 r_min = 0.001 * r_n_0; % Determine a minimum radius value ( avoid any zero values! )
 
 r_max = grid_max; % Set maximum radius to upper limit of spatial grid
 r_max( r_max < 100 * r_n_0 ) = 100 * r_n_0; % Match a meaningful radius width ( differences )
 
 f = @(r) exp( - r / r_n_0 ) ./ r; % Define an exponetial decay function with greater radius values
 f_phi = @(r) 2 * pi * exp( - r / r_n_0 ); % Consider a rotation symmetry of the radial dose
   
 r = LogSpace([r_min,r_max],300); % Create a logarithmic scaled radius grid to display energy dose
 
 threshold = find(r > grid_max, 1,'first'); % Set upper threshold to maximum of origin grid values
  
 Dose = f(r); % Assign exponential decay function to a radial dose variable
 Dose = Dose / max(Dose); % Normalize dose distribution
  
 Energy = f_phi(r); % Assign rotational dose function to energy input variable
 Energy = Energy / max(Energy); % Normalize energy input distribution
 
 Deposited_Energy = 1 - Energy; % Deposited energy is assumed be unitary
 
 if ~isempty(threshold) % Check that an upper threshold exists
     
     Transfered_Energy = Deposited_Energy( threshold - 1 ); 
     % Determine transfered energy from spatial energy distribution and upper radius threshold
     
 else
     
     Transfered_Energy = 1; % Set transfered energy to unitary ( energy is completely transfered )
     
 end
 
 assignin('base','Transfered_Energy_2',Transfered_Energy); 
 % Assign transfered energy of the atomic subsystem in workspace
  
 if isequal(Source.Type,'uimenu') % Detect "uimenu" is activated ( procced with uimenu after right mouse click )
 
    C = figure; % Create a new figure
    set(C,'Name','Atomic Subsystem','Units','normalized','Position',[0.05 0.10 0.90 0.80]);
    set(C,'Toolbar','none','Menubar','none','Color','w','NumberTitle','off');
    set(C,'CloseRequest',{@closerequest,Source});
    % Set meaningful figure properties
      
    if isequal(get(findobj(Fig,'Tag','Atomic Radius'),'Checked'),'on') 
    % << Atomic Radius >> menu is selected by the user
    
        subs_str = strcat('$$\;nm\;\;$$\fontsize{12}{12}\selectfont {\boldmath$!$}');
        % Set string settings to bold appearance while adding unit in nanometers
    else
        
        subs_str = strcat('$$\;nm$$');
        % Keep previous settings and add unit in nanometers
        
    end
    
    str = strcat('\textbf{Collision Energy:}$$\;\;\;E_{ion} =\;$$',num2str(E_ion), ...
            '$$\;MeV/u\;\;\;\;\;\;\;\;$$','\textbf{Coupling Radius:}','$$\;\;\;r_{0} =\;$$', ...
            num2str(10^(9)*r_n_0),subs_str);
    % Create a complete string to display collision energy and atomic coupling radius 
 
    t = annotation(C,'textbox','String',str,'Interpreter','latex','FontSize',12);
    t.Units = 'normalized';
    t.LineStyle = 'none';
    t.Position = [0.10 0.92 0.65 0.05]; 
    % Open a textbox to display calculated atomic coupling radius
  
    P = uipanel(C,'Units','normalized','Position',[0.01 0.02 0.98 0.88]);
    % Create a user interface panel that contains spatial dose and deposited energy figure    
    
    uicontrol(P,'Style', 'popup','String', {'Dose','Energy'},'Units','normalized',...
            'Position',[0.39 0.89 0.07 0.05],'Callback',@Distribution,'FontWeight','bold',...
            'FontName','Cambria','ForegroundColor',[0.20 0.20 0.20],'Tag','Selection');
    % Add a uibutton to toggle from radial dose plot to energy input curve
        
    str = strcat('\textbf{Subsystem Deposition:}$$\;D(R)\;\approx\;\;$$',...
                  num2str(round(Transfered_Energy,3)),'$$\;\cdot\;D_{max}$$');
   % Create a complete string to display the amount of energy transfered to the atoms 
    
    a = annotation(C,'textbox','String',str,'Units','normalized','Interpreter','latex');
    a.Position = [0.58 0.92 0.29 0.05];
    a.FontSize = 12;
    a.LineStyle = 'none';
    % Open a textbox to display energy transfered to the atomic subsystem
        
    %% Subplot:  Atomic coupling distribution of the atomic energy transfer
 
    axes(P,'Units','normalized','Position',[0.08 0.12 0.38 0.75]);
    % Create left 1D-plot ( dose distribution ) axes
    
    Coupling = plot(gca,10^(9)*r,Dose,'LineWidth',1.5,'Color','k');
    xlabel(gca,'Radius: $$ \;\;r\;\; \left(nm\right)$$','Interpreter','latex','FontSize',12);
    ylabel(gca,'Radial Dose: $$\;\; \frac{f\left(r\right)}{f\left(r_{min}\right)}$$',...
      'Interpreter','latex','FontSize',13);
    % Plot spatial dose versus radius grid values

    title_name = {'Radial distribution of the Dose',''};
    title(gca,title_name,'Interpreter','latex'); % Set previous string to subplot title

    set(gca,'XScale','log','Tag','Dose Distribution'); % Use logarithmic horizontal scaling
    grid(gca,'on'); % Activate an axis grid
    axis(gca,'tight'); % Set axis appearance to tight mode
    box(gca,'on'); % Draw a box around plot graphics
    
    set(gca,'YLim',[min(Dose),1]); % Set vertical axes limit to [dose minimum, unitary]
    
    %% Subplot: Deposited energy of the atomic subsystem 
   
    axes(P,'Units','normalized','Position',[0.56 0.12 0.41 0.75]); 
    % Create right 1D-plot ( deposited energy ) axes 
        
    s = scatter(gca,10^(9)*max(radius_grid),Transfered_Energy,'filled');       
    s.MarkerEdgeColor = 'r';
    s.MarkerFaceColor = 'r';
    % Plot transfered energy distribution versus radius grid
    
    hold(gca,'on'); % Keep settings of current axes
    
    plot(gca,10^(9)*r,Deposited_Energy,'LineWidth',1.5,'Color','b')
    xlabel(gca,'Radius: $$ \;\;r\;\; \left(nm\right)$$','Interpreter','latex','FontSize',12);
    ylabel(gca,'Deposited Energy: $$\;\; \frac{D\left(r\right)}{D_{max}}$$',...
        'Interpreter','latex','FontSize',13);
    % Plot deposited energy data versus radius grid values
    
    title_name = {'Energy deposition of the Atomic Subsystem',''};
    title(gca,title_name,'Interpreter','latex');
    set(gca,'Tag','Deposited Energy');

    line(gca,10^(9)*[max(radius_grid),max(radius_grid)],[0,Transfered_Energy],'Color','r');      
    line(gca,10^(9)*[min(r),max(radius_grid)],[Transfered_Energy,Transfered_Energy],'Color','r');
    % Draw a horizontal and a vertical line to mark transfered energy at maximum grid radius 
   
    if Transfered_Energy > 0.50 % Transfered energy exceeds 50 percent of its maximum value
    
        txt_1 = strcat({'$$R\;=\;$$'},num2str(round(10^(9)*max(radius_grid),2)),'$$\;nm$$');  
        txt_2 = strcat({'$$D\left(R\right)=\;$$'},num2str(round(Transfered_Energy,3)), ...
                        '$$\;\cdot D_{max}$$');
    
        x_pos = 10^(0.50*(log10(10^(9)*max(radius_grid))+log10(10^(9)*min(r))));
        % textbox horizontal position should be at a certain x-value
        
        y_pos = Transfered_Energy + 0.02; 
        % textbox vertical position should be at a certain y-value ( transfered energy + 2 percent )    
        
        if y_pos > 0.96 % Textbox vertical position has got to match plot axes limits
            
            x_pos = 10^(0.30*(log10(10^(9)*max(radius_grid))+log10(10^(9)*min(r))));
            y_pos = Transfered_Energy - 0.03; 
            % Substitute origin y-position of textbox
           
        end           
        
        text(gca,x_pos,y_pos,txt_1,'Color','red','Interpreter','latex', ...
            'HorizontalAlignment','center','FontSize',11.5); 
        % Display first text according to pre-defined x and y-position
    
        xpos = 1.35 * 10^(9) * max(radius_grid); % Check horizontal position to match plot limits      
               
        if xpos > 0.96 * 10^(9) * r_max % Horizontal position is greater than 96 percent of maximum radius
            
            xpos = 0.70 * 10^(9) * max(radius_grid); % Set new horizontal textbox position
            
        end
                
        text(gca,xpos,0.50*Transfered_Energy,txt_2,'Color','red',...
            'Rotation',90,'Interpreter','latex','HorizontalAlignment','center','FontSize',12);
        % Display second text according to changed x-position
        
    end
    
    set(gca,'XScale','log','YScale','linear'); 
    % Use logarithmic horizontal scaling and a linear vertical scaling
    grid(gca,'on'); % Activate an axis grid
    axis(gca,'tight'); % Set axis appearance to tight mode
    box(gca,'on'); % Draw a box around the graphics
      
 end
 
 function Distribution(hObject,~)
 % A nested function that reacts on a popupmenu callback that toggles
 % between radial dose to energy input settings.
    
    value = hObject.Value; % Get current popup value
    entries = hObject.String; % Get all string entries of popupmenu
    selection = char(entries(value)); % Extract current string from given entry value

    if isequal(selection,'Dose') % Radial dose distribution is being requested

        title_name = {'Radial distribution of the Dose',''}; % Set dose distribution title
        y_label = 'Radial Dose: $$\;\; \frac{f\left(r\right)}{f\left(r_{min}\right)}$$';
        Coupling.YData = Dose; % Assign radial dose values to vertical plot data
        set(Coupling.Parent,'YLim',[min(Dose),1.01*max(Dose)]); % Apply radial dose vertical limits

    else % Energy input distribution is chosen

        title_name = {'Radial distribution of the Energy',''}; % Set deposited energy title              
        y_label = 'Radial Energy: $$\;\; \frac{u\left(r\right)}{u\left(r_{min}\right)}$$';
        Coupling.YData = Energy; % Assign deposited energy values to vertical plot data
        set(Coupling.Parent,'YLim',[min(Energy),max(Energy)]); % Apply deposited energy vertical limits

    end

    assignin('base','Coupling_ax',Coupling.Parent); % Assign atomic coupling axes in workspace

    set(Coupling.Parent.Title,'String',title_name); % Change title name respectively
    set(Coupling.Parent.YLabel,'String',y_label); % Set new vertical label to selected item
    
 end

 Fig.Pointer = 'arrow'; % Set mouse pointer to arrow symbol
 drawnow; % Refresh all figure properties
 
 dcm = datacursormode(gcf); % Create a handle to Waligorski plot figure
 dcm.Enable = 'on'; % Enable datacursor mode automatically 
 set(dcm,'UpdateFcn',@myCursor); % Call data cursor function
    
end

function closerequest(hObject,~,Source)
% Close request function

 if isequal(Source.Tag,'Atomic Coupling Info') 
     % Figure close process is requested by "atomic coupling inforamtion" window
     
    set(Source,'Enable','on'); % Set uimenu to enabled state
    set(Source,'Checked','off'); % Set uimenu to not checked state
    
 end
 
 delete(hObject) % Delete open GUI figure
    
end 

function output_txt = myCursor(source,event_obj)
% The function "myCursorn" defines a callback funktion of Data Cursor that
% makes use of event data of the object. Find variable declaration below :
%
% pos         --->   Actual position of data cursor
% event_obj   --->   Access to object events
% output_txt  --->   Data cursor string variable (cell array with strings)
%
% In addition to the actual simulation time and radial distance from the
% center of the incidence ion, the data cursor allows information on radial
% dose distribution and energy deposition of the atomic subsystem.
%
% Measuring sizes of the data cursor:
% 
% - Temporary radius: r  in units of nanometers [ nm ]
% - Respective values:  all being normalized   [% max]       
  

%% Determine actual cursor position

pos = get(event_obj,'Position');  %  Get cursor position from event data


%% Create data cursor output text

assignin('base','ATSource',source);

switch source.Parent.Tag % Distinguish spatial dose and deposited energy popupmenu entries
    
    case 'Deposited Energy' % Deposited energy is currently selected

        output_txt = {strcat('Radius:',32,32,32,'r ',32,32,' = ',32,32,sprintf('%.2f',pos(1)),32,32,'nm'),...
            strcat('Energy:',32,32,'D',32,' = ',32,32,sprintf('%.2f',pos(2)),32,32,'max')};
        % Output text of the data cursor is a cell variable consiting of each string
    
    otherwise % Spatial dose distribution is selected
        
        Obj = findobj(gcf,'Tag','Selection'); % Find current plot obejct
        Strings = Obj.String; % Get all entries of current obect
        Selection = char(Strings(Obj.Value)); % Determine selected object value
        
        if isequal(Selection,'Dose') % Popup item equals spatial dose distrbution
        
            output_txt = {strcat('Radius:',32,32,'r ',32,' = ',32,32,num2str(round(pos(1),2,'significant')),32,'nm'),...
            	strcat('Dose:',32,32,32,32,'f',32,32,' = ',32,32,sprintf('%.2f',pos(2)),32,'max')};

        else
            
            output_txt = {strcat('Radius:',32,32,32,'r ',32,' = ',32,32,num2str(round(pos(1),2,'significant')),32,'nm'),...
            	strcat('Energy:',32,32,'u',32,' = ',32,32,sprintf('%.2f',pos(2)),32,'max')};

        end
            
end

%% Define output of the data cursor

alldatacursors = findall(gcf,'type','hggroup'); % Find all type of valid datacursor
set(alldatacursors,'FontSize',10,'Marker','s','MarkerFaceColor','w','MarkerEdgeColor','k')
set(alldatacursors,'FontName','Times','Tag','DataTipMarker')
set(findall(gcf,'Type','text','Tag','DataTipMarker'), 'Interpreter','tex')
% Define datacursor appearance settings ( e.g. tex interpreter, fontstyle, markerstyle, ... )

end
