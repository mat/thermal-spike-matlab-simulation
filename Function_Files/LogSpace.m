function Log_Vector = LogSpace(Linear_Vector,Iterations)
% User defined function to construct a logarithmic scaled vector, based on
% the amount of supportive places as well as starting and endpoint.

Log_Vector = 10.^linspace(log10(Linear_Vector(1)),log10(Linear_Vector(2)),Iterations);
% Calculate the logarithmic scaled vector due to the basis of ten ( log10( ... ) )

end