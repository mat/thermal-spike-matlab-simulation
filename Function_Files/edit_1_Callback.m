function edit_1_Callback (obj,~)
% User defined function to change the initial temperature of both subsystems.
% Symbol: T_0  ,  Unit: K

 Thermal_Properties = evalin('base','Thermal_Properties');
 Material_Selection = evalin('base',' Material_Selection');
 
 Thresholds_Heat = Thermal_Properties.(Material_Selection).('Lattice_Heat').('Thresholds');
 Thresholds_Cond = Thermal_Properties.(Material_Selection).('Thermal_Conductivity').('Thresholds');

 Allowed_Min = max([min(min(cellfun(@min,Thresholds_Heat))), ...
                    min(min(cellfun(@min,Thresholds_Cond)))]);
           
 if evalin('base','exist(''Current_Character'')') 
 % Does variable current character exist in workspace
    
    if isequal(evalin('base','Current_Character'),'return')
    % Compare if current key is return key
                
        Initial_Temp = str2double(obj.String(~isletter(obj.String)));
        Initial_Temp( Initial_Temp < Allowed_Min ) = Allowed_Min;
        Initial_Temp( Initial_Temp == 0 ) = eps;    
       
        set(obj,'String',strcat(num2str(Initial_Temp),' K')); 
        % Remove unit [Kelvin] from text string

    end
  
    set(obj,'Enable','inactive'); % Toggle editbox enable state to inactive         
    
 end

end