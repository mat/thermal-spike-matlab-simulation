function Desorption_Yields(hObject,~)
% User defined function that determines particle desorption yields due to 
% the spatial resolved transient lattice temperatures. 

%% Load necessary variables and create a plot figure 

 global Bohr_Radius; % Load Bohr's atom radius, a_0 , Value:  5.29 * 10^-11 , Unit: m;
 
 set(hObject,'Checked','on'); % Set current object to checked state
 
 Des = figure; % Open an empty matlab figure 
 Des.Units = 'normalized'; % Position units are set to normalized
 
 [Radius,Time] = meshgrid(10^9 * evalin('base','Radial_Mesh_2'), evalin('base','Time_Mesh_2'));
 % Calculate a ( radius x time ) meshgrid based on radius vector and time vector
 Lattice_Temperature = evalin('base','Lattice_Temperature'); 
 % Get atomic subsystem temperatures from Matlab workspace
 
%% Desorption heat plot
 
 if isequal(hObject.Label,'Show desorption heat') 
 % Check if current object is labeled as desorption heat
          
    [Desorption_Heat,~] = Desorption_Calculation(Time,Lattice_Temperature);
    % Determine desorption heat values by running desorption calculation function. Units: W/m^3
     
    Desorption_Heat = abs(Bohr_Radius * Desorption_Heat);
    % Calculate surface area scaled desorption heat. Units: W/m^2
    
    des_ax = axes(Des); % Obtain the empty axes of the desorption figure
    
    surf(des_ax,Radius,Time,log10(Desorption_Heat)); 
    % Create a surface plot (3D-Plot) with desorption heat data
    set(des_ax,'YScale','log','ZScale','log'); % Set logarithmic scales
    axis(des_ax,'tight'); % Set all plot axis to tight appearance
    shading(des_ax,'interp'); % Interpolate all kind of plot shadings
    box(des_ax,'on'); % Draw a box around the given axis
    view(des_ax,[0 90]); % Apply a two dimensional view using colormap values 
    
    colmap = str2func('parula'); % Set the colormap to << parula >> appearance
    colormap(des_ax,colmap(128)); % Use a 128 Bit sized colormap
    hold(des_ax,'on'); % Hold on current axis settings
    
    colormap(des_ax,flipud(colormap(des_ax)));
    % Bring current colormap to the back layer of the stack
    
    contour_num = 10; % Define the amount of contour lines within the surface plot
    contours = LogSpace([min(min(Desorption_Heat)),max(max(Desorption_Heat))],contour_num);
    % Create a logarithmic spaced contour line vector from desorption heat minimum until 
    % the maximum value of the same array
    contours = round(contours,2,'significant'); % Format contour values to a significance of two 
    
    contour3(des_ax,Radius,Time,Desorption_Heat,contours,'k'); 
    % Draw the contour lines on top of the surface plot
        
    colormap(des_ax,colmap(128)); % Reset the colormap to previous settings
    
    cb = colorbar(des_ax,'TickLabelInterpreter','latex','FontSize',10.5); % Display a colorbar
    cb.Position = cb.Position + [0 0.02 0 -0.1]; % Change the colorbar position slightly
    des_ax.Position = des_ax.Position + [0 0 -0.12 0]; % Change the position of plot axes
    cb.Label.String = '$$\mathrm{\dot{Q}_{des}\;\;\left[\frac{W}{m^2}\right]}$$'; % Colorbar label
    cb.Label.Interpreter = 'latex'; % Set latex colorbar interpreter
    cb_pos = get(cb,'Position'); % Get temporary position of the colorbar
    cb.Label.Units = 'normalized'; % Set colorbar label units to normalized
    cb.Label.Position = [cb_pos(1) + 0.15, cb_pos(2) + cb_pos(4) + 0.33, 0]; 
    cb.Label.Rotation = 0; % Rotate the label of desorption heat colorbar
    cb.Ticks = log10(contours); % Set logaritmic spaced colorbar steps 
    cb.TickLabels = contours; % Set the manual colorbar labels 
    cb.TickLength = 0.090; % Set length of the horizontal colorbar ticks
    caxis(des_ax,log10([contours(1) contours(end)])); 
    % Define logarithmic color axis of given surface plot
    
    title(des_ax,'Desorption Heat','Interpreter','latex'); % Set title of desorption heat plot
    xlabel(des_ax,'Radius r $$ \left[nm\right] $$','Interpreter','latex','FontSize',13); % Set x-axis label
    ylabel(des_ax,'Time t $$ \left[s\right] $$','Interpreter','latex','FontSize',13.5); % Set y-axis label
    
    set(des_ax,'XGrid','on','Layer','top'); % Activate a cartesian grid on top of the plot
    
    uicontrol(gcf,'Style','popupmenu','String',{'logarithmic scale';'linear scale'},...
        'Units','normalized','Position',[0.81 0.00 0.19 0.053],'Callback',@Data_Plot); 
    % Create a popup-menu to change axis scaling from logarithmic to linear and vice versa    
 
%% Desorption yields plot
    
 else % Current object label is equal to calculate desorption yields
 
     
    [~,Desorption_Yield] = Desorption_Calculation(Time,Lattice_Temperature);
    % Determine desorption yield values by opening desorption calculation function. Units: s^(-1) m^(-2)
     
    Time_Ref = diff(Time); % Get the time difference between two subsequent time steps
    Time_Ref = [Time_Ref;Time_Ref(end,:)]; % Add an additional time row that is missing
    Time_Delta = min(min(Time)); % Determine the minimum time value of the time array
    Radius_Delta = 10^(-9) * gradient(Radius); % Get the equidistant radius differences 
        
    Radius_Value = ones(size(Desorption_Yield)) * 10^(-9) .* Radius;
    % Calculate the ( n x m ) array of given radius values
      
    Zero_Index = Radius_Value == 0; % Find all radius values that are equal to zero
    Radius_Value( Zero_Index ) = 0.01 * mean( transpose( Radius_Delta ) ); % Substitute zero radius values
     
    Desorbed_Molecules = Desorption_Yield .* Radius_Value .* Radius_Delta .* Time_Delta;
    % Calculate the amount of desorbed molecules with respect to the desorption yields
          
    des_ax = axes(Des); % Obtain the empty axes of the desorption figure
     
    P = surf(des_ax,Radius,Time,Desorbed_Molecules); 
    % Create a surface plot (3D-Plot) with desorbed molecules data
     
    set(des_ax,'YScale','log'); % Set logarithmic scales
    axis(des_ax,'tight'); % Set all plot axis to tight appearance
    shading(des_ax,'interp'); % Interpolate all kind of plot shadings
    box(des_ax,'on'); % Draw a box around the given axis
    view(des_ax,[0 90]); % Apply a two dimensional view using colormap values 
     
    colmap = str2func('parula'); % Set the colormap to << parula >> appearance
    colormap(des_ax,colmap(128)); % Use a 128 Bit sized colormap
    hold(des_ax,'on'); % Hold on current axis settings
    
    colormap(des_ax,flipud(colormap(des_ax))); 
    % Bring current colormap to the back of the stack
    
    contour_num = 8; % Define the amount of contour lines within the surface plot
    contours = linspace(min(min(Desorbed_Molecules)),max(max(Desorbed_Molecules)),contour_num);
    % Create a logarithmic spaced contour line vector from desorption heat minimum until 
    % the maximum value of the same array
    contours = round(contours,2,'significant'); % Format contour values to a significance of two 
    
    contour3(des_ax,Radius,Time,Desorbed_Molecules,contours,'k');
    % Draw the contour lines on top of the surface plot   
    
    colormap(des_ax,colmap(128)); % Reset the colormap to previous settings
    
    cb = colorbar(des_ax,'TickLabelInterpreter','latex','FontSize',10.5); % Display a colorbar
    cb.Position = cb.Position + [0.02 0 0 -0.25]; % Change the colorbar position slightly
    des_ax.Position = des_ax.Position + [0 0.03 -0.14 -0.05]; % Change the position of plot axes
    cb.Label.String = '$$\mathrm{N_{des}\left(r,t\right)}$$'; % Colorbar label
    cb.Label.Interpreter = 'latex'; % Set latex colorbar interpreter
    cb_pos = get(cb,'Position'); % Get temporary position of the colorbar
    cb.Label.Units = 'normalized'; % Set colorbar label units to normalized
    cb.Label.Position = [cb_pos(1) + 0.15, cb_pos(2) + cb_pos(4) + 0.47, 0]; 
    cb.Label.Rotation = 0; % Rotate the label of electronic subsystem colorbar
    cb.Ticks = contours; % Set logaritmic spaced colorbar steps 
    cb.TickLabels = contours; % Set the manual colorbar labels 
    cb.TickLength = 0.11; % Set length of the horizontal colorbar ticks
    caxis(des_ax,[contours(1) contours(end)]);
    % Define logarithmic color axis of given surface plot
    
    title(des_ax,'Desorbed Molecules','Interpreter','latex'); % Set title of desorbed molecules plot
    xlabel(des_ax,'Radius r $$ \left[nm\right] $$','Interpreter','latex','FontSize',13); % Set x-axis label
    ylabel(des_ax,'Time t $$ \left[s\right] $$','Interpreter','latex','FontSize',13.5); % Set y-axis label
    
    set(des_ax,'XGrid','on','Layer','top'); % Activate a cartesian grid on top of the plot
 
    Molecule_Yield = Desorbed_Molecules .* Time_Ref / Time_Delta; % Determine desorption yield
    Yield = sprintf('%.2e\n',sum(Molecule_Yield(:))); % set desorption yield format to exponential
       

    panel = uipanel('Parent',gcf,'Title','','TitlePosition','centertop','FontSize',9,...
         'BorderWidth',2,'Units','normalized','BackgroundColor',[0.95 0.95 0.95],...
         'Position',[0.79 0.80 0.20 0.18]);
    % Generate a uipanel to plot the absolute desorption yields
     
    txt1 = annotation(panel,'textbox','Units','normalized','Position',[0 0.60 0.98 0.30],...
        'String','Desorption Yield','Interpreter','latex',...
        'LineStyle','none','FontSize',9,'BackgroundColor',[0.95 0.95 0.955]);
    % Create a first textbox to display the label of desorption yields 
    
    txt2 = annotation(panel,'textbox','Units','normalized','Position',[0.07 0.20 0.20 0.30],...
        'String','$$\eta = $$','Interpreter','latex',...
        'LineStyle','none','FontSize',10);
    % Create a second textbox to display the greek symbol of desorption yields

    uicontrol(panel,'Style','text','Units','normalized','Position',[0.32 0.05 0.60 0.40],...
        'BackgroundColor',[0.95 0.95 0.95],'String',Yield,'FontSize',8.5,...
        'ForegroundColor',[0.3 0.3 0.3]);
    % Create a uicontrol text that displays the current desorption yield value
    
    context_1 = uicontextmenu; % Allocate a context menu to desorption yields field
    panel.UIContextMenu = context_1; % Assign the uipanel to first context menu
    txt1.UIContextMenu = context_1; 
    txt2.UIContextMenu = context_1;
    % Assign  both textboxes to the first context menu
    
    uimenu(context_1,'Tag','Yield','Label','Show calculation formula','Callback',@desorption_formula);
    % Assign a user interface menu to first context menu that will show the formula used on 
    % desorption yields calculation 
    
    context_2 = uicontextmenu; % Allocate a context menu to desorbed molecules surface plot
    P.UIContextMenu = context_2; % Assign surface plot to second context menu
    des_ax.UIContextMenu = context_2; % Assign desorption plot axes to the above context menu
    
    uimenu(context_2,'Label','Show calculation formula','Callback',@desorption_formula);
    % Assign a user interface menu to second context menu that will show the formula used on 
    % desorbed molecules calculation
    
    uicontrol(gcf,'Style','popupmenu','String',{'linear scale';'logarithmic scale'},...
        'Units','normalized','Position',[0.81 0.00 0.19 0.053],'Callback',@Data_Plot);
    % Create a popup-menu to change axis scaling from linear to logarithmic and vice versa
 
 end
 
 %% Figure properties and nested functions
 
 Des.NextPlot = 'replace'; % Replace open figure when called again 
 Des.NumberTitle = 'off'; % No number title is displayed 
 Des.Resize = 'off'; % Do not allow manual window resizing 
 Des.MenuBar = 'none'; % Menubar is not available
 Des.ToolBar = 'none'; % Toolbar is not available
 Des.Color = 'w'; % Use a white background color  
 Des.CloseRequestFcn = @close_request; 
 % Run callback function "@close_request" on a figure close request
   
 function Data_Plot(Handle,~)
 % A nested function that applies a logarithmic or linear scale on the chosen
 % surface plot with respect to the popup menu settings. 
 
     str = Handle.String; % Get the cell-string of current object
     Str = char(str(Handle.Value)); % Extract the chosen element according to popup value
    
     if isequal(hObject.Label,'Show desorption heat') % Current plot equals desorption heat
            
            Data = Desorption_Heat; % Assign desorption heat matrix to data variable
            contour_num = 10; % Set the amount of contour lines
            
     else % Current plot equals desorbed molecules
            
            Data = Desorbed_Molecules; % Assign desorbed molecules matrix to data variable
            contour_num = 8; % Set the amount of contour lines
            
     end
     
     if isequal(Str,'logarithmic scale') % Logarithmic data plot is requested         
           
        contours = LogSpace([min(min(Data)),max(max(Data))],contour_num);
        % Create a logarithmic spaced contour line vector from data variable's minimum until 
        % the maximum value of the same array
        contours = round(contours,2,'significant'); % Format contour values to a significance of two 
        set(findobj(gcf,'Type','Surface'),'ZData',log10(Data));
        set(findobj(gcf,'Type','Contour'),'ZData',log10(Data),'LevelList',log10(contours));
        cb = get(gca,'colorbar'); % Get the handle to current colorbar
        cb.Ticks = log10(contours); % Set logaritmic spaced colorbar steps
        cb.TickLabels = contours; % Set the manual colorbar labels 
        caxis(gca,log10([contours(1) contours(end)]));
        % Define logarithmic color axis of given surface plot          
          
      elseif isequal(Str,'linear scale') % Linear data plot is requested
           
        contours = linspace(min(min(Data)),max(max(Data)),contour_num);
        % Create a linear spaced contour line vector from data variable's minimum until 
        % the maximum value of the same array
        contours = round(contours,2,'significant');
        set(findobj(gcf,'Type','Surface'),'ZData',Data);
        set(findobj(gcf,'Type','Contour'),'ZData',Data,'LevelList',contours);
        cb = get(gca,'colorbar'); % Get the handle to current colorbar
        cb.Ticks = contours; % Set linear spaced colorbar steps
        cb.TickLabels = contours; % Set the manual colorbar labels 
        caxis(gca,[contours(1) contours(end)]);
        % Define linear color axis of given surface plot
    
      end
      
 end

 function desorption_formula(Object,~)
 % User nested function that will display the respective formula used on
 % any calculation process ( = desorption yields || desorbed molecules ).
              
     if isequal(Object.Tag,'Yield') % Desorption yields formula is requested
     
        Formula = '$$\;\;\eta\;=\;\sum_i\left(\sum_j\;N_{des}\left(r_i,t_j\right)\right)$$';
        Description = 'Desorption Yield'; % Define a messagebox description
        
     else % Desorbed molecules formula is requested
         
         Formula = '$$N_{des}=2\pi\cdot r_i\cdot R_{des}\cdot\Delta r_i\cdot\Delta t_j$$';
         Description = 'Desorbed Molecules'; % Define a messagebox description
         
     end
     
     CreateStruct.Interpreter = 'latex'; % Use messagebox latex interpreter
     CreateStruct.WindowStyle = 'modal'; % Set the window style to modal
     msg = msgbox(Formula,Description,'help',CreateStruct); 
     % Open a messagebox with neccessary user information
     handle = findall(msg, 'Type', 'Text'); % Get handle to text within msgbox
     handle.FontSize = 11; % Enhance the font size of formula text
     deltaWidth = sum(handle.Extent([1,3]))-msg.Position(3) + handle.Extent(1);
     deltaHeight = sum(handle.Extent([2,4]))-msg.Position(4) + 10;
     msg.Position([3,4]) = msg.Position([3,4]) + [deltaWidth, deltaHeight];
     % Resize messagebox window due to an enhanced font size of the text
         
 end
 
 function close_request(HObject,~)
 % Close request function to display a question dialog box 
              
    delete(HObject); % Delete GUI figure         
    set(hObject,'Checked','off'); 
    % Set the closed object to unchecked state ( figure does not exist any more )
  
 end

end