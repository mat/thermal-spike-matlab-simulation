function Radiation_Menu(Source,~)
% A user defined function to create a thermal radiation settings menu, 
% while the user activates a respective context menu property.

set(Source,'Enable','off');

 %% Create an empty desorption settings figure

 Rad_fig = figure(4); % Open an additional matlab figure
 Rad_fig.Name = 'Heat Radiation Settings'; % Set matlab figure name
 Rad_fig.NextPlot = 'replace'; % Replace open figure when called again 
 Rad_fig.NumberTitle = 'off'; % No number title is displayed 
 Rad_fig.Resize = 'off'; % Do not allow manual window resizing 
 Rad_fig.MenuBar = 'none'; % Menubar is not available
 Rad_fig.ToolBar = 'none'; % Toolbar is not available
 Rad_fig.Units = 'normalized'; % Position units are set to normalized
 Rad_fig.Position = [0.35 0.35 0.30 0.30]; % Define figure position on screen
 Rad_fig.CloseRequestFcn = {@closerequest,Source};
   
 %% Read variables from workspace
 
 if evalin('base','exist(''Ambient_Temperature'',''var'')')
 % Check if ambient temperature variable is member of workspace
     
     Ambient_Temperature = evalin('base','Ambient_Temperature');
     % Get ambient temperature value from workspace. Units: K
     
 else % Ambient temperature variable does not exist in workspace
     
     Temp = get(findobj(evalin('base','Fig'),'Tag','Editbox'),'String');
     Ambient_Temperature = str2double(Temp(~isletter(Temp)));
     % Set initial ambient temperature to intial target temperature. Units: K
          
 end
  
 if evalin('base','exist(''Ambient_Emissivity'',''var'')')
 % Check if ambient emissivity variable is available in workspace
     
     Ambient_Emissivity = evalin('base','Ambient_Emissivity');
     % Get ambient emissivity value from workspace. Units: normalized
     
 else % Ambient emissivity variable does not exist in workspace
     
     Ambient_Emissivity = 1;
      % Set initial ambient emissivity to unity. Units: normalized
               
 end
  
 if evalin('base','exist(''Surface_Emissivity'',''var'')')
 % Check if surface emissivity variable is member of workspace
     
     Surface_Emissivity = evalin('base','Surface_Emissivity');
     
 else % Surface emissivity variable does not exist in workspace
     
     Surface_Emissivity = 0.73; % Units: normalized
     % Set meaningful surface emissivity value of copper @ 300 ... 800 K. 
               
 end
 
 %% Define thermal radiation active states
 
 Panel_1 = uipanel(Rad_fig,'Title','','TitlePosition','centertop','FontSize',11,...
    'Units','normalized','BorderType','etchedout','BorderWidth',2,'BackgroundColor','w',...
    'Position',[0.01 0.02 0.98 0.25]);
 % Create a user interface panel that contains all meaningful thermal radiation properties
  
 Fig = evalin('base','Fig'); % Get the handle to main simulation interface ( = Fig )
    
 States = {' radiation disabled ' , ' radiation enabled '};
 % Define two different thermal radiation states ( = disabled || enabled )
    
 Radiation_Enable = uicontrol(Panel_1,'style','checkbox','units','normalized',...
             'position',[0.37,0.25,0.30,0.40]);
 % Create a user interface checkbox to select the different thermal radiation states
            
 Current_State = get(findobj(Fig,'Type','uimenu','label','Radiation settings'),'Checked');
 % Get the current thermal radiation state with respect to desorption settings label 
    
 if isequal(Current_State,'on') % Radiation settings label is checked                          
                   
     States = fliplr(States);
     % Change the sequence of radiation states ( from front to back of the stack )
        
     set(Radiation_Enable,'string',States{1},'value',1); % Set checkbox to true appearance
        
 else % Radiation settings label is not checked
        
     set(Radiation_Enable,'string',States{1},'value',0); % Set checkbox to false appearance
            
 end
                    
 set(Radiation_Enable,'Backgroundcolor','w','Callback',@Radiation_States);
 % Assign a callback function ( reacts on any user click ) to radiation enable checkbox 
 % and set a white background color for thermal radiation checkbox
    
 function Radiation_States(hObject,~)
 % A nested callback function to distinguish different radiation states
    
     States = fliplr(States); % Change the sequence of radiation states 
     set(hObject,'string',States{1}); % Set new radiation state to the checkbox
                        
 end

 %% Generate data while closing desorption figure

 uicontrol(Panel_1,'style','pushbutton','units','normalized','Tag','OK',...
         'position',[0.72,0.15,0.25,0.60],'string','OK','Callback',@Button_pressed);
 % Create a pushbutton to accept the radiation settings, once the uibutton is pressed
 
 uicontrol(Panel_1,'style','pushbutton','units','normalized','Tag','Cancel',...
         'position',[0.03,0.15,0.25,0.60],'string','Cancel','Callback',@Button_pressed);      
 % Create a pushbutton to cancel the radiation settings, once the uibutton is pressed 
     
 function Button_pressed(hObject,~) % Nested callback function of both pushbuttons
        
     switch hObject.Tag % Distinguish two different pushbutton types ( = OK || Cancel )
            
         case 'OK' % OK button is pressed
                
             Status = get(findobj(gcf,'Type','uicontrol','Style','Checkbox'),'value');
             % Get the current desorption checkbox value ( = true || false )
             Fig = evalin('base','Fig'); % Get the handle to main simulation interface
                
             if isequal(Status,1) % Checkbox value is equal to true ( = 1 )
                    
                 set(findobj(Fig,'Type','uimenu','label','Radiation settings'),'checked','on');
                 % Set the radiation settings label of UIcontextmenu to checked state
                 assignin('base','Radiation_Status','enabled');
                 % Set radiation state variable to enabled
                                        
             else % Checkbox value is equal to false ( = 0 )
                    
                 set(findobj(Fig,'Type','uimenu','label','Radiation settings'),'checked','off');
                 % Set the radiation settings label of UIcontextmenu to unchecked state
                 assignin('base','Radiation_Status','disabled');
                 % Set radiation state variable to disabled
                    
             end 
                
             Ambient_Temperature = get(findobj(gcf,'Tag','Ambient_Temperature'),'value');
             % Get ambient temperature value from respective slider handle
             Ambient_Emissivity = get(findobj(gcf,'Tag','Ambient_Emissivity'),'value');
             % Get ambient emissivity value from respective slider handle
             Surface_Emissivity = get(findobj(gcf,'Tag','Surface_Emissivity'),'value');
             % Get surface emissivity value from respective slider handle 
               
             assignin('base','Ambient_Temperature',Ambient_Temperature);
             assignin('base','Ambient_Emissivity',Ambient_Emissivity);
             assignin('base','Surface_Emissivity',Surface_Emissivity);
              % Transfer all three variables to Matlab workspace
                    
         case 'Cancel' % Cancel button is pressed 
             
     end
        
     delete(gcf); % Delete the current thermal radiation figure       
     set(Source,'Enable','on');   
     
 end
 
 %% Display thermal radiation parameter settings
 
 Panel_2 = uipanel(Rad_fig,'Title','','TitlePosition','centertop','FontSize',11,...
    'Units','normalized','BorderType','etchedout','BorderWidth',2,'BackgroundColor','w',...
    'Position',[0.01 0.30 0.98 0.67]);
 % Create a user interface panel that contains all meaningful radiation properties

    Subpanel_1 = uipanel(Panel_2,'Title','Environment Conditions','TitlePosition','centerbottom',...
        'FontSize',9,'Units','normalized','BorderType','etchedout','BorderWidth',2,...
        'BackgroundColor','w','Position',[0.02 0.02 0.50 0.92]);
    % Create a subpanel of second uipanel that contains radiation parameter input
    
        annotation(Subpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.13 0.75 0.44 0.20],'String','Emissivity: \boldmath$$\epsilon_{\infty} = $$',...
            'BackgroundColor','w','EdgeColor','none'); % Define an ambient emissivity label       

        uicontrol(Subpanel_1,'Style','slider','Units','normalized','Position',...
            [0.10 0.60 0.80 0.15],'Min',0,'Max',1,'Value',Ambient_Emissivity,...
            'BackgroundColor','w','Tag','Ambient_Emissivity','Callback',@slider_value);
        % Create a slider control to set ambient emissivity values
                
        annotation(Subpanel_1,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.04 0.29 0.47 0.20],'String','Temperature: \boldmath$$T_{\infty}=\;$$',...
            'BackgroundColor','w','EdgeColor','none'); % Show an ambient temperature label  

        uicontrol(Subpanel_1,'Style','slider','Units','normalized','Position',[0.10 0.15 0.80 0.15],...
            'Min',0,'Max',2000,'Value',Ambient_Temperature,'BackgroundColor','w','FontSize',11,...
            'SliderStep',[0.0005,0.005],'Tag','Ambient_Temperature','Callback',@slider_value);
        % Define a slider to put ambient temperature values. Units: K

     Subpanel_2 = uipanel(Panel_2,'Title','Surface Conditions','TitlePosition','centerbottom',...
        'FontSize',9,'Units','normalized','BorderType','etchedout','BorderWidth',2,...
        'BackgroundColor','w','Position',[0.54 0.02 0.44 0.92]);
     % Create a additional subpanel of second uipanel that contains radiation parameter input
    
        annotation(Subpanel_2,'textbox','Units','normalized','Interpreter','latex',...
            'Position',[0.07 0.75 0.48 0.20],'String','Emissivity: \boldmath$${\epsilon_S =} $$',...
            'BackgroundColor','w','EdgeColor','none');
        % Show a textbox with surface emissivity description
    
        uicontrol(Subpanel_2,'Style','slider','Units','normalized','Position',...
            [0.10 0.60 0.80 0.15],'Min',0,'Max',1,'Value',Surface_Emissivity,...
            'BackgroundColor','w','Tag','Surface_Emissivity','Callback',@slider_value);
        % Define a slider to put surface emissivity values. Units: normalized
        
 Sliders = findall(gcf,'Type','uicontrol','Style','Slider');
 % Get handle to all slider variables

 for i = 1 : 1 : length(Sliders) % Iterate all existing slider handles

     slider_value(Sliders(i));
     % Run slider_value functions and extract all selected values 

 end   
       
        
 function slider_value(hObject,~)
 % A nested function to extract and display a selected value of any present slider
            
  if isequal(hObject.Tag,'Ambient_Temperature')   
 
        val = strcat(num2str(round(hObject.Value,3,'significant')),' K');
        % Get respective slider value and round it significantly
        
  else
      
        val = round(hObject.Value,3,'significant');     
        
  end
  
  uicontrol(hObject.Parent,'Style','text','Units','normalized','Position',...
    hObject.Position + [0.60 0.15 -0.55 0],'String',val,'BackgroundColor','w',...
    'FontName','Cambria','FontSize',9,'FontWeight','bold','FontAngle','Italic',...
    'ForegroundColor',[0.2 0.2 0.2]);

  % Display the obtained slider value in a textlabel next to a chosen slider
            
 end
        
end

function closerequest(hObject,~,Source)
% Close request function

 set(Source,'Enable','on');   
 delete(hObject) % Delete open GUI figure
 
end 
