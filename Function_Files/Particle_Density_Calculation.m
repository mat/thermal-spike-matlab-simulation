function [Electron_Density,Atomic_Density] = Particle_Density_Calculation(Lattice_Temperature)
% User defined function to calculate the eletron density according to a defined valence electron
% number and the atomic density of the target material.

 global Avogadro_Constant; % Avogadro Constant, N_A ,  Value:  6.022 * 10^23 ,   Unit: 1/mol;

 Thermal_Properties = evalin('base','Thermal_Properties'); % Get thermal properties from workspace
 Material_Selection = evalin('base','Material_Selection'); % Get target material selection from workspace

 Valence_Electron_Number = evalin('base','Valence_Electrons'); 
 % Amount of valence electrons per target atom. Unit: none

 if Lattice_Temperature < ( Thermal_Properties.(Material_Selection).Melting_Point )   
    
     Mass_Density = Thermal_Properties.(Material_Selection).Solid_Mass_Density * 10^3;    
     % Defined solid mass density of chosen target material. Unit: kg/m^3
    
 else
    
     Mass_Density = Thermal_Properties.(Material_Selection).Liquid_Mass_Density * 10^3;    
     % Defined liquid mass density of chosen target material. Unit: kg/m^3
    
 end

 Molare_Mass = Thermal_Properties.(Material_Selection).Molare_Mass * 10^(-3);  
 % Respective molare mass of selected target material. Unit: kg/mol

 Atomic_Density = Mass_Density / Molare_Mass * Avogadro_Constant; 
 % Compute an atomic density with respect to mass density and molare mass of target material. Unit: m^-3
 Electron_Density = Valence_Electron_Number * Atomic_Density;
 % Calculate an eletron density according to a defined valence electron number and a computed atomic 
 % density of the target material. Unit: m^-3

end