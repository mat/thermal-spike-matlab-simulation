function Electronic_Energy_Loss = Check_Electronic_Energy_Density(Radial_Grid,Time_Grid)
% This user defined function accumulates differentially electronic energy 
% losses which will afterwards be used to classify simulation results accuracy.

global t_e_0, global sigma_e_t, global S_e, global b, global Electron_Charge
% Load global variables

add = [0,LogSpace([0.0025,0.9]* evalin('base','theta'),10)];

%r_mesh = [add,LogSpace([Radial_Grid(2),max(Radial_Grid)],dot(multiplier,gridsize_r))];
r_mesh = [add,LogSpace([Radial_Grid(2),max(Radial_Grid)], length(Radial_Grid))];
%r_mesh = linspace(min(Radial_Grid),max(Radial_Grid),multiplier*(length(Radial_Grid)));
% Generate a linear radius vector with respect to parameter settings and spatial grid size

%t_mesh = LogSpace([min(Time_Grid), max(Time_Grid)], dot(multiplier,gridsize_t));
t_mesh = LogSpace([min(Time_Grid), max(Time_Grid)], length(Time_Grid));
%t_mesh = LogSpace([min(Time_Grid),max(Time_Grid)],multiplier*length(Time_Grid));
% Generate a logarithmic time vector with respect to parameter settings and time grid size

[dr,dt] = meshgrid(gradient(r_mesh),gradient(t_mesh)); % Meshgrid containing gradient vectors
[r,t]= meshgrid(r_mesh,t_mesh); % Create a meshgrid that consists of values in time and space

r( r == 0 ) = 0.001 * evalin('base','theta'); % Avoid any zero values during calculation steps

f_r = evalin('base','f_r'); % Get radial distribution function from workspace

r_max = evalin('base','r_max'); % Evaluate maximum input radius in workspace

if max(Radial_Grid) < r_max % Check radial grid size does not exceed maximum input dose radius
    
    Radial_Expression = f_r(r) * evalin('base','Norm_f'); % Use complete calculation function
    
else
    
    Radial_Expression = f_r(r) * evalin('base','Norm_f') .* heaviside( r_max - r );
    % Cut off data at a certain maximum input radius
    
end

Time_Expression = b * 2 * pi * S_e .* exp( -1/2 * ( ( t - t_e_0 ) / sigma_e_t ).^2 ).* r .* dr .* dt;
Expression = Time_Expression .* Radial_Expression;
% Determine electronic energy losses according to specific distributions in time and space
% Time distribution     =     << normalized gaussian >>
% Spatial distribution  =  << scaled waligorski decay >>

Electronic_Energy_Loss = dot(Expression,ones(size(Expression)));
Electronic_Energy_Loss = dot(Electronic_Energy_Loss,ones(size(Electronic_Energy_Loss)))...
                            * 10^(-12) / Electron_Charge;
% Electronic_Energy_Loss = Electronic_Energy_Loss / evalin('base','Transfered_Energy');
% Accumulate all electronic energy losses by applying << dot product >>                       
% Unit: keV/nm                   

end





