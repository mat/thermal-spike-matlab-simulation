function Frequency = Oscillation_Frequency(Lattice_Temperature)
% A user defined function that determines the mean oscillation frequency of
% an adsorbed molecules in the influence of a Lennard-Jones-Potential or
% due to a quantum mechanical oscillation procedure ( thermal oscillations ).

 global Boltzmann_Constant; 
 global Planck_Constant;
 global Avogadro_Constant;
 global Electron_Charge;
 % Load global variables
 
 
 if isequal(evalin('base','LJ_Active'),true) % Lennard-Jones-Potetial algortihmn is activated
     
     Adsorbate_Mass = evalin('base','Adsorbate_Mass'); % Evaluate particle adsorbate mass in workspace
     Zero_Crossing = evalin('base','Zero_Crossing'); % Evaluate zero crossing potetial in workspace
     Desorption_Energy = Electron_Charge * evalin('base','Desorption_Energy');
     % Calculate desorption energy in SI-Units
      
     Frequency = ( ( 6 * sqrt(Desorption_Energy * Avogadro_Constant / Adsorbate_Mass ) ) / ...
         ( pi * 2^(1/6) * Zero_Crossing ) ) * ones(size(Lattice_Temperature));
     % Determine mean molecule frequency while bound in a Lennard-Jones-Potential

 else % Quantum mechanical oscillations are requested
     
     Frequency = Boltzmann_Constant * Lattice_Temperature / Planck_Constant; 
     % Determine mean molecule frequency while using thermal description
     
 end 
  
end