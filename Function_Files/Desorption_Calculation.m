function [Desorption_Heat,Desorption_Yield] = Desorption_Calculation(Time,Lattice_Temperature)
% User defined function with the aim to determine specific desorption heat and desorption yields
% according to time values and lattice temperatures.

 %% Load necessary global variables
 
 global Boltzmann_Constant; % Boltzmann constant, k_B , Value:  1.38 * 10^-23 , Unit: J/K;
 global Avogadro_Constant; % Avogadro Constant, N_A , Value:  6.022 * 10^23 , Unit: 1/mol; 
 global Electron_Charge; % Charge of an Electron, e , Value: 1.602 * 10^-19 , Unit: C
 global Monolayer; % Molecule density of an adsorbed monolayer, Value: tunable , Unit: m^(-2)
 global Molare_Volume; % Molare volume of any molecules, Value: tunable , Unit: m^3/mol

 %% Apply main calculation routine
 
 if evalin('base','exist(''Desorption_Status'',''var'')') 
     % Check that desorption status is member of Matlab workspace
     
     switch evalin('base','Desorption_Status') % Allocate different desorption states                 
         
         case 'enabled' % Desorption calculation is enabled                          
         
             Desorption_Energy = Electron_Charge * evalin('base','Desorption_Energy');
             % Calculate desorption energy from variable input ( eV ). Units:  J
             Adsorbate_Coverage = evalin('base','Adsorbate_Coverage'); 
             % Get initial adsorbate surface coverage. Units: normalized
             Degrees_of_freedom = evalin('base','Degrees_of_freedom');
             % Get particle degrees of freedom after being desorbed from surface. Units: counts
             
             Frequency = Oscillation_Frequency(Lattice_Temperature); 
             % Determine oscillation frequency of molecules bound to the surface. Units: s^(-1)          
             Desorption_Rate =  Frequency * log(2) .* ...
                 exp( - Desorption_Energy ./ ( Boltzmann_Constant * Lattice_Temperature ) );
             % Calculate the desorption rate based on determined oscillation frequencies and 
             % temperature solutions of the atomic subsystem. Units: m^(-2)* s^(-1)
                         
             Desorption_Heat = - ( Adsorbate_Coverage * Avogadro_Constant / Molare_Volume ) .* ...
                 ( Desorption_Energy + 0.5 * Degrees_of_freedom * Boltzmann_Constant * Lattice_Temperature ) .* ...
                 Desorption_Rate .* exp( - Desorption_Rate .* Time );
             % Determine the desorption heat based on desorption rate and necessary energy as well as
             % the particle's degrees of freedom after being desorbed from the surface. Units: W/m^3
             

             Desorption_Yield = 2 * pi * Desorption_Rate .* exp( - Desorption_Rate .* Time ) * ...
                 Adsorbate_Coverage * Monolayer;
             % Obtain the desorption yield by a multiplication of the desorption rate k_des and 
             % the time dependant surface coverage n(r,t). Units: molecules * m^(-2)* s^(-1) 
             
         otherwise % Desorption calculation is disabled  

             Desorption_Heat = zeros(size(Lattice_Temperature)); % Set desorption heat to zero
             Desorption_Yield = zeros(size(Lattice_Temperature)); % Set desorption yield to zero
             
     end
     
 else
     
     Desorption_Heat = zeros(size(Lattice_Temperature)); % Set desorption heat to zero
     Desorption_Yield = zeros(size(Lattice_Temperature)); % Set desorption yield to zero
     
 end
    
end