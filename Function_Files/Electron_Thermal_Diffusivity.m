function Diffusivity =  Electron_Thermal_Diffusivity(Electronic_Temperature)
% User defined function to determine the electron thermal diffusivity with respect to the
% electronic subsystem temperature and three different temperautre regimes.

global D_e_300, global D_e_min; % Load global variables of electron thermal diffusivity

%% Calculation routine

k_1 = D_e_300 * 300^2; 
k_2 = k_1 / 300;
% Define a constant scaling factors with respect to eelctron thermal diffusivity 

T_lim = ( 300 * D_e_300 ) / D_e_min; 
% Electron thermal diffusivity becomes constant above electronic limitation temperature.
% Compare PhD thesis of Christian Dufour !

lim_1 = arrayfun(@(x) x < 300, Electronic_Temperature); % Electronic temperatures below limitation
lim_2 = arrayfun(@(x) x > T_lim , Electronic_Temperature); % Electronic temperatures above limitation

Diffusivity = k_2 * ones(size(Electronic_Temperature)) ./ Electronic_Temperature;
Diffusivity(lim_1) = k_1 * Electronic_Temperature(lim_1).^(-2);
Diffusivity(lim_2) = D_e_min;
% Calculate electron thermal diffusivity with respect to different temperatures regimes

 if evalin('base','exist(''subs_coeff'',''var'')') 
 % Check if coefficients substitution variable does exist in workspace

     coeff_data = evalin('base','coeff_data'); % Read coefficients data in workspace

     if isequal(coeff_data{3,4},'Yes') 
     % Neccessary to apply substitution of electron thermal diffusivity    

             Diffusivity = ones(size(Electronic_Temperature)) * ...
                    10^(-4) * str2double(coeff_data{3,3});
             % Set electron thermal diffusivity to values of coefficients substitution

     end

 end
 
 %% Data output
 
 Diffusivity( Diffusivity < 0 ) = 0; % Avoid negative output values

end