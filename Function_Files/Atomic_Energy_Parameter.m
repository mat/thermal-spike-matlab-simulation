function Answer = Atomic_Energy_Parameter()
% The user defined function generates and opens an input dialog that allows
% to get all simulation parameters with respect to the atomic energy input 
% due to an incidence of swift heavy ions. 
%
% Function Routine:
%
%  Pushbutton  --->  Input Dialog  --->  get parameters  --->  output
% 
% Definition of output values:
%
% The output of simulation parameters are given as a cell variable which  
% is specified as << Answer >>


 %% Atomic Subsystem Parameters

 % Definition of input dialog window to read neccessary simulation parameters

 prompt = {'\fontsize{11}{11}\selectfont Ion Energy: $$\;E_{Ion}\;\;(MeV/u)$$',...
           '\fontsize{11}{11}\selectfont Oscillation time of atoms: $$\;t_0\;\;(fs)$$',...
           '\fontsize{11}{11}\selectfont Oscillation time deviation: $$\;\sigma_t\;\;(fs)$$',...
           '\fontsize{11}{11}\selectfont Nuclear stopping loss: $$\;S_n\;\;(keV/nm)$$'};
 % Prompt text messages coded with latex sequence 
      
 global E_ion, global t_n_0, global sigma_n_t, global S_n;
 global Electron_Charge; % Load necessary global variables
  
 default_1 = num2str( E_ion );                                                   % Unit: nm
 default_2 = num2str( 10^15 * t_n_0 );                                           % Unit:  s
 default_3 = num2str( 10^15 * sigma_n_t );                                       % Unit:  s
 default_4 = num2str( 10^(-3) * 10^(-9)* S_n / Electron_Charge );                % Unit: keV/nm
 
 default = {default_1, default_2, default_3, default_4}; % Set dynamic default values
  
 title = 'Atomic Subsystem'; % Title of the dialog window

 lineNo = 1; % Character input fields only consist of one line

 options.Interpreter='latex';  % Use Latex Interpreter

 strings = inputdlg (prompt, title, lineNo, default, options);
 % Open simulation input dialog window and read electronic parameter setting

 Answer = str2double(strings);
 % Convert string variables to double precission numeric variables

end